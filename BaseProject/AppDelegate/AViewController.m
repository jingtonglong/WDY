//
//  AViewController.m
//  BaseProject
//
//  Created by apple on 2017/6/22.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//
#import "CDKHomeDataAuditVc.h"
#import "AViewController.h"
#import "YSLoginViewController.h"
#import "SZKRoundScrollView.h"
#import "CDKVisitVC.h"
#import "CDKHomeSearchVC.h"
#import "CDKNewPatientBuildProfile_1.h"
#import "SZKRoundScrollView.h"
#import "CDKNewsModel.h"
#import "CDKWebViewVC.h"
#import "MyQRVC.h"
@interface AViewController ()<UINavigationControllerDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *speedBtn;
@property (weak, nonatomic) IBOutlet UIButton *hightEfectBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeight;
@property (weak, nonatomic) IBOutlet UIView *navView;
@property (weak, nonatomic) IBOutlet UILabel *todayPeopleCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *examinePeopleCountLabel;
@property (weak, nonatomic) IBOutlet UIView *bgRoundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *qrBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navView_top;

/** 轮播图图片数组 */
@property (nonatomic,strong) NSMutableArray  *imageArray;

/** 咨询模型数组 */
@property (nonatomic,strong) NSMutableArray  *newsModelArray;

@property (nonatomic,strong) SZKRoundScrollView *roundScrollView;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *roudViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *buildBtn;

@property (weak, nonatomic) IBOutlet UIImageView *buildinfoImgv;
@property (weak, nonatomic) IBOutlet UILabel *buildLab1;
@property (weak, nonatomic) IBOutlet UILabel *buildLab2;
@end

@implementation AViewController
- (instancetype)init
{
    self = [super init];
    if (self) {
        UIStoryboard *st=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self=[st instantiateViewControllerWithIdentifier:@"AViewController"];
        
    }
    return self;
}
- (IBAction)visitBtnClicked:(id)sender {// 今日随访
    
    CDKVisitVC *vc=[[CDKVisitVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)addNutraitionEavule:(id)sender {// 新增营养评估
    CDKHomeSearchVC *vc=[[CDKHomeSearchVC alloc] init];
    vc.type=1;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)addExcelEavule:(id)sender {// 新增量表评估
    CDKHomeSearchVC *vc=[[CDKHomeSearchVC alloc] init];
     vc.type=2;
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (IBAction)addBiologyeData:(id)sender {// 新增生化数据
    CDKHomeSearchVC *vc=[[CDKHomeSearchVC alloc] init];
     vc.type=3;
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)data_aditing:(id)sender {// 数据审核
    
    CDKHomeDataAuditVc *vc=[[CDKHomeDataAuditVc alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)QRcODE:(id)sender {
    MyQRVC *vc=[[MyQRVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)addNewPationtProfile_home:(id)sender {
    CDKNewPatientBuildProfile_1 *vc=[[CDKNewPatientBuildProfile_1 alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.view.backgroundColor = [UIColor colorWithHexString:@"#557CE3"];
//    [self loadUserInterface];
//    self.edgesForExtendedLayout=UIRectEdgeNone;
    self.scrollView.delegate=self;
    self.speedBtn.layer.borderWidth=1;
    self.speedBtn.layer.borderColor=[UIColor colorWithHexString:@"#FF8B00"].CGColor;
    self.hightEfectBtn.layer.borderWidth=1;
    self.hightEfectBtn.layer.borderColor=[UIColor colorWithHexString:@"#FF8B00"].CGColor;
    CGFloat inset=0;
    if (StatusHeight>=44) {
        inset=34;
    }
    self.contentHeight.constant=KScreenHeight-49-inset;
    if ([[UserInfoManager shareInstance].userInfo.role isEqualToString:@"护士"]) {
        self.qrBtn.hidden=YES;
        self.buildinfoImgv.image=[UIImage imageNamed:@"图-灰"];
        
        self.buildLab1.textColor=[UIColor colorWithHexString:@"#aeaeae"];
        self.buildLab2.textColor=[UIColor colorWithHexString:@"#aeaeae"];
        [self.speedBtn setTitleColor:[UIColor colorWithHexString:@"#aeaeae"] forState:UIControlStateNormal];
        [self.hightEfectBtn setTitleColor:[UIColor colorWithHexString:@"#aeaeae"] forState:UIControlStateNormal];
        self.speedBtn.borderColor=[UIColor colorWithHexString:@"#e5e5e5"];
        self.hightEfectBtn.borderColor=[UIColor colorWithHexString:@"#e5e5e5"];
        self.buildBtn.userInteractionEnabled=NO;
        
    }
    self.navView_top.constant=NavHeight;
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.navView.bounds;  // 设置显示的frame
    gradientLayer.colors = @[(id)RGBA(0, 0, 0, 0.4).CGColor,(id)RGBA(0,0,0,0).CGColor];  // 设置渐变颜色
//        gradientLayer.locations = @[@0.0, @0.5];    // 颜色的起点位置，递增，并且数量跟颜色数量相等
    //
  
    
    [self.navView.layer insertSublayer:gradientLayer atIndex:0];
//    self.roudViewHeight.constant=(170/320.0*KScreenWidth);
    self.navigationController.delegate=self;
//    if (@available(iOS 11.0, *)) {
//        self.scrollView.contentInsetAdjustmentBehavior=UIScrollViewContentInsetAdjustmentNever;
//    } else {
//        // Fallback on earlier versions
//        self.automaticallyAdjustsScrollViewInsets=NO;
//    }
    WeakSelf(weak);
    self.scrollView.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weak netWorkForScorllViewImage];
//        [weak networkForQueryVisitCount];
//        [weak networkForQueryAuditCount];
    }];
    if (@available(iOS 11.0, *)){
        self.scrollView.contentInsetAdjustmentBehavior=UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets=NO;
    }
    [self loaddefaultgroupinfo];
    
}
// 获取医院默认群信息
-(void)loaddefaultgroupinfo{
    NSString *api=@"/api/services/app/chatgroup/GetHospitalGroupsV2";
    NSDictionary *para=@{
                         @"userId":[UserInfoManager shareInstance].userInfo.id,
                         @"roleType":@2
                         };
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        
        if(SUCCEESS){
            YSLog(@"---");
            //            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:(UITableViewRowAnimationFade)];
            NSArray *arr=responseObject[@"result"];
            if (arr.count>0) {
                [[NSUserDefaults standardUserDefaults] setObject:arr[0][@"easemobGroupId"] forKey:@"defaultGroupId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            //            [self.tableView reloadData];
            
        }
    } errorHandel:^(NSError *error) {
        
    }];
}

-(void)loginEMob:(NSString*)userName{
    //    [self showHudInView:self.view hint:NSLocalizedString(@"login.ongoing", @"Is Login...")];
    //异步登陆账号
//    __weak typeof(self) weakself = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        EMError *error = [[EMClient sharedClient] loginWithUsername:userName password:@"ckdcloud2017"];
        YSLog(@"%@",error);
//        dispatch_async(dispatch_get_main_queue(), ^{
//            //            [weakself hideHud];
//            [MBProgressHUD hideHUDForView:weakself.view animated:YES];
            //            YSLog(@"%@",[EMClient sharedClient].currentUsername);
            //            YSLog(@"%ld",[EMClient sharedClient].isLoggedIn);
//            if (!error) {
                //设置是否自动登录
//                [weakself showMessageHud:@"登录成功"];
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:[NSNumber numberWithBool:YES]];
//                });
               
                //                YSLog(@"-----");
                //                YSLog(@"%ld",[EMClient sharedClient].isLoggedIn);
                
                //保存最近一次登录用户名
                //                [weakself saveLastLoginUsername];
                //发送自动登陆状态通知
                //                [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:[NSNumber numberWithBool:YES]];
                
//            } else {
//                switch (error.code)
//                {
//                    default:{
//
//                    }
//                }
//            }
//        });
    });
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    YSLog(@"%@",[EMClient sharedClient].currentUsername);
//    self.navigationController.navigationBar.hidden=YES;
//    YSLog(@"viewWillAppear");
    if (self.imageArray.count==0) {
         [self netWorkForScorllViewImage];
    }
   
    [self networkForQueryVisitCount];
    [self networkForQueryAuditCount];
}
// 将要显示控制器
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // 判断要显示的控制器是否是自己
    
    BOOL isShowHomePage = NO;
    if ([viewController isKindOfClass:[self class]]) {
        isShowHomePage=YES;
    }
    
    if ([viewController isKindOfClass:[CDKVisitVC class]]) {
        isShowHomePage=YES;
    }
    [self.navigationController setNavigationBarHidden:isShowHomePage animated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    kApplication.statusBarStyle=UIStatusBarStyleLightContent;
//    YSLog(@"viewDidAppear");
    if (![EMClient sharedClient].isLoggedIn) {
        [self loginEMob:[UserInfoManager shareInstance].userInfo.easemobId];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    kApplication.statusBarStyle=UIStatusBarStyleDefault;
//    self.navigationController.navigationBar.hidden=NO;
//    YSLog(@"viewWillDisappear");

}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
//    YSLog(@"viewDidDisappear");
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    YSLog(@"scrollView%@",[NSValue valueWithCGPoint:scrollView.contentOffset]);
    if (scrollView.contentOffset.y>=0) {
        [scrollView setContentOffset:CGPointMake(0, 0)];
    }
}
- (void)setScorllView{
    // 轮播图
    [self.imageArray removeAllObjects];
    for (CDKNewsModel *model in self.newsModelArray) {
        [self.imageArray addObject:model.imageUrl];
    }
    CGFloat roundScrillViewHerght = (68.0/108*KScreenWidth);
    self.roundScrollView = [SZKRoundScrollView roundScrollViewWithFrame:CGRectMake(0, 0, KScreenWidth, roundScrillViewHerght) imageArr:self.imageArray timerWithTimeInterval:3 imageClick:^(NSInteger imageIndex) {
        YSLog(@"%ld",(long)imageIndex);
        CDKWebViewVC *vc = [[CDKWebViewVC alloc] init];
        CDKNewsModel *model = self.newsModelArray[imageIndex];
        vc.titleName = model.title;
        vc.url = model.linkUrl;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    self.roundScrollView.pageControlAlignment = NSPageControlAlignmentRight;
    [self.bgRoundView addSubview:self.roundScrollView];
}

#pragma mark -- network

/**
 获取轮播图数据
 */
- (void)netWorkForScorllViewImage{
    NSString *url = @"/api/services/app/ad/QueryDoctorAdListV2";
    [YSNetworkingManager requestWithUrl:url :POST paramiters:nil success:^(id responseObject) {
        [self.scrollView.mj_header endRefreshing];
        if (SUCCEESS) {
            [self.newsModelArray removeAllObjects];
            [self.newsModelArray addObjectsFromArray:[NSArray modelArrayWithClass:[CDKNewsModel class] json:responseObject[@"result"]]];
            if (self.newsModelArray.count > 0) {
                [self setScorllView];
            }
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self.scrollView.mj_header endRefreshing];

        if (error.code == - 1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}


/**
 今日随访人数
 */
- (void)networkForQueryVisitCount{
    NSString *url = @"/api/services/ckd/visit/QueryVisitCount";
    [YSNetworkingManager requestWithUrl:url :POST paramiters:nil success:^(id responseObject) {
        if (SUCCEESS) {
            self.todayPeopleCountLabel.text = [NSString stringWithFormat:@"%@ ",responseObject[@"result"]];
            
        }else{
//            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
//        if (error.code == - 1001 ) {
//            [self showMessageHud:@"请求超时"];
//        }else{
//            [self showMessageHud:@"请检查网络"];
//        }
    }];
}


/**
 审核人数
 */
- (void)networkForQueryAuditCount{
    NSString *url = @"/api/services/ckd/audit/QueryAuditCount";
    [YSNetworkingManager requestWithUrl:url :POST paramiters:nil success:^(id responseObject) {
        if (SUCCEESS) {
            self.examinePeopleCountLabel.text = [NSString stringWithFormat:@"%@ ",responseObject[@"result"]];
        }else{
//            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
//        if (error.code == - 1001 ) {
//            [self showMessageHud:@"请求超时"];
//        }else{
//            [self showMessageHud:@"请检查网络"];
//        }
    }];
}


#pragma mark -- Lazy
- (NSMutableArray *)imageArray{
    if (!_imageArray){
        _imageArray = [NSMutableArray array];
    }
    return _imageArray;
}

- (NSMutableArray *)newsModelArray{
    if (!_newsModelArray){
        _newsModelArray = [NSMutableArray array];
    }
    return _newsModelArray;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
