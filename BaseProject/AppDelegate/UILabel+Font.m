//
//  UILabel+Font.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/7/24.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "UILabel+Font.h"
#import <objc/runtime.h>
@implementation UILabel (Font)
+ (void)load{
    Method imp = class_getInstanceMethod([self class], @selector(initWithCoder:));
    Method myImp = class_getInstanceMethod([self class], @selector(myInitWithCoder:));
    method_exchangeImplementations(imp, myImp);
}

- (id)myInitWithCoder:(NSCoder*)aDecode{
    [self myInitWithCoder:aDecode];
    if (self) {
        //部分不像改变字体的 把tag值设置成333跳过
        CGFloat scale=1;
        if (KScreenWidth>320) {
            scale=1.12;
        }
            CGFloat fontSize = self.font.pointSize;
            self.font = [UIFont systemFontOfSize:fontSize*scale];
     
    }
    return self;
}
@end
