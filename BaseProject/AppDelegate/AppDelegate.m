//
//  AppDelegate.m
//  BaseProject
//
//  Created by apple on 2017/6/19.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//
#import "AppDelegate.h"
#import "YSTabBarController.h"
#import <IQKeyboardManager.h>
#import <UMCommon/UMCommon.h>           // 友盟公共组件是所有友盟产品的基础组件，必选
#import <UMAnalytics/MobClick.h>  // 友盟统计组件
#define UMengAppKey @"5a542850b27b0a59d500010f"

// 引入JPush功能所需头文件
#import "JPUSHService.h"
// iOS10注册APNs所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif
#define JPUSHAppKey @"5a542850b27b0a59d500010f"
#import "YSLoginViewController.h"
#import "AppDelegate+EaseMob.h"
#import "AppDelegate+Parse.h"
#import <Bugly/Bugly.h>
/* ShareSDK
#import <ShareSDKConnector/ShareSDKConnector.h>
//腾讯开放平台（对应QQ和QQ空间）SDK头文件
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
//微信SDK头文件
#import "WXApi.h"
//新浪微博SDK头文件
#import "WeiboSDK.h"
//新浪微博SDK需要在项目Build Settings中的Other Linker Flags添加"-ObjC"
*/

#import <AFNetworkReachabilityManager.h>
@interface AppDelegate ()
@property(nonatomic,strong) AFNetworkReachabilityManager *mananger;
@property(nonatomic,assign) AFNetworkReachabilityStatus stauts;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
     _connectionState = EMConnectionConnected;
    self.window = [[UIWindow alloc] init];
    self.window.frame = [UIScreen mainScreen].bounds;
    self.window.backgroundColor=[UIColor whiteColor];

    self.stauts=AFNetworkReachabilityStatusReachableViaWiFi;
    [self.window makeKeyAndVisible];
   
//    self.window.rootViewController=[CDKFinishResumeVC new];
//    return YES;
    [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithHexString:@"#656565"]];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} forState:UIControlStateHighlighted];
   
    [Bugly startWithAppId:@"c29d9f57cc"];
#pragma 友盟配置
    // 配置友盟SDK产品并并统一初始化
    // [UMConfigure setEncryptEnabled:YES]; // optional: 设置加密传输, 默认NO.
    // [UMConfigure setLogEnabled:YES]; // 开发调试时可在console查看友盟日志显示，发布产品必须移除。
//    [UMConfigure initWithAppkey:UMengAppKey channel:@"Enterprise"];
    /* appkey: 开发者在友盟后台申请的应用获得（可在统计后台的 “统计分析->设置->应用信息” 页面查看）*/
    // 统计组件配置
//    [MobClick setScenarioType:E_UM_NORMAL];
    // [MobClick setScenarioType:E_UM_GAME];  // optional: 游戏场景设置
#pragma JPUSH配置
//    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
//    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
//    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        // 可以添加自定义categories
        // NSSet<UNNotificationCategory *> *categories for iOS10 or later
        // NSSet<UIUserNotificationCategory *> *categories for iOS8 and iOS9
//    }
//    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];

//    [JPUSHService setupWithOption:launchOptions appKey:JPUSHAppKey
//                          channel:@"Enterprise"
//                 apsForProduction:0
//            advertisingIdentifier:nil];
#pragma iqkeyboard
    IQKeyboardManager *iqkeyboard=[IQKeyboardManager sharedManager];
    iqkeyboard.enable=YES;
    iqkeyboard.shouldResignOnTouchOutside=YES;
    iqkeyboard.toolbarDoneBarButtonItemText=@"完成";
#pragma 读取本地用户信息
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSData *user=[defaults objectForKey:@"user"];
    if (user) {
        [UserInfoManager modelWithJSON:user];
    }
#pragma 配置ShareSDK
  /*
    [ShareSDK registerActivePlatforms:@[
                                        @(SSDKPlatformTypeSinaWeibo),
                                        @(SSDKPlatformTypeWechat),
                                        @(SSDKPlatformTypeQQ)
                        
                                        ]
                             onImport:^(SSDKPlatformType platformType)
     {
         switch (platformType)
         {
             case SSDKPlatformTypeWechat:
                 [ShareSDKConnector connectWeChat:[WXApi class]];
                 break;
             case SSDKPlatformTypeQQ:
                 [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
                 break;
             case SSDKPlatformTypeSinaWeibo:
                 [ShareSDKConnector connectWeibo:[WeiboSDK class]];
                 break;
            
             default:
                 break;
         }
     }
                      onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo)
     {
         
         switch (platformType)
         {
             case SSDKPlatformTypeSinaWeibo:
                 //设置新浪微博应用信息,其中authType设置为使用SSO＋Web形式授权
                 [appInfo SSDKSetupSinaWeiboByAppKey:@"568898243"
                                           appSecret:@"38a4f8204cc784f81f9f0daaf31e02e3"
                                         redirectUri:@"http://www.sharesdk.cn"
                                            authType:SSDKAuthTypeBoth];
                 break;
             case SSDKPlatformTypeWechat:
                 [appInfo SSDKSetupWeChatByAppId:@"wx4868b35061f87885"
                                       appSecret:@"64020361b8ec4c99936c0e3999a9f249"];
                 break;
             case SSDKPlatformTypeQQ:
                 [appInfo SSDKSetupQQByAppId:@"100371282"
                                      appKey:@"aed9b0303e3ed1e27bae87c33761161d"
                                    authType:SSDKAuthTypeBoth];
                 break;
            
            
             
         }
     }];
   */
#pragma 环信
    // 环信UIdemo中有用到Parse，您的项目中不需要添加，可忽略此处。
    [self parseApplication:application didFinishLaunchingWithOptions:launchOptions];
    
#warning Init SDK，detail in AppDelegate+EaseMob.m
#warning SDK注册 APNS文件的名字, 需要与后台上传证书时的名字一一对应
    NSString *apnsCertName = nil;
#if DEBUG
    apnsCertName = @"apns_dev";
#else
    apnsCertName = @"NutritionApnsName";
#endif
    
   
    
    [self easemobApplication:application
didFinishLaunchingWithOptions:launchOptions
                      appkey:EaseMobAppKey
                apnsCertName:apnsCertName
                 otherConfig:@{kSDKConfigEnableConsoleLogger:[NSNumber numberWithBool:NO]}];
    
    
    //创建网络监听对象
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    //开始监听
    self.mananger=manager;
    [manager startMonitoring];
    //监听改变
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
            case AFNetworkReachabilityStatusNotReachable:
            {
                 [kAppWindow showMessageHud:@"当前网络不可用,请检查网络"];
                    self.stauts=status;
            }
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
            {
                self.stauts=status;
            }
                break;
        }
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(emobloginfailed) name:@"EMobLoginFailed" object:nil];
    
    return YES;
}
-(void)emobloginfailed{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [kAppWindow showMessageHud:@"聊天服务器登录失败,聊天功能会受到影响"];
    });
}
//-(void)setStauts:(AFNetworkReachabilityStatus)stauts{
//    _stauts=stauts;
//
//}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //Optional
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
    
}



// 将得到的deviceToken传给SDK
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
//    [JPUSHService registerDeviceToken:deviceToken];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[EMClient sharedClient] bindDeviceToken:deviceToken];
    });
}

//#pragma mark- JPUSHRegisterDelegate 处理通知的方法

// iOS 10 Support
//- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
//    // Required
//    NSDictionary * userInfo = notification.request.content.userInfo;
//    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
//        [JPUSHService handleRemoteNotification:userInfo];
//    }
//    completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
//    [self easemobApplication:[UIApplication sharedApplication] didReceiveRemoteNotification:userInfo];
//}

// iOS 10 Support
//- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
//    // Required
//    NSDictionary * userInfo = response.notification.request.content.userInfo;
//    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
//        [JPUSHService handleRemoteNotification:userInfo];
//    }
//    completionHandler();  // 系统要求执行这个方法
//    [self easemobApplication:[UIApplication sharedApplication] didReceiveRemoteNotification:userInfo];
//    [[NSNotificationCenter defaultCenter] postNotificationName:UNREADMESSAGE object:nil];
//    //    if (_mainController) {
//    //                [_mainController didReceiveLocalNotification:notification];
//    //            }
//    // 点击推送跳转到聊天页面
//
//
//}
// iOS 10 Support



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    // Required, iOS 7 Support
    completionHandler(UIBackgroundFetchResultNewData);
    [self easemobApplication:[UIApplication sharedApplication] didReceiveRemoteNotification:userInfo];
//    if (_mainController) {
//                [_mainController didReceiveLocalNotification:notification];
//            }
    [[NSNotificationCenter defaultCenter] postNotificationName:UNREADMESSAGE object:nil];
    YSLog(@"local notifacaiton");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        YSTabBarController *tab=self.window.rootViewController;
        if ([tab isKindOfClass:[YSTabBarController class]]) {
            tab.selectedIndex=2;
        }
    });
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
//    if (_mainController) {
//        [_mainController didReceiveLocalNotification:notification];
//    }
    YSLog(@"local notifacaiton");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        YSTabBarController *tab=self.window.rootViewController;
        if ([tab isKindOfClass:[YSTabBarController class]]) {
            tab.selectedIndex=2;
        }
    });
}



- (void)applicationWillResignActive:(UIApplication *)application {
   
}






// APP进入后台
- (void)applicationDidEnterBackground:(UIApplication *)application
{
//    [[EMClient sharedClient] applicationDidEnterBackground:application];
}

// APP将要从后台返回
- (void)applicationWillEnterForeground:(UIApplication *)application
{
//    [[EMClient sharedClient] applicationWillEnterForeground:application];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if ((self.stauts == AFNetworkReachabilityStatusNotReachable)||(self.stauts == AFNetworkReachabilityStatusUnknown)) {
        [kAppWindow showMessageHud:@"当前网络不可用,请检查网络"];

    }
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
