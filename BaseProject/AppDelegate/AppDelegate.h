//
//  AppDelegate.h
//  BaseProject
//
//  Created by apple on 2017/6/19.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YSTabBarController.h"
#import "MainViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    EMConnectionState _connectionState;
}
@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,assign) BOOL  isLight;
@property(nonatomic,strong) MainViewController *mainController;
@property(nonatomic,strong) ConversationListController *conversationListVC;
@property (nonatomic, weak) ChatViewController *chatVC;

@property (strong, nonatomic) NSDate *lastPlaySoundDate;




@end

