//
//  CViewController.m
//  BaseProject
//
//  Created by apple on 2017/6/22.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "CViewController.h"
#import "AppDelegate.h"
#import "AdressListVC.h"
#import "ConversationListController.h"
#import "ContactListViewController.h"
@interface CViewController ()<ZJScrollPageViewDelegate>
@property(strong, nonatomic)NSArray<NSString *> *titles;
@property(strong, nonatomic)NSArray<UIViewController<ZJScrollPageViewChildVcDelegate> *> *childVcs;
@property (weak, nonatomic) ZJScrollSegmentView *segmentView;
@property (weak, nonatomic) ZJContentView *contentView;
@end

@implementation CViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    //必要的设置, 如果没有设置可能导致内容显示不正常
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.childVcs = [self setupChildVc];
    // 初始化
    [self setupSegmentView];
    [self setupContentView];
    
//    [self unreadMessage];
}

- (NSArray *)setupChildVc {
    ConversationListController *vc1 ;
    AppDelegate *delegate=(AppDelegate*)kApplication.delegate;
    if (delegate.conversationListVC) {
        vc1=delegate.conversationListVC;
    }else{
        vc1=[ConversationListController new];
        delegate.conversationListVC=vc1;
    }
   AdressListVC  *vc2 = [AdressListVC new];
    NSArray *childVcs = [NSArray arrayWithObjects:vc1, vc2, nil];
    return childVcs;
}
- (void)setupSegmentView {
    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
    style.showLine = YES;
    // 不要滚动标题, 每个标题将平分宽度
    style.scrollTitle = NO;
//    style.titleMargin=30;
//    style.showCover=YES;
    style.autoAdjustTitlesWidth=NO;
    style.contentViewBounces=NO;
    style.scrollContentView=NO;
//    style.adjustTitleWhenBeginDrag=YES;
    style.scrollLineColor=RGB(69,213,160);
    // 渐变
//    style.gradualChangeTitleColor = YES;
    // 遮盖背景颜色
    style.coverBackgroundColor = [UIColor redColor];
    
    //标题一般状态颜色 --- 注意一定要使用RGB空间的颜色值
    style.normalTitleColor = RGB(50, 50, 50);
    //标题选中状态颜色 --- 注意一定要使用RGB空间的颜色值
    style.selectedTitleColor = RGB(69,213,160);
    
    self.titles = @[@"消息", @"通讯录"];
    
    // 注意: 一定要避免循环引用!!
    __weak typeof(self) weakSelf = self;
    ZJScrollSegmentView *segment = [[ZJScrollSegmentView alloc] initWithFrame:CGRectMake(0, 0, 280, 44) segmentStyle:style delegate:self titles:self.titles titleDidClick:^(ZJTitleView *titleView, NSInteger index) {
        
        [weakSelf.contentView setContentOffSet:CGPointMake(weakSelf.contentView.bounds.size.width * index, 0.0) animated:YES];
        
    }];
    
    // 自定义标题的样式
//    segment.layer.cornerRadius = 14.0;
    segment.backgroundColor = [UIColor whiteColor];
    // 当然推荐直接设置背景图片的方式
    //    segment.backgroundImage = [UIImage imageNamed:@"extraBtnBackgroundImage"];
    
    self.segmentView = segment;
    self.navigationItem.titleView = self.segmentView;
//    self.contentView.collectionView.delaysContentTouches=NO;
}

- (void)setupContentView {
    
    CGFloat bottom=0;
    if (StatusHeight>20) {
        bottom=34;
    }
    ZJContentView *content = [[ZJContentView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, KScreenHeight - NavHeight-49-bottom) segmentView:self.segmentView parentViewController:self delegate:self];
    self.contentView = content;
    [self.view addSubview:self.contentView];
    
}



- (NSInteger)numberOfChildViewControllers {
    return self.titles.count;
}




- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
    UIViewController<ZJScrollPageViewChildVcDelegate> *childVc = reuseViewController;
    
    if (!childVc) {
        childVc = self.childVcs[index];
    }
    
    return childVc;
}

-(BOOL)shouldAutomaticallyForwardAppearanceMethods{
    return NO;
}
//-(CGRect)frameOfChildControllerForContainer:(UIView *)containerView {
//    return  CGRectInset(containerView.bounds, 20, 20);
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
 
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
