//
//  BViewController.m
//  BaseProject
//
//  Created by apple on 2017/6/22.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "BViewController.h"
#import "PatientMannagerCell.h"
#import "YSBaseNewsController.h"
#import "YSBaseNewsCell.h"
#import "YSBaseNewsMorImageCell.h"
#import "CDKPatientManagerVC.h"
#import "CDKNewPatientBuildProfile_1.h"
#import "CDKPationtListModel.h"
static NSString *newsCell = @"NEWSCELL";
static NSString *moreNewsCell = @"MORENEWSCELL";
@interface BViewController ()<UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textfile;
@property (weak, nonatomic) IBOutlet UILabel *countLab;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/** 页数 */
@property (nonatomic,assign) NSInteger page;
@property(nonatomic,strong) NSMutableArray *datas;


@end

@implementation BViewController
- (instancetype)init
{
    self = [super init];
    if (self) {
        UIStoryboard *st=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self=[st instantiateViewControllerWithIdentifier:@"BViewController"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView.rowHeight=82;
    self.tableView.emptyDataSetSource=self;
    self.tableView.emptyDataSetDelegate=self;
    self.textfile.clearButtonMode=UITextFieldViewModeAlways;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textfileChange:) name:UITextFieldTextDidChangeNotification object:self.textfile];
    [self.tableView registerNib:[UINib nibWithNibName:@"PatientMannagerCell" bundle:nil] forCellReuseIdentifier:@"Patient"];
    [self.view addSubview:self.tableView];
    
    WeakSelf(weak);
    if (![[UserInfoManager shareInstance].userInfo.role isEqualToString:@"护士"]) {
        UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithTitle:@"新患者建档" style:UIBarButtonItemStylePlain handler:^(id  _Nonnull sender) {
            CDKNewPatientBuildProfile_1 *vc=[[CDKNewPatientBuildProfile_1 alloc] init];
            [weak.navigationController pushViewController:vc animated:YES];
            
        }];
        self.navigationItem.rightBarButtonItem=item;
    }
    
    self.datas=[NSMutableArray array];
    
    self.tableView.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weak loadData];
    }];
    self.tableView.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weak loadMoreData];
    }];
    [self.tableView.mj_header beginRefreshing];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)textfileChange:(NSNotification*)ntf{
    NSLog(@"");
    if (self.textfile.text.length==0) {
         [self searchData];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    if (textField.text.length==0) {
        [self showMessageHud:@"请输入内容"];
    }else{
       [self searchData];
    }
    
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}
-(void)searchData{
    [self.tableView.mj_footer endRefreshing];
    [self.tableView.mj_header endRefreshing];

    NSString *api=@"/api/services/ckd/patient/QueryList";
    self.page=1;
    NSString *keywords=@"";
    if (self.textfile.text.length>0) {
        keywords=self.textfile.text;
    }
    
    NSMutableDictionary  *para=[NSMutableDictionary dictionary];
    para[@"Keyword"]=keywords;
    para[@"PageIndex"]=@(self.page);
    para[@"PageSize"]=@15;
        [self showLoadingHud:nil];
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            
            [self.datas removeAllObjects];
            NSArray *arr=[NSArray modelArrayWithClass:[CDKPationtListModel class] json:responseObject[@"result"][@"items"]];
            [self.datas addObjectsFromArray:arr];
            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
        
    }];
}
- (void)loadData{
    [self.tableView.mj_footer endRefreshing];
    NSString *api=@"/api/services/ckd/patient/QueryList";
    self.page=1;
    NSString *keywords=@"";
    if (self.textfile.text.length>0) {
        keywords=self.textfile.text;
    }
    
    NSMutableDictionary  *para=[NSMutableDictionary dictionary];
    para[@"Keyword"]=keywords;
    para[@"PageIndex"]=@(self.page);
    para[@"PageSize"]=@15;
//    [self showLoadingHud:nil];
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        if (SUCCEESS) {
            [self.datas removeAllObjects];
            NSArray *arr=[NSArray modelArrayWithClass:[CDKPationtListModel class] json:responseObject[@"result"][@"items"]];
            [self.datas addObjectsFromArray:arr];
            NSInteger peopleCount = [responseObject[@"result"][@"recordCount"] integerValue];
            self.countLab.text=[NSString stringWithFormat:@"共%ld人",peopleCount];
            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];

        }
        
    } errorHandel:^(NSError *error) {
        
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
        [self.tableView.mj_header endRefreshing];

    }];
}

- (void)loadMoreData{
    
    [self.tableView.mj_header endRefreshing];
    NSString *api=@"/api/services/ckd/patient/QueryList";
    self.page++;
    NSString *keywords=@"";
    if (self.textfile.text.length>0) {
        keywords=self.textfile.text;
    }
    NSMutableDictionary  *para=[NSMutableDictionary dictionary];
    para[@"Keyword"]=keywords;
    para[@"PageIndex"]=@(self.page);
    para[@"PageSize"]=@15;
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        
        if (SUCCEESS) {
            NSArray *arr=[NSArray modelArrayWithClass:[CDKPationtListModel class] json:responseObject[@"result"][@"items"]];
            if (arr.count>0) {
                [self.tableView.mj_footer endRefreshing];
                [self.datas addObjectsFromArray:arr];
            }else{
                if (self.page>1) {
                    self.page--;
                }
                [self.tableView.mj_footer endRefreshingWithNoMoreData];

            }
            
            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self.tableView.mj_footer endRefreshing];
        if (self.page>1) {
            self.page--;
        }
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
        [self.tableView.mj_header endRefreshing];
        
    }];
}

#pragma mark -UITableViewDelegate & UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PatientMannagerCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Patient" forIndexPath:indexPath];
    cell.model=self.datas[indexPath.row];
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CDKPatientManagerVC *vc=[[CDKPatientManagerVC alloc] init];
    vc.pationtModel=self.datas[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}



      
        



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma emptyDataset


-(NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    NSAttributedString *str=[[NSAttributedString alloc] initWithString:@"没有数据" attributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    return str;
}
- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView{
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
