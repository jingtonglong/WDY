//
//  DViewController.m
//  BaseProject
//
//  Created by apple on 2017/6/22.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "DViewController.h"
#import "CustomNavView.h"
#import "MyQRVC.h"
#import "CDKAboutUsVC.h"
#import "AccuntSecurityVC.h"
#import "CDKGroupMenberVC.h"
#import "CDKMyProfileVC.h"
@interface DViewController ()<UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *title_constant;
@property (weak, nonatomic) IBOutlet UIImageView *headportraitView;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;

@property (weak, nonatomic) IBOutlet UIView *navView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *qrcontentheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *qrheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentmiddel;

@end

@implementation DViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        UIStoryboard *st=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self=[st instantiateViewControllerWithIdentifier:@"DViewController"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navView.height+=StatusHeight;
    self.bgHeight.constant=114+StatusHeight;
    self.title_constant.constant=10+StatusHeight;
    self.tableView.bounces=NO;
    self.tableView.scrollEnabled=NO;
    // Do any additional setup after loading the view.
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior=UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
        self.automaticallyAdjustsScrollViewInsets=NO;
    }
    if ([[UserInfoManager shareInstance].userInfo.role isEqualToString:@"护士"]) {
        if(KScreenWidth>320){
            self.qrheight.constant=0;
            self.qrcontentheight.constant=61;
            self.cellheight.constant=60;
            self.contentmiddel.constant=121;
        }else{
            self.qrheight.constant=0;
            self.qrcontentheight.constant=51;
            self.cellheight.constant=50;
            self.contentmiddel.constant=101;
        }
        
    }else{
        
        if(KScreenWidth>320){
            self.qrheight.constant=60;
            self.qrcontentheight.constant=121;
            self.cellheight.constant=60;
            self.contentmiddel.constant=121;
        }else{
            self.qrheight.constant=50;
            self.qrcontentheight.constant=101;
            self.cellheight.constant=50;
            self.contentmiddel.constant=101;
        }
        
    }
    self.navigationController.delegate=self;
    [self updateUI];
}
//-(void)loadData{
//    [self showLoadingHud:nil];
//    NSString *api=@"/api/services/ckd/user/QueryUserInfo";
////    NSDictionary *para=@{};
//    [YSNetworkingManager requestWithUrl:api :POST paramiters:nil success:^(id responseObject) {
//        [self dismissHud];
//        if (SUCCEESS) {
//            [UserInfoManager shareInstance].userInfo=[CDKUserInfo modelWithJSON:responseObject[@"result"]];
//            [self updateUI];
//
//        }else{
//            [self showMessageHud:responseObject[@"error"][@"message"]];
//        }
//    } errorHandel:^(NSError *error) {
//        [self dismissHud];
//        if (error.code ==-1001 ) {
//            [self showMessageHud:@"请求超时"];
//        }else{
//            [self showMessageHud:@"请检查网络"];
//        }
//    }];
//}
-(void)updateUI{
    [self.headportraitView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[UserInfoManager shareInstance].userInfo.headPicture]] placeholderImage:[UIImage imageNamed:@"默认头像"]];
    self.nameLab.text=[UserInfoManager shareInstance].userInfo.name;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    kApplication.statusBarStyle=UIStatusBarStyleLightContent;
//    self.navigationController.navigationBar.hidden=YES
//     [self.navigationController setNavigationBarHidden:YES animated:YES];
//    YSLog(@"viewWillAppear");
    [self.headportraitView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[UserInfoManager shareInstance].userInfo.headPicture]] placeholderImage:[UIImage imageNamed:@"默认头像"]];
//    self.headportraitView.image=[UIImage imageNamed:@"head001"];
    

}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    kApplication.statusBarStyle=UIStatusBarStyleLightContent;
//    YSLog(@"viewDidAppear");

}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    kApplication.statusBarStyle=UIStatusBarStyleDefault;
//    self.navigationController.navigationBar.hidden=NO;
//     [self.navigationController setNavigationBarHidden:NO animated:YES];
//    YSLog(@"viewWillDisappear");
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
//    YSLog(@"viewDidDisappear");
}

// 将要显示控制器
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // 判断要显示的控制器是否是自己
    BOOL isShowHomePage = [viewController isKindOfClass:[self class]];
    
    [self.navigationController setNavigationBarHidden:isShowHomePage animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 我的二维码

 @param sender UIButton
 */
- (IBAction)myQR:(id)sender {
    
    MyQRVC *vc=[[MyQRVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];

}
/**
 我的资料
 
 */
- (IBAction)myProfile:(id)sender {
    CDKMyProfileVC *vc=[[CDKMyProfileVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];

}

- (IBAction)acuntSecurity:(id)sender {
    AccuntSecurityVC *vc=[[AccuntSecurityVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)aboutUs:(id)sender {
    CDKAboutUsVC *vc=[[CDKAboutUsVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];

}
- (IBAction)loginOut:(id)sender {
    
    [self showLoadingHud:nil];
    [[EMClient sharedClient] logout:YES completion:^(EMError *aError) {
        [self hideHud];
//        [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"result"][0][@"easemobGroupId"] forKey:@"defaultGroupId"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"defaultGroupId"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user"];
        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
        
    }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
