//
//  CustomNavView.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CustomNavView.h"

@implementation CustomNavView
-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        _titleLab=[[UILabel alloc] init];
        _titleLab.font=[UIFont systemFontOfSize:15];
        _titleLab.textColor=[UIColor whiteColor];
        
        _titleLab.textAlignment=NSTextAlignmentCenter;
       
        CAGradientLayer *gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = self.bounds;  // 设置显示的frame
        gradientLayer.colors = @[(id)RGB(99,220,175).CGColor,(id)RGB(98,228,150).CGColor];  // 设置渐变颜色
        //    gradientLayer.locations = @[@0.0, @0.2, @0.5];    // 颜色的起点位置，递增，并且数量跟颜色数量相等
           //
        [self.layer addSublayer:gradientLayer];
        [self addSubview:_titleLab];
        [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.width.equalTo(self);
            make.height.mas_equalTo(30);
            make.bottom.mas_equalTo(-7);
        }];
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
