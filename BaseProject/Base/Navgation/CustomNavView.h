//
//  CustomNavView.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavView : UIView
@property(nonatomic,strong) UILabel *titleLab;

@end
