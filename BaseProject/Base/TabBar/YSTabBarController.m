//
//  YSTabBarController.m
//  BaseProject
//
//  Created by apple on 2017/6/22.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "YSTabBarController.h"
#import "YSNavigationController.h"

#import "AViewController.h"
#import "BViewController.h"
#import "CViewController.h"
#import "DViewController.h"
@interface YSTabBarController ()
{
    UIBarButtonItem *_addFriendItem;
    EMConnectionState _connectionState;
}

@end

@implementation YSTabBarController

+(void)initialize{
    // 通过appearance统一设置所有UITabBarItem的文字属性
    // 后面带有UI_APPEARANCE_SELECTOR的方法，都可以通过appearance对象来统一设置
    
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:12];
    attrs[NSForegroundColorAttributeName] = [UIColor grayColor];
    
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSFontAttributeName] = [UIFont systemFontOfSize:12];
    selectedAttrs[NSForegroundColorAttributeName] = RGB(69,213,160);
    
    UITabBarItem *item = [UITabBarItem appearance];
    [item setTitleTextAttributes:attrs forState:UIControlStateNormal];
    [item setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unreadMessage) name:UNREADMESSAGE object:nil];
#warning ------------------------Class
    [self setupChildVc:[[AViewController alloc] init] title:@"首页" image:@"首页" selectedImage:@"首页-选中"];
    [self setupChildVc:[[BViewController alloc] init] title:@"患者管理" image:@"患者管理" selectedImage:@"患者管理-选中"];
    [self setupChildVc:[[CViewController alloc] init] title:@"在线随访" image:@"在线随访" selectedImage:@"在线随访-选中"];
    [self setupChildVc:[[DViewController alloc] init] title:@"个人中心" image:@"我的" selectedImage:@"我的-选中"];
    
}

- (void)setupChildVc:(UIViewController *)vc title:(NSString *)title image:(NSString *)image
       selectedImage:(NSString *)selectedImage{
    vc.navigationItem.title = title;
    vc.tabBarItem.title = title;
    vc.tabBarItem.image = [UIImage imageNamed:image];
    vc.tabBarItem.selectedImage = [UIImage imageNamed:selectedImage];
    
    // 包装一个导航控制器，添加导航控制器为tabbarcontroller的子控制器
    [self addChildViewController:vc];
    
    YSNavigationController *nav = [[YSNavigationController alloc] initWithRootViewController:vc];
    [self addChildViewController:nav];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)unreadMessage{
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    UIViewController *vc=self.childViewControllers[2];
    if (unreadCount > 0) {
        vc.tabBarItem.badgeValue = [NSString stringWithFormat:@"%i",(int)unreadCount];
    }else{
        vc.tabBarItem.badgeValue = nil;
    }
    
    
    UIApplication *application = [UIApplication sharedApplication];
    [application setApplicationIconBadgeNumber:unreadCount];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
