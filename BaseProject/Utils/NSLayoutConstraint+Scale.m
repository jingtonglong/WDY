//
//  NSLayoutConstraint+Scale.m
//  NavBack
//
//  Created by sss on 2018/7/25.
//  Copyright © 2018年 kb210. All rights reserved.
//
#import <objc/runtime.h>
#import "NSLayoutConstraint+Scale.h"
static void *vauleKey = &vauleKey;
@implementation NSLayoutConstraint (Scale)
-(void)setVaule:(NSString *)vaule {
    CGFloat w = [UIScreen mainScreen].bounds.size.width;
    objc_setAssociatedObject(self, vauleKey, vaule, OBJC_ASSOCIATION_ASSIGN);
    self.constant=w/320.0*([vaule floatValue]);
    
}
-(NSString *)vaule{
    
    return objc_getAssociatedObject(self, vauleKey);
}
@end
