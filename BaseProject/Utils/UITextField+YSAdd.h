//
//  UITextField+YSAdd.h
//  BaseProject
//
//  Created by apple on 2017/6/20.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (YSAdd)
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender;
@end
