//
//  NSDictionary+YSAdd.m
//  BaseProject
//
//  Created by apple on 2017/6/20.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "NSDictionary+YSAdd.h"
#import "NSArray+YSAdd.h"
@implementation NSDictionary (YSAdd)
- (NSString *)dicURLSort {
    if (self.allKeys == 0) {
        return @"";
    }
    
    NSString *str = @"";
    for (NSString *key in [self.allKeys objSort]) {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"%@=%@&",key,[self objectForKey:key]]];
    }
    return [str substringToIndex:str.length - 1];
}

- (NSDictionary *)dicAppendingParams:(NSDictionary *)params{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:self];
    for (NSString *key in params.allKeys) {
        [dic setObject:[params objectForKey:key] forKey:key];
    }
    return dic;
}

- (NSString *)convertToJSONString {
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if (jsonData == nil) {
        return nil;
    }
    NSString *res = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return res;
}

#pragma mark - dic 转 string
+ (NSString*)dictionaryToJson:(NSDictionary *)dic{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

#pragma mark - string 转 dic
+(NSDictionary *)parseJSONStringToNSDictionary:(NSString *)JSONString {
    NSData *JSONData = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
    return responseJSON;
}

+ (NSString *)arrayToJSONString:(NSArray *)array{
    NSError *error = nil;
    //    NSMutableArray *muArray = [NSMutableArray array];
    //    for (NSString *userId in array) {
    //        [muArray addObject:[NSString stringWithFormat:@"\"%@\"", userId]];
    //    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //    NSString *jsonTemp = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    //    NSString *jsonResult = [jsonTemp stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    NSLog(@"json array is: %@", jsonResult);
    return jsonString;
}

#pragma mark - 时间转换
+ (NSString *)getTimeWithTimeIntervel:(NSTimeInterval)timeInterval formater:(NSString *)formater {
    NSDateFormatter * temp = [[NSDateFormatter alloc] init];
    temp.dateFormat = formater;
    return [temp stringFromDate:[NSDate dateWithTimeIntervalSince1970:timeInterval/1000]];
}

@end
