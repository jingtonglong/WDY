//
//  NSObject+YSAdd.h
//  BaseProject
//
//  Created by apple on 2017/6/20.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MBProgressHUD.h"

#pragma mark - UIView
@interface UIView (toast)

- (MBProgressHUD *)showMessageHud:(NSString *)message;
- (MBProgressHUD *)showSuccessHud:(NSString *)message;
- (MBProgressHUD *)showFailureHud:(NSString *)message;
- (MBProgressHUD *)showFailureHudWithYOffset:(NSString *)message;
- (MBProgressHUD *)showUpImageSuccessHud:(NSString *)message;

- (MBProgressHUD *)showLoadingHud:(NSString *)message;
- (void)dismissHud;

@end
#pragma mark - UIViewController
@interface UIViewController (toast)

- (MBProgressHUD *)showMessageHud:(NSString *)message;
- (MBProgressHUD *)showSuccessHud:(NSString *)message;
- (MBProgressHUD *)showFailureHud:(NSString *)message;
- (MBProgressHUD *)showFailureHudWithYOffset:(NSString *)message;
- (MBProgressHUD *)showUpImageSuccessHud:(NSString *)message;

- (MBProgressHUD *)showLoadingHud:(NSString *)message;
- (void)dismissHud;

@end
