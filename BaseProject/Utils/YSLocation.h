//
//  YSLocation.h
//  BaseProject
//
//  Created by apple on 2017/12/11.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void(^YSLocationBlock)(CLLocationCoordinate2D coordinate);

@interface YSLocation : NSObject<CLLocationManagerDelegate>
@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,strong) CLGeocoder *geocoder;// 地理编码
@property(nonatomic,strong) CLPlacemark *locationMark;//定位点
@property (nonatomic,copy) YSLocationBlock  block;

- (void)getMapLocation:(UIViewController *)viewController;
@end
