//
//  CALayer+Xibconfigs.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/15.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CALayer+Xibconfigs.h"

@implementation CALayer (Xibconfigs)
- (void)setBorderColorWithUIColor:(UIColor *)color
{
    
    self.borderColor = color.CGColor;
}
@end
