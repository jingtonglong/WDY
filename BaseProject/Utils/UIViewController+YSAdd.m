//
//  UIViewController+YSAdd.m
//  BaseProject
//
//  Created by sss on 2017/7/27.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "UIViewController+YSAdd.h"
#import "YSLoginViewController.h"
@implementation UIViewController (YSAdd)
-(void)showLogin{
    YSLoginViewController *vc = [[YSLoginViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}
@end
