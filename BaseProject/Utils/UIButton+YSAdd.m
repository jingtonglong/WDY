//
//  UIButton+YSAdd.m
//  BaseProject
//
//  Created by apple on 2017/6/20.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "UIButton+YSAdd.h"

@import ObjectiveC.runtime;

static const void *YSBarButtonItemBlockKey = &YSBarButtonItemBlockKey;

@interface UIBarButtonItem (BlockPrivate)

- (void)handleAction:(UIBarButtonItem *)sender;

@end
@implementation UIBarButtonItem (SYAdd)

- (instancetype)initWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem
                                    handler:(void (^)(id sender))action {
    
    self = [self initWithBarButtonSystemItem:systemItem
                                      target:self
                                      action:@selector(handleAction:)];
    if (!self) return nil;
    objc_setAssociatedObject(self,
                             YSBarButtonItemBlockKey,
                             action,
                             OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    return self;
}

- (instancetype)initWithImage:(UIImage *)image
                        style:(UIBarButtonItemStyle)style
                      handler:(void (^)(id sender))action {
    
    self = [self initWithImage:image
                         style:style
                        target:self
                        action:@selector(handleAction:)];
    if (!self) return nil;
    objc_setAssociatedObject(self,
                             YSBarButtonItemBlockKey,
                             action,
                             OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    return self;
}

- (instancetype)initWithImage:(UIImage *)image
          landscapeImagePhone:(UIImage *)landscapeImagePhone
                        style:(UIBarButtonItemStyle)style
                      handler:(void (^)(id sender))action {
    
    self = [self initWithImage:image
           landscapeImagePhone:landscapeImagePhone
                         style:style
                        target:self
                        action:@selector(handleAction:)];
    if (!self) return nil;
    objc_setAssociatedObject(self,
                             YSBarButtonItemBlockKey,
                             action,
                             OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    return self;
}

- (instancetype)initWithTitle:(NSString *)title
                        style:(UIBarButtonItemStyle)style
                      handler:(void (^)(id sender))action {
    
    self = [self initWithTitle:title
                         style:style
                        target:self
                        action:@selector(handleAction:)];
    if (!self) return nil;
    objc_setAssociatedObject(self,
                             YSBarButtonItemBlockKey,
                             action,
                             OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    return self;
}

- (void)handleAction:(UIBarButtonItem *)sender {
    
    void (^block)(id) = objc_getAssociatedObject(self, YSBarButtonItemBlockKey);
    if (block) block(sender);
}


@end

