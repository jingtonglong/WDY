//
//  NSDictionary+YSAdd.h
//  BaseProject
//
//  Created by apple on 2017/6/20.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (YSAdd)
/**
 对字典中的键值进行‘类URL’排序
 例如: {
 a : 111,
 e : 222,
 c : 333,
 b : 444
 }
 => a=111&b=444&c=333&e=222
 */
- (NSString *)dicURLSort;

/**
 添加另外的字典的数据
 */
- (NSDictionary *)dicAppendingParams:(NSDictionary *)params;


/**
 转换成Json数组
 */
- (NSString *)convertToJSONString;


// 数组转字符串
+ (NSString *)arrayToJSONString:(NSArray *)array;

// dic 转 string
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;

// 转时间
+ (NSString *)getTimeWithTimeIntervel:(NSTimeInterval)timeInterval formater:(NSString *)formater;

@end
