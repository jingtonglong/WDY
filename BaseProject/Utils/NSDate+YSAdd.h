//
//  NSDate+YSAdd.h
//  BaseProject
//
//  Created by apple on 2017/6/20.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SECOND	(1)
#define MINUTE	(60 * SECOND)
#define HOUR	(60 * MINUTE)
#define DAY		(24 * HOUR)
#define MONTH	(30 * DAY)
#define YEAR	(12 * MONTH)

#pragma mark -

@interface NSDate (YSAdd)

@property (nonatomic, readonly) NSInteger	year;
@property (nonatomic, readonly) NSInteger	month;
@property (nonatomic, readonly) NSInteger	day;
@property (nonatomic, readonly) NSInteger	hour;
@property (nonatomic, readonly) NSInteger	minute;
@property (nonatomic, readonly) NSInteger	second;
@property (nonatomic, readonly) NSInteger	weekday;

- (NSDate*)firstTime;

- (NSDate*)lastTime;

+ (NSDate*)dateWithFormat:(NSString *)format;

- (NSString*)weekdayStr;

+ (NSString*)timeInterval1970_13:(NSDate*)date;

+ (NSString*)timeInterval1970_10:(NSDate*)date;

+ (NSDate*)currentDateCMT;

@end
