//
//  AKWebSSLTwo-Way.m
//  AKWebViewSelfSignedHttps
//
//  Created by 李亚坤 on 2017/6/21.
//  Copyright © 2017年 Kuture. All rights reserved.
//

#import "AKWebSSLTwo-Way.h"

@interface AKWebSSLTwo_Way ()<UIWebViewDelegate,NSURLConnectionDataDelegate,NSURLSessionDelegate>

@property (nonatomic,strong) NSURLConnection *urlConnection;
@property (nonatomic,strong) NSURLRequest *requestW;
@property (nonatomic) SSLAuthenticate authenticated;
@property (nonatomic,copy) NSString *serverStr;
@property (nonatomic,copy) NSString *clientStr;
@property(nonatomic,assign) BOOL isOnceTrue;

@end

@implementation AKWebSSLTwo_Way

+ (instancetype)shareWebViewTwoWay{
    static dispatch_once_t onece = 0;
    static AKWebSSLTwo_Way *webView = nil;
    dispatch_once(&onece, ^(void){
        webView = [[self alloc]init];
    });
    return webView;
}

#pragma mark ***UIWebView 加载方法***
- (void)webViewTwoWayWithLoadRequestWithURL:(NSURL *)url Server_Cer:(NSString *)server Client_P12:(NSString *)client ClientPassword:(NSString *)clientPassword Fram:(CGRect)fram{
    
    _serverStr = server;
    _clientStr = client;
    _clientPasswordStr = clientPassword;
    
    self.frame = fram;
    self.delegate = self;
//    _requestW = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    _requestW=[NSURLRequest requestWithURL:url];
    ;
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:[NSOperationQueue mainQueue]];
//    
//    [[session dataTaskWithRequest:_requestW completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//        
//        NSLog(@"%s",__FUNCTION__);
//        NSLog(@"RESPONSE:%@",response);
//        NSLog(@"ERROR:%@",error);
//        
//        NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
////        NSLog(@"dataString:%@",dataString);
//        
//        [self loadHTMLString:dataString baseURL:nil];
//    }] resume];
    
    [self loadRequest:_requestW];
}

#pragma mark ***UIWebView 代理方法***
/*
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSString *tmp = [request.URL absoluteString];
    
    NSLog(@"request url :%@",tmp);

    if ([request.URL.scheme rangeOfString:@"https"].location != NSNotFound) {
        
        //开启同步的请求去双向认证
        
        if (!_authenticated) {
            
//                        _requestW = request;
            request = _requestW;
            
            NSURLConnection *conn = [NSURLConnection connectionWithRequest:request delegate:self];
            
            [conn start];
            
            [webView stopLoading];
            
            return false;
        }
    }
    
    return YES;
}
*/
#pragma mark ***NSURLConnection代理方法***
- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    
    NSURLCredential * credential;
    
    assert(challenge != nil);
    
    credential = nil;
    
    NSLog(@"----收到质询----");
    
    NSString *authenticationMethod = [[challenge protectionSpace] authenticationMethod];
    
    if ([authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        
        NSLog(@"----服务器验证客户端----");
        
        NSString *host = challenge.protectionSpace.host;
        
        SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
        
        BOOL validDomain = false;
        
        NSMutableArray *polices = [NSMutableArray array];
        
        if (validDomain) {
            
            [polices addObject:(__bridge_transfer id)SecPolicyCreateSSL(true, (__bridge CFStringRef)host)];
            
        }else{
            
            [polices addObject:(__bridge_transfer id)SecPolicyCreateBasicX509()];
            
        }
        
        SecTrustSetPolicies(serverTrust, (__bridge CFArrayRef)polices);
        
        //导入证书
        NSString *path = [[NSBundle mainBundle] pathForResource:_serverStr ofType:@"cer"];
        
        NSData *certData = [NSData dataWithContentsOfFile:path];
        
        NSMutableArray *pinnedCerts = [NSMutableArray arrayWithObjects:(__bridge_transfer id)SecCertificateCreateWithData(NULL, (__bridge CFDataRef)certData), nil];
        
        SecTrustSetAnchorCertificates(serverTrust, (__bridge CFArrayRef)pinnedCerts);
        
        credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        
    } else {
        
        NSLog(@"----客户端验证服务端----");
        
        SecIdentityRef identity = NULL;
        
        SecTrustRef trust = NULL;
        
        NSString *p12 = [[NSBundle mainBundle] pathForResource:_clientStr ofType:@"p12"];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if (![fileManager fileExistsAtPath:p12]) {
            
            NSLog(@"客户端.p12证书 不存在!");
            
        }else{
            
            NSData *pkcs12Data = [NSData dataWithContentsOfFile:p12];
            
            if ([self extractIdentity:&identity andTrust:&trust fromPKCS12Data:pkcs12Data]) {
                
                SecCertificateRef certificate = NULL;
                
                SecIdentityCopyCertificate(identity, &certificate);
                
                const void *certs[] = {certificate};
                
                CFArrayRef certArray = CFArrayCreate(kCFAllocatorDefault, certs, 1, NULL);
                
                credential = [NSURLCredential credentialWithIdentity:identity certificates:(__bridge NSArray *)certArray persistence:NSURLCredentialPersistencePermanent];
//                credential = [NSURLCredential credentialWithIdentity:identity certificates:nil persistence:NSURLCredentialPersistencePermanent];
            }
            
        }
        
    }
    
    [challenge.sender useCredential:credential forAuthenticationChallenge:challenge];
    
}

- (BOOL)extractIdentity:(SecIdentityRef *)outIdentity andTrust:(SecTrustRef *)outTrust fromPKCS12Data:(NSData *)inPKCS12Data {
    
    OSStatus securityErr = errSecSuccess;
    
    //输入客户端证书密码
    NSDictionary *optionsDic = [NSDictionary dictionaryWithObject:_clientPasswordStr  forKey:(__bridge id)kSecImportExportPassphrase];
    
    CFArrayRef items = CFArrayCreate(NULL, 0, 0, NULL);
    
    securityErr = SecPKCS12Import((__bridge CFDataRef)inPKCS12Data, (__bridge CFDictionaryRef)optionsDic, &items);
    
    if (securityErr == errSecSuccess) {
        
        CFDictionaryRef mineIdentAndTrust = CFArrayGetValueAtIndex(items, 0);
        
        const void *tmpIdentity = NULL;
        
        tmpIdentity = CFDictionaryGetValue(mineIdentAndTrust, kSecImportItemIdentity);
        
        *outIdentity = (SecIdentityRef)tmpIdentity;
        
        const void *tmpTrust = NULL;
        
        tmpTrust = CFDictionaryGetValue(mineIdentAndTrust, kSecImportItemTrust);
        
        *outTrust = (SecTrustRef)tmpTrust;
        
    }else{
        
        return false;
        
    }
    
    return true;
    
}


-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)pResponse {
    
    _authenticated = YES;
    
    //webview 重新加载请求。
    
    [self loadRequest:_requestW];
    
    [connection cancel];
    
}

///////////////////////////////////////////////


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if ([[request.URL scheme] isEqualToString:@"https"]) {
        if (!_isOnceTrue) {
            _isOnceTrue=YES;//全局定义一个BOOL类型的属性,只作为每当打开webview时一次性ssl双向验证
            //开启同步的请求去双向认证
            NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            queue.name = @"Mutual Author";
            //NSURLSession *session = [NSURLSession sharedSession];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:conf delegate:self delegateQueue:queue];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if ([error localizedDescription].length==0) {
                    NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    NSLog(@"dataString:%@",dataString);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [self loadHTMLString:dataString baseURL:nil];
                         [self loadRequest:request];
                    });
                    //证书验证正确,则会求H5页面[NSURL URLWithString:@"https://192.168.1.128"]
//
                }
            }];
            [task resume];
            [webView stopLoading];
            return NO;
        }
    }
    return YES;
}


- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
    NSString *method = challenge.protectionSpace.authenticationMethod;
    NSLog(@"challenge auth method:%@",method);
    if ([method isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        NSString *host = challenge.protectionSpace.host;
        NSLog(@"host:%@",host);
        
        SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
        BOOL validDomain = false;
        NSMutableArray *polices = [NSMutableArray array];
        if (validDomain) {
            [polices addObject:(__bridge_transfer id)SecPolicyCreateSSL(true, (__bridge CFStringRef)host)];
        }else{
            [polices addObject:(__bridge_transfer id)SecPolicyCreateBasicX509()];
        }
        SecTrustSetPolicies(serverTrust, (__bridge CFArrayRef)polices);
        //pin mode for certificate
        NSString *path = [[NSBundle mainBundle] pathForResource:_serverStr ofType:@"cer"];
        NSData *certData = [NSData dataWithContentsOfFile:path];
        NSMutableArray *pinnedCerts = [NSMutableArray arrayWithObjects:(__bridge_transfer id)SecCertificateCreateWithData(NULL, (__bridge CFDataRef)certData),nil];
        SecTrustSetAnchorCertificates(serverTrust, (__bridge CFArrayRef)pinnedCerts);
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        return;
    }
    
    //client authentication
    NSString *thePath = [[NSBundle mainBundle] pathForResource:_clientStr ofType:@"p12"];
    NSData *pkcs12Data = [NSData dataWithContentsOfFile:thePath];
    CFDataRef inPKCS12Data = (CFDataRef)CFBridgingRetain(pkcs12Data);
    SecIdentityRef identity;
    
    OSStatus ret = [self extractP12Data:inPKCS12Data toIdentity:&identity];
    if (ret != errSecSuccess) {
        completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge,nil);
        return;
    }
    _isOnceTrue=YES;
    SecCertificateRef certificate = NULL;
    SecIdentityCopyCertificate(identity, &certificate);
    const void *certs[] = {certificate};
    CFArrayRef certArray = CFArrayCreate(kCFAllocatorDefault, certs, 1, NULL);
    NSURLCredential *credential = [NSURLCredential credentialWithIdentity:identity certificates:(NSArray *)CFBridgingRelease(certArray) persistence:NSURLCredentialPersistencePermanent];
    completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
}

- (OSStatus)extractP12Data:(CFDataRef)inP12Data toIdentity:(SecIdentityRef *)identity {
    OSStatus securityErr = errSecSuccess;
    
    CFStringRef pwd = CFSTR("123456");
    const  void *keys[] = {kSecImportExportPassphrase};
    const  void *values[] = {pwd};
    
    CFDictionaryRef options = CFDictionaryCreate(NULL, keys, values, 1, NULL, NULL);
    CFArrayRef items = CFArrayCreate(NULL, 0, 0, NULL);
    securityErr = SecPKCS12Import(inP12Data, options, &items);
    
    if (securityErr == errSecSuccess) {
        CFDictionaryRef ident = CFArrayGetValueAtIndex(items, 0);
        const void *tmpIdent = NULL;
        tmpIdent = CFDictionaryGetValue(ident, kSecImportItemIdentity);
        *identity = (SecIdentityRef)tmpIdent;
    }
    
    if (options) {
        CFRelease(options);
    }
    
    return securityErr;
}
@end
