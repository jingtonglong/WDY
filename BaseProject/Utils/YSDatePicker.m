//
//  YSDatePicker.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "YSDatePicker.h"

@implementation YSDatePicker

-(void)awakeFromNib{
    [super awakeFromNib];
    self.datePicker.maximumDate=[NSDate date];
    self.defaultDate=[NSDate date];
}
-(UIView *)coverView{
    if (!_coverView) {
        _coverView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
        _coverView.backgroundColor=[UIColor blackColor];
        _coverView.alpha=0.4;
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clear)];
        [_coverView addGestureRecognizer:tap];
    }
    return _coverView;
}
-(void)setDefaultDate:(NSDate *)defaultDate{
    _defaultDate=defaultDate;
    [self.datePicker setDate:defaultDate animated:YES];
}
- (IBAction)cancel_clicked:(id)sender {// 取消
    [self clear];
}
- (IBAction)comfirm_clicked:(id)sender {// 确定
    if (self.callback) {
        self.callback([self.datePicker date]);
    }
    [self clear];
  
}
-(void)clear{
    [UIView animateWithDuration:0.25 animations:^{
        self.top=KScreenHeight;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [self.coverView removeFromSuperview];
    }];
    
}
-(void)showDatePiker{
    [kAppWindow addSubview:self.coverView];
    
    self.frame=CGRectMake(0, KScreenHeight, KScreenWidth, 212+iPhoneX_BOTTOM_HEIGHT);
    [kAppWindow addSubview:self];
    [UIView animateWithDuration:0.25 animations:^{
        self.top=KScreenHeight-212-iPhoneX_BOTTOM_HEIGHT;
        
    }];
}
@end
