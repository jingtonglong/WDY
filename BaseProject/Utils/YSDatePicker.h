//
//  YSDatePicker.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YSDatePicker : UIView
@property(nonatomic,strong) UIView *coverView;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property(nonatomic,copy) void(^callback)(NSDate*);
@property(nonatomic,strong) NSDate *defaultDate;
-(void)showDatePiker;
//-(void)hideDatePicker;
@end
