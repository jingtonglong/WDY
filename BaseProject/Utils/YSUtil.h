//
//  YSUtil.h
//  BaseProject
//
//  Created by apple on 2017/6/20.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface YSUtil : NSObject

// 设置不同字体颜色
+ (void)setTextColor:(UILabel *)label Index:(int)index AndColor:(UIColor *)vaColor;

// 添加单击手势
+ (void)addClickEvent:(id)target action:(SEL)action owner:(UIView *)view;

// 设置圆角
+ (void)makeCornerRadius:(CGFloat)cornerRadius view:(UIView *)view;

// 设置边框宽度颜色
+ (void)makeBorderWidth:(CGFloat)borderWidth view:(UIView *)view borderColor:(UIColor *)borderColor;

// 控件边缘灰色线条
+ (void)setFoursides:(UIView *)view Direction:(NSString *)dirction sizeW:(CGFloat)sizew Color:(UIColor *)color;

// 设置UILabel的不同颜色与大小
+ (void)setUILabel:(UILabel *)label Data:(NSString *)string SetData:(NSString *)setstring Color:(UIColor *)color Font:(CGFloat)font Underline:(BOOL)isbool;

+ (CGFloat)ReturnViewFrame:(UIView *)view Direction:(NSString *)string;

/**
 *  动态计算行高
 *
 *  @param text     文本内容
 *  @param width    控件的宽
 *  @param fontSize 字体大小
 *
 *  @return 行高
 */
+ (CGFloat)returnLabelHeightForText:(NSString *)text labelWidth:(CGFloat)width fontSize:(CGFloat)fontSize;

//创建一个返回富文本的方法
+ (NSMutableAttributedString *)changeLabelWithText:(CGFloat)price leftFont:(NSInteger)leftFont rightFont:(NSInteger)rightFont;
+ (NSMutableAttributedString *)changeLabelWithText:(CGFloat)price leftFont:(NSInteger)leftFont rightFont:(NSInteger)rightFont needLine:(BOOL)isNeed;

//身份证号
+ (BOOL)validateIdentityCard:(NSString *)IDCardNumber;

@end
