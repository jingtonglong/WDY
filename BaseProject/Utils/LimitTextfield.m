//
//  LimitTextfield.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/5.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "LimitTextfield.h"

@implementation LimitTextfield
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textfielchange:) name:UITextFieldTextDidChangeNotification object:nil];
        self.maxlimit=10;
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textfielchange:) name:UITextFieldTextDidChangeNotification object:nil];
    self.maxlimit=10;
}
-(void)textfielchange:(NSNotification*)obj{
    UITextField *textField = (UITextField *)obj.object;
    
    
    NSString *toBeString = textField.text;
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    
   
        //         // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position||!selectedRange)
        {
            if (toBeString.length > self.maxlimit)
            {
                NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:self.maxlimit];
                if (rangeIndex.length == 1)
                {
                    textField.text = [toBeString substringToIndex:self.maxlimit];
                }
                else
                {
                    NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, self.maxlimit)];
                    textField.text = [toBeString substringWithRange:rangeRange];
                }
            }
        }
        
    
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
