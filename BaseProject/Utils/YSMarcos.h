//
//  YSMarcos.h
//  BaseProject
//
//  Created by apple on 2017/6/20.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#ifndef YSMarcos_h
#define YSMarcos_h

#pragma mark - ===================屏幕宽高===========================
#define KScreenWidth ([[UIScreen mainScreen]bounds].size.width)
#define KScreenHeight [[UIScreen mainScreen]bounds].size.height
#define kScreen_Bounds [UIScreen mainScreen].bounds
#define Iphone6ScaleWidth KScreenWidth/375.0
#define Iphone6ScaleHeight KScreenHeight/667.0
#define kRealValue (with)((with)*(KScreenWidth/375.0f))

#pragma mark - ===================背景颜色===========================
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r,g,b) RGBA(r,g,b,1.0f)
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]
#define kDefaultBackColor RGB(0, 190, 220)
#define kBACKGROUDcolor RGB(237, 241, 244)
#define kRandomColor RGBA(arc4random_uniform(256), arc4random_uniform(256), arc4random_uniform(256), arc4random_uniform(256))

#define StatusHeight  [UIApplication sharedApplication].statusBarFrame.size.height
#define NavHeight (StatusHeight+44)
#pragma mark - ===================字体===========================
#define BOLDSYSTEMFONT(FONTSIZE)[UIFont boldSystemFontOfSize:FONTSIZE]
#define SYSTEMFONT(FONTSIZE)[UIFont systemFontOfSize:FONTSIZE]
#define FONT(NAME,FONTSIZE)[UIFont fontWithName:(NAME)size:(FONTSIZE)]


#pragma mark - ===================获取系统对象===========================
#define kApplication [UIApplication sharedApplication]
#define kAppWindow [UIApplication sharedApplication].keyWindow
#define kAppDelegate [AppDelegate shareAppDelegate]
#define kRootViewController [UIApplication sharedApplication].delegate.window.rootViewController
#define kUserDefaults [NSUserDefaults standardUserDefaults]
#define kNotificationCenter [NSNotificationCenter defaultCenter]
#define SUCCEESS ([responseObject[@"success"] boolValue])
#define URL(str)  [NSURL URLWithString:str]
#pragma mark - ===================强弱引用===========================
#define kWeakSelf (type)__weak typeof(type)weak##type = type;
#define kStrongSelf (type)__strong typeof(type)type = weak##type;


#pragma mark - ===================数据验证===========================
#define YSStrValid(f)(f!=nil &&[f isKindOfClass:[NSString class]]&& ![f isEqualToString:@""])
#define YSSafeStr(f)(YSStrValid(f)?f:@"")
#define YSHasString(str,eky)([str rangeOfString:key].location!=NSNotFound)
#define YSValidStr(f)YSStrValid(f)
#define YSValidDict(f)(f!=nil &&[f isKindOfClass:[NSDictionary class]])
#define YSValidArray(f)(f!=nil &&[f isKindOfClass:[NSArray class]]&&[f count]>0)
#define YSValidNum(f)(f!=nil &&[f isKindOfClass:[NSNumber class]])
#define YSValidClass(f,cls)(f!=nil &&[f isKindOfClass:[cls class]])
#define YSValidData(f)(f!=nil &&[f isKindOfClass:[NSData class]])



#pragma mark - ===================打印日志===========================
#ifdef DEBUG
#   define YSLog(fmt, ...) NSLog((@"✪✪✪✪✪✪: %s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define YSLog(...)
#endif

#endif /* YSMarcos_h */
