//
//  UIButton+YSButtonAndImage.h
//  BaseProject
//
//  Created by apple on 2017/6/27.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (YSButtonAndImage)

/**
 *  文字左 图片右
 */
- (void)mic_titleImageHorizontalAlignmentWithSpace:(float)space;

/**
 *  图片左 文字右
 */
- (void)mic_imageTitleHorizontalAlignmentWithSpace:(float)space;

/**
 *  文字上 图片下
 */

- (void)mic_titleImageVerticalAlignmentWithSpace:(float)space;

/**
 *  图片上 文字下
 */
- (void)mic_imageTitleVerticalAlignmentWithSpace:(float)space;

@end
