//
//  YSLocation.m
//  BaseProject
//
//  Created by apple on 2017/12/11.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "YSLocation.h"
#import "JXTAlertTools.h"

@implementation YSLocation

-(CLGeocoder*)geocoder{
    
    if (!_geocoder) {
        _geocoder=[[CLGeocoder alloc] init];
    }
    return _geocoder;
}

#pragma mark--获取定位

- (void)getMapLocation:(UIViewController *)viewController{
    //初始化locationManger管理器对象
    CLLocationManager *locationManager=[[CLLocationManager alloc]init];
    self.locationManager=locationManager;
    //判断当前设备定位服务是否打开
    if (![CLLocationManager locationServicesEnabled]) {
        [JXTAlertTools showTipAlertViewWith:viewController title:@"提示" message:@"设备尚未打开定位服务" buttonTitle:@"确定" buttonStyle:JXTAlertActionStyleDefault];
    }
    
    //判断当前设备版本大于iOS8以后的话执行里面的方法
    if ([UIDevice currentDevice].systemVersion.floatValue >=8.0) {
        //持续授权
        //        [locationManager requestAlwaysAuthorization];
        //当用户使用的时候授权
        [locationManager requestWhenInUseAuthorization];
    }
    
    //设置代理
    locationManager.delegate=(id)self;
    //设置定位的精度
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    //设置定位的频率
    CLLocationDistance distance=10;
    //给精度赋值
    locationManager.distanceFilter=distance;
    //开始启动定位
    [locationManager startUpdatingLocation];
}

//当位置发生改变的时候调用(上面我们设置的是10米,也就是当位置发生>10米的时候该代理方法就会调用)
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    //取出第一个位置
    CLLocation *location=[locations firstObject];
    //位置坐标
    CLLocationCoordinate2D coordinate=location.coordinate;
//    NSLog(@"经度：%f,纬度：%f,海拔：%f,航向：%f,速度：%f",coordinate.longitude,coordinate.latitude,location.altitude,location.course,location.speed);
    
    if (self.block) {
        self.block(coordinate);
    }
    
    [_locationManager stopUpdatingLocation];
}

@end
