//
//  CDKContactInfo.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/25.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKContactInfo : NSObject
@property(nonatomic,copy) NSString *easemobId;
@property(nonatomic,copy) NSString *headPicture;
@property(nonatomic,copy) NSString *hospital;
@property(nonatomic,copy) NSString *id;
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *namePinyin;
@property(nonatomic,copy) NSString *role;
@property(nonatomic,copy) NSString *sex;
@property(nonatomic,copy) NSString *orgDisease;
@property(nonatomic,copy) NSString *implantedDate;
@property(nonatomic,assign) BOOL hasInfection;
@property(nonatomic,assign) BOOL followed;

@property(nonatomic,copy) NSString *birthday;
@property(nonatomic,assign) NSInteger treatmentType;
@property(nonatomic,assign) NSInteger age;
@property (nonatomic,assign) BOOL isSeleted;
@property(nonatomic,assign) BOOL noEdit;
@end
//"id": 0,
//"easemobId": "string",
//"headPicture": "string",
//"sex": 0,
//"name": "string",
//"namePinyin": "string",
//"orgDisease": "string",
//"implantedDate": "2018-06-25T14:36:01.468Z",
//"hasInfection": true,
//"followed": true,
//"birthday": "2018-06-25T14:36:01.468Z",
//"treatmentType": 0,
//"age": 0
