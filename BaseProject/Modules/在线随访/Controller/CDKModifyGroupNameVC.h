//
//  CDKModifyGroupNameVC.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/8/23.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKModifyGroupNameVC : UIViewController
@property(nonatomic,strong) EMGroup *group;
@property(nonatomic,copy) void(^callback)(NSString*);
@property(nonatomic,copy) NSString *groupid;

@end
