//
//  CDKPatientProfileVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/5.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKPatientProfileVC.h"
#import "ModiryRemarkInfoVC.h"
#import "CDKPationInfoModel.h"
#import "CDKPatientManagerVC.h"
#import "CDKChangeRemarkVC.h"
@interface CDKPatientProfileVC ()
@property (weak, nonatomic) IBOutlet UILabel *ageLab;

@property (weak, nonatomic) IBOutlet UILabel *sexLab;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *identifierLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UILabel *originDeseaseLab;
@property (weak, nonatomic) IBOutlet UILabel *educationLab;

@property (weak, nonatomic) IBOutlet UILabel *baoxiaoAwayLab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phonewidth;
@property (weak, nonatomic) IBOutlet UILabel *followDesease;
@property (weak, nonatomic) IBOutlet UILabel *treatMethodLab;
@property (weak, nonatomic) IBOutlet UILabel *remarkLab;


@property (weak, nonatomic) IBOutlet UIImageView *headPotrait;
@property(nonatomic,strong) CDKPationInfoModel *pationInfoModel;

@end

@implementation CDKPatientProfileVC
- (instancetype)init
{
    self = [super init];
    if (self) {
        UIStoryboard *st=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self=[st instantiateViewControllerWithIdentifier:@"CDKPatientProfileVC"];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"详细资料";
    self.phoneBtn.layer.cornerRadius=11;
    self.phoneBtn.layer.masksToBounds=YES;
        self.phoneBtn.layer.borderWidth=1;
    if (KScreenWidth>320) {
        self.phonewidth.constant=140;
    } self.phoneBtn.layer.borderColor=RGB(167,237,197).CGColor;
//    if (self.isFromChat) {
//        [self loadUserData];
//    }else{
        [self showLoadingHud:nil];
        [self loadPationtInfo];
//    }
   
}
-(void)loadUserData{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/app/chatgroup/GetUserInfoByEasemobId";
    NSDictionary *para=@{
                         @"easemobId": self.easemobid
                         };
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        //        {
        //            headPicture = "";
        //            id = 765;
        //            name = "\U8d1d\U5c14";
        //        };
        if(SUCCEESS){
            if ([responseObject[@"result"] isEqual:[NSNull null]]) {
                return ;
            }
            NSDictionary *dic=responseObject[@"result"];
            self.pationtid=dic[@"id"];
            [self loadPationtInfo];
//            [[YSFMDBTool shareInstance] insertData:@{@"name":dic[@"name"],@"headPicture":headPicture?headPicture:@"",@"easemobId":model.message.from}];
            
        }else{
            [self dismissHud];
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadPationtInfo{
    
    NSString *api=@"/api/services/ckd/patient/QueryPatientById";
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"PatientId":self.pationtid} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            self.pationInfoModel=[CDKPationInfoModel modelWithJSON:responseObject[@"result"]];
//            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        [self updateUI];
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        [self updateUI];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)updateUI{
    
//    if (self.pationInfoModel) {
        [self.headPotrait sd_setImageWithURL:URL(self.pationInfoModel.headPicture) placeholderImage:[UIImage imageNamed:@"默认头像"]];
        self.nameLab.text=self.pationInfoModel.name;
        self.sexLab.text=self.pationInfoModel.sex;
    if (self.pationInfoModel.age) {
        self.ageLab.text=[NSString stringWithFormat:@"%@岁",self.pationInfoModel.age];
    }
    
        [self.phoneBtn setTitle:[NSString stringWithFormat:@"%@",self.pationInfoModel.mobile] forState:UIControlStateNormal];
    if (self.pationInfoModel.cardNo.length == 18) {
        NSString *str1 = [self.pationInfoModel.cardNo substringToIndex:3];
        NSString *str2 = [self.pationInfoModel.cardNo substringFromIndex:14];
        NSString *card = [NSString stringWithFormat:@"%@***********%@",str1,str2];
        self.identifierLab.text = card;
    }else if (self.pationInfoModel.cardNo.length == 15){
        NSString *str1 = [self.pationInfoModel.cardNo substringToIndex:3];
        NSString *str2 = [self.pationInfoModel.cardNo substringFromIndex:11];
        NSString *card = [NSString stringWithFormat:@"%@*******%@",str1,str2];
        self.identifierLab.text = card;
    }else{
        self.identifierLab.text = @"";
    }
        self.addressLab.text=self.pationInfoModel.fullAddress;
        self.baoxiaoAwayLab.text=self.pationInfoModel.expenseType;
        self.educationLab.text=self.pationInfoModel.education;
        self.originDeseaseLab.text=self.pationInfoModel.orgDisease;
        self.followDesease.text=self.pationInfoModel.withDisease;
        self.treatMethodLab.text=self.pationInfoModel.mainTreatment;
        self.remarkLab.text=self.pationInfoModel.remarkCkd;
    [self.phoneBtn addTarget:self action:@selector(callphone) forControlEvents:UIControlEventTouchUpInside];
//    }
}
-(void)callphone{
    if (self.pationInfoModel) {
        NSString *url=[NSString stringWithFormat:@"tel:%@",self.pationInfoModel.mobile];
        if ([[UIApplication sharedApplication] canOpenURL:URL(url)]) {
//            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"拨打用户电话" message:nil preferredStyle:(UIAlertControllerStyleAlert)];
//            UIAlertAction *comfirm=[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                [[UIApplication sharedApplication] openURL:URL(url)];
//            }];
            
            
//            UIAlertAction *cacncel=[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
//
//            }];
//
//            [alert addAction:cacncel];
//            [alert addAction:comfirm];
//            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)lookDatas:(id)sender {// 查看数据
    if (!self.pationtid) {
        [self loadUserData];
        return;
    }
    CDKPatientManagerVC *vc=[[CDKPatientManagerVC alloc] init];
    vc.isPOPToRoot=YES;
    CDKPationtListModel *model= [[CDKPationtListModel alloc] init];
    model.id=self.pationtid;
    vc.pationtModel=model;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)sendMessage:(id)sender {// 发送消息
    if (self.isFromChat) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
        return;
    }else{
        
        ChatViewController *vc=[[ChatViewController alloc] initWithConversationChatter:self.model.easemobId conversationType:(EMConversationTypeChat)];
        vc.title=self.model.name;
        vc.headPicture=self.model.headPicture;
        vc.nickName=self.model.name;
        [self.navigationController pushViewController:vc animated:YES];
    }
    

    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==8) {
        if (self.pationInfoModel) {
            CDKChangeRemarkVC *remark=[[CDKChangeRemarkVC alloc] init];

            remark.model=self.pationInfoModel;
            remark.callback = ^{
                self.remarkLab.text=self.pationInfoModel.remarkCkd;
            };
            [self.navigationController pushViewController:remark animated:YES];
        }
        
       
    }
    
    
}

//代理方法
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
        
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
        
    }
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
        
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
