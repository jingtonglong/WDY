//
//  CDKMycollegueVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/6.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKMycollegueVC.h"
#import "AdressCell.h"
#import "AdressSection.h"
#import "CDKPatientProfileVC.h"
#import "CDKContactInfo.h"
@interface CDKMycollegueVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *dataArray;
@property(nonatomic,strong) NSMutableArray *sectionTitles;
@property(nonatomic,strong) MyLinearLayout *sectionView;
//@property(nonatomic,strong) NSArray *datas;

@end

@implementation CDKMycollegueVC

-(UITableView *)tableView{
    if (!_tableView) {
      
        _tableView=[[UITableView alloc] init];
        _tableView.rowHeight=60;
        [_tableView registerNib:[UINib nibWithNibName:@"AdressCell" bundle:nil] forCellReuseIdentifier:@"AdressCell"];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.backgroundColor=RGB(247,247,247);
        //        self.tableView.contentInset=UIEdgeInsetsMake(10, 0, 0, 0);
        
        _tableView.tableFooterView=[UIView new];
        _tableView.sectionIndexColor=RGB(97,97,97);
        _tableView.sectionIndexBackgroundColor=[UIColor clearColor];

        [_tableView registerClass:[AdressSection class] forHeaderFooterViewReuseIdentifier:@"section"];
    }
    return _tableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //
    self.title=@"我的同事";
    // Do any additional setup after loading the view.
    self.dataArray=[NSMutableArray array];
    self.sectionTitles=[NSMutableArray array];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self loadData];
}
-(void)loadData{
    [self showLoadingHud:nil];//查询医护人员的同事
    NSString *api=@"/api/services/app/doctor/QueryColleaguesV2";
    NSDictionary *para=@{@"doctorId":[UserInfoManager shareInstance].userInfo.id,@"roleType":@"2"};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            NSArray *arr=[NSArray modelArrayWithClass:[CDKContactInfo class] json:responseObject[@"result"]];
            [self _sortDataArray:arr];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];

        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

#pragma tableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sectionTitles.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    NSArray *arr = self.dataArray[section];
    return arr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AdressCell *cell =[tableView dequeueReusableCellWithIdentifier:@"AdressCell" forIndexPath:indexPath];
   
        
        CDKContactInfo *model=self.dataArray[indexPath.section][indexPath.row];
        
        cell.arrow.hidden=YES;
    [cell.headPotrait sd_setImageWithURL:IMG_URL(model.headPicture) placeholderImage:[UIImage imageNamed:@"默认头像"]];
//    .image=[UIImage imageNamed:@"logo"];
        cell.titleLab.text=model.name;
    
    
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
   
    AdressSection *view=[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"section"];
    if (!view) {
        view=[[AdressSection alloc] initWithReuseIdentifier:@"section"];
        
    }
    view.titleLab.text=self.sectionTitles[section];
    
    return view;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 16;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
 
    CDKContactInfo *model=self.dataArray[indexPath.section][indexPath.row];
//    [self showMessageHud:@"点击了"];
    ChatViewController *vc=[[ChatViewController alloc] initWithConversationChatter:model.easemobId conversationType:(EMConversationTypeChat)];
    vc.title=model.name;
    vc.isCollegue=YES;
    vc.headPicture=model.headPicture;
    vc.nickName=model.name;
    [self.navigationController pushViewController:vc animated:YES];
    
    
}



- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return self.sectionTitles;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete|UITableViewCellEditingStyleInsert;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)_sortDataArray:(NSArray *)buddyList
{
    [self.dataArray removeAllObjects];
    [self.sectionTitles removeAllObjects];
    NSMutableArray *contactsSource = [NSMutableArray arrayWithArray:buddyList];
    
    //从获取的数据中剔除黑名单中的好友
    //    NSArray *blockList = [[EMClient sharedClient].contactManager getBlackList];
    //    for (NSString *buddy in buddyList) {
    //        if (![blockList containsObject:buddy]) {
    //            [contactsSource addObject:buddy];
    //        }
    //    }
    
    //建立索引的核心, 返回27，是a－z和＃
    UILocalizedIndexedCollation *indexCollation = [UILocalizedIndexedCollation currentCollation];
    [self.sectionTitles addObjectsFromArray:[indexCollation sectionTitles]];
    
    NSInteger highSection = [self.sectionTitles count];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithCapacity:highSection];
    for (int i = 0; i < highSection; i++) {
        NSMutableArray *sectionArray = [NSMutableArray arrayWithCapacity:1];
        [sortedArray addObject:sectionArray];
    }
    
    //按首字母分组
    for (CDKContactInfo *buddy in contactsSource) {
            NSString *firstLetter = [EaseChineseToPinyin pinyinFromChineseString:buddy.name];
            NSInteger section;
            if (firstLetter.length > 0) {
                section = [indexCollation sectionForObject:[firstLetter substringToIndex:1] collationStringSelector:@selector(uppercaseString)];
            } else {
                section = [sortedArray count] - 1;
            }
            
            NSMutableArray *array = [sortedArray objectAtIndex:section];
            [array addObject:buddy];
        
    }
    
    //每个section内的数组排序
    for (int i = 0; i < [sortedArray count]; i++) {
        NSArray *array = [[sortedArray objectAtIndex:i] sortedArrayUsingComparator:^NSComparisonResult(CDKContactInfo *obj1, CDKContactInfo *obj2) {
            NSString *firstLetter1 = [EaseChineseToPinyin pinyinFromChineseString:obj1.name];
            firstLetter1 = [[firstLetter1 substringToIndex:1] uppercaseString];
            
            NSString *firstLetter2 = [EaseChineseToPinyin pinyinFromChineseString:obj2.name];
            firstLetter2 = [[firstLetter2 substringToIndex:1] uppercaseString];
            
            return [firstLetter1 caseInsensitiveCompare:firstLetter2];
        }];
        
        
        [sortedArray replaceObjectAtIndex:i withObject:[NSMutableArray arrayWithArray:array]];
    }
    
    //去掉空的section
    for (NSInteger i = [sortedArray count] - 1; i >= 0; i--) {
        NSArray *array = [sortedArray objectAtIndex:i];
        if ([array count] == 0) {
            [sortedArray removeObjectAtIndex:i];
            [self.sectionTitles removeObjectAtIndex:i];
        }
    }
    [self.dataArray addObjectsFromArray:sortedArray];
    [self.tableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
