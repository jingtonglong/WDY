//
//  CDKModifyGroupNameVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/8/23.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKModifyGroupNameVC.h"
#import "AppDelegate.h"
@interface CDKModifyGroupNameVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textfile;

@end

@implementation CDKModifyGroupNameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"修改群名称";
    // Do any additional setup after loading the view from its nib.
    WeakSelf(weak);
    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithTitle:@"确定" style:(UIBarButtonItemStylePlain) handler:^(id  _Nonnull sender) {
        [weak save];
    }];
    self.textfile.text=self.group.subject;
    self.navigationItem.rightBarButtonItem=item;
}
-(void)save{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/app/chatgroup/Update";
    NSDictionary *para=@{
                         @"id": self.groupid,
                         @"groupName": self.textfile.text
                         };
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
//        [self dismissHud];
        if (SUCCEESS) {
            
            EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:self.group.groupId type:EMConversationTypeGroupChat createIfNotExist:NO];
            EMError *error = nil;
            [[EMClient sharedClient].groupManager changeGroupSubject:self.textfile.text forGroup:self.group.groupId error:&error];
            if (!error) {
                [self dismissHud];
                if ([_group.groupId isEqualToString:conversation.conversationId]) {
                    NSMutableDictionary *ext = [NSMutableDictionary dictionaryWithDictionary:conversation.ext];
                    [ext setObject:self.group.subject forKey:@"subject"];
                    //            [ext setObject:[NSNumber numberWithBool:_group.isPublic] forKey:@"isPublic"];
                    conversation.ext = ext;
                    AppDelegate *delegate=(AppDelegate*)kApplication.delegate;
                    ConversationListController *list=delegate.conversationListVC;
                    [list refreshDataSource];
                }
                [self.textfile resignFirstResponder];
                if(self.callback){
                    self.callback(self.textfile.text);
                }
                [self.navigationController popViewControllerAnimated:YES];


            }else{
                 [self dismissHud];
                [self showMessageHud:@"操作失败"];
            }

        }else{
            [self dismissHud];
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
   
    
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
