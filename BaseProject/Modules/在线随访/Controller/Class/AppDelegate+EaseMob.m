/************************************************************
 *  * Hyphenate CONFIDENTIAL
 * __________________
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Hyphenate Inc.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Hyphenate Inc.
 */

#import "AppDelegate+EaseMob.h"
#import "AppDelegate+Parse.h"
#import "ApplyViewController.h"
#import "EMNavigationController.h"
#import "LoginViewController.h"
#import "ChatDemoHelper.h"
#import "EMDingMessageHelper.h"
#import "YSNavigationController.h"
#import "MBProgressHUD.h"
#import "YSLoginViewController.h"
/**
 *  本类中做了EaseMob初始化和推送等操作
 */
//#define  kDefaultInterval = 3.0
//两次提示的默认间隔
static const CGFloat kDefaultPlaySoundInterval = 3.0;
static NSString *kMessageType = @"MessageType";
static NSString *kConversationChatter = @"ConversationChatter";
static NSString *kGroupName = @"GroupName";
@implementation AppDelegate (EaseMob)

- (void)easemobApplication:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
                    appkey:(NSString *)appkey
              apnsCertName:(NSString *)apnsCertName
               otherConfig:(NSDictionary *)otherConfig
{
    //注册登录状态监听
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginStateChange:)
                                                 name:KNOTIFICATION_LOGINCHANGE
                                               object:nil];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL isHttpsOnly = [ud boolForKey:@"identifier_httpsonly"];
    
    [[EaseSDKHelper shareHelper] hyphenateApplication:application
                    didFinishLaunchingWithOptions:launchOptions
                                           appkey:appkey
                                     apnsCertName:apnsCertName
                                      otherConfig:@{@"httpsOnly":[NSNumber numberWithBool:isHttpsOnly], kSDKConfigEnableConsoleLogger:[NSNumber numberWithBool:YES],@"easeSandBox":[NSNumber numberWithBool:0]}];
    
//    [ChatDemoHelper shareHelper];
    [[EMClient sharedClient] addDelegate:self delegateQueue:nil];
    
    [[EMClient sharedClient].groupManager addDelegate:self delegateQueue:nil];
    [[EMClient sharedClient].contactManager addDelegate:self delegateQueue:nil];
    [[EMClient sharedClient].roomManager addDelegate:self delegateQueue:nil];
    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
    
    BOOL isAutoLogin = [EMClient sharedClient].isAutoLogin;
    if (isAutoLogin){
        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@YES];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
    }
}

- (void)easemobApplication:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[EaseSDKHelper shareHelper] hyphenateApplication:application didReceiveRemoteNotification:userInfo];
}

#pragma mark - App Delegate



// 注册deviceToken失败，此处失败，与环信SDK无关，一般是您的环境配置或者证书配置有误
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"apns.failToRegisterApns", Fail to register apns)
                                                    message:error.description
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - login changed

- (void)loginStateChange:(NSNotification *)notification
{
    BOOL loginSuccess = [notification.object boolValue];
//    EMNavigationController *navigationController = nil;
    if (loginSuccess) {//登陆成功加载主窗口控制器
        //加载申请通知的数据
//        [[ApplyViewController shareController] loadDataSourceFromLocalDB];
//        if (self.mainController == nil) {
//            self.mainController = [[YSTabBarController alloc] init];
////            navigationController = [[EMNavigationController alloc] initWithRootViewController:self.mainController];
//        }else{
////            navigationController  = (EMNavigationController *)self.mainController.navigationController;
//        }
        // 环信UIdemo中有用到Parse，您的项目中不需要添加，可忽略此处
//        [self initParse];
        
//        [ChatDemoHelper shareHelper].mainVC = self.mainController;
        
//        [[ChatDemoHelper shareHelper] asyncGroupFromServer];
//        [[ChatDemoHelper shareHelper] asyncConversationFromDB];
//        [[ChatDemoHelper shareHelper] asyncPushOptions];
        
        
        CATransition *animation = [CATransition animation];
        
        [animation setDuration:0.6];//设置动画时间
        animation.type = kCATransitionFade;//设置动画类型
        [[UIApplication sharedApplication].keyWindow.layer addAnimation:animation forKey:nil];
        self.window.rootViewController = [[YSTabBarController alloc] init];
        [self asyncConversationFromDB];

    }
    else{//登陆失败加载登陆页面控制器
//        if (self.mainController) {
//            [self.mainController.navigationController popToRootViewControllerAnimated:NO];
//        }
//        self.mainController = nil;
//        [ChatDemoHelper shareHelper].mainVC = nil;
        self.conversationListVC=nil;
        YSLoginViewController *loginController = [[YSLoginViewController alloc] init];
//        navigationController = [[EMNavigationController alloc] initWithRootViewController:loginController];
//        [self clearParse];
        CATransition *animation = [CATransition animation];
        
        [animation setDuration:0.6];//设置动画时间
        
        animation.type = kCATransitionFade;//设置动画类型
        
        [[UIApplication sharedApplication].keyWindow.layer addAnimation:animation forKey:nil];
        self.window.rootViewController = [[YSNavigationController alloc] initWithRootViewController:loginController];
    }
    
  
    
    
}

#pragma mark - EMPushManagerDelegateDevice

// 打印收到的apns信息
-(void)didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSError *parseError = nil;
    NSData  *jsonData = [NSJSONSerialization dataWithJSONObject:userInfo
                                                        options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *str =  [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"apns.content", @"Apns content")
                                                    message:str
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
                                          otherButtonTitles:nil];
    [alert show];
    
}
#pragma EMClientDelegate
// 自动登录失败
-(void)autoLoginDidCompleteWithError:(EMError *)aError{
    YSLog(@"islogin----%d",[EMClient sharedClient].isLoggedIn);
    if (aError) {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"自动登录失败，请重新登录" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//        alertView.tag = 100;
//        [alertView show];
        [JXTAlertTools showAlertWith:kAppWindow.rootViewController title:nil message:@"自动登录失败，请重新登录" callbackBlock:^(NSInteger btnIndex) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
        } cancelButtonTitle:@"确定" destructiveButtonTitle:nil otherButtonTitles:nil, nil];
        
    } else if([[EMClient sharedClient] isConnected]){
        UIView *view = kRootViewController.view;
        if (self.conversationListVC==nil) {
            self.conversationListVC=[[ConversationListController alloc] init];
        }
        [MBProgressHUD showHUDAddedTo:view animated:YES];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            BOOL flag = [[EMClient sharedClient] migrateDatabaseToLatestSDK];
            if (flag) {
////                [self asyncGroupFromServer];
//                [self asyncConversationFromDB];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:view animated:YES];
            });
        });
    }
    
}
- (void)asyncConversationFromDB
{
    __weak typeof(self) weakself = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray *array = [[EMClient sharedClient].chatManager getAllConversations];
        [array enumerateObjectsUsingBlock:^(EMConversation *conversation, NSUInteger idx, BOOL *stop){
            if(conversation.latestMessage == nil){
                [[EMClient sharedClient].chatManager deleteConversation:conversation.conversationId isDeleteMessages:NO completion:nil];
            }
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (weakself.conversationListVC) {
                [weakself.conversationListVC refreshDataSource];
            }
            
//            if (weakself.mainVC) {
//                [weakself.mainVC setupUnreadMessageCount];
//            }
            [[NSNotificationCenter defaultCenter] postNotificationName:UNREADMESSAGE object:nil];
        });
    });
}
-(void)userAccountDidLoginFromOtherDevice{
    [JXTAlertTools showAlertWith:kAppWindow.rootViewController title:nil message:@"账号在其他设备登录，请重新登录" callbackBlock:^(NSInteger btnIndex) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
    } cancelButtonTitle:@"确定" destructiveButtonTitle:nil otherButtonTitles:nil, nil];
}

#pragma mark - EMChatManagerDelegate

- (void)didUpdateConversationList:(NSArray *)aConversationList
{
//    if (self.mainVC) {
//        [_mainVC setupUnreadMessageCount];
//    }
    [[NSNotificationCenter defaultCenter] postNotificationName:UNREADMESSAGE object:nil];

    if (self.conversationListVC) {
        [self.conversationListVC refreshDataSource];
    }
}

- (void)messagesDidReceive:(NSArray *)aMessages
{
    BOOL isRefreshCons = YES;
    for(EMMessage *message in aMessages){
        if ([EMDingMessageHelper isDingMessage:message]) {
            EMMessage *ack = [[EMDingMessageHelper sharedHelper] createDingAckForMessage:message];
            if (ack) {
                [[EMClient sharedClient].chatManager sendMessage:ack progress:nil completion:nil];
            }
        }
        
        BOOL needShowNotification = (message.chatType != EMChatTypeChat) ? [self _needShowNotification:message.conversationId] : YES;
        
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        if (needShowNotification) {
#if !TARGET_IPHONE_SIMULATOR
            switch (state) {
                case UIApplicationStateActive:
                    [self playSoundAndVibration];
                    break;
                case UIApplicationStateInactive:
                    [self playSoundAndVibration];
                    break;
                case UIApplicationStateBackground:
                    [self showNotificationWithMessage:message];
                    break;
                default:
                    break;
            }
#endif
        }
        
        if (self.chatVC == nil) {
//            _chatVC = [self _getCurrentChatView];
        }
        BOOL isChatting = NO;
        if (self.chatVC) {
            isChatting = [message.conversationId isEqualToString:self.chatVC.conversation.conversationId];
        }
        if (self.chatVC == nil || !isChatting || state == UIApplicationStateBackground) {
            [self _handleReceivedAtMessage:message];
            
            if (self.conversationListVC) {
                [self.conversationListVC refresh];
            }
            
//            if (self.mainVC) {
//                [_mainVC setupUnreadMessageCount];
//            }
            [[NSNotificationCenter defaultCenter] postNotificationName:UNREADMESSAGE object:nil];

            return;
        }
        
        if (isChatting) {
            isRefreshCons = NO;
        }
    }
    
    if (isRefreshCons) {
        if (self.conversationListVC) {
            [self.conversationListVC refresh];
        }
        
//        if (self.mainVC) {
//            [_mainVC setupUnreadMessageCount];
//        }
        [[NSNotificationCenter defaultCenter] postNotificationName:UNREADMESSAGE object:nil];

    }
}

- (void)messagesDidRecall:(NSArray *)aMessages
{
    for (EMMessage *msg in aMessages) {
        NSString *text;
        if ([msg.from isEqualToString:[EMClient sharedClient].currentUsername]) {
            text = [NSString stringWithFormat:NSLocalizedString(@"message.recall", @"You recall a message")];
        } else {
            text = [NSString stringWithFormat:NSLocalizedString(@"message.recallByOthers", @"%@ recall a message"),msg.from];
        }
        EMMessage *message = [EaseSDKHelper getTextMessage:text to:msg.conversationId messageType:msg.chatType messageExt:@{@"em_recall":@(YES)}];
        message.isRead = YES;
        [message setTimestamp:msg.timestamp];
        [message setLocalTime:msg.localTime];
        EMConversationType conversatinType = EMConversationTypeChat;
        switch (msg.chatType) {
            case EMChatTypeChat:
                conversatinType = EMConversationTypeChat;
                break;
            case EMChatTypeGroupChat:
                conversatinType = EMConversationTypeGroupChat;
                break;
            case EMChatTypeChatRoom:
                conversatinType = EMConversationTypeChatRoom;
                break;
            default:
                break;
        }
        EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:msg.conversationId type:conversatinType createIfNotExist:NO];
        NSDictionary *dict = msg.ext;
        if (dict && [dict objectForKey:@"em_at_list"]) {
            NSArray *atList = [dict objectForKey:@"em_at_list"];
            if ([atList containsObject:[EMClient sharedClient].currentUsername]) {
                NSMutableDictionary *conversationExt = conversation.ext ? [conversation.ext mutableCopy] : [NSMutableDictionary dictionary];
                [conversationExt removeObjectForKey:kHaveUnreadAtMessage];
                conversation.ext = conversationExt;
            }
        }
        [conversation insertMessage:message error:nil];
    }
    
    if (self.conversationListVC) {
        [self.conversationListVC refresh];
    }
    
//    if (self.mainVC) {
//        [_mainVC setupUnreadMessageCount];
//    }
    [[NSNotificationCenter defaultCenter] postNotificationName:UNREADMESSAGE object:nil];

}

- (void)cmdMessagesDidReceive:(NSArray *)aCmdMessages
{
    for (EMMessage *message in aCmdMessages) {
        if ([EMDingMessageHelper isDingMessageAck:message]) {
            NSString *msgId = [[EMDingMessageHelper sharedHelper] addDingMessageAck:message];
            if (self.chatVC) {
                [self.chatVC reloadDingCellWithAckMessageId:msgId];
            }
        }
    }
}

#pragma others

- (void)playSoundAndVibration{
    NSTimeInterval timeInterval = [[NSDate date]
                                   timeIntervalSinceDate:self.lastPlaySoundDate];
    if (timeInterval < 3.0) {
        //如果距离上次响铃和震动时间太短, 则跳过响铃
        NSLog(@"skip ringing & vibration %@, %@", [NSDate date], self.lastPlaySoundDate);
        return;
    }
    
    //保存最后一次响铃时间
    self.lastPlaySoundDate = [NSDate date];
    
    // 收到消息时，播放音频
    [[EMCDDeviceManager sharedInstance] playNewMessageSound];
    // 收到消息时，震动
    [[EMCDDeviceManager sharedInstance] playVibration];
}

- (void)showNotificationWithMessage:(EMMessage *)message
{
    EMPushOptions *options = [[EMClient sharedClient] pushOptions];
    NSString *alertBody = nil;
    if (options.displayStyle == EMPushDisplayStyleMessageSummary) {
        EMMessageBody *messageBody = message.body;
        NSString *messageStr = nil;
        switch (messageBody.type) {
            case EMMessageBodyTypeText:
            {
                messageStr = ((EMTextMessageBody *)messageBody).text;
            }
                break;
            case EMMessageBodyTypeImage:
            {
                messageStr = NSLocalizedString(@"message.image", @"Image");
            }
                break;
            case EMMessageBodyTypeLocation:
            {
                messageStr = NSLocalizedString(@"message.location", @"Location");
            }
                break;
            case EMMessageBodyTypeVoice:
            {
                messageStr = NSLocalizedString(@"message.voice", @"Voice");
            }
                break;
            case EMMessageBodyTypeVideo:{
                messageStr = NSLocalizedString(@"message.video", @"Video");
            }
                break;
            default:
                break;
        }
        
        do {
//            NSString *title = [[UserProfileManager sharedInstance] getNickNameWithUsername:message.from];
            NSString *title = message.from;
            if (message.chatType == EMChatTypeGroupChat) {
                NSDictionary *ext = message.ext;
                if (ext && ext[kGroupMessageAtList]) {
                    id target = ext[kGroupMessageAtList];
                    if ([target isKindOfClass:[NSString class]]) {
                        if ([kGroupMessageAtAll compare:target options:NSCaseInsensitiveSearch] == NSOrderedSame) {
                            alertBody = [NSString stringWithFormat:@"%@%@", title, NSLocalizedString(@"group.atPushTitle", @" @ me in the group")];
                            break;
                        }
                    }
                    else if ([target isKindOfClass:[NSArray class]]) {
                        NSArray *atTargets = (NSArray*)target;
                        if ([atTargets containsObject:[EMClient sharedClient].currentUsername]) {
                            alertBody = [NSString stringWithFormat:@"%@%@", title, NSLocalizedString(@"group.atPushTitle", @" @ me in the group")];
                            break;
                        }
                    }
                }
                NSArray *groupArray = [[EMClient sharedClient].groupManager getJoinedGroups];
                for (EMGroup *group in groupArray) {
                    if ([group.groupId isEqualToString:message.conversationId]) {
                        title = [NSString stringWithFormat:@"%@(%@)", message.from, group.subject];
                        break;
                    }
                }
            }
            else if (message.chatType == EMChatTypeChatRoom)
            {
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                NSString *key = [NSString stringWithFormat:@"OnceJoinedChatrooms_%@", [[EMClient sharedClient] currentUsername]];
                NSMutableDictionary *chatrooms = [NSMutableDictionary dictionaryWithDictionary:[ud objectForKey:key]];
                NSString *chatroomName = [chatrooms objectForKey:message.conversationId];
                if (chatroomName)
                {
                    title = [NSString stringWithFormat:@"%@(%@)", message.from, chatroomName];
                }
            }
            
            alertBody = [NSString stringWithFormat:@"%@:%@", title, messageStr];
        } while (0);
    }
    else{
        alertBody = NSLocalizedString(@"receiveMessage", @"you have a new message");
    }
    
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSinceDate:self.lastPlaySoundDate];
    BOOL playSound = NO;
    if (!self.lastPlaySoundDate || timeInterval >= kDefaultPlaySoundInterval) {
        self.lastPlaySoundDate = [NSDate date];
        playSound = YES;
    }
    
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
    [userInfo setObject:[NSNumber numberWithInt:message.chatType] forKey:kMessageType];
    [userInfo setObject:message.conversationId forKey:kConversationChatter];
    
    //发送本地推送
    if (NSClassFromString(@"UNUserNotificationCenter")) {
        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:0.01 repeats:NO];
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        if (playSound) {
            content.sound = [UNNotificationSound defaultSound];
        }
        content.body =alertBody;
        content.userInfo = userInfo;
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:message.messageId content:content trigger:trigger];
        [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:nil];
    }
    else {
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = [NSDate date]; //触发通知的时间
        notification.alertBody = alertBody;
        notification.alertAction = NSLocalizedString(@"open", @"Open");
        notification.timeZone = [NSTimeZone defaultTimeZone];
        if (playSound) {
            notification.soundName = UILocalNotificationDefaultSoundName;
        }
        notification.userInfo = userInfo;
        
        //发送通知
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
}

#pragma mark - private
- (BOOL)_needShowNotification:(NSString *)fromChatter
{
    BOOL ret = YES;
    NSArray *igGroupIds = [[EMClient sharedClient].groupManager getGroupsWithoutPushNotification:nil];
    for (NSString *str in igGroupIds) {
        if ([str isEqualToString:fromChatter]) {
            ret = NO;
            break;
        }
    }
    return ret;
}

- (void)_handleReceivedAtMessage:(EMMessage*)aMessage
{
    if (aMessage.chatType != EMChatTypeGroupChat || aMessage.direction != EMMessageDirectionReceive) {
        return;
    }
    
    NSString *loginUser = [EMClient sharedClient].currentUsername;
    NSDictionary *ext = aMessage.ext;
    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:aMessage.conversationId type:EMConversationTypeGroupChat createIfNotExist:NO];
    if (loginUser && conversation && ext && [ext objectForKey:kGroupMessageAtList]) {
        id target = [ext objectForKey:kGroupMessageAtList];
        if ([target isKindOfClass:[NSString class]] && [(NSString*)target compare:kGroupMessageAtAll options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            NSNumber *atAll = conversation.ext[kHaveUnreadAtMessage];
            if ([atAll intValue] != kAtAllMessage) {
                NSMutableDictionary *conversationExt = conversation.ext ? [conversation.ext mutableCopy] : [NSMutableDictionary dictionary];
                [conversationExt removeObjectForKey:kHaveUnreadAtMessage];
                [conversationExt setObject:@kAtAllMessage forKey:kHaveUnreadAtMessage];
                conversation.ext = conversationExt;
            }
        }
        else if ([target isKindOfClass:[NSArray class]]) {
            if ([target containsObject:loginUser]) {
                if (conversation.ext[kHaveUnreadAtMessage] == nil) {
                    NSMutableDictionary *conversationExt = conversation.ext ? [conversation.ext mutableCopy] : [NSMutableDictionary dictionary];
                    [conversationExt setObject:@kAtYouMessage forKey:kHaveUnreadAtMessage];
                    conversation.ext = conversationExt;
                }
            }
        }
    }
}


#pragma mark - EMGroupManagerDelegate

- (void)didReceiveLeavedGroup:(EMGroup *)aGroup
                       reason:(EMGroupLeaveReason)aReason
{
    // 主动退群或者被踢出群聊会收到
    NSString *str = NSLocalizedString(@"group.leaved", nil);
    if (aReason == EMGroupLeaveReasonBeRemoved) {
        str = [NSString stringWithFormat:NSLocalizedString(@"group.kicked", nil), aGroup.subject, aGroup.groupId];
    } else if (aReason == EMGroupLeaveReasonDestroyed) {
        str = [NSString stringWithFormat:NSLocalizedString(@"group.destroyed", nil), aGroup.subject, aGroup.groupId];
    }

//    if (str.length > 0) {
//        TTAlertNoTitle(str);
//    }
    
    YSTabBarController *tab = [UIApplication sharedApplication].delegate.window.rootViewController;
    YSNavigationController *nav=tab.selectedViewController;
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:nav.viewControllers];
    ChatViewController *chatViewContrller = nil;
    for (id viewController in viewControllers)
    {
        if ([viewController isKindOfClass:[ChatViewController class]] && [aGroup.groupId isEqualToString:[(ChatViewController *)viewController conversation].conversationId])
        {
            chatViewContrller = viewController;
            break;
        }
    }
    if (chatViewContrller)
    {
        [viewControllers removeObject:chatViewContrller];
        if ([viewControllers count] > 0) {
            [nav setViewControllers:@[viewControllers[0]] animated:YES];
        } else {
            [nav setViewControllers:viewControllers animated:YES];
        }
    }
}
- (void)userDidLeaveGroup:(EMGroup *)aGroup
                     user:(NSString *)aUsername
{
    // 有用户退群
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateGroupDetail" object:aGroup];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"removeGroupMenber" object:@{@"groupId":aGroup.groupId,@"user":aUsername}];
    
    
    NSDictionary *dic=[[YSFMDBTool shareInstance] readdata:aUsername];
    if (dic) {
        
        NSString *msg = [NSString stringWithFormat:@"%@ %@ %@", dic[@"name"], NSLocalizedString(@"group.leave", @"Leave group"), aGroup.subject];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"group.membersUpdate", @"Group Members Update") message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"Ok") otherButtonTitles:nil, nil];
        [alertView show];
    }else{
        
        NSString *api=@"/api/services/app/chatgroup/GetUserInfoByEasemobId";
        NSDictionary *para=@{
                             @"easemobId": aUsername
                             };
        [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
            if(SUCCEESS){
                if ([responseObject[@"result"] isEqual:[NSNull null]]) {
                    return ;
                }
                NSDictionary *dic=responseObject[@"result"];
                NSString *headPicture=dic[@"headPicture"];
                [[YSFMDBTool shareInstance] insertData:@{@"name":dic[@"name"],@"headPicture":headPicture?headPicture:@"",@"easemobId":aUsername}];
                
                NSString *msg = [NSString stringWithFormat:@"%@ %@ %@", dic[@"name"], NSLocalizedString(@"group.leave", @"Leave group"), aGroup.subject];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"group.membersUpdate", @"Group Members Update") message:msg delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"Ok") otherButtonTitles:nil, nil];
                [alertView show];
                
            }
        } errorHandel:^(NSError *error) {
            
        }];
    }

    
}

- (void)didJoinedGroup:(EMGroup *)aGroup
               inviter:(NSString *)aInviter
               message:(NSString *)aMessage
{
    // 收到群邀请并自动同意入群
    if ([aGroup.owner isEqualToString:[UserInfoManager shareInstance].userInfo.easemobId]) {
        //群主
        return;
    }
    NSDictionary *dic=[[YSFMDBTool shareInstance] readdata:aGroup.owner];
    if (dic) {
        [self insertMessage:dic aGroup:aGroup];
    }else{
        
        NSString *api=@"/api/services/app/chatgroup/GetUserInfoByEasemobId";
        NSDictionary *para=@{
                             @"easemobId": aGroup.owner
                             };
        [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
            if(SUCCEESS){
                if ([responseObject[@"result"] isEqual:[NSNull null]]) {
                    return ;
                }
                NSDictionary *dic=responseObject[@"result"];
                NSString *headPicture=dic[@"headPicture"];
                [[YSFMDBTool shareInstance] insertData:@{@"name":dic[@"name"],@"headPicture":headPicture?headPicture:@"",@"easemobId":aGroup.owner}];
                [self insertMessage:dic aGroup:aGroup];
                
            }
        } errorHandel:^(NSError *error) {
            
        }];
    }
    
}
-(void)insertMessage:(NSDictionary*)dic aGroup:(EMGroup*)aGroup{
    NSString *msg = [NSString stringWithFormat:@"%@ 邀请你加入了群聊",dic[@"name"]];
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:msg];
    
    //
    //    //生成Message
        EMMessage *message = [[EMMessage alloc] initWithConversationID:aGroup.groupId from:aGroup.owner to:aGroup.groupId body:body ext:nil];
        //    message.chatType = EMChatTypeChat;// 设置为单聊消息
        message.chatType = EMChatTypeGroupChat;// 设置为群聊消息
        //message.chatType = EMChatTypeChatRoom;// 设置为聊天室消息
//        message.timestamp = 1509689222137; 消息时间
        message.direction=EMMessageDirectionReceive;
        message.timestamp=(long long)[[NSDate date] timeIntervalSince1970]*1000;
        EMConversation *conversation =  [[EMClient sharedClient].chatManager getConversation:message.conversationId type:EMConversationTypeGroupChat createIfNotExist:YES];
        message.isRead = NO;
//     type: 会话类型
//          EMConversationTypeChat // 单聊
//              EMConversationTypeGroupChat // 群聊
//          EMConversationTypeChatRoom // 聊天室
        [conversation insertMessage:message error:nil];
}
@end
