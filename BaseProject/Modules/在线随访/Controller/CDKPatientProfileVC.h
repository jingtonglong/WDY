//
//  CDKPatientProfileVC.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/5.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKContactInfo.h"
@interface CDKPatientProfileVC : UITableViewController
@property(nonatomic,strong) CDKContactInfo *model;
@property(nonatomic,copy) NSString *pationtid;
@property(nonatomic,copy) NSString *easemobid;
@property(nonatomic,assign) BOOL isFromChat;

@end
