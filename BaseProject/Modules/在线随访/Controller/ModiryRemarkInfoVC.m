//
//  ModiryRemarkInfoVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/5.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "ModiryRemarkInfoVC.h"

@interface ModiryRemarkInfoVC ()

@end

@implementation ModiryRemarkInfoVC
- (instancetype)init
{
    self = [super init];
    if (self) {
        UIStoryboard *st=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self=[st instantiateViewControllerWithIdentifier:@"ModiryRemarkInfoVC"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
