//
//  CDKGroupMenberVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKGroupMenberVC.h"
#import "CDKGroupMenberCell.h"
#import "CDKModifyGroupNameVC.h"
#import "CDKContactSeletetVC.h"
@interface CDKGroupMenberVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UIView *menberContaintView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic,strong) NSMutableArray *datas;
@property (weak, nonatomic) IBOutlet UILabel *groupNameLab;

@property(nonatomic,copy) NSString *groupId;
@property(nonatomic,strong) EMGroup *group;
@property (weak, nonatomic) IBOutlet UISwitch *groupNotice;
@property (nonatomic, strong) NSString *cursor;
@property(nonatomic,strong) NSDictionary *groupInfoData;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inviteBtnHeigt;

@property (weak, nonatomic) IBOutlet UIView *addBtnContainer;//添加按钮容器

@end

@implementation CDKGroupMenberVC
- (instancetype)initWithGroupId:(NSString *)aGroupId

{
    self = [super init];
    if (self) {
        UIStoryboard *st=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self=[st instantiateViewControllerWithIdentifier:@"CDKGroupMenberVC"];
        _groupId=aGroupId;
    }
    return self;
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"群成员";
     self.deleteBtn.hidden=YES;
//    self.menberContaintView.height=250*Iphone6ScaleHeight;

    [self.collectionView registerNib:[UINib nibWithNibName:@"CDKGroupMenberCell" bundle:nil] forCellWithReuseIdentifier:@"groupMenber"];
    _datas=[NSMutableArray array];
    [self fetchGroupInfo];
    self.datas=[NSMutableArray array];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI:) name:@"removeGroupMenber" object:nil];
//    __weak typeof(self) weak=self;
//    self.collectionView.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        weak.cursor=@"";
//        [weak fetchMembersWithPage:1 isHeader:YES];
//    }];
//    self.collectionView.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
////        [weak fetchMembersWithPage:1 isHeader:NO];
//    }];
//    [self.collectionView.mj_header beginRefreshing];
//     [weak fetchMembersWithPage:1 isHeader:YES];
    self.tableView.bounces=NO;
}
-(void)updateUI:(NSNotification*)ntf{
    NSDictionary *dic=ntf.object;
//    @{@"groupId":aGroup.groupId,@"user":aUsername}
    NSString *groupid=dic[@"groupId"];
    NSString *user=dic[@"user"];
    if ([groupid isEqualToString:self.groupId]) {//当前群
        for (CDKGroupMenber *menber in self.datas) {
            if ([menber.easemobId isEqualToString:user]) {
                [self.datas removeObject:menber];
                [self.collectionView reloadData];
                return;
            }
        }
    }
    
}
- (IBAction)inviteMenber:(id)sender {// 邀请成员
    if(!self.groupInfoData){
        [self fetchGroupInfo];
        return;
    }
    CDKContactSeletetVC *vc=[[CDKContactSeletetVC alloc] init];
    vc.isFromInvite=YES;
    vc.groupID=self.groupInfoData[@"id"];
    vc.seletedMenbers=self.datas;
    [self.navigationController pushViewController:vc animated:YES];

}
- (IBAction)dissolve:(id)sender {//解散群组并退出
    if(!self.groupInfoData){
        [self fetchGroupInfo];
        return;
    }
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/app/chatgroup/Exit";
    NSDictionary *para=@{
                         @"groupId": self.groupInfoData[@"id"],
                         @"userId": [UserInfoManager shareInstance].userInfo.id
                         };
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
                [self dismissHud];
        if (SUCCEESS) {
//            [self showMessageHud:@""];
            [self.navigationController popToRootViewControllerAnimated:NO];
        }else{
            
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
    
}

- (IBAction)groupNotice:(UISwitch*)sender {// 群消息提醒设置
    [self enablePush:sender.isOn];
}

- (void)enablePush:(BOOL)isEnable
{
    [self showHudInView:self.view hint:NSLocalizedString(@"group.setting.save", @"set properties")];
    
    __weak typeof(self) weakSelf = self;
    [[EMClient sharedClient].groupManager updatePushServiceForGroup:_group.groupId isPushEnabled:isEnable completion:^(EMGroup *aGroup, EMError *aError) {
        [weakSelf hideHud];
        if (!aError) {
            [weakSelf showHint:NSLocalizedString(@"group.setting.success", @"set success")];
        }
        else{
            [weakSelf showHint:NSLocalizedString(@"group.setting.fail", @"set failure")];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadGroupInfo{
//    [self showLoadingHud:nil];
    NSString *api=@"/api/services/app/chatgroup/GetGroupInfo";
    NSDictionary *para=@{
                         @"easemobGroupId": self.groupId,
                         @"userId": [UserInfoManager shareInstance].userInfo.id
                         };
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
//        [self dismissHud];
        if (SUCCEESS) {
            self.groupInfoData=responseObject[@"result"];
            [self loadGroupMenber:self.groupInfoData[@"id"]];
            if ([self.groupInfoData[@"groupType"] integerValue]==2) {
                self.deleteBtn.hidden=NO;
            }
        }else{
            [MBProgressHUD hideHUDForView:self.view animated:YES];

            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadGroupMenber:(NSString*)groupid{
    NSString *api=@"/api/services/app/chatgroup/QueryPagedMembersV2";
    NSDictionary *para=@{
                         @"groupId": groupid
                         };
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
         if (SUCCEESS) {
             [self.datas removeAllObjects];
            NSArray *arr=[NSArray modelArrayWithClass:[CDKGroupMenber class] json:responseObject[@"result"]];
            [self.datas addObjectsFromArray:arr];
            [self.collectionView reloadData];
             [self.tableView reloadData];
            
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
- (void)fetchGroupInfo
{
    __weak typeof(self) weakSelf = self;
    [self showHudInView:self.view hint:NSLocalizedString(@"loadData", @"Load data...")];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
        EMError *error = nil;
        EMGroup *group = [[EMClient sharedClient].groupManager getGroupSpecificationFromServerWithId:weakSelf.groupId error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
//            [weakSelf hideHud];
        });

        if (!error) {
            weakSelf.group = group;
            EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:group.groupId type:EMConversationTypeGroupChat createIfNotExist:YES];


            if ([group.groupId isEqualToString:conversation.conversationId]) {
                NSMutableDictionary *ext = [NSMutableDictionary dictionaryWithDictionary:conversation.ext];
                [ext setObject:group.subject forKey:@"subject"];
                [ext setObject:[NSNumber numberWithBool:group.isPublic] forKey:@"isPublic"];
                conversation.ext = ext;
            }

            [weakSelf updateUI];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [weakSelf hideHud];
                [weakSelf showHint:NSLocalizedString(@"group.fetchInfoFail", @"failed to get the group details, please try again later")];
            });
        }

    });

//    [[EMClient sharedClient].groupManager getGroupAnnouncementWithId:_groupId completion:^(NSString *aAnnouncement, EMError *aError) {
//        if (!aError) {
//            [weakSelf.tableView reloadData];
//        } else {
//            [weakSelf showHint:NSLocalizedString(@"group.fetchAnnouncementFail", @"fail to get announcement")];
//        }
//    }];
}
-(void)updateUI{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.groupNameLab.text=self.group.subject;
        self.groupNotice.on=self.group.isPushNotificationEnabled;
        if([self.group.owner isEqualToString:[UserInfoManager shareInstance].userInfo.easemobId]){
//            self.deleteBtn.hidden=NO;
             self.addBtnContainer.hidden=NO;
//            self.inviteBtnHeigt.constant=40;
//            self.menberContaintView.height=230;
//            self.addbomconstant.constant=15;
            
        }else{
//            self.deleteBtn.hidden=YES;
//            self.inviteBtnHeigt.constant=0;
            self.addBtnContainer.hidden=YES;
//            self.menberContaintView.height=190;
//            self.addbomconstant.constant=0;
        }
        [self.tableView reloadData];
//        [self fetchMembersWithPage:1 isHeader:YES];
        [self loadGroupInfo];
    });
    
}
#pragma collectionView Delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        NSInteger count=0;
        if (self.datas.count<=0) {
            count=1;
        }else{
            count=self.datas.count;
        }
        if (self.addBtnContainer.hidden==YES) {
           
            return  75*((count-1)/5+1)+20+(count-1)/5*5;
        }else{
            return  75*((count-1)/5+1)+70+(count-1)/5*5; //defaut 230
        }
    }
    return 60;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   
    return self.datas.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CDKGroupMenberCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"groupMenber" forIndexPath:indexPath];
   
    CDKGroupMenber *dic=self.datas[indexPath.row];
    cell.nameLab.text=dic.name;
    if (dic.headPicture) {
         [cell.portraitHead sd_setImageWithURL:IMG_URL(dic.headPicture) placeholderImage:[UIImage imageNamed:@"默认头像"]];
    }else{
        cell.portraitHead.image=[UIImage imageNamed:@"默认头像"];
    }

    if([self.group.owner isEqualToString:[UserInfoManager shareInstance].userInfo.easemobId]){
        cell.deleteBtn.hidden=NO;
        if ([dic.easemobId isEqualToString:self.group.owner]) {
            cell.deleteBtn.hidden=YES;
        }
    }else{
        cell.deleteBtn.hidden=YES;
        
    }
    cell.groupMenber=dic;
    WeakSelf(weak);
    cell.callback = ^(CDKGroupMenber *data) {
        [weak showLoadingHud:nil];
        NSString *api=@"/api/services/app/chatgroup/RemoveMember";
        NSDictionary *para=@{  @"groupId": weak.groupInfoData[@"id"],
                               @"userId": data.id};
        [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
            [weak dismissHud];
            if (SUCCEESS) {
                [weak.datas removeObject:data];
                [weak.collectionView reloadData];
            }else{
                [weak showMessageHud:responseObject[@"error"][@"message"]];

            }
            
        } errorHandel:^(NSError *error) {
            [weak dismissHud];
            if (error.code ==-1001 ) {
                [self showMessageHud:@"请求超时"];
            }else{
                [self showMessageHud:@"请检查网络"];
            }
        }];
    };
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==1) {
        if(!self.groupInfoData){
            [self fetchGroupInfo];
            return;
        }
        if([self.group.owner isEqualToString:[UserInfoManager shareInstance].userInfo.easemobId]){
            // 群组可以修改群名
            
            CDKModifyGroupNameVC *vc=[[CDKModifyGroupNameVC alloc] init];
            vc.group=self.group;
            vc.groupid=self.groupInfoData[@"id"];
            vc.callback = ^(NSString *name) {
                self.groupNameLab.text=name;
            };
            [self.navigationController pushViewController:vc animated:YES];
            
        }else{
        }
    }
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    CGFloat w=(KScreenWidth-5*65*Iphone6ScaleWidth-10*2)/4.0;
    return w;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(65*Iphone6ScaleWidth, 75);
}
- (void)fetchMembersWithPage:(NSInteger)aPage
                    isHeader:(BOOL)aIsHeader
{
    NSInteger pageSize = 50;
    __weak typeof(self) weakSelf = self;
//    [self showHudInView:self.view hint:NSLocalizedString(@"loadData", @"Load data...")];
    [[EMClient sharedClient].groupManager getGroupMemberListFromServerWithId:self.group.groupId cursor:self.cursor pageSize:pageSize completion:^(EMCursorResult *aResult, EMError *aError) {
        weakSelf.cursor = aResult.cursor;
//        [weakSelf hideHud];
//        [weakSelf tableViewDidFinishTriggerHeader:aIsHeader reload:NO];
        [weakSelf hideHud];
        if (!aError) {
            if (aIsHeader) {
                [weakSelf.datas removeAllObjects];
                [weakSelf.collectionView.mj_header endRefreshing];
            }else{
                [weakSelf.collectionView.mj_footer endRefreshing];
            }
            
            [weakSelf.datas addObjectsFromArray:aResult.list];
            [weakSelf.collectionView reloadData];
        } else {
            [weakSelf showHint:NSLocalizedString(@"group.fetchInfoFail", @"failed to get the group details, please try again later")];
        }
        
        if ([aResult.list count] == 0 || [aResult.cursor length] == 0) {
//            weakSelf.showRefreshFooter = NO;
            [weakSelf.collectionView.mj_footer endRefreshingWithNoMoreData];
        } else {
//            weakSelf.showRefreshFooter = YES;
        }
    }];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
        
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
        
    }
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
        
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
