//
//  CDKMycollegueSelectVC.h
//  BaseProject
//
//  Created by sss on 2018/7/2.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "BaseViewController.h"

@interface CDKMycollegueSelectVC : BaseViewController
@property (nonatomic,strong) NSMutableArray *seletedDatas;
@property (nonatomic,copy) NSArray *previous;//之前选中的人
@property(nonatomic,copy) void(^callback)(NSArray *collegues);
@property(nonatomic,assign) BOOL isFromInvite;// 邀请群成员
@property(nonatomic,strong) NSArray *seletedMenbers;//已经在群里的人
//@property(nonatomic,copy) NSString *groupID;//群组id
@end
