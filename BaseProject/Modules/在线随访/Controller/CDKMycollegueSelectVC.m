//
//  CDKMycollegueSelectVC.m
//  BaseProject
//
//  Created by sss on 2018/7/2.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKMycollegueSelectVC.h"
#import "CDKNavTitleView.h"
#import "CDKContactInfo.h"
#import "CDKContactSeletetCell.h"
#import "AdressSection.h"
#import "CDKGroupMenber.h"
@interface CDKMycollegueSelectVC ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UITextField *textfiled;
@property(nonatomic,strong) NSMutableArray *dataArray;
@property(nonatomic,strong) NSMutableArray *sectionTitles;
@property(nonatomic,strong) UIButton *selectAllBtn;
@property(nonatomic,strong) UITableView *tableView;

@property(nonatomic,strong) NSMutableArray *searchResult;//搜索结果
@property(nonatomic,copy) NSArray *originDatas;
@property(nonatomic,strong) UIView *bottomView;
@property(nonatomic,strong) UILabel *allCount;
@property(nonatomic,strong) HanyuPinyinOutputFormat *pinyinFormat;

@end

@implementation CDKMycollegueSelectVC
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] init];
        [_tableView registerNib:[UINib nibWithNibName:@"AdressCell" bundle:nil] forCellReuseIdentifier:@"AdressCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"CDKContactSeletetCell" bundle:nil] forCellReuseIdentifier:@"CDKContactSeletetCell"];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.backgroundColor=RGB(247,247,247);
        //        self.tableView.contentInset=UIEdgeInsetsMake(10, 0, 0, 0);
        UIView *header=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 10)];
        header.backgroundColor=RGB(247,247,247);
        _tableView.tableHeaderView=header;
        _tableView.tableFooterView=[UIView new];
        _tableView.sectionIndexColor=RGB(97,97,97);
        _tableView.sectionIndexBackgroundColor=[UIColor clearColor];
        [_tableView registerClass:[AdressSection class] forHeaderFooterViewReuseIdentifier:@"section"];
    }
    return _tableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // 添加同事到群组
    self.dataArray=[NSMutableArray array];
    self.searchResult=[NSMutableArray array];
    self.pinyinFormat=[[HanyuPinyinOutputFormat alloc] init];
    [self.pinyinFormat setToneType:ToneTypeWithoutTone];
    [self.pinyinFormat setVCharType:VCharTypeWithV];
    [self.pinyinFormat setCaseType:CaseTypeLowercase];
    WeakSelf(weak);
    self.seletedDatas=[NSMutableArray array];
    self.sectionTitles=[NSMutableArray array];
    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithTitle:@" 确定" style:(UIBarButtonItemStylePlain) handler:^(id  _Nonnull sender) {
        // 选择完成
        if (weak.textfiled.text.length>0) {
            [weak back];
            return ;
        }
        [weak.seletedDatas removeAllObjects];
        for (NSArray *arr in weak.dataArray) {
            for (CDKContactInfo *model in arr) {
                if ((model.isSeleted)&&(!model.noEdit)) {
                    [weak.seletedDatas addObject:model.id];
                }
            }
        }
        if (weak.callback) {
            weak.callback(weak.seletedDatas);
        }
        [weak.navigationController popViewControllerAnimated:YES];
        
    }];
    CDKNavTitleView *titleView=[[CDKNavTitleView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 24)];
    titleView.backgroundColor=RGB(240,240,240);
    //    customView.translatesAutoresizingMaskIntoConstraints = false
    [YSUtil makeCornerRadius:12 view:titleView];
    UIImageView *seartchIcon=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"搜索"]];
    [titleView addSubview:seartchIcon];
    [seartchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(10);
        make.centerY.equalTo(titleView);
        make.width.mas_equalTo(17);
        make.height.mas_equalTo(17);
    }];
    
    UITextField *textfile=[[UITextField alloc] init];
    textfile.placeholder=@"输入联系人姓名";
    textfile.font=[UIFont systemFontOfSize:12];
    textfile.borderStyle=UITextBorderStyleNone;
    textfile.spellCheckingType=UITextSpellCheckingTypeNo;
    textfile.autocapitalizationType=UITextAutocapitalizationTypeNone;
    textfile.autocorrectionType=UITextAutocorrectionTypeNo;
    textfile.returnKeyType=UIReturnKeySearch;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textfileChange:) name:UITextFieldTextDidChangeNotification object:textfile];
    textfile.clearButtonMode=UITextFieldViewModeAlways;
    self.textfiled=textfile;
    textfile.delegate=self;
    [titleView addSubview:textfile];
    
    [textfile mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(seartchIcon.mas_trailing).offset(6);
        make.trailing.equalTo(titleView);
        make.height.mas_equalTo(titleView);
        make.top.mas_equalTo(0);
    }];
    
    self.navigationItem.titleView=titleView;
    self.navigationItem.rightBarButtonItem=item;
    
    [self.view addSubview:self.tableView];
    
    UIView *bottomView=[[UIView alloc] init];
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
    line.backgroundColor=RGB(229,229,229);
    [bottomView addSubview:line];
    self.bottomView=bottomView;
    self.selectAllBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [self.selectAllBtn setTitle:@"  全选" forState:UIControlStateNormal];
    [self.selectAllBtn setImage:[UIImage imageNamed:@"未选择"] forState:UIControlStateNormal];
    [self.selectAllBtn setImage:[UIImage imageNamed:@"选择"] forState:UIControlStateSelected];
    self.selectAllBtn.titleLabel.font=[UIFont systemFontOfSize:15];
    [self.selectAllBtn setTitleColor:RGB(83,83,83) forState:UIControlStateNormal];
    [self.selectAllBtn addTarget:self action:@selector(selectedAll:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:self.selectAllBtn];
    bottomView.clipsToBounds=YES;
    [self.selectAllBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.size.mas_equalTo(CGSizeMake(22, 22));
        make.centerY.equalTo(bottomView);
        make.leading.mas_equalTo(10);
    }];
    [self.view addSubview:bottomView];
    
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.trailing.mas_equalTo(0);
        make.height.mas_equalTo(45);
        make.bottom.mas_equalTo(iPhoneX_BOTTOM_HEIGHT);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.trailing.mas_equalTo(0);
        make.bottom.equalTo(bottomView.mas_top);
    }];
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0, 0, 40, 40);
    [btn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    UIBarButtonItem *leftitem=[[UIBarButtonItem alloc] initWithCustomView:btn];
    btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=leftitem;
}
#pragma textfile notifacation
-(void)textfileChange:(NSNotification*)obj{
    UITextField *textfile=obj.object;
    YSLog(@"%@",textfile.text);
    
    if (textfile.text.length==0) {
        [self.tableView reloadData];
        [self.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(45);
        }];
    }else{
//        [self.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(0);
//        }];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(searchName) object:nil];
        [self performSelector:@selector(searchName) withObject:nil afterDelay:0.5];
        // 搜索
    }
    
}
-(void)searchName{
    
     NSString *searchStr = [PinyinHelper toHanyuPinyinStringWithNSString:self.textfiled.text withHanyuPinyinOutputFormat:self.pinyinFormat withNSString:@""];
    
    // 谓词搜索
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self contains [cd] %@",searchStr];
    [self.searchResult removeAllObjects];
    for (CDKContactInfo *obj in self.originDatas) {
        BOOL isContain =  [predicate evaluateWithObject:obj.namePinyin];
        if (isContain) {
            [self.searchResult addObject:obj];
        }
    }
    [self.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(0);
                }];
    
    [self.tableView reloadData];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.textfiled resignFirstResponder];
    return YES;
}
-(void)back{
    if (self.textfiled.text.length>0) {
        [self.textfiled setText:@""];
        [self.textfiled resignFirstResponder];
        [self.tableView reloadData];
        [self.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(45);
        }];
        
        BOOL isAll=YES;
//        NSInteger count=0;// 这次选中的人数
        for (NSArray *arr in self.dataArray) {
            for (CDKContactInfo *model in arr) {
                if (!model.isSeleted) {
                    isAll=NO;
                }else{
//                    if(!model.noEdit){
//                        count++;
//                    }
                    
                }
            }
        }
//        if (self.isFromInvite){
//            count+=self.seletedMenbers.count;
//        }
        if (isAll) {
            self.selectAllBtn.selected=isAll;
        }else{
            self.selectAllBtn.selected=NO;
        }
//        count+=self.selectCollegues.count;
//        YSLog(@"--总人数--%ld-",count);
//        self.allCount.text=[NSString stringWithFormat:@"共%ld人",count];
        
        
    }else{
        [self.seletedDatas removeAllObjects];
        for (NSArray *arr in self.dataArray) {
            for (CDKContactInfo *model in arr) {
                if ((model.isSeleted)&&(!model.noEdit)) {
                    [self.seletedDatas addObject:model.id];
                }
            }
        }
        if (self.callback) {
            self.callback(self.seletedDatas);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)selectedAll:(UIButton*)sender{
    sender.selected=!sender.selected;
    if (sender.isSelected==YES) {
        for (NSArray *arr in self.dataArray) {
            for (CDKContactInfo *model in arr) {
                model.isSeleted=YES;
            }
        }
    }else{
        for (NSArray *arr in self.dataArray) {
            for (CDKContactInfo *model in arr) {
                if (!model.noEdit) {
                    model.isSeleted=NO;
                }
                
            }
        }
    }
    [self.tableView reloadData];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.dataArray.count==0) {
        [self loadData];
    }
}
-(void)loadData{
    [self showLoadingHud:nil];//查询医护人员的同事
    NSString *api=@"/api/services/app/doctor/QueryColleaguesV2";
    NSDictionary *para=@{@"doctorId":[UserInfoManager shareInstance].userInfo.id,@"roleType":@"2"};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            NSArray *arr=[NSArray modelArrayWithClass:[CDKContactInfo class] json:responseObject[@"result"]];
            for (NSString *easemobid in self.previous) {
                for (CDKContactInfo *model in arr) {
                    if ([easemobid isEqualToString:model.id]) {
                        model.isSeleted=YES;
                    }
                }
            }
            if (self.isFromInvite) {
                for (CDKGroupMenber *mebenr in self.seletedMenbers) {
                    for (CDKContactInfo *model in arr) {
                        if ([model.easemobId isEqualToString:mebenr.easemobId]) {
                            model.noEdit=YES;
                            model.isSeleted=YES;
                        }
                    }
                }
            }
            self.originDatas=arr;
            [self _sortDataArray:arr];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-(BOOL)textFieldShouldReturn:(UITextField *)textField{
//    [textField resignFirstResponder];
//    if (textField.text.length>0) {
//        WeakSelf(weak);
//        [self showLoadingHud:nil];
//        __block BOOL isFind=NO;
//        __block NSString *text=textField.text;
//        dispatch_async(dispatch_get_global_queue(0, 0), ^{
//            for (int i=0;i<weak.dataArray.count;i++) {
//                NSArray *arr=weak.dataArray[i];
//                for (int j=0;j<arr.count;j++) {
//                    CDKContactInfo *model=arr[j];
//                    if ([model.name isEqualToString:text]) {//找到了
//                        isFind=YES;
//                        NSIndexPath *index=[NSIndexPath indexPathForRow:j inSection:i];
//                        YSLog(@"0--%f-",[NSDate date].timeIntervalSinceNow);
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            YSLog(@"1--%f-",[NSDate date].timeIntervalSinceNow);
//                            [weak dismissHud];
//                            [weak.tableView scrollToRowAtIndexPath:index atScrollPosition:(UITableViewScrollPositionTop) animated:YES];
//
//                            return ;
//                        });
//                    }
//                }
//            }
//            if (!isFind) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [weak dismissHud];
//                    [weak showMessageHud:@"换个名字试试"];
//
//                });
//            }
//            YSLog(@"-2--");
//        });
//    }else{
//        [self showMessageHud:@"请输入搜索内容"];
//    }
//
//    return YES;
//}
#pragma tableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.textfiled.text.length>0) {
        return 1;
    }
    return self.sectionTitles.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.textfiled.text.length>0) {
        return self.searchResult.count;
    }
    NSArray *arr = self.dataArray[section];
    return arr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 16;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
   
    if (self.textfiled.text.length>0) {
        CDKContactSeletetCell *cell =[tableView dequeueReusableCellWithIdentifier:@"CDKContactSeletetCell" forIndexPath:indexPath];
        CDKContactInfo *model=self.searchResult[indexPath.row];
        cell.model=model;
        return cell;
    }
        CDKContactSeletetCell *cell =[tableView dequeueReusableCellWithIdentifier:@"CDKContactSeletetCell" forIndexPath:indexPath];
        CDKContactInfo *model=self.dataArray[indexPath.section][indexPath.row];
        
        
        cell.model=model;
        return cell;
    
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    AdressSection *view=[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"section"];
    if (!view) {
        view=[[AdressSection alloc] initWithReuseIdentifier:@"section"];
        
    }
    view.titleLab.text=self.sectionTitles[section];
    
    return view;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
    if (self.textfiled.text.length>0) {
        
        CDKContactInfo *model=self.searchResult[indexPath.row];
        if (model.noEdit) {
            return;
        }
        model.isSeleted=!model.isSeleted;
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:(UITableViewRowAnimationFade)];
        return;
    }
    CDKContactInfo *model=self.dataArray[indexPath.section][indexPath.row];
    if (model.noEdit) {
        return;
    }
    //         CDKContactSeletetCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    //         [cell select_btn_clicked:cell.selectedBtn];
    model.isSeleted=!model.isSeleted;
    
    if (model.isSeleted) {
        BOOL isAll=YES;
        for (NSArray *arr in self.dataArray) {
            for (CDKContactInfo *model in arr) {
                if (!model.isSeleted) {
                    isAll=NO;
                }else{
                    continue;
                }
            }
        }
        self.selectAllBtn.selected=isAll;
    }else{
        self.selectAllBtn.selected=NO;
    }
    
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:(UITableViewRowAnimationFade)];
        
    
    
    
}



- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    if (self.textfiled.text.length>0) {
        return nil;
    }
    return self.sectionTitles;
}





- (void)_sortDataArray:(NSArray *)buddyList
{
    [self.dataArray removeAllObjects];
    [self.sectionTitles removeAllObjects];
    NSMutableArray *contactsSource = [NSMutableArray arrayWithArray:buddyList];
    
    //从获取的数据中剔除黑名单中的好友
    //    NSArray *blockList = [[EMClient sharedClient].contactManager getBlackList];
    //    for (NSString *buddy in buddyList) {
    //        if (![blockList containsObject:buddy]) {
    //            [contactsSource addObject:buddy];
    //        }
    //    }
    
    //建立索引的核心, 返回27，是a－z和＃
    UILocalizedIndexedCollation *indexCollation = [UILocalizedIndexedCollation currentCollation];
    [self.sectionTitles addObjectsFromArray:[indexCollation sectionTitles]];
    
    NSInteger highSection = [self.sectionTitles count];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithCapacity:highSection];
    for (int i = 0; i < highSection; i++) {
        NSMutableArray *sectionArray = [NSMutableArray arrayWithCapacity:1];
        [sortedArray addObject:sectionArray];
    }
    
    //按首字母分组
    for (CDKContactInfo *buddy in contactsSource) {
        
        NSString *firstLetter = [EaseChineseToPinyin pinyinFromChineseString:buddy.name];
        NSInteger section;
        if (firstLetter.length > 0) {
            section = [indexCollation sectionForObject:[firstLetter substringToIndex:1] collationStringSelector:@selector(uppercaseString)];
        } else {
            section = [sortedArray count] - 1;
        }
        
        NSMutableArray *array = [sortedArray objectAtIndex:section];
        [array addObject:buddy];
        
    }
    
    //每个section内的数组排序
    for (int i = 0; i < [sortedArray count]; i++) {
        NSArray *array = [[sortedArray objectAtIndex:i] sortedArrayUsingComparator:^NSComparisonResult(CDKContactInfo *obj1, CDKContactInfo *obj2) {
            NSString *firstLetter1 = [EaseChineseToPinyin pinyinFromChineseString:obj1.name];
            firstLetter1 = [[firstLetter1 substringToIndex:1] uppercaseString];
            
            NSString *firstLetter2 = [EaseChineseToPinyin pinyinFromChineseString:obj2.name];
            firstLetter2 = [[firstLetter2 substringToIndex:1] uppercaseString];
            
            return [firstLetter1 caseInsensitiveCompare:firstLetter2];
        }];
        
        
        [sortedArray replaceObjectAtIndex:i withObject:[NSMutableArray arrayWithArray:array]];
    }
    
    //去掉空的section
    for (NSInteger i = [sortedArray count] - 1; i >= 0; i--) {
        NSArray *array = [sortedArray objectAtIndex:i];
        if ([array count] == 0) {
            [sortedArray removeObjectAtIndex:i];
            [self.sectionTitles removeObjectAtIndex:i];
        }
    }
    //    [self.sectionTitles insertObject:@"" atIndex:0];
//    [self.sectionTitles insertObject:@"" atIndex:0];
    [self.dataArray addObjectsFromArray:sortedArray];
    [self.tableView reloadData];
}

-(void)dealloc{
    YSLog(@"dealloc----%@",self);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
