//
//  AdressListVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/4/30.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "AdressListVC.h"
#import "AdressCell.h"
#import "AdressSection.h"
#import "CDKPatientProfileVC.h"
#import "CDKMycollegueVC.h"
#import "GroupListViewController.h"
#import "CreateGroupViewController.h"
#import "CDKContactInfo.h"
#import "CDKMyGroupList.h"
@interface AdressListVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *dataArray;
@property(nonatomic,strong) NSMutableArray *sectionTitles;
@property(nonatomic,strong) MyLinearLayout *sectionView;

@property(nonatomic,strong) NSArray *section1data;



@end

@implementation AdressListVC
-(UITableView *)tableView{
    if (!_tableView) {
        CGFloat bottom=0;
        if (StatusHeight>20) {
            bottom=34;
        }
        _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight-NavHeight-49-bottom)];
        _tableView.rowHeight=60;
        [_tableView registerNib:[UINib nibWithNibName:@"AdressCell" bundle:nil] forCellReuseIdentifier:@"AdressCell"];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.showsVerticalScrollIndicator=NO;
        _tableView.backgroundColor=RGB(247,247,247);
//        self.tableView.contentInset=UIEdgeInsetsMake(10, 0, 0, 0);
        UIView *header=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 10)];
        header.backgroundColor=RGB(247,247,247);
        _tableView.tableHeaderView=header;
        _tableView.tableFooterView=[UIView new];
        _tableView.sectionIndexColor=RGB(97,97,97);
        _tableView.sectionIndexBackgroundColor=[UIColor clearColor];
        [_tableView registerClass:[AdressSection class] forHeaderFooterViewReuseIdentifier:@"section"];
        WeakSelf(weak);
        _tableView.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weak refreshData];
        }];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // 通讯录
    // Do any additional setup after loading the view.
    self.dataArray=[NSMutableArray array];
    self.sectionTitles=[NSMutableArray array];
    [self.view addSubview:self.tableView];
    self.section1data=@[@{@"title":@"我的群组",@"imageName":@"我的群组"},@{@"title":@"我的同事",@"imageName":@"我的同事"}];

    
//    YSLog(@"viewdidload");
    
}
-(void)zj_viewDidAppearForIndex:(NSInteger)index{
    if (self.dataArray.count>0) {
        return;
    }
    [self loadData];
}
-(void)refreshData{
//    [self showLoadingHud:nil];//查询医护人员的患者
    NSString *api=@"/api/services/app/doctor/QueryPatientsForCkdV2";
    NSDictionary *para=@{@"doctorId":[UserInfoManager shareInstance].userInfo.id};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
//        [self dismissHud];
        [self.tableView.mj_header endRefreshing];

        if (SUCCEESS) {
            NSArray *arr=[NSArray modelArrayWithClass:[CDKContactInfo class] json:responseObject[@"result"]];
            [self _sortDataArray:arr];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
//        [self dismissHud];
        [self.tableView.mj_header endRefreshing];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadData{
    
    [self showLoadingHud:nil];//查询医护人员的患者
    NSString *api=@"/api/services/app/doctor/QueryPatientsForCkdV2";
    NSDictionary *para=@{@"doctorId":[UserInfoManager shareInstance].userInfo.id};
    YSLog(@"%@",para);
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            NSArray *arr=[NSArray modelArrayWithClass:[CDKContactInfo class] json:responseObject[@"result"]];
            [self _sortDataArray:arr];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        YSLog(@"responseObject%@",responseObject);
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
#pragma tableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sectionTitles.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return 1;
    }else if (section==1){
        return 2;
    }
   NSArray *arr = self.dataArray[section-2];
    return arr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AdressCell *cell =[tableView dequeueReusableCellWithIdentifier:@"AdressCell" forIndexPath:indexPath];
    if (indexPath.section==0) {
        cell.arrow.hidden=NO;
        cell.headPotrait.image=[UIImage imageNamed:@"群图标"];
        cell.titleLab.text=@"创建群聊";
    }else if(indexPath.section==1){
        cell.arrow.hidden=NO;
        cell.headPotrait.image=[UIImage imageNamed:self.section1data[indexPath.row][@"imageName"]];
        cell.titleLab.text=self.section1data[indexPath.row][@"title"];
    }else{
        
        CDKContactInfo *model=self.dataArray[indexPath.section-2][indexPath.row];
        
        cell.arrow.hidden=YES;
        [cell.headPotrait sd_setImageWithURL:IMG_URL(model.headPicture) placeholderImage:[UIImage imageNamed:@"默认头像"]];
        cell.titleLab.text=model.name;
    }
   
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section<=1) {
        return nil;
    }
    AdressSection *view=[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"section"];
    if (!view) {
        view=[[AdressSection alloc] initWithReuseIdentifier:@"section"];
    
    }
    view.titleLab.text=self.sectionTitles[section];

    return view;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section==0) {
        UIView *secview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 10)];
        secview.backgroundColor=RGB(247,247,247);
        return secview;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section<=1) {
        return 0.0;
    }
    return 16;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==0) {
        return 10;
    }
    return 0.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section==0) {// 创建群组
        CreateGroupViewController *vc=[[CreateGroupViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section==1){
        
        if (indexPath.row==0) {//我的群组
//            GroupListViewController *vc=[[GroupListViewController alloc] init];
             CDKMyGroupList *vc=[[CDKMyGroupList alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }else{// 我的同事
            
            CDKMycollegueVC *vc=[[CDKMycollegueVC alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else{
        CDKContactInfo *model=self.dataArray[indexPath.section-2][indexPath.row];
        CDKPatientProfileVC *vc=[[CDKPatientProfileVC alloc] init];
        vc.model=model;
        vc.pationtid=model.id;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}



- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return self.sectionTitles;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)_sortDataArray:(NSArray *)buddyList
{
    [self.dataArray removeAllObjects];
    [self.sectionTitles removeAllObjects];
    NSMutableArray *contactsSource = [NSMutableArray arrayWithArray:buddyList];
    
    //从获取的数据中剔除黑名单中的好友
//    NSArray *blockList = [[EMClient sharedClient].contactManager getBlackList];
//    for (NSString *buddy in buddyList) {
//        if (![blockList containsObject:buddy]) {
//            [contactsSource addObject:buddy];
//        }
//    }
    
    //建立索引的核心, 返回27，是a－z和＃
    UILocalizedIndexedCollation *indexCollation = [UILocalizedIndexedCollation currentCollation];
    [self.sectionTitles addObjectsFromArray:[indexCollation sectionTitles]];
    
    NSInteger highSection = [self.sectionTitles count];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithCapacity:highSection];
    for (int i = 0; i < highSection; i++) {
        NSMutableArray *sectionArray = [NSMutableArray arrayWithCapacity:1];
        [sortedArray addObject:sectionArray];
    }
    
    //按首字母分组
    for (CDKContactInfo *buddy in contactsSource) {
        
            NSString *firstLetter = [EaseChineseToPinyin pinyinFromChineseString:buddy.name];
            NSInteger section;
            if (firstLetter.length > 0) {
                section = [indexCollation sectionForObject:[firstLetter substringToIndex:1] collationStringSelector:@selector(uppercaseString)];
            } else {
                section = [sortedArray count] - 1;
            }
            
            NSMutableArray *array = [sortedArray objectAtIndex:section];
            [array addObject:buddy];
        
    }
    
    //每个section内的数组排序
    for (int i = 0; i < [sortedArray count]; i++) {
        NSArray *array = [[sortedArray objectAtIndex:i] sortedArrayUsingComparator:^NSComparisonResult(CDKContactInfo *obj1, CDKContactInfo *obj2) {
            NSString *firstLetter1 = [EaseChineseToPinyin pinyinFromChineseString:obj1.name];
            firstLetter1 = [[firstLetter1 substringToIndex:1] uppercaseString];
            
            NSString *firstLetter2 = [EaseChineseToPinyin pinyinFromChineseString:obj2.name];
            firstLetter2 = [[firstLetter2 substringToIndex:1] uppercaseString];
            
            return [firstLetter1 caseInsensitiveCompare:firstLetter2];
        }];
        
        
        [sortedArray replaceObjectAtIndex:i withObject:[NSMutableArray arrayWithArray:array]];
    }
    
    //去掉空的section
    for (NSInteger i = [sortedArray count] - 1; i >= 0; i--) {
        NSArray *array = [sortedArray objectAtIndex:i];
        if ([array count] == 0) {
            [sortedArray removeObjectAtIndex:i];
            [self.sectionTitles removeObjectAtIndex:i];
        }
    }
    [self.sectionTitles insertObject:@"" atIndex:0];
    [self.sectionTitles insertObject:@"" atIndex:0];
    [self.dataArray addObjectsFromArray:sortedArray];
    [self.tableView reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
