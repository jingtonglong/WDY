//
//  CDKMyGroupList.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/26.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKMyGroupList.h"
#import "AdressCell.h"
@interface CDKMyGroupList ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSArray *datas;

@end

@implementation CDKMyGroupList
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] init];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        [_tableView registerNib:[UINib nibWithNibName:@"AdressCell" bundle:nil] forCellReuseIdentifier:@"AdressCell"];
        _tableView.rowHeight=60;
        _tableView.tableFooterView=[UIView new];
        
    }
    return _tableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"我的群组";
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self loadGroupList];
    
}
-(void)loadGroupList{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/app/chatgroup/GetUserGroupsV2";
    NSDictionary *para=@{
        @"userId": [UserInfoManager shareInstance].userInfo.id,
        @"roleType": @2
    };
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            self.datas=responseObject[@"result"];
            [self.tableView reloadData];
        }else{
            
           [self showMessageHud:responseObject[@"error"][@"message"]];
        }
       
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AdressCell *cell=[tableView dequeueReusableCellWithIdentifier:@"AdressCell" forIndexPath:indexPath];
    
    NSDictionary *dic=self.datas[indexPath.row];
    
    cell.arrow.hidden=YES;
    cell.headPotrait.image=[UIImage imageNamed:@"群图标"];
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"defaultGroupId"];

    if ([dic[@"easemobGroupId"] isEqualToString:str]) {
        cell.headPotrait.image=[UIImage imageNamed:@"logo"];

    }
    cell.titleLab.text=dic[@"name"];
    return cell;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dic=self.datas[indexPath.row];
    ChatViewController *vc=[[ChatViewController alloc] initWithConversationChatter:dic[@"easemobGroupId"] conversationType:(EMConversationTypeGroupChat)];
    vc.title=dic[@"name"];
    [self.navigationController pushViewController:vc animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
