//
//  CDKContactSeletetVC.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/6.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKContactSeletetVC : BaseViewController
@property(nonatomic,copy) NSString *groupName;
@property(nonatomic,assign) BOOL isFromInvite;// 邀请群成员
@property(nonatomic,strong) NSArray *seletedMenbers;//已经在群里的人
@property(nonatomic,copy) NSString *groupID;//群组id

@end
