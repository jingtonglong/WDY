//
//  CDKGroupMenberCell.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKGroupMenberCell.h"

@implementation CDKGroupMenberCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)delete:(id)sender {// 剔除群成员
    if (self.callback) {
        self.callback(self.groupMenber);
    }
}
- (UIViewController *)ControllerForView:(UIView *)next;
{
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}
@end
