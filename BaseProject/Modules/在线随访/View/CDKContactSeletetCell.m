//
//  CDKContactSeletetCell.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/6.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKContactSeletetCell.h"

@implementation CDKContactSeletetCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(CDKContactInfo *)model{
    _model=model;
    if (model.noEdit) {//不能编辑
        
//        self.selectedBtn.enabled=NO;
        [self.selectedBtn setImage:[UIImage imageNamed:@"select"] forState:UIControlStateNormal];
        
    }else if(model.isSeleted){
         [self.selectedBtn setImage:[UIImage imageNamed:@"选择_add"] forState:UIControlStateNormal];
    }else{
        [self.selectedBtn setImage:[UIImage imageNamed:@"未选择"] forState:UIControlStateNormal];
    }
    self.nameLab.text=model.name;
    [self.headPotrait sd_setImageWithURL:IMG_URL(model.headPicture) placeholderImage:[UIImage imageNamed:@"默认头像"]];
    
    
}
- (IBAction)select_btn_clicked:(UIButton*)sender {
    sender.selected=!sender.selected;
    self.model.isSeleted=sender.selected;
    if (self.model.noEdit) {//不能编辑
        
        //        self.selectedBtn.enabled=NO;
        [self.selectedBtn setImage:[UIImage imageNamed:@"select"] forState:UIControlStateNormal];
        
    }else if(self.model.isSeleted){
        [self.selectedBtn setImage:[UIImage imageNamed:@"选择_add"] forState:UIControlStateNormal];
    }else{
        [self.selectedBtn setImage:[UIImage imageNamed:@"未选择"] forState:UIControlStateNormal];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
