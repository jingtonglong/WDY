//
//  CDKGroupMenberCell.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKGroupMenber.h"
@interface CDKGroupMenberCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *portraitHead;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property(nonatomic,strong) CDKGroupMenber *groupMenber;
@property(nonatomic,copy) void(^callback)(CDKGroupMenber *data);

@end
