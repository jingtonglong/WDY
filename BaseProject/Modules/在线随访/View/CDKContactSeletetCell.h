//
//  CDKContactSeletetCell.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/6.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKContactInfo.h"
@interface CDKContactSeletetCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headPotrait;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UIButton *selectedBtn;
@property(nonatomic,strong) CDKContactInfo *model;
- (IBAction)select_btn_clicked:(UIButton*)sender;
@end
