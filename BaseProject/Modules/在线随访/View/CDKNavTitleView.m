//
//  CDKNavTitleView.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/6.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKNavTitleView.h"

@implementation CDKNavTitleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(CGSize)intrinsicContentSize{
    return CGSizeMake(KScreenWidth-100, 24);
}
@end
