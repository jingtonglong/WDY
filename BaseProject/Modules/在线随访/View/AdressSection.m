//
//  AdressSection.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/4/30.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "AdressSection.h"

@implementation AdressSection

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLab=[[UILabel alloc] initWithFrame:CGRectMake(20, 0, KScreenWidth-20, 16)];
        _titleLab.font=[UIFont systemFontOfSize:11];
        _titleLab.textColor=RGB(83,83,83);
        [self.contentView addSubview:_titleLab];
       
    }
    return self;
}
@end
