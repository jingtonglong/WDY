//
//  AdressCell.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/4/30.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIImageView *headPotrait;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;

@end
