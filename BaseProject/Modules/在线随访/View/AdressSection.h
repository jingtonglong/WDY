//
//  AdressSection.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/4/30.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdressSection : UITableViewHeaderFooterView
@property(nonatomic,strong) UILabel *titleLab;

@end
