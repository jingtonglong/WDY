//
//  YSBaseNewsCell.h
//  BaseProject
//
//  Created by apple on 2017/6/22.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YSBaseNewsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UILabel *newsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsTimeLabel;

@end
