//
//  YSBaseNewsMorImageCell.h
//  BaseProject
//
//  Created by apple on 2017/6/27.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YSBaseNewsMorImageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *moreNewsTitle;
@property (weak, nonatomic) IBOutlet UIImageView *moreImageViewOne;
@property (weak, nonatomic) IBOutlet UIImageView *moreImageViewTwo;
@property (weak, nonatomic) IBOutlet UIImageView *moreImageViewthree;
@property (weak, nonatomic) IBOutlet UILabel *moreNewsTime;


@end
