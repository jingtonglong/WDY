//
//  YSBaseNewsMorImageCell.m
//  BaseProject
//
//  Created by apple on 2017/6/27.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "YSBaseNewsMorImageCell.h"

@implementation YSBaseNewsMorImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [YSUtil makeCornerRadius:6 view:self.moreImageViewOne];
    [YSUtil makeCornerRadius:6 view:self.moreImageViewTwo];
    [YSUtil makeCornerRadius:6 view:self.moreImageViewthree];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
