//
//  YSBaseNewsCell.m
//  BaseProject
//
//  Created by apple on 2017/6/22.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "YSBaseNewsCell.h"

@implementation YSBaseNewsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [YSUtil makeCornerRadius:6 view:self.newsImageView];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
