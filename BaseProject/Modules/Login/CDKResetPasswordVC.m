//
//  CDKResetPasswordVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/7.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKResetPasswordVC.h"

@interface CDKResetPasswordVC ()
@property (weak, nonatomic) IBOutlet UIView *containerView1;
@property (weak, nonatomic) IBOutlet UIView *containerView2;
@property (weak, nonatomic) IBOutlet UIButton *comfirmBtn;
@property (weak, nonatomic) IBOutlet UITextField *tx_password1;
@property (weak, nonatomic) IBOutlet UITextField *tx_password2;

@end

@implementation CDKResetPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title=@"重置密码";
    [YSUtil makeCornerRadius:21 view:self.containerView1];
    [YSUtil makeCornerRadius:21 view:self.containerView2];
    [YSUtil makeCornerRadius:21 view:self.comfirmBtn];
    [YSUtil makeBorderWidth:1 view:self.containerView1 borderColor:RGB(173,173,173)];
    [YSUtil makeBorderWidth:1 view:self.containerView2 borderColor:RGB(173,173,173)];
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.comfirmBtn.bounds;  // 设置显示的frame
    gradientLayer.colors = @[(id)RGB(69,213,160).CGColor,(id)RGB(112,223,182).CGColor];  // 设置渐变颜色
    //    gradientLayer.locations = @[@0.0, @0.2, @0.5];    // 颜色的起点位置，递增，并且数量跟颜色数量相等
    //
    [self.comfirmBtn.layer insertSublayer:gradientLayer atIndex:0];
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0, 0, 40, 40);
    [btn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithCustomView:btn];
    btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=item;
    
}
-(void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)comfirClicked:(id)sender {
    if (self.tx_password1.text.length==0) {
        [self showMessageHud:@"请输入密码"];
    }else if (self.tx_password1.text.length<6){
        [self showMessageHud:@"密码长度不能低于6位"];
    }else if (![self.tx_password1.text isEqualToString:self.tx_password2.text]){
        [self showMessageHud:@"两次密码不一致"];
    } else{
        //
        [self resetPassword];
    }
}
-(void)resetPassword{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/user/UpdateUserPwd";
    NSDictionary *para=@{@"loginPhone":self.phone,@"newPassword":self.tx_password1.text,@"newPassword2":self.tx_password2.text};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popToRootViewControllerAnimated:NO];
            });
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];

        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@" "]) {
        return NO;
    }
    if ((textField.text.length+string.length)>20) {
        [self showMessageHud:@"密码长度不能超过20位"];
        return NO;
    }
    return YES;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
