//
//  CDKHospitalList.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKHospitalList.h"

@implementation CDKHospitalList
- (instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] init];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight=44;
    }
    return _tableView;
}
-(void)setDatas:(NSArray *)datas{
    _datas=datas;
    [self.tableView reloadData];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    CDKHospital_List_Model *model=self.datas[indexPath.row];
    cell.textLabel.text=model.name;
    cell.textLabel.textAlignment=NSTextAlignmentCenter;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(self.delegate){
        [self.delegate selectedHospital:self.datas[indexPath.row]];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
