//
//  CDKHospitalList.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKHospital_List_Model.h"
@protocol CDKHospitalListDelegate<NSObject>
-(void)selectedHospital:(CDKHospital_List_Model*)model;
@end
@interface CDKHospitalList : UIView<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSArray<CDKHospital_List_Model*> *datas;
@property(nonatomic,weak) id<CDKHospitalListDelegate> delegate;


@end
