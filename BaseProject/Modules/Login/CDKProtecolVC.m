
//
//  CDKProtecolVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/7.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKProtecolVC.h"

@interface CDKProtecolVC ()<UIWebViewDelegate>
@property(nonatomic,strong) UIWebView *webView;
@end

@implementation CDKProtecolVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title=@"服务条款声明";
  
    [self setupUI];
}

- (void)setupUI{
    self.webView = [[UIWebView alloc] init];
    
    self.webView.delegate = self;
    NSString *str=[NSString stringWithFormat:@"%@/h5/index.html#/fwxy",BASE_URL];
    NSURL *url = [NSURL URLWithString:str];
    NSURLRequest *request = [NSURLRequest requestWithURL: url];
    
    [self.webView loadRequest:request];
    
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self showLoadingHud:nil];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self dismissHud];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self dismissHud];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
