//
//  CDKForgetPasswordVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/7.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKForgetPasswordVC.h"
#import "CDKResetPasswordVC.h"
@interface CDKForgetPasswordVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *containerView1;
@property (weak, nonatomic) IBOutlet UIView *containerView2;
@property (weak, nonatomic) IBOutlet UITextField *tx_code;
@property (weak, nonatomic) IBOutlet UIButton *nextStep;
@property (weak, nonatomic) IBOutlet UITextField *tx_phone;
@property (weak, nonatomic) IBOutlet UIButton *getCodeBtn;
@property(nonatomic,strong) NSTimer *timer;
@property(nonatomic,assign) NSInteger timer_Counter;
@property(nonatomic,copy) NSString *codeStr;// 验证码


@end

@implementation CDKForgetPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"忘记密码";
    // Do any additional setup after loading the view from its nib.
    [YSUtil makeCornerRadius:21 view:self.containerView1];
    [YSUtil makeCornerRadius:21 view:self.containerView2];
      [YSUtil makeCornerRadius:21 view:self.nextStep];
    [YSUtil makeBorderWidth:1 view:self.containerView1 borderColor:RGB(173,173,173)];
    [YSUtil makeBorderWidth:1 view:self.containerView2 borderColor:RGB(173,173,173)];
    self.timer_Counter=60;
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = CGRectMake(0, 0, KScreenWidth-30, 42);  // 设置显示的frame
    gradientLayer.colors = @[(id)RGB(69,213,160).CGColor,(id)RGB(112,223,182).CGColor];  // 设置渐变颜色
    //    gradientLayer.locations = @[@0.0, @0.2, @0.5];    // 颜色的起点位置，递增，并且数量跟颜色数量相等
    //
    [self.nextStep.layer insertSublayer:gradientLayer atIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)next:(id)sender {
    
    if (![NSString isMobileNumber:self.tx_phone.text]) {
        [self showMessageHud:@"请输入正确的手机号"];
        return;
    }else if (self.tx_code.text.length==0){
        [self showMessageHud:@"请输入验证码"];
        return;
    }else if (self.tx_code.text !=self.codeStr){
        [self showMessageHud:@"验证码不正确"];
        return;
    }
   
    // 下一步
    
    CDKResetPasswordVC *vc=[[CDKResetPasswordVC alloc] init];
    vc.phone=self.tx_phone.text;
    
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)getcode_clicked:(id)sender {
    if (![NSString isMobileNumber:self.tx_phone.text]) {
        [self showMessageHud:@"请输入正确的手机号"];
        return;
    }
    [self.view endEditing:YES];
    NSString *api=@"/Token";
    
    NSDictionary *dic=@{@"grant_type":@"client_credentials",
                        @"client_id":@"ios_123",
                        @"client_secret":@"123123"
                        };
    //客户端授权（加载非用户数据）
    [self showLoadingHud:nil];
    [YSNetworkingManager requestWithUrl:api :POST paramiters:dic success:^(id responseObject) {
        if(responseObject[@"error"]){
            [self dismissHud];
            [self showMessageHud:responseObject[@"error_description"]];
        }else{
            
            [UserInfoManager shareInstance].tokenInfo=[CDKTokenInfo modelWithJSON:responseObject];
            [self getCode];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
        
    }];
}
-(void)getCode{
    
    NSString *api=@"/api/services/app/user/UpdateAndSendMobileCode";
    
    NSDictionary *dic=@{@"mobile":self.tx_phone.text
                        };
    //获取验证码
    self.getCodeBtn.userInteractionEnabled=NO;
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:dic success:^(id responseObject) {
        [self dismissHud];
        if([responseObject[@"success"] boolValue]){
            [self showMessageHud:@"发送验证码成功"];
            [UserInfoManager shareInstance].loginAccount=self.tx_phone.text;
            self.codeStr=responseObject[@"result"][@"code"];
            [self.getCodeBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            self.timer.fireDate = [NSDate distantPast];
            
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            self.getCodeBtn.userInteractionEnabled=YES;
        }
        
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
        self.getCodeBtn.userInteractionEnabled=YES;
    }];
    
}

-(NSTimer*)timer{
    
    if (!_timer) {
        _timer= [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
    }
    return _timer;
}
-(void)timerAction{
    [self.getCodeBtn setTitle:[NSString stringWithFormat:@"%ld秒后重发",self.timer_Counter--] forState:UIControlStateNormal];
    if (self.timer_Counter==-1) {
        
        [self.getCodeBtn setTitle:@"重发验证码" forState:UIControlStateNormal];
        self.getCodeBtn.userInteractionEnabled=YES;
        [self.getCodeBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        self.timer.fireDate=[NSDate distantFuture];
        self.timer_Counter=60;
    }
}
-(void)dealloc{
    
    YSLog(@"---");
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [self.timer invalidate];
    self.timer=nil;
    self.timer_Counter=60;
    [self.getCodeBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    self.getCodeBtn.userInteractionEnabled=YES;
    [self.getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (![NSString isNumber:string]) {
        return NO;
    }
    if (textField==self.tx_phone) {
        if((textField.text.length+string.length>11)){
            return NO;
        }
    }else{
        if ((textField.text.length+string.length>6)) {
            return NO;
        }
    }
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
