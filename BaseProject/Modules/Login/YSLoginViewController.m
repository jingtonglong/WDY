//
//  YSLoginViewController.m
//  BaseProject
//
//  Created by apple on 2017/6/22.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "YSLoginViewController.h"
#import "YSTabBarController.h"
#import "CDKRegisterVC.h"
#import "CDKForgetPasswordVC.h"
#import "LimitTextfield.h"
@interface YSLoginViewController ()<UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *accountTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIView *tx_container1;
@property (weak, nonatomic) IBOutlet UIView *tx_container2;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property(nonatomic,assign) NSInteger loingCount;

@end

@implementation YSLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    [self initUserInterface];
    self.loingCount=0;
    self.navigationController.delegate=self;
    
     NSString *account = [[NSUserDefaults standardUserDefaults] objectForKey:@"login_account"];
    if (account.length > 0) {
        self.accountTextField.text = account;
    }
}
// 将要显示控制器
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // 判断要显示的控制器是否是自己
    BOOL isShowHomePage = [viewController isKindOfClass:[self class]];
    
    [self.navigationController setNavigationBarHidden:isShowHomePage animated:YES];
    if (isShowHomePage) {
        [UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleLightContent;

    }else{
        [UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleDefault;

    }
}
- (void)initUserInterface{
    // 设置Button边框
    [YSUtil makeCornerRadius:21 view:self.loginButton];
    
    [YSUtil makeCornerRadius:21 view:self.tx_container1];
    [YSUtil makeCornerRadius:21 view:self.tx_container2];

    [YSUtil makeBorderWidth:1 view:self.tx_container1 borderColor:RGB(216,216,216)];
    [YSUtil makeBorderWidth:1 view:self.tx_container2 borderColor:RGB(216,216,216)];

    [self.loginButton addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self.navigationController setNavigationBarHidden:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO];

}
-(void)login{
 

    [self loginWithUsername:self.accountTextField.text password:self.passwordTextField.text];
    
   
}

// 注册
- (IBAction)registerNow:(id)sender {
    
    CDKRegisterVC *vc=[[CDKRegisterVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
// 忘记密码

- (IBAction)forgetpassword:(id)sender {
    
    CDKForgetPasswordVC *vc=[[CDKForgetPasswordVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
//点击登陆后的操作
- (void)loginWithUsername:(NSString *)username password:(NSString *)password
{
    
    if (self.accountTextField.text.length==0) {
        [self showMessageHud:@"请输入账号"];
        return;
    }else if (self.passwordTextField.text.length==0){
        [self showMessageHud:@"请输入密码"];
        return;
    }
    [self showLoadingHud:@"正在登录"];
    NSString *api=@"/Token";
  
    NSDictionary *para=@{@"grant_type":@"password",
                         @"client_id":@"app",
                         @"scope":@"ckdDoctor",
                         @"client_secret":@"123123",
                         @"username":self.accountTextField.text,
                         @"password":self.passwordTextField.text
                         };
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
//        [self dismissHud];
        if(responseObject[@"error"]){
            [self dismissHud];
            if ([responseObject[@"error_description"] isEqualToString:@"您输入的账号或密码有误，请重新输入！"]) {
                self.loingCount++;
                if (self.loingCount>=5) {//可通过“忘记密码”找回密码
                    [self showMessageHud:[NSString stringWithFormat:@"%@,可通过\"忘记密码\"找回密码",responseObject[@"error_description"]]];
                    
                }else{
                    [self showMessageHud:responseObject[@"error_description"]];

                    
                }
            }else{
                 [self showMessageHud:responseObject[@"error_description"]];
            }
           
        }else{
//            [self showMessageHud:@"登录成功"];
           
            [UserInfoManager shareInstance].tokenInfo=[CDKTokenInfo modelWithJSON:responseObject];
            [UserInfoManager shareInstance].isLogin=YES;
            
            [[NSUserDefaults standardUserDefaults] setObject:self.accountTextField.text forKey:@"login_account"];
            
            [self loadUserInfoData];
           
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
    return;
    
}
-(void)loginEMob:(NSString*)userName{
//    [self showHudInView:self.view hint:NSLocalizedString(@"login.ongoing", @"Is Login...")];
    //异步登陆账号
    __weak typeof(self) weakself = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        EMError *error = [[EMClient sharedClient] loginWithUsername:userName password:@"ckdcloud2017"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
//            [weakself hideHud];
           [MBProgressHUD hideHUDForView:weakself.view animated:YES];
//            YSLog(@"%@",[EMClient sharedClient].currentUsername);
//            YSLog(@"%ld",[EMClient sharedClient].isLoggedIn);
            if (!error) {
                //设置是否自动登录
                [weakself showMessageHud:@"登录成功"];
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:[NSNumber numberWithBool:YES]];
                            });
                [[EMClient sharedClient].options setIsAutoLogin:YES];
                NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                NSData *user=[[UserInfoManager shareInstance] modelToJSONData];
                [defaults setObject:user forKey:@"user"];
                [defaults synchronize];
//                YSLog(@"-----");
//                YSLog(@"%ld",[EMClient sharedClient].isLoggedIn);

                //保存最近一次登录用户名
                //                [weakself saveLastLoginUsername];
                //发送自动登陆状态通知
//                [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:[NSNumber numberWithBool:YES]];
                
            } else {
                switch (error.code)
                {
                    case EMErrorUserNotFound:
                        TTAlertNoTitle(NSLocalizedString(@"error.usernotExist", @"User not exist!"));
                        break;
                    case EMErrorNetworkUnavailable:
                        TTAlertNoTitle(NSLocalizedString(@"error.connectNetworkFail", @"No network connection!"));
                        break;
                    case EMErrorServerNotReachable:
                        TTAlertNoTitle(NSLocalizedString(@"error.connectServerFail", @"Connect to the server failed!"));
                        break;
                    case EMErrorUserAuthenticationFailed:
                        TTAlertNoTitle(error.errorDescription);
                        break;
                    case EMErrorServerTimeout:
                        TTAlertNoTitle(NSLocalizedString(@"error.connectServerTimeout", @"Connect to the server timed out!"));
                        break;
                    case EMErrorServerServingForbidden:
                        TTAlertNoTitle(NSLocalizedString(@"servingIsBanned", @"Serving is banned"));
                        break;
                    case EMErrorUserLoginTooManyDevices:
                        TTAlertNoTitle(NSLocalizedString(@"alert.multi.tooManyDevices", @"Login too many devices"));
                        break;
                    default:
//                        TTAlertNoTitle(NSLocalizedString(@"login.fail", @"Login failure"));
                         [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:[NSNumber numberWithBool:YES]];
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"EMobLoginFailed" object:nil];
                        break;
                }
            }
        });
    });
}
-(void)loadUserInfoData{
//    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/user/QueryUserInfo";
    //    NSDictionary *para=@{};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:nil success:^(id responseObject) {
//        [self dismissHud];
        if (SUCCEESS) {
            [UserInfoManager shareInstance].userInfo=[CDKUserInfo modelWithJSON:responseObject[@"result"]];
//            [self updateUI];
            //发送自动登陆状态通知
//            [self showMessageHud:@"登录成功"];
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:[NSNumber numberWithBool:YES]];
//            });
            
            [self loginEMob:[UserInfoManager shareInstance].userInfo.easemobId];
//            [self loaddefaultgroupinfo];
            
        }else{
            [self dismissHud];
            [self showMessageHud:@"登录失败"];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
