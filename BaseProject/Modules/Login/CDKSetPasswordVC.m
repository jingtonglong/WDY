//
//  CDKSetPasswordVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/7.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKSetPasswordVC.h"
#import "CDKFinishResumeVC.h"
@interface CDKSetPasswordVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *containerView1;
@property (weak, nonatomic) IBOutlet UIView *containerView2;
@property (weak, nonatomic) IBOutlet UIButton *comfirmBtn;
@property (weak, nonatomic) IBOutlet UITextField *tx_password;// 密码
@property (weak, nonatomic) IBOutlet UITextField *tx_passwordAgain;// 确认密码

@end

@implementation CDKSetPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title=@"设置密码";
    [YSUtil makeCornerRadius:21 view:self.containerView1];
    [YSUtil makeCornerRadius:21 view:self.containerView2];
    [YSUtil makeCornerRadius:21 view:self.comfirmBtn];
    [YSUtil makeBorderWidth:1 view:self.containerView1 borderColor:RGB(173,173,173)];
    [YSUtil makeBorderWidth:1 view:self.containerView2 borderColor:RGB(173,173,173)];
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.comfirmBtn.bounds;  // 设置显示的frame
    gradientLayer.colors = @[(id)RGB(69,213,160).CGColor,(id)RGB(112,223,182).CGColor];  // 设置渐变颜色
    //    gradientLayer.locations = @[@0.0, @0.2, @0.5];    // 颜色的起点位置，递增，并且数量跟颜色数量相等
    //
    [self.comfirmBtn.layer insertSublayer:gradientLayer atIndex:0];

    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0, 0, 40, 40);
    [btn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithCustomView:btn];
    btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=item;
    
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)comfirmClicked:(id)sender {
    if (self.tx_password.text.length==0) {
        [self showMessageHud:@"请输入密码"];
    }else if (self.tx_password.text.length<6){
        [self showMessageHud:@"密码长度不能低于6位"];
    }else if (![self.tx_password.text isEqualToString:self.tx_passwordAgain.text]){
        [self showMessageHud:@"两次密码不一致"];
    }else if (self.tx_password.text.length>16){
        [self showMessageHud:@"密码长度不能超过16位！"];
    } else{
        // 下一步
        [UserInfoManager shareInstance].loginPassword=self.tx_passwordAgain.text;
        
        CDKFinishResumeVC *vc=[[CDKFinishResumeVC alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@" "]) {
        return NO;
    }
    
    return YES;
  
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
