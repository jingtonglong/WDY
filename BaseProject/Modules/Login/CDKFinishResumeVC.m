//
//  CDKFinishResumeVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/7.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKFinishResumeVC.h"
#import "YSDatePicker.h"
#import "CDKHospitalList.h"
#import "CDKRegisterSuccessVC.h"
@interface CDKFinishResumeVC ()<UITextFieldDelegate,CDKHospitalListDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tx_name;
@property(nonatomic,strong) UIButton *sexBtn;
@property (weak, nonatomic) IBOutlet UIButton *sex_man;
@property(nonatomic,strong) UIButton *userTypeBtn;// 医生或护士
@property (weak, nonatomic) IBOutlet UIButton *usertType_doctor;

@property (weak, nonatomic) IBOutlet UILabel *birthday;

@property (weak, nonatomic) IBOutlet UITextField *tx_hospital;
@property (weak, nonatomic) IBOutlet UITableViewCell *hospitalCell;
@property(nonatomic,strong) YSDatePicker *pickerDate;
@property(nonatomic,strong) CDKHospitalList *hospitalTableView;
@property(nonatomic,strong) CDKHospital_List_Model *seleteHospital_model;
@property(nonatomic,strong) NSArray *hospitals;


@end

@implementation CDKFinishResumeVC
- (instancetype)init
{
    self = [super init];
    if (self) {
        UIStoryboard *st=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self=[st instantiateViewControllerWithIdentifier:@"CDKFinishResumeVC"];
    }
    return self;
}
-(YSDatePicker *)pickerDate{
    if (!_pickerDate) {
        _pickerDate=(YSDatePicker*)[UIView xibinstance:@"YSDatePicker"];
        WeakSelf(weak);
        _pickerDate.callback = ^(NSDate *date) {
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            formatter.dateFormat=@"yyyy-MM-dd";
            weak.birthday.text=[formatter stringFromDate:date];
        };
    }
    return _pickerDate;
}
-(CDKHospitalList *)hospitalTableView{
    if (!_hospitalTableView) {
        _hospitalTableView=[[CDKHospitalList alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.hospitalCell.frame), KScreenWidth, 200)];
        _hospitalTableView.backgroundColor=[UIColor whiteColor];
        _hospitalTableView.delegate=self;
       
    }
    return _hospitalTableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"完善资料";
    self.sexBtn=self.sex_man;
    self.userTypeBtn=self.usertType_doctor;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textfileChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0, 0, 40, 40);
    [btn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithCustomView:btn];
    btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=item;
    
}
-(void)back{
//    [self.navigationController popToRootViewControllerAnimated:NO];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)sex_btn_click:(UIButton*)sender {
    self.sexBtn.selected=NO;
    sender.selected=YES;
    self.sexBtn=sender;
}

- (IBAction)userType_btn_click:(UIButton*)sender {
    self.userTypeBtn.selected=NO;
    sender.selected=YES;
    self.userTypeBtn=sender;
}
-(void)textfileChanged:(NSNotification*)ntf{
    self.seleteHospital_model=nil;
    UITextRange *selectedRange = [self.tx_hospital markedTextRange];
    // 输入部分在变化 不处理
    if (selectedRange) {
        return;
    }
    if (self.tx_hospital.text.length>0) {
        
//        [self getHospitalList];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(getHospitalList) object:nil];
        [self performSelector:@selector(getHospitalList) withObject:nil afterDelay:1.0];
//        [self.tableView addSubview:self.hospitalTableView];
        
    }else{
        [self.hospitalTableView removeFromSuperview];
        self.hospitalTableView.datas=@[];
    }

    
}
- (IBAction)down_click:(id)sender {// 完成
    if (self.tx_name.text.length==0) {
        [self showMessageHud:@"请输入姓名"];
        return;
    }
    
//    if ([self.birthday.text isEqualToString:@"年/月/日"]) {
//        [self showMessageHud:@"请选择出生日期"];
//        return;
//    }
    if (!self.seleteHospital_model) {
        [self showMessageHud:@"请输入正确的医院"];
        return;
    }
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/user/Register";
    
    UserInfoManager *user=[UserInfoManager shareInstance];
    NSInteger sexindex=0;
    if (![self.sexBtn.currentTitle containsString:@"男"]) {
        sexindex=1;
    }
    NSInteger roleindex=0;
    if (![self.userTypeBtn.currentTitle containsString:@"医生"]) {
        roleindex=1;
    }
    NSDictionary *dic=@{@"loginPhone":user.loginAccount,
                        @"password":user.loginPassword,
                        @"password2":user.loginPassword,
                        @"name":self.tx_name.text,
                        @"sex":@(sexindex),
                        @"hospitalId":self.seleteHospital_model.id,
                        @"role":@(roleindex)
                        };
    NSMutableDictionary *mdic=[NSMutableDictionary dictionaryWithDictionary:dic];
    if ([self.birthday.text isEqualToString:@"年/月/日"]) {
        
        mdic[@"birthday"]=nil;
    }else{
        mdic[@"birthday"]=self.birthday.text;
    }
   
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:mdic success:^(id responseObject) {
        [self dismissHud];
        if(SUCCEESS){
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//            });
            CDKRegisterSuccessVC *vc=[[CDKRegisterSuccessVC alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
           
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
        
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)getHospitalList{
    NSString *api=@"/api/services/ckd/user/QueryHospitals";
    
    NSDictionary *dic=@{@"Keyword":self.tx_hospital.text
                        };
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:dic success:^(id responseObject) {
        if(SUCCEESS){
           
            NSArray *arr=[NSArray modelArrayWithClass:[CDKHospital_List_Model class] json:responseObject[@"result"]];
            self.hospitals=arr;
            if (arr.count>0) {
                [self.tableView addSubview:self.hospitalTableView];
                self.hospitalTableView.datas=arr;
            }else{
                [self.hospitalTableView removeFromSuperview];
//                [self showMessageHud:@"没有相关医院"];
            }

           
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
        
        
    } errorHandel:^(NSError *error) {
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.view endEditing:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
        if (indexPath.row==2) {// 出生日期
            [self.pickerDate showDatePiker];
        }
    
   
}
#pragma mark--CDKHospitalListDelegate
-(void)selectedHospital:(CDKHospital_List_Model *)model{
    self.tx_hospital.text=model.name;
    self.seleteHospital_model=model;
    [self.hospitalTableView removeFromSuperview];
}

#pragma markk
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"^[\u4e00-\u9fa5_a-zA-Z0-9➋➌➍➎➏➐➑➒]*$"];
    
    return [predicate evaluateWithObject:string];
  
    

}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField==self.tx_hospital) {
        if (!(self.hospitals.count>0)) {
//            [textField ]
            [self showMessageHud:@"没有相关医院"];
        }
    }
    YSLog(@"%@",textField.text);

}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
        
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
        
    }
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
        
    }
    
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//   
//    return 44;
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
