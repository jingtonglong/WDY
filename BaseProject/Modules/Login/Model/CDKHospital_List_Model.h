//
//  CDKHospital_List_Model.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKHospital_List_Model : NSObject
@property(nonatomic,copy) NSString *id;
@property(nonatomic,copy) NSString *areaCode;
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *grade;


@end

