//
//  CDKRegisterVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/7.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKRegisterVC.h"
#import "CDKProtecolVC.h"
#import "CDKSetPasswordVC.h"
@interface CDKRegisterVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *containerView1;
@property (weak, nonatomic) IBOutlet UIView *containerView2;
@property (weak, nonatomic) IBOutlet UITextField *tx_phone;
@property (weak, nonatomic) IBOutlet UITextField *tx_code;
@property (weak, nonatomic) IBOutlet UIButton *nextStep;
@property (weak, nonatomic) IBOutlet UIButton *aggreBtn;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
@property(nonatomic,strong) NSTimer *timer;
@property(nonatomic,assign) NSInteger timer_Counter;
@property(nonatomic,copy) NSString *codeStr;// 验证码



@end

@implementation CDKRegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title=@"注册账号";
    self.timer_Counter=60;
    [YSUtil makeCornerRadius:21 view:self.containerView1];
    [YSUtil makeCornerRadius:21 view:self.containerView2];
    [YSUtil makeCornerRadius:21 view:self.nextStep];
    [YSUtil makeBorderWidth:1 view:self.containerView1 borderColor:RGB(173,173,173)];
    [YSUtil makeBorderWidth:1 view:self.containerView2 borderColor:RGB(173,173,173)];
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.nextStep.bounds;  // 设置显示的frame
    gradientLayer.colors = @[(id)RGB(69,213,160).CGColor,(id)RGB(112,223,182).CGColor];  // 设置渐变颜色
    //    gradientLayer.locations = @[@0.0, @0.2, @0.5];    // 颜色的起点位置，递增，并且数量跟颜色数量相等
    //
    [self.nextStep.layer insertSublayer:gradientLayer atIndex:0];

}
- (IBAction)nextClicked:(id)sender {// 下一步
    if (![NSString isMobileNumber:self.tx_phone.text]) {
        [self showMessageHud:@"请输入正确的手机号"];
        return;
    }else if (self.tx_code.text.length==0){
        [self showMessageHud:@"请输入验证码"];
        return;
    }else if (self.tx_code.text!=self.codeStr){
        [self showMessageHud:@"验证码不正确"];
        return;
    }
    if (!self.aggreBtn.selected) {
        [self showMessageHud:@"未同意服务条款"];
        return;
    }
    // 下一步
    CDKSetPasswordVC *vc=[[CDKSetPasswordVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
- (IBAction)agree:(UIButton*)sender {
    sender.selected=!sender.selected;
}
- (IBAction)protecolClicked:(id)sender {//服务条款
    CDKProtecolVC *vc=[[CDKProtecolVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)getCheckingCode:(id)sender {// 获取验证码
    if (![NSString isMobileNumber:self.tx_phone.text]) {
        [self showMessageHud:@"请输入正确的手机号"];
        return;
    }
    [self.view endEditing:YES];
    NSString *api=@"/Token";
    
    NSDictionary *dic=@{@"grant_type":@"client_credentials",
                        @"client_id":@"ios_123",
                        @"client_secret":@"123123"
                        };
    //客户端授权（加载非用户数据）
    [self showLoadingHud:nil];
    [YSNetworkingManager requestWithUrl:api :POST paramiters:dic success:^(id responseObject) {
        if(responseObject[@"error"]){
            [self dismissHud];
            [self showMessageHud:responseObject[@"error_description"]];
        }else{
            
        [UserInfoManager shareInstance].tokenInfo=[CDKTokenInfo modelWithJSON:responseObject];
        [self getCode];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
        
    }];
}
-(void)getCode{
    
    NSString *api=@"/api/services/app/user/CheckAndSendMobileCode";
    
    NSDictionary *dic=@{@"mobile":self.tx_phone.text
                        };
    //获取验证码
    self.checkBtn.userInteractionEnabled=NO;
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:dic success:^(id responseObject) {
        [self dismissHud];
        if([responseObject[@"success"] boolValue]){
            [self showMessageHud:@"发送验证码成功"];
            [UserInfoManager shareInstance].loginAccount=self.tx_phone.text;
            self.codeStr=responseObject[@"result"][@"code"];
            [self.checkBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            self.timer.fireDate = [NSDate distantPast];
            
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            self.checkBtn.userInteractionEnabled=YES;
        }
       
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
        self.checkBtn.userInteractionEnabled=YES;
    }];
    
}

-(NSTimer*)timer{
    
    if (!_timer) {
        _timer= [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
    }
    return _timer;
}
-(void)timerAction{
    [self.checkBtn setTitle:[NSString stringWithFormat:@"%ld秒后重发",self.timer_Counter--] forState:UIControlStateNormal];
    if (self.timer_Counter==-1) {
        
        [self.checkBtn setTitle:@"重发验证码" forState:UIControlStateNormal];
        self.checkBtn.userInteractionEnabled=YES;
        [self.checkBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        self.timer.fireDate=[NSDate distantFuture];
        self.timer_Counter=60;
    }
}
-(void)dealloc{
    
    YSLog(@"---");
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [self.timer invalidate];
    self.timer=nil;
    self.timer_Counter=60;
    [self.checkBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    self.checkBtn.userInteractionEnabled=YES;
    [self.checkBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (![NSString isNumber:string]) {
        return NO;
    }
    if (textField==self.tx_phone) {
       if((textField.text.length+string.length>11)){
            return NO;
        }
    }else{
        if ((textField.text.length+string.length>6)) {
            return NO;
        }
    }
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
