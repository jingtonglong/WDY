//
//  CDKFoodImagesModel.h
//  BaseProject
//
//  Created by QianYuZ on 2018/7/3.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKFoodImagesModel : NSObject

/** 图片di*/
@property (nonatomic,copy) NSString *id;

/** 地址*/
@property (nonatomic,copy) NSString *dietImagePath;
@end
