//
//  CDKPationtListModel.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/6.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKPationtListModel : NSObject
@property(nonatomic,copy) NSString *id;
@property(nonatomic,copy) NSString *headPicture;
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *sex;
@property(nonatomic,copy) NSString *brithday;
@property(nonatomic,copy) NSString *age;
@property(nonatomic,copy) NSString *lastVisitTime;
@property(nonatomic,copy) NSString *visitTime;
@property(nonatomic,copy) NSString *registerType;
@property(nonatomic,copy) NSString *remark;// 备注
@property(nonatomic,assign) BOOL isBefore;//--过去随访标识（0，过去；1 现在）
@end
