//
//  CDKAssessmentModel.h
//  BaseProject
//
//  Created by QianYuZ on 2018/7/30.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKAssessmentModel : NSObject

/** 左边文字*/
@property (nonatomic,copy) NSString *leftText;

/** 右边文字*/
@property (nonatomic,copy) NSString *rightText;

/** value*/
@property (nonatomic,copy) NSString *valueText;
@end
