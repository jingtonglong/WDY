//
//  CDKFoodTypeModel.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/18.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//
#import "CDKFoodModel.h"
#import <Foundation/Foundation.h>
// 饮食分类
@interface CDKFoodTypeModel : NSObject

/**
 饮食分类id
 */
@property(nonatomic,copy) NSString *id;
/**
 饮食分类名字
 */
@property(nonatomic,copy) NSString *foodTypeName;

/**
 饮食分类图片
 */
@property(nonatomic,copy) NSString *imagePath;

/**
 大分类下的小分类
 */
@property(nonatomic,strong) NSArray *children;
@property(nonatomic,strong) NSArray *section_titles;


@end
