//
//  CDKFoodCellStytleModel.h
//  BaseProject
//
//  Created by QianYuZ on 2018/8/29.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKFoodCellStytleModel : NSObject

/** id*/
@property (nonatomic,copy) NSString *id;

/** header的名字*/
@property (nonatomic,copy) NSString *haveMealsTypeName;

/** 图片数量*/
@property (nonatomic,assign) NSInteger count;
@end
