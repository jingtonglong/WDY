//
//  CDKNutrationHistoryModel.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/15.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKNutrationHistoryModel : NSObject

@property(nonatomic,copy) NSString *id;
@property(nonatomic,copy) NSString *pHeight;
@property(nonatomic,copy) NSString *pWeight;
@property(nonatomic,copy) NSString *bim;

@property(nonatomic,copy) NSString *upperArm;
@property(nonatomic,copy) NSString *tricepsBrachii;
@property(nonatomic,copy) NSString *leftGrip;
@property(nonatomic,copy) NSString *rightGrip;

@property(nonatomic,copy) NSString *lowBlood;
@property(nonatomic,copy) NSString *highBlood;
@property(nonatomic,copy) NSString *assessmentTime;
@property(nonatomic,assign) NSInteger brId;


//id    是    int    评估id
//pHeight    是    float    身高
//pWeight    是    float    体重
//bmi    是    decimal    标准指数

//upperArm    是    decimal    上臂围
//tricepsBrachii    是    decimal    肱三头肌皮褶厚度
//leftGrip    是    decimal    左臂力
//rightGrip    是    decimal    右臂力

//lowBlood    是    decimal    血压低压
//highBlood    是    decimal    血压高压
//assessmentTime    是    string    评估日期
//BrId    0：未引用，可编辑，1：已引用，不可编辑，2：已过期，不可编辑

@end
