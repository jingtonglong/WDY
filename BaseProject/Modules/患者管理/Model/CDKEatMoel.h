//
//  CDKEatMoel.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKEatMoel : NSObject

@property(nonatomic,copy) NSString *energy;// 能量
@property(nonatomic,copy) NSString *protein;//蛋白质
@property(nonatomic,copy) NSString *potassium;// 钾
@property(nonatomic,copy) NSString *sodium; // 钠
@property(nonatomic,copy) NSString *phosphorus;//磷
@property(nonatomic,copy) NSString *calcium;// 钙
@property(nonatomic,copy) NSString *water;
@property(nonatomic,copy) NSString *basicTime;//饮食时间
@property(nonatomic,copy) NSString *id;//--饮食记录id
@property(nonatomic,copy) NSString *dietTime;//--

@property(nonatomic,copy) NSString *auditingState;//--审核状态



@end

