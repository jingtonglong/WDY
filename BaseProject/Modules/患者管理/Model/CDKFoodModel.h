//
//  CDKFoodModel.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/18.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKFoodModel : NSObject
@property(nonatomic,copy) NSString *id;//食物ID
@property(nonatomic,copy) NSString *foodName;
@property(nonatomic,copy) NSString *pyCode;//食物名称拼音
@property(nonatomic,copy) NSString *energy;
@property(nonatomic,copy) NSString *protein;
@property(nonatomic,copy) NSString *potassium;
@property(nonatomic,copy) NSString *sodium;

@property(nonatomic,copy) NSString *phosphorus;

@property(nonatomic,copy) NSString *calcium;
@property(nonatomic,copy) NSString *water;
@property(nonatomic,copy) NSString *foodTypeId;// 食物类型id
@property(nonatomic,copy) NSString *picPath;//食物图片
@property(nonatomic,copy) NSString *gram;// 食量（g）
@property(nonatomic,copy) NSString *haveMealsTypeId;
@property(nonatomic,copy) NSString *pinying;// 食物名称pinying



@end
