//
//  CDKPationInfoModel.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/7.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKPationInfoModel : NSObject
@property(nonatomic,copy) NSString *age;
@property(nonatomic,copy) NSString *cardNo;
@property(nonatomic,copy) NSString *education;
@property(nonatomic,copy) NSString *expenseType;
@property(nonatomic,copy) NSString *fullAddress;
@property(nonatomic,copy) NSString *headPicture;
@property(nonatomic,copy) NSString *id;
@property(nonatomic,copy) NSString *mainTreatment;
@property(nonatomic,copy) NSString *mobile;
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *orgDisease;
@property(nonatomic,copy) NSString *remarkCkd;
@property(nonatomic,copy) NSString *remarkCKD;
@property(nonatomic,copy) NSString *sex;
@property(nonatomic,copy) NSString *withDisease;
@property(nonatomic,copy) NSString *areaCode;
@property(nonatomic,copy) NSString *brithday;

@property(nonatomic,copy) NSString *loginPhone;

@property(nonatomic,copy) NSString *provinceCode;
@property(nonatomic,copy) NSString *cityCode;
@property(nonatomic,copy) NSString *registerType;






@end
