//
//  CDKFoodDietDetailsModel.h
//  BaseProject
//
//  Created by QianYuZ on 2018/6/18.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CDKHaveMealsTypeModel.h"
@interface CDKFoodDietDetailsModel : NSObject

/** 饮食记录详细id*/
@property (nonatomic,copy) NSString *id;

/** 用餐类型id*/
@property (nonatomic,copy) NSString *haveMealsTypeId;

/** 用餐类型*/
@property (nonatomic,copy) NSString *haveMealsTypeName;

/** 图片路径*/
@property (nonatomic,copy) NSString *dietImagePath;

/** 食物类型id*/
@property (nonatomic,copy) NSString *foodTypeId;

/** 食物类型*/
@property (nonatomic,copy) NSString *foodTypeName;

/** 食物id*/
@property (nonatomic,copy) NSString *foodId;

/** 食物*/
@property (nonatomic,copy) NSString *foodName;

/** 食量*/
@property (nonatomic,copy) NSString *gram;
@end
