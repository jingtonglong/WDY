//
//  CDKHaveMealsTypeModel.h
//  BaseProject
//
//  Created by QianYuZ on 2018/6/18.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
// 用餐类型

#import <Foundation/Foundation.h>
@interface CDKHaveMealsTypeModel : NSObject

/** 类型id*/
@property (nonatomic,copy) NSString *id;

/** 类型名称*/
@property (nonatomic,copy) NSString *haveMealsTypeName;

/** 数组*/
@property (nonatomic,strong) NSMutableArray *subArray;
@end
