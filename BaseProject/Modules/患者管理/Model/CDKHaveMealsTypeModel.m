//
//  CDKHaveMealsTypeModel.m
//  BaseProject
//
//  Created by QianYuZ on 2018/6/18.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKHaveMealsTypeModel.h"

@implementation CDKHaveMealsTypeModel
- (NSMutableArray *)subArray{
    if (!_subArray) {
        _subArray = [NSMutableArray array];
    }
    return _subArray;
}
@end
