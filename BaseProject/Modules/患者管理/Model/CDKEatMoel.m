//
//  CDKEatMoel.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKEatMoel.h"
//@property(nonatomic,copy) NSString *energy;// 能量
//@property(nonatomic,copy) NSString *protein;//蛋白质
//@property(nonatomic,copy) NSString *potassium;// 钾
//@property(nonatomic,copy) NSString *sodium; // 钠
//@property(nonatomic,copy) NSString *phosphorus;//磷
//@property(nonatomic,copy) NSString *calcium;// 钙
//@property(nonatomic,copy) NSString *water;
@implementation CDKEatMoel
-(NSString *)water{
   
    return [self floavaulestr:_water];
}
-(NSString *)calcium{
    return [self floavaulestr:_calcium];
}
-(NSString *)phosphorus{
    return [self floavaulestr:_phosphorus];
}
-(NSString *)sodium{
    return [self floavaulestr:_sodium];
}
-(NSString *)potassium{
    return [self floavaulestr:_potassium];
}
-(NSString *)protein{
    return [self floavaulestr:_protein];
}
-(NSString *)energy{
    return [self floavaulestr:_energy];
}
-(NSString*)floavaulestr:(NSString*)desolvStr{
    if (!desolvStr) {
        return nil;
    }
    NSArray *arr=[desolvStr componentsSeparatedByString:@"."];
    NSString *final=@"";
    if (arr.count>1) {
        NSString *last=arr.lastObject;
        NSString *firstr=[last substringToIndex:1];
        NSString *laststr=[last substringFromIndex:1];
        
        //        @"00 01 10 11"
        if (([firstr isEqualToString:@"0"])&&([laststr isEqualToString:@"0"])) {
            final=@"";
        }else if ((![firstr isEqualToString:@"0"])&&([laststr isEqualToString:@"0"])) {
            final=[NSString stringWithFormat:@"%@",firstr];
        }else if ((![firstr isEqualToString:@"0"])&&(![laststr isEqualToString:@"0"])){
            final=[NSString stringWithFormat:@"%@%@",firstr,laststr];
        }else{
            
            final=[NSString stringWithFormat:@"%@%@",firstr,laststr];
        }
    }
    NSString *s;
    if (final.length>0) {
        s=[NSString stringWithFormat:@"%@.%@",arr.firstObject,final];
    }else{
        s=[NSString stringWithFormat:@"%@",arr.firstObject];
    }
    return s;
}
@end
