//
//  CDKPrescribePrinceple.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/15.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKPrescribePrinceple.h"

@implementation CDKPrescribePrinceple

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+(CDKPrescribePrinceple*)xibinstanse{
    NSArray *arr=[[NSBundle mainBundle] loadNibNamed:@"CDKPrescribePrinceple" owner:nil options:nil];
    return arr.firstObject;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    [YSUtil makeCornerRadius:6 view:self];
    [YSUtil makeBorderWidth:1 view:self borderColor:RGB(213,236,223)];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openOrColse)];
    [self.topView addGestureRecognizer:tap];
}
-(void)openOrColse{
    self.openOrCloseBtn.selected=!self.openOrCloseBtn.selected;
    if (self.delegate) {
        [self.delegate openOrClosePricelpleView:self.openOrCloseBtn];
    }
}
@end
