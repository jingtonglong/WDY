//
//  CDKPationtManagerInfoCell.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKPationInfoModel.h"

//typedef void(^openOrclolse)(void);
#import "CDKPationtListModel.h"
@interface CDKPationtManagerInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *remark;
@property (weak, nonatomic) IBOutlet UILabel *treat_way;
@property (weak, nonatomic) IBOutlet UILabel *follow_desease;
@property (weak, nonatomic) IBOutlet UILabel *origin_desease;
@property (weak, nonatomic) IBOutlet UILabel *education;
@property (weak, nonatomic) IBOutlet UILabel *submit_way;// 报销方式
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *identify_id;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *age;
@property (weak, nonatomic) IBOutlet UILabel *sex;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containtinfoheight;
@property (weak, nonatomic) IBOutlet UIStackView *infocontaintView;
@property (weak, nonatomic) IBOutlet UIButton *arrowBtn;
@property(nonatomic,copy) void(^callback)(void);
@property(nonatomic,strong) CDKPationtListModel *infoModel;
@property(nonatomic,strong) CDKPationInfoModel *pation_info_model;

@end
