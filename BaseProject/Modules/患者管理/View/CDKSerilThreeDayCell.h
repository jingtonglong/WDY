//
//  CDKSerilThreeDayCell.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKSerilThreeDayCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *containt1;
@property (weak, nonatomic) IBOutlet UIView *containt2;
@property (weak, nonatomic) IBOutlet UIView *containt3;
@property (weak, nonatomic) IBOutlet UIView *containt4;
@property (weak, nonatomic) IBOutlet UIView *containt5;
@property (weak, nonatomic) IBOutlet UIView *containt6;
@property (weak, nonatomic) IBOutlet UIView *containt7;
@property (weak, nonatomic) IBOutlet UILabel *energe;
@property (weak, nonatomic) IBOutlet UILabel *protien;
@property (weak, nonatomic) IBOutlet UILabel *potassium;
@property (weak, nonatomic) IBOutlet UILabel *sodium;
@property (weak, nonatomic) IBOutlet UILabel *phosphorus;
@property (weak, nonatomic) IBOutlet UILabel *calcium;
@property (weak, nonatomic) IBOutlet UILabel *water;
@end
