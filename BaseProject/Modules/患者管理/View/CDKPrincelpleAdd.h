//
//  CDKPrincelpleAdd.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//
@protocol CDKPrincelpleAddViewDelegate<NSObject>
-(void)openOrCloseCDKPrincelpleAddView:(UIButton*)sender;
@end
#import <UIKit/UIKit.h>
// 新增处方原则
@interface CDKPrincelpleAdd : UIView
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *openOrCloseBtn;
@property(nonatomic,weak) id<CDKPrincelpleAddViewDelegate> delegate;

+(CDKPrincelpleAdd*)xibinstanse;
@end
