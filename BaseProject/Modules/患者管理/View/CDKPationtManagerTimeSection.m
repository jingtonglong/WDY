//
//  CDKPationtManagerTimeSection.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKPationtManagerTimeSection.h"

@implementation CDKPationtManagerTimeSection
-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor=RGB(247,247,247);
        _lab=[UILabel new];
        _lab.textColor=RGB(154,154,154);
        _lab.font=SYSTEMFONT(14);
        _lab.frame=CGRectMake(13, 0, KScreenWidth, 24);
        [self addSubview:_lab];
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
