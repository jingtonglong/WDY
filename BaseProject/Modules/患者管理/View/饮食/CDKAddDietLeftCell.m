//
//  CDKAddDietLeftCell.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/15.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKAddDietLeftCell.h"

@implementation CDKAddDietLeftCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
