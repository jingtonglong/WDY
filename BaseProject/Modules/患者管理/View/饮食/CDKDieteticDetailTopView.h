//
//  CDKDieteticDetailTopView.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DieteticDetailTopViewBlock)(NSString *chooseId,NSString *updateTime);

@interface CDKDieteticDetailTopView : UIView
- (instancetype)initWithFrame:(CGRect)frame withTime:(NSArray *)timeArray idArray:(NSArray *)idArray timeYearMonthDay:(NSArray *)timeArrayYMD selectedDaty:(NSString *)selectedDay;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIView *chooseDateView;
@property (weak, nonatomic) IBOutlet UILabel *chooseDateLabel;
@property (weak, nonatomic) IBOutlet UIView *sunView;
@property (weak, nonatomic) IBOutlet UILabel *sunLabel;
@property (weak, nonatomic) IBOutlet UIButton *subButton;
@property (weak, nonatomic) IBOutlet UIView *monView;
@property (weak, nonatomic) IBOutlet UILabel *monLabel;
@property (weak, nonatomic) IBOutlet UIButton *monButton;
@property (weak, nonatomic) IBOutlet UIView *tueView;
@property (weak, nonatomic) IBOutlet UILabel *tueLabel;
@property (weak, nonatomic) IBOutlet UIButton *tueButton;
@property (weak, nonatomic) IBOutlet UIView *wedView;
@property (weak, nonatomic) IBOutlet UILabel *wedLabel;
@property (weak, nonatomic) IBOutlet UIButton *wedButton;
@property (weak, nonatomic) IBOutlet UIView *thuView;
@property (weak, nonatomic) IBOutlet UILabel *thuLabel;
@property (weak, nonatomic) IBOutlet UIButton *thuButton;
@property (weak, nonatomic) IBOutlet UIView *friView;
@property (weak, nonatomic) IBOutlet UILabel *friLabel;
@property (weak, nonatomic) IBOutlet UIButton *friButton;
@property (weak, nonatomic) IBOutlet UIView *staView;
@property (weak, nonatomic) IBOutlet UILabel *staLabel;
@property (weak, nonatomic) IBOutlet UIButton *staButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backButtonTopConstant;

/** 时间数组*/
@property (nonatomic,copy) NSArray *timeArray;
@property (nonatomic,copy) NSArray *timeArrayYMD;

/** 回调*/
@property (nonatomic,copy) DieteticDetailTopViewBlock block;

/** 检查id 数组*/
@property (nonatomic,copy) NSArray *idArray;

/** 上个页面选择的时间*/
@property (nonatomic,copy) NSString *selectedDay;
@end
