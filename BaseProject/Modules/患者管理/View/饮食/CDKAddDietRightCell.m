//
//  CDKAddDietRightCell.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/15.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKAddDietRightCell.h"

@implementation CDKAddDietRightCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.reduceButton addTarget:self action:@selector(clickReduceButton) forControlEvents:UIControlEventTouchUpInside];
    [self.addButton addTarget:self action:@selector(clickAddButton) forControlEvents:UIControlEventTouchUpInside];
    self.textField.delegate=self;
}

- (void)clickReduceButton{
    if (self.textField.text.length > 0) {
        NSInteger num = [self.textField.text integerValue] - 1;
        if (num < 0) {
            self.textField.text = @"";
        }else{
            self.textField.text = [NSString stringWithFormat:@"%ld",num];
        }
        self.model.gram=self.textField.text;
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (![NSString isNumber:string]) {
        return NO;
    }
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.model.gram=textField.text;
}
- (void)clickAddButton{
    if (self.textField.text.length > 0) {
        NSInteger num = [self.textField.text integerValue] + 1;
        self.textField.text = [NSString stringWithFormat:@"%ld",num];
        self.model.gram=self.textField.text;
    }
}
-(void)setModel:(CDKFoodModel *)model{
    _model=model;
    self.rightLabel.text=model.foodName;
    self.textField.text=model.gram;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
