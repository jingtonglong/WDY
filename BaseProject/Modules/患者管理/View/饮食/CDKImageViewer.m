//
//  CDKImageViewer.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/18.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKImageViewer.h"
#import "CDKImageViewerCell.h"
@implementation CDKImageViewer

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor=[UIColor whiteColor];
        [self initUI];
    }
    return self;
}
-(UICollectionView *)topCollectionView{
    if (!_topCollectionView) {
        UICollectionViewFlowLayout *layout=[UICollectionViewFlowLayout new];
        layout.itemSize=CGSizeMake(KScreenWidth-20, KScreenHeight-NavHeight-45-100-iPhoneX_BOTTOM_HEIGHT);
        layout.scrollDirection=UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing=10;
        layout.sectionInset=UIEdgeInsetsMake(0, 10, 0, 10);
        _topCollectionView=[[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _topCollectionView.delegate=self;
        _topCollectionView.dataSource=self;
//        _topCollectionView.pagingEnabled=YES;
        _topCollectionView.backgroundColor=[UIColor whiteColor];
       _topCollectionView.decelerationRate = UIScrollViewDecelerationRateFast; _topCollectionView.showsHorizontalScrollIndicator=NO;
        [_topCollectionView registerNib:[UINib nibWithNibName:@"CDKImageViewerCell" bundle:nil] forCellWithReuseIdentifier:@"topCell"];
       
    }
    return _topCollectionView;
}
-(UICollectionView *)bottomCollectionView{
    if (!_bottomCollectionView) {
        UICollectionViewFlowLayout *layout=[UICollectionViewFlowLayout new];
        layout.itemSize=CGSizeMake(90, 90);
        layout.scrollDirection=UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing=9;
        layout.sectionInset=UIEdgeInsetsMake(0, 10, 0, 10);
        _bottomCollectionView=[[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _bottomCollectionView.delegate=self;
        _bottomCollectionView.dataSource=self;
        _bottomCollectionView.backgroundColor=[UIColor whiteColor];
        _bottomCollectionView.showsHorizontalScrollIndicator=NO;
        [_bottomCollectionView registerNib:[UINib nibWithNibName:@"CDKImageViewerCell" bundle:nil] forCellWithReuseIdentifier:@"bottomCell"];

//        _bottomCollectionView.pagingEnabled=YES;
    }
    return _bottomCollectionView;
}
-(void)initUI{
    UILabel *title=[UILabel new];
    title.font=SYSTEMFONT(15);
    title.textColor=RGB(101,101,101);
    title.text=@"图片预览";
    [self addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(0);
    }];
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"关闭" forState:UIControlStateNormal];
    [btn setTitleColor:RGB(101,101,101) forState:UIControlStateNormal];
    btn.titleLabel.font=SYSTEMFONT(15);
    [self addSubview:btn];
    [btn addTarget:self action:@selector(clear) forControlEvents:UIControlEventTouchUpInside];

    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(title);
        make.right.mas_equalTo(-10);
        
    }];
    
    [self addSubview:self.bottomCollectionView];
    [self.bottomCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-iPhoneX_BOTTOM_HEIGHT-10);
        make.height.mas_equalTo(90);
    }];
    [self addSubview:self.topCollectionView];
    [self.topCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.equalTo(self.bottomCollectionView.mas_top).offset(-10);
        make.top.equalTo(title.mas_bottom);
    }];
    UIView *indicator=[[UIView alloc] init];
    indicator.backgroundColor=[UIColor clearColor];
    [self addSubview:indicator];
    [indicator mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.topCollectionView.mas_bottom);
        make.height.mas_equalTo(30);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        
    }];
    UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:effect];
    effectView.alpha=0.6;
    [indicator addSubview:effectView];
    [effectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(indicator);
    }];
    UILabel *control_title=[UILabel new];
    control_title.text=@"左右翻动";
    control_title.textColor=[UIColor whiteColor];
    control_title.font=SYSTEMFONT(14);
    [indicator addSubview:control_title];
    [control_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(indicator);
    }];
    UIButton *left=[UIButton buttonWithType:UIButtonTypeCustom];
    [left setImage:[UIImage imageNamed:@"左翻页"] forState:UIControlStateNormal];
    [indicator addSubview:left];
    [left mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(title.mas_left).offset(-30);
        make.height.mas_equalTo(30);
        make.centerY.equalTo(indicator);
    }];
    [left addTarget:self action:@selector(scrollLeft) forControlEvents:UIControlEventTouchUpInside];
    UIButton *right=[UIButton buttonWithType:UIButtonTypeCustom];
    [right setImage:[UIImage imageNamed:@"右翻页"] forState:UIControlStateNormal];
        [right addTarget:self action:@selector(scrollright) forControlEvents:UIControlEventTouchUpInside];
    [indicator addSubview:right];
    [right mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(title.mas_right).offset(30);
        make.centerY.equalTo(indicator);

        make.height.mas_equalTo(30);
    }];
}
-(UIView *)coverView{
    if (!_coverView) {
        _coverView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
        _coverView.backgroundColor=[UIColor blackColor];
        _coverView.alpha=0.4;
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clear)];
        [_coverView addGestureRecognizer:tap];
    }
    return _coverView;
}

-(void)clear{
    
    [self removeFromSuperview];
    [self.coverView removeFromSuperview];
  
}
-(void)showImageViewer:(UIView*)view{
    [view addSubview:self.coverView];
    
    self.frame=CGRectMake(0, NavHeight, KScreenWidth,KScreenHeight- NavHeight);
    
    [view addSubview:self];
//    [UIView animateWithDuration:0.25 animations:^{
//        self.alpha=1;
//
//    }];
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.imgDatas.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView==self.topCollectionView) {
        CDKImageViewerCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"topCell" forIndexPath:indexPath];
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:self.imgDatas[indexPath.item]]];
        return cell;
    }else{
        CDKImageViewerCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"bottomCell" forIndexPath:indexPath];
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:self.imgDatas[indexPath.item]]];

        return cell;
    }
   
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView==self.bottomCollectionView) {
        [self.topCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:(UICollectionViewScrollPositionCenteredHorizontally) animated:NO];
    }
}
- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    if (scrollView==self.bottomCollectionView) {
        return;
    }
    CGFloat offSetX = targetContentOffset->x; //偏移量
    
    CGFloat itemWidth = KScreenWidth-20;   //itemSizem 的宽
    
    //itemSizem的宽度+行间距 = 页码的宽度
    
    NSInteger pageWidth = itemWidth + 10;
    
    
    
    NSInteger pageNum = (offSetX+pageWidth/2)/pageWidth;
    
    //根据显示的第几页,从而改变偏移量
    
    targetContentOffset->x = pageNum*pageWidth;
 
    
}
-(void)scrollLeft{
    NSArray *indexs=self.topCollectionView.indexPathsForVisibleItems;
    NSIndexPath *indexpath=indexs.firstObject;
    if (indexpath.row>0) {
        NSInteger index=indexpath.row-1;
        NSIndexPath *newindexpath=[NSIndexPath indexPathForItem:index inSection:0];
        [self.topCollectionView scrollToItemAtIndexPath:newindexpath atScrollPosition:(UICollectionViewScrollPositionCenteredHorizontally) animated:YES];
    }
}
-(void)scrollright{
    NSArray *indexs=self.topCollectionView.indexPathsForVisibleItems;
    NSIndexPath *indexpath=indexs.firstObject;
    if (indexpath.row<self.imgDatas.count-1) {
        NSInteger index=indexpath.row+1;
        NSIndexPath *newindexpath=[NSIndexPath indexPathForItem:index inSection:0];
        [self.topCollectionView scrollToItemAtIndexPath:newindexpath atScrollPosition:(UICollectionViewScrollPositionCenteredHorizontally) animated:YES];
    }
}

@end
