//
//  CDKCDKDieteticDetailHeader.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKFoodCellStytleModel.h"
@interface CDKDieteticDetailHeader : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property(nonatomic,copy) void(^callback)(NSInteger);
@property(nonatomic,assign) NSInteger section;
@property (weak, nonatomic) IBOutlet UIButton *rightImageButton;

@property(nonatomic,strong) CDKFoodCellStytleModel *stytleModel;
@end
