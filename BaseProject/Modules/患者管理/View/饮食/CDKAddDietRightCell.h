//
//  CDKAddDietRightCell.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/15.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKFoodModel.h"
@interface CDKAddDietRightCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UIButton *reduceButton;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property(nonatomic,strong) CDKFoodModel *model;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftLabelWidth;

@end
