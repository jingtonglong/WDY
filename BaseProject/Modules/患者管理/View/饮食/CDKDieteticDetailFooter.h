//
//  CDKDieteticDetailFooter.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKDieteticDetailFooter : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet UIButton *footerButton;
@property(nonatomic,copy) void(^callback)(void);

@end
