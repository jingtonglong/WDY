//
//  CDKDieteticDetailCell.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKFoodDietDetailsModel.h"
typedef void(^DieteticDetailBlock)(CDKFoodDietDetailsModel *blockModel);

@interface CDKDieteticDetailCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UITextField *numberTextfield;


/** 模型*/
@property (nonatomic,strong) CDKFoodDietDetailsModel *model;
/** 回调*/
@property (nonatomic,copy) DieteticDetailBlock block;

@end
