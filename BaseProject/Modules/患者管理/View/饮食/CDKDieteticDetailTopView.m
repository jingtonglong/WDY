//
//  CDKDieteticDetailTopView.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKDieteticDetailTopView.h"
#import <YYKit/NSDate+YYAdd.h>
@implementation CDKDieteticDetailTopView

- (instancetype)initWithFrame:(CGRect)frame withTime:(NSArray *)timeArray idArray:(NSArray *)idArray timeYearMonthDay:(NSArray *)timeArrayYMD selectedDaty:(NSString *)selectedDay{
    self =  [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"CDKDieteticDetailTopView" owner:self options:nil] lastObject];
        self.frame = frame;
        
        NSString *version= [UIDevice currentDevice].systemVersion;
        
        if(version.doubleValue >=11.0) {

        }else{
            self.backButtonTopConstant.constant = 25;
        }
        
        self.chooseDateLabel.text = selectedDay;
        
        
//        self.idArray = idArray;
//        self.timeArray = timeArray;
//        self.timeArrayYMD = timeArrayYMD;
        self.selectedDay = selectedDay;
        
        self.idArray = [[idArray reverseObjectEnumerator] allObjects];
        self.timeArray = [[timeArray reverseObjectEnumerator] allObjects];
        self.timeArrayYMD = [[timeArrayYMD reverseObjectEnumerator] allObjects];
        
        [self color:frame];

        NSArray *buttonArray = @[self.subButton,self.monButton,self.tueButton,self.wedButton,self.thuButton,self.friButton,self.staButton];
        
        NSArray *labelArray = @[self.sunLabel,self.monLabel,self.tueLabel,self.wedLabel,self.thuLabel,self.friLabel,self.staLabel];
        
        buttonArray = [[buttonArray reverseObjectEnumerator] allObjects];
//        labelArray = [[labelArray reverseObjectEnumerator] allObjects];
        
        NSMutableArray *weekArray = [NSMutableArray array];
        for (NSString *time in self.timeArrayYMD) {
            NSDate *date = [NSDate dateWithFormat:time];
            NSString *week = [self weekdayStringFromDate:date];
            [weekArray addObject:week];
        }
        
        for (int i = 0; i < weekArray.count; i ++) {
            [labelArray[i] setText:weekArray[i]];
        }
        for (int i = 0; i < timeArray.count; i ++) {
            [buttonArray[i] setTitle:timeArray[i] forState:UIControlStateNormal];
        }

        [self.subButton addTarget:self action:@selector(clickSubButton) forControlEvents:UIControlEventTouchUpInside];
        [self.monButton addTarget:self action:@selector(clickMonButton) forControlEvents:UIControlEventTouchUpInside];
        [self.tueButton addTarget:self action:@selector(clickTueButton) forControlEvents:UIControlEventTouchUpInside];
        [self.wedButton addTarget:self action:@selector(clickWedButton) forControlEvents:UIControlEventTouchUpInside];
        [self.thuButton addTarget:self action:@selector(clickThuButton) forControlEvents:UIControlEventTouchUpInside];
        [self.friButton addTarget:self action:@selector(clickFriButton) forControlEvents:UIControlEventTouchUpInside];
        [self.staButton addTarget:self action:@selector(clickStaButton) forControlEvents:UIControlEventTouchUpInside];

        [self getNowWeekByChangeLabelColor];
    }
    return self;
}

- (void)clickSubButton{
    
    [self selectedButtonStyle:self.subButton label:self.sunLabel];
    
    [self unSelectedButtonStyle:self.monButton label:self.monLabel];
    [self unSelectedButtonStyle:self.tueButton label:self.tueLabel];
    [self unSelectedButtonStyle:self.wedButton label:self.wedLabel];
    [self unSelectedButtonStyle:self.thuButton label:self.thuLabel];
    [self unSelectedButtonStyle:self.friButton label:self.friLabel];
    [self unSelectedButtonStyle:self.staButton label:self.staLabel];
    
    
    [self getNowWeekByChangeLabelColor];
    
    if (self.block) {
        self.block(self.idArray[0], self.timeArrayYMD[0]);
    }
    
    self.chooseDateLabel.text = self.timeArrayYMD[0];
//    for (UIButton *button in self.subviews) {
//        if (![button isEqual:self.subButton]) {
//            [button setBackgroundColor:[UIColor whiteColor]];
//            [button setTitleColor:[UIColor colorWithHexString:@"#9BF7C4"] forState:UIControlStateNormal];
//            [YSUtil makeCornerRadius:self.subButton.height / 2 view:self.subButton];
//        }
//    }
}
- (void)clickMonButton{
    [self selectedButtonStyle:self.monButton label:self.monLabel];
    
    [self unSelectedButtonStyle:self.subButton label:self.sunLabel];
    [self unSelectedButtonStyle:self.tueButton label:self.tueLabel];
    [self unSelectedButtonStyle:self.wedButton label:self.wedLabel];
    [self unSelectedButtonStyle:self.thuButton label:self.thuLabel];
    [self unSelectedButtonStyle:self.friButton label:self.friLabel];
    [self unSelectedButtonStyle:self.staButton label:self.staLabel];
    
    [self getNowWeekByChangeLabelColor];
    
    if (self.block) {
        self.block(self.idArray[1], self.timeArrayYMD[1]);
    }
    self.chooseDateLabel.text = self.timeArrayYMD[1];
}

- (void)clickTueButton{
    [self selectedButtonStyle:self.tueButton label:self.tueLabel];
    
    [self unSelectedButtonStyle:self.subButton label:self.sunLabel];
    [self unSelectedButtonStyle:self.monButton label:self.monLabel];
    [self unSelectedButtonStyle:self.wedButton label:self.wedLabel];
    [self unSelectedButtonStyle:self.thuButton label:self.thuLabel];
    [self unSelectedButtonStyle:self.friButton label:self.friLabel];
    [self unSelectedButtonStyle:self.staButton label:self.staLabel];
    
    [self getNowWeekByChangeLabelColor];
    
    if (self.block) {
        self.block(self.idArray[2], self.timeArrayYMD[2]);
    }
    self.chooseDateLabel.text = self.timeArrayYMD[2];
}

- (void)clickWedButton{
    [self selectedButtonStyle:self.wedButton label:self.wedLabel];
    
    [self unSelectedButtonStyle:self.subButton label:self.sunLabel];
    [self unSelectedButtonStyle:self.monButton label:self.monLabel];
    [self unSelectedButtonStyle:self.tueButton label:self.tueLabel];
    [self unSelectedButtonStyle:self.thuButton label:self.thuLabel];
    [self unSelectedButtonStyle:self.friButton label:self.friLabel];
    [self unSelectedButtonStyle:self.staButton label:self.staLabel];
    
    [self getNowWeekByChangeLabelColor];
    
    if (self.block) {
        self.block(self.idArray[3], self.timeArrayYMD[3]);
    }
    self.chooseDateLabel.text = self.timeArrayYMD[3];
}

- (void)clickThuButton{
    [self selectedButtonStyle:self.thuButton label:self.thuLabel];
    
    [self unSelectedButtonStyle:self.subButton label:self.sunLabel];
    [self unSelectedButtonStyle:self.monButton label:self.monLabel];
    [self unSelectedButtonStyle:self.tueButton label:self.tueLabel];
    [self unSelectedButtonStyle:self.wedButton label:self.wedLabel];
    [self unSelectedButtonStyle:self.friButton label:self.friLabel];
    [self unSelectedButtonStyle:self.staButton label:self.staLabel];
    
    [self getNowWeekByChangeLabelColor];
    
    if (self.block) {
        self.block(self.idArray[4], self.timeArrayYMD[4]);
    }
    self.chooseDateLabel.text = self.timeArrayYMD[4];
}

- (void)clickFriButton{
    [self selectedButtonStyle:self.friButton label:self.friLabel];
    
    [self unSelectedButtonStyle:self.subButton label:self.sunLabel];
    [self unSelectedButtonStyle:self.monButton label:self.monLabel];
    [self unSelectedButtonStyle:self.tueButton label:self.tueLabel];
    [self unSelectedButtonStyle:self.wedButton label:self.wedLabel];
    [self unSelectedButtonStyle:self.thuButton label:self.thuLabel];
    [self unSelectedButtonStyle:self.staButton label:self.staLabel];
    
    [self getNowWeekByChangeLabelColor];
    
    if (self.block) {
        self.block(self.idArray[5], self.timeArrayYMD[5]);
    }
    self.chooseDateLabel.text = self.timeArrayYMD[5];
}

- (void)clickStaButton{
    [self selectedButtonStyle:self.staButton label:self.staLabel];
    
    [self unSelectedButtonStyle:self.subButton label:self.sunLabel];
    [self unSelectedButtonStyle:self.monButton label:self.monLabel];
    [self unSelectedButtonStyle:self.tueButton label:self.tueLabel];
    [self unSelectedButtonStyle:self.wedButton label:self.wedLabel];
    [self unSelectedButtonStyle:self.thuButton label:self.thuLabel];
    [self unSelectedButtonStyle:self.friButton label:self.friLabel];
    
    [self getNowWeekByChangeLabelColor];
    
    if (self.block) {
        self.block(self.idArray[6], self.timeArrayYMD[6]);
    }
    self.chooseDateLabel.text = self.timeArrayYMD[6];
}

/**
 选中的当天

 @param button 选中的button样式
 @param label 选中label的样式
 */
- (void)selectedButtonStyle:(UIButton *)button label:(UILabel *)label{
    [button setBackgroundColor:[UIColor whiteColor]];
    [button setTitleColor:[UIColor colorWithHexString:@"#9BF7C4"] forState:UIControlStateNormal];
    [YSUtil makeCornerRadius:button.height / 2 view:button];
    label.textColor = [UIColor whiteColor];
}

/**
 非选中

 @param button 非选中button的样式
 @param label 非选中label的样式
 */
- (void)unSelectedButtonStyle:(UIButton *)button label:(UILabel *)label{
    [button setBackgroundColor:[UIColor clearColor]];
    [button setTitleColor:[UIColor colorWithHexString:@"#9BF7C4"] forState:UIControlStateNormal];
    [YSUtil makeCornerRadius:button.height / 2 view:button];
    label.textColor = [UIColor colorWithHexString:@"#9BF7C4"];
}

/**
 给当天lable改变颜色
 */
- (void)getNowWeekByChangeLabelColor{
    NSDate *date = [NSDate dateWithFormat:self.selectedDay];
    NSString *week = [self weekdayStringFromDate:date];
    
    if ([week isEqualToString:self.sunLabel.text]) {
        [YSUtil makeBorderWidth:1 view:self.subButton borderColor:[UIColor whiteColor]];
        [YSUtil makeCornerRadius:self.subButton.height / 2 view:self.subButton];
    }else if ([week isEqualToString:self.monLabel.text]){
        [YSUtil makeBorderWidth:1 view:self.monButton borderColor:[UIColor whiteColor]];
        [YSUtil makeCornerRadius:self.monButton.height / 2 view:self.monButton];
    }else if ([week isEqualToString:self.tueLabel.text]){
        [YSUtil makeBorderWidth:1 view:self.tueButton borderColor:[UIColor whiteColor]];
        [YSUtil makeCornerRadius:self.tueButton.height / 2 view:self.tueButton];
    }else if ([week isEqualToString:self.wedLabel.text]){
        [YSUtil makeBorderWidth:1 view:self.wedButton borderColor:[UIColor whiteColor]];
        [YSUtil makeCornerRadius:self.wedButton.height / 2 view:self.wedButton];
    }else if ([week isEqualToString:self.thuLabel.text]){
        [YSUtil makeBorderWidth:1 view:self.thuButton borderColor:[UIColor whiteColor]];
        [YSUtil makeCornerRadius:self.thuButton.height / 2 view:self.thuButton];
    }else if ([week isEqualToString:self.friLabel.text]){
        [YSUtil makeBorderWidth:1 view:self.friButton borderColor:[UIColor whiteColor]];
        [YSUtil makeCornerRadius:self.friButton.height / 2 view:self.friButton];
    }else if ([week isEqualToString:self.staLabel.text]){
        [YSUtil makeBorderWidth:1 view:self.staButton borderColor:[UIColor whiteColor]];
        [YSUtil makeCornerRadius:self.staButton.height / 2 view:self.staButton];
    }
}


// 当天星期几
- (NSString *)weekdayStringFromDate:(NSDate*)inputDate {
    NSArray *weekdays = [NSArray arrayWithObjects: [NSNull null], @"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六", nil];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/SuZhou"];
    [calendar setTimeZone: timeZone];
    NSCalendarUnit calendarUnit = NSWeekdayCalendarUnit;
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:inputDate];
    return [weekdays objectAtIndex:theComponents.weekday];
}

- (void)color:(CGRect)colorFrame{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    //设置开始和结束位置(设置渐变的方向)
    gradient.startPoint = CGPointMake(0, 1);
    gradient.endPoint = CGPointMake(1, 0);
    gradient.frame = colorFrame;
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColor colorWithHexString:@"#63DCAF"].CGColor,(id)[UIColor colorWithHexString:@"#62E496"].CGColor,nil];
    [self.layer insertSublayer:gradient atIndex:0];
}

@end
