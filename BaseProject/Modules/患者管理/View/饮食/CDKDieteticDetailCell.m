//
//  CDKDieteticDetailCell.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKDieteticDetailCell.h"

@implementation CDKDieteticDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.deleteButton addTarget:self action:@selector(clickDeleteButton) forControlEvents:UIControlEventTouchUpInside];
    self.numberTextfield.delegate = self;
}

- (void)clickDeleteButton{
    if (self.block) {
        self.block(self.model);
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
     self.model.gram = textField.text;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
