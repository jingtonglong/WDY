//
//  CDKDieteticDetailFooter.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKDieteticDetailFooter.h"

@implementation CDKDieteticDetailFooter

- (void)awakeFromNib{
    [super awakeFromNib];
    self.backgroundView = ({
        UIView * view = [[UIView alloc] initWithFrame:self.bounds];
        view.backgroundColor = [UIColor whiteColor];
        view;
    });
    
}
- (IBAction)adddirect:(id)sender {
    if (self.callback) {
        self.callback();
    }
}

@end
