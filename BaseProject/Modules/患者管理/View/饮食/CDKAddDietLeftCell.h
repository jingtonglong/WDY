//
//  CDKAddDietLeftCell.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/15.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKAddDietLeftCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@end
