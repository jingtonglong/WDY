//
//  CDKCDKDieteticDetailHeader.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKDieteticDetailHeader.h"

@implementation CDKDieteticDetailHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)previewImage:(id)sender {
    if (self.callback) {
        self.callback(self.section);
    }
}

-(void)setStytleModel:(CDKFoodCellStytleModel *)stytleModel{
    _stytleModel = stytleModel;
    if (stytleModel.count > 0) {
        [self.rightImageButton setImage:[UIImage imageNamed:@"图片预览"] forState:UIControlStateNormal];
    }else{
        [self.rightImageButton setImage:[UIImage imageNamed:@"yulan_hui"] forState:UIControlStateNormal];
    }
}


@end
