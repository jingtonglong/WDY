//
//  CDKImageViewer.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/18.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKImageViewer : UIView<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>
@property(nonatomic,strong) UICollectionView *topCollectionView;
@property(nonatomic,strong) UICollectionView *bottomCollectionView;
@property(nonatomic,strong) NSArray *imgDatas;
@property(nonatomic,strong) UIView *coverView;

-(void)showImageViewer:(UIView*)view;

@end
