//
//  CDKImageViewerCell.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/18.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKImageViewerCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end
