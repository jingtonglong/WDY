//
//  CDKDeseaseSituaionAdd.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/16.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
// 新增患病情况

#import <UIKit/UIKit.h>
@protocol CDKAddDeseaseSituationViewDelegate<NSObject>
-(void)openOrCloseAddDeseaseSituationView:(UIButton*)sender;
@end
@interface CDKDeseaseSituaionAdd : UIView
@property (weak, nonatomic) IBOutlet UIButton *openOrCloseBtn;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *oneillView;
@property (weak, nonatomic) IBOutlet UIView *oneillLab;
@property (weak, nonatomic) IBOutlet UIView *towillView;
@property (weak, nonatomic) IBOutlet UILabel *towillLab;
@property(nonatomic,weak) id<CDKAddDeseaseSituationViewDelegate> delegate;

+(CDKDeseaseSituaionAdd*)xibinstanse;
@end
