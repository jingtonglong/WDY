//
//  CDKCustomBtn.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/12.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKCustomBtn : UIButton
@property(nonatomic,strong) UIImageView *imgV;
@property(nonatomic,strong) UILabel *titleLab;
@property(nonatomic,strong) UIColor *seleteCorlor;
@property(nonatomic,strong) UIImage  *normolImg;
@property(nonatomic,strong) UIImage  *seletedImg;


@end
