//
//  CDKPationtManagerInfoCell.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKPationtManagerInfoCell.h"
#import "CDKChangeRemarkVC.h"
@implementation CDKPationtManagerInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)openOrClose:(id)sender {
    if (self.callback) {
        self.callback();
    }
}
- (IBAction)changeRemark:(id)sender {
    UIViewController *vc=[self viewController:self];
    if (vc) {
        CDKChangeRemarkVC *remark=[[CDKChangeRemarkVC alloc] init];
        remark.model=self.pation_info_model;
        remark.callback = ^{
            self.remark.text=self.pation_info_model.remarkCkd;
        };
        [vc.navigationController pushViewController:remark animated:YES];
    }
}
-(void)setInfoModel:(CDKPationtListModel *)infoModel{
    _infoModel=infoModel;
    self.name.text=infoModel.name;
    self.sex.text=infoModel.sex;
    self.age.text=[NSString stringWithFormat:@"%@岁",infoModel.age];
    self.phone.text=@"";
    self.remark.text=infoModel.remark;
    self.treat_way.text=@"";
    self.follow_desease.text=@"";
    self.origin_desease.text=@"";
    self.education.text=@"";
    self.submit_way.text=@"";
    self.address.text=@"";
    self.identify_id.text=@"";

}
- (UIViewController *)viewController:(UIView *)next;
{
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}
-(void)setPation_info_model:(CDKPationInfoModel *)pation_info_model{
    _pation_info_model=pation_info_model;
    self.name.text=pation_info_model.name;
    self.sex.text=pation_info_model.sex;
    self.age.text=[NSString stringWithFormat:@"%@岁",pation_info_model.age];
    self.phone.text=pation_info_model.mobile;
    self.remark.text=pation_info_model.remarkCkd;
    self.treat_way.text=pation_info_model.mainTreatment;
    self.follow_desease.text=pation_info_model.withDisease;
    self.origin_desease.text=pation_info_model.orgDisease;
    self.education.text=pation_info_model.education;
    self.submit_way.text=pation_info_model.expenseType;
    self.address.text=pation_info_model.fullAddress;
    
    if (pation_info_model.cardNo.length == 18) {
        NSString *str1 = [pation_info_model.cardNo substringToIndex:3];
        NSString *str2 = [pation_info_model.cardNo substringFromIndex:14];
        NSString *card = [NSString stringWithFormat:@"%@***********%@",str1,str2];
        self.identify_id.text = card;
    }else if (pation_info_model.cardNo.length == 15){
        NSString *str1 = [pation_info_model.cardNo substringToIndex:3];
        NSString *str2 = [pation_info_model.cardNo substringFromIndex:11];
        NSString *card = [NSString stringWithFormat:@"%@*******%@",str1,str2];
        self.identify_id.text = card;
    }else{
        self.identify_id.text = @"";
    }
}
//-(void)setFrame:(CGRect)frame{
//    frame.size.height-=10;
//    [super setFrame:frame];
//}

@end
