//
//  CDKBiologeCheckCell.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKBiologeCheckCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *titleLab;
@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UIButton *addTitleLab;

@end
