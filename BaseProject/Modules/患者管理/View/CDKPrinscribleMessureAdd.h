//
//  CDKPrinscribleMessureAdd.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//
@protocol CDKPrinscribleMessureAddViewDelegate<NSObject>
-(void)openOrCloseCDKPrinscribleMessureAddView:(UIButton*)sender;
@end
#import <UIKit/UIKit.h>
// 新增处方措施
@interface CDKPrinscribleMessureAdd : UIView
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *openOrCloseBtn;
@property(nonatomic,weak) id<CDKPrinscribleMessureAddViewDelegate> delegate;

+(CDKPrinscribleMessureAdd*)xibinstanse;
@end
