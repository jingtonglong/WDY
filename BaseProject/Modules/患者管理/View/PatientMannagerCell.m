//
//  PatientMannagerCell.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/4/30.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "PatientMannagerCell.h"

@implementation PatientMannagerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setFrame:(CGRect)frame{
    frame.size.height-=10;
  
    [super setFrame:frame];
}
-(void)setModel:(CDKPationtListModel *)model{
    _model=model;
    
    [self.headPotrait sd_setImageWithURL:URL(model.headPicture) placeholderImage:[UIImage imageNamed:@"默认头像"]];
    self.name.text=model.name;
    if ([model.registerType isEqualToString:@"短信"]) {
        self.register_type.image= [UIImage imageNamed:@"短信"];
    }else{
       self.register_type.image= [UIImage imageNamed:@"手机"];
    }
    self.sex.text=model.sex;
    self.age.text=[NSString stringWithFormat:@"%@岁",model.age];
    self.last_visit.text=[NSString stringWithFormat:@"上次随访：%@",model.lastVisitTime];
    self.next_visit.text=[NSString stringWithFormat:@"下次随访：%@",model.visitTime];
}
@end
