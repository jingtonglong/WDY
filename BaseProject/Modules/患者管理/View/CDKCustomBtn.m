//
//  CDKCustomBtn.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/12.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKCustomBtn.h"

@implementation CDKCustomBtn
-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}
-(void)setupUI{
    
    _imgV=[[UIImageView alloc] init];
    _titleLab=[UILabel new];
    _titleLab.textAlignment=NSTextAlignmentCenter;
    [self addSubview:_imgV];
    [self addSubview:_titleLab];
    [_imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(37);
        make.height.mas_equalTo(37);
        make.centerX.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.equalTo(_imgV.mas_bottom).offset(5);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(12);
    }];
    
}
-(void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    if (selected) {
        self.titleLab.textColor=self.seleteCorlor;
        self.imgV.image=self.seletedImg;
    }else{
        self.titleLab.textColor=RGB(101,101,101);
        self.imgV.image=self.normolImg;
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
