//
//  CDKChartVauleCellTableViewCell.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKChartVauleCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@end
