//
//  CDKPrescribePrinceple.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/15.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CDKPricelpleDelegate<NSObject>
-(void)openOrClosePricelpleView:(UIButton*)sender;
@end
@interface CDKPrescribePrinceple : UIView// 处方原则
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *openOrCloseBtn;
@property(nonatomic,weak) id<CDKPricelpleDelegate> delegate;

+(CDKPrescribePrinceple*)xibinstanse;
@end
