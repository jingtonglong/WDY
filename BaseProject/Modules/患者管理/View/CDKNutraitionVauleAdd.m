//
//  CDKNutraitionVauleAdd.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/16.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKNutraitionVauleAdd.h"

@implementation CDKNutraitionVauleAdd
+(CDKNutraitionVauleAdd*)xibinstanse{
    NSArray *arr=[[NSBundle mainBundle] loadNibNamed:@"CDKNutraitionVauleAdd" owner:nil options:nil];
    return arr.firstObject;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openOrColse)];
    [self.topView addGestureRecognizer:tap];
}
-(void)openOrColse{
    self.openOrCloseBtn.selected=!self.openOrCloseBtn.selected;
    if (self.delegate) {
        [self.delegate openOrCloseNutraitionVauleAddView:self.openOrCloseBtn];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
