//
//  CDKPationtManagerTimeSection.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKPationtManagerTimeSection : UITableViewHeaderFooterView
@property(nonatomic,strong) UILabel *lab;

@end
