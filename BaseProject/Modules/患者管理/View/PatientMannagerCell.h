//
//  PatientMannagerCell.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/4/30.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKPationtListModel.h"
@interface PatientMannagerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headPotrait;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *age;
@property (weak, nonatomic) IBOutlet UILabel *last_visit;
@property (weak, nonatomic) IBOutlet UIImageView *register_type;
@property (weak, nonatomic) IBOutlet UILabel *sex;
@property (weak, nonatomic) IBOutlet UILabel *next_visit;
@property(nonatomic,strong) CDKPationtListModel *model;

@end
