//
//  CDKPationtMHeader.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/12.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKCustomBtn.h"
typedef enum {
    EatData=0,//饮食数据
    Check,//生化检查
    Nutrition,//营养评估
    Excel,//量表评估
    NutritionReport//营养报告
} SeletedType;
@protocol CDKPationtMHeaderDelegate <NSObject>
@required
- (void)CDKPationtMHeader_SeletedType:(SeletedType)type;

@end

@interface CDKPationtMHeader : UIView
@property(nonatomic,strong) CDKCustomBtn *seletedBtn;
@property(nonatomic,weak) id<CDKPationtMHeaderDelegate> delegate;

@end
