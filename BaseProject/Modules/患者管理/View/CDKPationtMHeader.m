//
//  CDKPationtMHeader.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/12.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKPationtMHeader.h"
@implementation CDKPationtMHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpUI];
    }
    return self;
}
-(void)setUpUI{
   
    UIScrollView *scrollview=[[UIScrollView alloc] init];
    [self addSubview:scrollview];
    [scrollview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(-10);

    }];
    scrollview.backgroundColor=[UIColor whiteColor];
    self.backgroundColor=RGB(247,247,247);
    scrollview.contentSize=CGSizeMake(KScreenWidth, 0);
    NSArray *titles=@[@"饮食数据",@"生化检查",@"营养评估",@"量表评估",@"营养报告"];
    NSArray *imgs=@[@"饮食数据",@"生化检查",@"营养评估",@"量表评估",@"营养报告"];
    NSArray *seletedColors=@[RGB(69,213,160),RGB(56,183,193),RGB(255,135,191),RGB(247,169,139),RGB(140,198,63)];
    CGFloat btnW=50;
    CGFloat btnH=60;
    
    CGFloat padding=10;
    CGFloat btnInset=(KScreenWidth-2*padding-btnW*5)/4;
    for (int i=0; i<titles.count; i++) {
        CDKCustomBtn *btn=[[CDKCustomBtn alloc] initWithFrame:CGRectMake(padding+i*btnW+i*btnInset, 15, btnW, btnH)];
        btn.titleLab.text=titles[i];
        btn.imgV.image=[UIImage imageNamed:imgs[i]];
        btn.normolImg=[UIImage imageNamed:imgs[i]];
        btn.seletedImg=[UIImage imageNamed:[NSString stringWithFormat:@"%@-选中",imgs[i]]];
        
        btn.titleLab.font=[UIFont systemFontOfSize:11];
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        btn.seleteCorlor=seletedColors[i] ;
        btn.tag=100+i;
        if (i==0) {
            btn.selected=YES;
            self.seletedBtn=btn;
        }
         [scrollview addSubview:btn];
    };

}
-(void)btnClicked:(CDKCustomBtn*)sender{
    self.seletedBtn.selected=NO;
    sender.selected=YES;
    self.seletedBtn=sender;
    if (self.delegate) {
        [self.delegate CDKPationtMHeader_SeletedType:(SeletedType)(sender.tag-100)];
    }
    
}
@end
