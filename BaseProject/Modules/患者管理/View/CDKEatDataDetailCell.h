//
//  CDKEatDataDetailCell.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKEatMoel.h"
@interface CDKEatDataDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *arrow;
@property (weak, nonatomic) IBOutlet UILabel *chekedlab;
@property(nonatomic,strong) CDKEatMoel *model;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *energe;
@property (weak, nonatomic) IBOutlet UILabel *protien;
@property (weak, nonatomic) IBOutlet UILabel *patasium;
@property (weak, nonatomic) IBOutlet UILabel *sodium;
@property (weak, nonatomic) IBOutlet UILabel *phosphorus;

@property (weak, nonatomic) IBOutlet UILabel *calsium;

@property (weak, nonatomic) IBOutlet UILabel *water;

@end
