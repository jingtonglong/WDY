//
//  CDKPatientMannagerRecordCell.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKBiochemistryBaseDataModel.h"
#import "CDKAssessmentModel.h"
// 修改营养用的block
typedef void(^PatientMannagerRecordBlock)(NSInteger index,NSString *value);

@interface CDKPatientMannagerRecordCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UIImageView *centerImageView;

/** 赋值的text*/
@property (nonatomic,copy) NSArray *textArray;

/** 当前的index*/
@property (nonatomic,assign) NSInteger index;

/** 不显示文本占位符*/
@property (nonatomic,copy) NSString *noPlaceholder;

/** 模型*/
@property (nonatomic,strong) CDKBiochemistryBaseDataModel *model;

/** 营养新增模型*/
@property (nonatomic,strong) CDKAssessmentModel *assessModel;

/** 回调*/
@property (nonatomic,copy) PatientMannagerRecordBlock block;

@end
