//
//  CDKPatientMannagerRecordCell.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKPatientMannagerRecordCell.h"

@implementation CDKPatientMannagerRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.textField.delegate = self;
    [self.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

-(void)setModel:(CDKBiochemistryBaseDataModel *)model{
    _model = model;
    self.leftLabel.text = model.name;
    NSString *rightText = [NSString stringWithFormat:@"(%@~%@)%@",model.minValue,model.maxValue,model.unit];
    self.rightLabel.text = rightText;
    if (model.value.length > 0) {
        self.textField.text = model.value;
        // 判断图片显示上浮或者下浮
        if ([model.value floatValue] < [model.minValue floatValue]) {
            self.centerImageView.image = [UIImage imageNamed:@"数据下浮"];
        }else if ([model.value floatValue] > [model.maxValue floatValue]){
            self.centerImageView.image = [UIImage imageNamed:@"数据上浮"];
        }else{
            self.centerImageView.image = [UIImage imageNamed:@""];
        }
        self.centerImageView.hidden = NO;
    }else{
        if ([self.noPlaceholder isEqualToString:@"yes"]) {
            self.textField.text = @" ";
        }else{
            self.textField.text = @"";
        }
        self.centerImageView.hidden = YES;
    }
}

-(void)setAssessModel:(CDKAssessmentModel *)assessModel{
    _assessModel = assessModel;
    self.leftLabel.text = assessModel.leftText;
    self.rightLabel.text = assessModel.rightText;
    self.textField.text = assessModel.valueText;
}

// 新增营养评估计算BIM
- (void)textFieldDidChange:(UITextField *)theTextField{
    self.assessModel.valueText = theTextField.text;
    if (self.index == 0 || self.index == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshBMI" object:nil];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    // 仅仅作为新增生化数据判断
    if ([self.model.sort isEqualToString:@"13"] || [self.model.sort isEqualToString:@"14"]) {// 血糖
        if ([textField.text integerValue] >= 60) {
            [kAppWindow showMessageHud:@"最大值为60"];
            textField.text = @"";
        }else{
            self.model.value = textField.text;
        }
    }else if ([self.model.sort isEqualToString:@"31"] || [self.model.sort isEqualToString:@"32"]){// C超敏反应蛋白 24小时尿量
        if ([textField.text integerValue] >= 12000) {
            [kAppWindow showMessageHud:@"最大值为12000"];
            textField.text = @"";
        }else{
            self.model.value = textField.text;
        }
    }else if ([self.model.sort isEqualToString:@"6"]){// 血尿素氮
        if ([textField.text integerValue] >= 100) {
            [kAppWindow showMessageHud:@"最大值为100"];
            textField.text = @"";
        }else{
            self.model.value = textField.text;
        }
    }else if ([self.model.sort isEqualToString:@"5"]){// 血清肌酐
        if ([textField.text integerValue] >= 10000) {
            [kAppWindow showMessageHud:@"最大值为10000"];
            textField.text = @"";
        }else{
            self.model.value = textField.text;
        }
    }else{// 营养验证
        if ([self.leftLabel.text isEqualToString:@"身高"] || [self.leftLabel.text isEqualToString:@"体重"]) {
            if ([textField.text integerValue] > 500) {
                [kAppWindow showMessageHud:@"身高和体重最大值为500"];
                textField.text = @"";
            }else if ([textField.text integerValue] < 0){
                [kAppWindow showMessageHud:@"最小值为0"];
                textField.text = @"";
            }else{
                self.model.value = textField.text;
                if (self.block) {
                    self.block(self.index, textField.text);
                }
            }
        }else if ([self.leftLabel.text isEqualToString:@"上臀围"] || [self.leftLabel.text isEqualToString:@"肱三头肌皮褶厚度"]){
            if ([textField.text integerValue] > 100) {
                [kAppWindow showMessageHud:@"上臀围最大值为100"];
                textField.text = @"";
            }else if ([textField.text integerValue] < 0){
                [kAppWindow showMessageHud:@"最小值为0"];
                textField.text = @"";
            }else{
                self.model.value = textField.text;
                if (self.block) {
                    self.block(self.index, textField.text);
                }
            }
        }else if ([self.leftLabel.text isEqualToString:@"左手握力"] || [self.leftLabel.text isEqualToString:@"右手握力"]){
            if ([textField.text integerValue] > 500) {
                [kAppWindow showMessageHud:@"左手握力和右手握力最大值为500"];
                textField.text = @"";
            }else if ([textField.text integerValue] < 0){
                [kAppWindow showMessageHud:@"最小值为0"];
                textField.text = @"";
            }else{
                self.model.value = textField.text;
                if (self.block) {
                    self.block(self.index, textField.text);
                }
            }
        }else if ([self.leftLabel.text isEqualToString:@"舒张压"] || [self.leftLabel.text isEqualToString:@"收缩压"]){
            if ([textField.text integerValue] > 500) {
                [kAppWindow showMessageHud:@"舒张压和收缩压最大值为500"];
                textField.text = @"";
            }else if ([textField.text integerValue] < 0){
                [kAppWindow showMessageHud:@"最小值为0"];
                textField.text = @"";
            }else{
                self.model.value = textField.text;
                if (self.block) {
                    self.block(self.index, textField.text);
                }
            }
        }else{
            if ([textField.text integerValue] >= 800) {
                [kAppWindow showMessageHud:@"最大值为800"];
                textField.text = @"";
            }else if ([textField.text integerValue] < 0){
                [kAppWindow showMessageHud:@"最小值为0"];
                textField.text = @"";
            }else{
                self.model.value = textField.text;
                if (self.block) {
                 self.block(self.index, textField.text);
                }
            }
        }
    }    
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDian = YES;
    
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
    
    if ([string length] > 0) {
        
        unichar single = [string characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
            
            //            //首字母不能为0和小数点
            //            if([textField.text length] == 0){
            //                if(single == '.') {
            //                    [SVProgressHUD showInfoWithStatus:@"第一个数字不能为小数点"];
            //                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
            //                    return NO;
            //                }
            //                if (single == '0') {
            //                    [SVProgressHUD showInfoWithStatus:@"第一个数字不能为0"];
            //                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
            //                    return NO;
            //                }
            //            }
            
            //输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian = YES;
                    return YES;
                    
                }else{
                    [kAppWindow showMessageHud:@"您已经输入过小数点了"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                if (isHaveDian) {//存在小数点
                    
                    //判断小数点的位数
                    NSRange ran = [textField.text rangeOfString:@"."];
                    if (range.location - ran.location <= 2) {
                        return YES;
                    }else{
                        [kAppWindow showMessageHud:@"最多输入两位小数"];
                        return NO;
                    }
                }else{
                    return YES;
                }
            }
        }else{//输入的数据格式不正确
            [kAppWindow showMessageHud:@"您输入的格式不正确"];
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
