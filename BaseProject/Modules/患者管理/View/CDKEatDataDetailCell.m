//
//  CDKEatDataDetailCell.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKEatDataDetailCell.h"

@implementation CDKEatDataDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [YSUtil makeCornerRadius:9 view:self.chekedlab];
    [YSUtil makeBorderWidth:1 view:self.chekedlab borderColor:RGB(173,203,188)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setModel:(CDKEatMoel *)model{
    _model=model;
    self.time.text=model.basicTime;
    self.energe.text=model.energy;
    self.protien.text=model.protein;
    self.patasium.text=model.potassium;
    self.sodium.text=model.sodium;
    self.phosphorus.text=model.phosphorus;
    self.calsium.text=model.calcium;
    self.water.text=model.water;
    self.chekedlab.text=model.auditingState;
}

@end
