//
//  CDKNutraitionView.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/15.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol CDKNutraiViewDelegate<NSObject>
-(void)openOrCloseNutraitionView:(UIButton*)sender;
@end

@interface CDKNutraitionView : UIView
@property (weak, nonatomic) IBOutlet UIButton *openOrCloseBtn;
@property (weak, nonatomic) IBOutlet UIView *topview;
@property(nonatomic,weak) id<CDKNutraiViewDelegate> delegate;

+(CDKNutraitionView*)xibinstanse;
@end
