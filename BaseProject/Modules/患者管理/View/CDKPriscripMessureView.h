//
//  CDKPriscripMessureView.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/15.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CDKMessureDelegate<NSObject>
-(void)openOrCloseMessureView:(UIButton*)sender;
@end
@interface CDKPriscripMessureView : UIView// 处方措施
@property (weak, nonatomic) IBOutlet UIButton *openOrCloseBtn;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property(nonatomic,weak) id<CDKMessureDelegate> delegate;

+(CDKPriscripMessureView*)xibinstanse;

@end
