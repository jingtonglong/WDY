//
//  CDKNutraitionVauleAdd.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/16.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//
@protocol CDKNutraitionVauleAddViewDelegate<NSObject>
-(void)openOrCloseNutraitionVauleAddView:(UIButton*)sender;
@end
#import <UIKit/UIKit.h>
// 新增营养评估view
@interface CDKNutraitionVauleAdd : UIView
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *openOrCloseBtn;
@property(nonatomic,weak) id<CDKNutraitionVauleAddViewDelegate> delegate;

+(CDKNutraitionVauleAdd*)xibinstanse;
@end
