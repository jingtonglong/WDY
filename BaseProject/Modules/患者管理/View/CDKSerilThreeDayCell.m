//
//  CDKSerilThreeDayCell.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKSerilThreeDayCell.h"

@implementation CDKSerilThreeDayCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    [self layoutIfNeeded];
//    [YSUtil makeCornerRadius:3 view:self.containt1];
    [YSUtil makeBorderWidth:1 view:self.containt1 borderColor:RGB(112,240,193)];
//    [YSUtil makeCornerRadius:3 view:self.containt2];
    [YSUtil makeBorderWidth:1 view:self.containt2 borderColor:RGB(112,240,193)];
//    [YSUtil makeCornerRadius:3 view:self.containt3];
    [YSUtil makeBorderWidth:1 view:self.containt3 borderColor:RGB(112,240,193)];
//    [YSUtil makeCornerRadius:3 view:self.containt4];
    [YSUtil makeBorderWidth:1 view:self.containt4 borderColor:RGB(112,240,193)];
//    [YSUtil makeCornerRadius:3 view:self.containt5];
    [YSUtil makeBorderWidth:1 view:self.containt5 borderColor:RGB(112,240,193)];
//    [YSUtil makeCornerRadius:3 view:self.containt6];
    [YSUtil makeBorderWidth:1 view:self.containt6 borderColor:RGB(112,240,193)];
//    [YSUtil makeCornerRadius:3 view:self.containt7];
    [YSUtil makeBorderWidth:1 view:self.containt7 borderColor:RGB(112,240,193)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setFrame:(CGRect)frame{
    frame.size.height-=10;
    [super setFrame:frame];
}

@end
