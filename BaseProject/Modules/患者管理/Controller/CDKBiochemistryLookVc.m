//
//  CDKBiochemistryLookVc.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKBiochemistryLookVc.h"
#import "CDKPatientMannagerRecordCell.h"
#import "CDKHomeBiochemistryAuditBottomView.h"
#import "CDKBiochemistryBaseDataModel.h"
#import "CDKReviewReasonView.h"
#import "CDKBiochemistryImageModel.h"
#import "CDKImageViewer.h"
#import "STPickerDate.h"
static NSString *patientMannagerRecordCell = @"patientMannagerRecordCell";

@interface CDKBiochemistryLookVc ()<UITableViewDelegate,UITableViewDataSource,STPickerDateDelegate>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,copy) NSMutableArray *dataArray;
@property (nonatomic,strong) UILabel *rightLabel;

/** 选择年月日*/
@property (nonatomic,strong) STPickerDate *pickView;

/** 图片数组*/
@property (nonatomic,strong) NSMutableArray *imageModelArray;
/** 审核视图*/
@property (nonatomic,strong) CDKReviewReasonView *reasonView;

/** 图片查看*/
@property (nonatomic,strong) CDKImageViewer *viewer;
@end

@implementation CDKBiochemistryLookVc


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"生化数据";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupUI];
    if ([self.fromType isEqualToString:@"Home_Audit"]) {
        self.title = @"生化数据详情";
        WeakSelf(weak);
        
        UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"图片预览"] style:UIBarButtonItemStylePlain handler:^(id  _Nonnull sender) {
            [weak networkByImage];
        }];
         self.navigationItem.rightBarButtonItem = item;
        
        CDKHomeBiochemistryAuditBottomView *bottomView = [[CDKHomeBiochemistryAuditBottomView alloc]initWithFrame:CGRectZero];
        bottomView.aduitYesBlock = ^{
//            [weak networkByTime:@"yes"];
            [weak networkByEdit:@"yes"];
        };
        bottomView.aduitNoBlock = ^{
            [weak showAuditView];
        };
        [self.view addSubview:bottomView];
        [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.bottom.equalTo(self.view.mas_bottom);
            make.height.mas_equalTo(70 * Iphone6ScaleHeight);
        }];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
    }else{
        self.treatmentId = self.data[@"id"];
    }
    
    [self networkByBaseData];
}

- (void)setupUI{
    
    UIView *headView = [UIView new];
    headView.backgroundColor = RGB(247,247,247);
    [self.view addSubview:headView];
    [headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top);
        make.height.mas_equalTo(50 * Iphone6ScaleHeight);
    }];
    
    UILabel *leftLabel = [UILabel new];
    leftLabel.text = @"检查日期";
    leftLabel.font = SYSTEMFONT(14);
    leftLabel.textColor = RGB(50,50,50);
    [headView addSubview:leftLabel];
    [leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headView.mas_left).offset(15);
        make.centerY.equalTo(headView.mas_centerY);
    }];
    
    self.rightLabel = [UILabel new];
    self.rightLabel.font = SYSTEMFONT(14);
    if ([self.fromType isEqualToString:@"Home_Audit"]) {
        NSString *time = [self.time substringToIndex:10];
        self.rightLabel.text = time;
        [YSUtil addClickEvent:self action:@selector(clickHeadView) owner:headView];
    }else{
        self.rightLabel.text = self.data[@"measureDate"];
    }
    self.rightLabel.textColor = RGB(50,50,50);
    self.rightLabel.textAlignment = NSTextAlignmentRight;
    [headView addSubview:self.rightLabel];
    [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(headView.mas_right).offset(-15);
        make.centerY.equalTo(headView.mas_centerY);
    }];
    
}


#pragma mark -- Action
- (void)clickHeadView{
    self.pickView = [[STPickerDate alloc]init];
    [self.pickView setYearLeast:1900];
    [self.pickView setYearSum:200];
    [self.pickView setDelegate:self];
    [self.pickView show];
}
/**
 填写审核视图
 */
- (void)showAuditView{
    self.reasonView = [[CDKReviewReasonView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
    [self.reasonView.reReviewButton addTarget:self action:@selector(clickReviewButton) forControlEvents:UIControlEventTouchUpInside];
    [self.reasonView.doneButton addTarget:self action:@selector(clickDoneButton) forControlEvents:UIControlEventTouchUpInside];
    [[UIApplication sharedApplication].keyWindow addSubview:self.reasonView];
}

/**
 审核弹窗完成按钮
 */
- (void)clickDoneButton{
    [self.reasonView removeFromSuperview];
//    [self networkByTime:@"no"];
    [self networkByEdit:@"no"];
}

/**
 审核弹窗重新审核按钮
 */
- (void)clickReviewButton{
    [self.reasonView removeFromSuperview];
}

#pragma mark -- Network

/**
 获取基础数据
 */
- (void)networkByBaseData{
    [self showLoadingHud:nil];
    NSString *url = @"/api/services/ckd/audit/QueryInspectrecord";
    NSDictionary *dic = @{
                          @"treatmentId":self.treatmentId,
                          };
    [YSNetworkingManager requestWithUrl:url :POST paramiters:dic success:^(id responseObject) {
        if (SUCCEESS) {
            [self.dataArray addObjectsFromArray:[NSArray modelArrayWithClass:[CDKBiochemistryBaseDataModel class] json:responseObject[@"result"]]];
            [self.tableView reloadData];
        }
        [self dismissHud];
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

/**
 验证时间
 */
//- (void)networkByTime:(NSString *)pass{
//    if ([self.rightLabel.text isEqualToString:@"选择时间"]) {
//        [self showMessageHud:@"请选择时间"];
//        return;
//    }
//    NSString *url = @"/api/services/ckd/patient/CheckDate";
//    NSDictionary *dic = @{
//                          @"patientId":self.patientId,
//                          @"measureDate":self.rightLabel.text,
//                          };
//    [YSNetworkingManager requestWithUrl:url :POST paramiters:dic success:^(id responseObject) {
//        [self dismissHud];
//        if (SUCCEESS) {
//            [self networkByEdit:pass];
//        }else{
//            [self showMessageHud:responseObject[@"error"][@"message"]];
//        }
//    } errorHandel:^(NSError *error) {
//        [self dismissHud];
//        if (error.code ==-1001 ) {
//            [self showMessageHud:@"请求超时"];
//        }else{
//            [self showMessageHud:@"请检查网络"];
//        }
//    }];
//}

/**
 修改生化数据
 */
- (void)networkByEdit:(NSString *)pass{
    
    [self showLoadingHud:nil];
    NSString *url = @"/api/services/ckd/audit/UpdateTreatmentInspectrecord";
    NSString *type = @"1";
    NSString *one = @"";
    NSString *two = @"";
    for (CDKBiochemistryBaseDataModel *model in self.dataArray) {
        if (model.value.length > 0) {
            type = @"2";
        }
        if ([model.id isEqualToString:@"1"]) {
            if (model.value.length > 0) {
                one = @"haveValue";
            }
        }
        
        if ([model.id isEqualToString:@"2"]) {
            if (model.value.length > 0) {
                two = @"haveValue";
            }
        }
    }
    
    if ([type isEqualToString:@"1"]) {
        [self showMessageHud:@"请填写任意一项或多项检查指标数据"];
        return;
    }
    
    if (one.length > 0 && two.length > 0) {
        
    }else if (one.length == 0 && two.length == 0){
        
    }else{
        [self showMessageHud:@"舒张压、收缩压需要同时填写"];
        return;
    }
    
    NSArray *jsonArray = [self.dataArray modelToJSONObject];
    
    NSDictionary *dic = @{
                          @"TreatmentId":self.treatmentId,
                          @"patientId":self.patientId,
                          @"measureDate":self.rightLabel.text,
                          @"inspectrecords":jsonArray,
                          };
    [YSNetworkingManager requestWithUrl:url :POST paramiters:dic success:^(id responseObject) {
        if (SUCCEESS) {
            if ([pass isEqualToString:@"yes"]) {
                [self networkByAuditYes];
            }else{
                [self networkByAuditNo:self.reasonView.textView.text];
            }
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

/**
 审核通过
 */
- (void)networkByAuditYes{
    [self.tableView endEditing:YES];
    [self showLoadingHud:nil];
    NSString *url = @"/api/services/ckd/audit/BatchAudit";
    NSDictionary *dic = @{
                          @"treatmentIds":self.treatmentId,
                          @"remark":@"",
                          @"isPass":@1,
                          };
    [YSNetworkingManager requestWithUrl:url :POST paramiters:dic success:^(id responseObject) {
        if (SUCCEESS) {
             [self showMessageHud:@"审核成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
         [self dismissHud];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

/**
 审核不通过
 */
- (void)networkByAuditNo:(NSString *)reason{
    
    if (reason.length < 1) {
        [[UIApplication sharedApplication].keyWindow  showMessageHud:@"请填写审核不通过原因"];
        return;
    }
    
    [self showLoadingHud:nil];
    NSString *url = @"/api/services/ckd/audit/BatchAudit";
    NSDictionary *dic = @{
                          @"treatmentIds":self.treatmentId,
                          @"remark":reason,
                          @"isPass":@0,
                          };
    [YSNetworkingManager requestWithUrl:url :POST paramiters:dic success:^(id responseObject) {
        if (SUCCEESS) {
            [self showMessageHud:@"审核成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}


/**
 查看图片
 */
- (void)networkByImage{
    NSString *url = @"/api/services/ckd/audit/QueryInspectrecordImage";
    [self showLoadingHud:nil];
    NSDictionary *dic = @{
                          @"treatmentId":self.treatmentId,
                          };
    YSLog(@"%@",dic);
    [YSNetworkingManager requestWithUrl:url :POST paramiters:dic success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            YSLog(@"%@",responseObject);
            [self.imageModelArray removeAllObjects];
            [self.imageModelArray addObjectsFromArray:[NSArray modelArrayWithClass:[CDKBiochemistryImageModel class] json:responseObject[@"result"]]];
            
            NSMutableArray *images = [NSMutableArray array];
            for (CDKBiochemistryImageModel *model in self.imageModelArray) {
                [images addObject:model.imagePath];
            }
            if (images.count>0) {
                
                self.viewer.imgDatas = images;
                [self.viewer showImageViewer:kAppWindow];
            }else{
                [self showMessageHud:@"暂无图片"];
            }
         
        }
      
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code == -1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

#pragma mark -- Delegate
- (void)pickerDate:(STPickerDate *)pickerDate year:(NSInteger)year month:(NSInteger)month day:(NSInteger)day{
    self.rightLabel.text = [NSString stringWithFormat:@"%zd-%zd-%zd", year, month, day];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CDKPatientMannagerRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:patientMannagerRecordCell forIndexPath:indexPath];
    if ([self.fromType isEqualToString:@"Home_Audit"]){
        cell.textField.enabled = YES;
        cell.noPlaceholder = @"no";
    }else{
        cell.textField.enabled = NO;
        cell.noPlaceholder = @"yes";
    }
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50 * Iphone6ScaleHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

///键盘显示事件
- (void) keyboardWillShow:(NSNotification *)notification {
    CGFloat kbHeight = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        self.reasonView.frame = CGRectMake(0.0f, -kbHeight + NavHeight, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

//键盘消失事件
- (void) keyboardWillHide:(NSNotification *)notify {
    //键盘动画时间
    double duration = [[notify.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        self.reasonView.frame = CGRectMake(0, 0, KScreenWidth, KScreenHeight);
    }];
    
}


#pragma mark -- Lazy
-(UITableView *)tableView {
    // 是否显示审核view
    CGFloat viewHeight = 0.0;
    if ([self.fromType isEqualToString:@"Home_Audit"]) {
        viewHeight = 70.0;
    }
    
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 50 * Iphone6ScaleHeight, KScreenWidth, KScreenHeight - 50 * Iphone6ScaleHeight - NavHeight - viewHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CDKPatientMannagerRecordCell class]) bundle:nil] forCellReuseIdentifier:patientMannagerRecordCell];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

-(NSMutableArray *) dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)imageModelArray{
    if (!_imageModelArray) {
        _imageModelArray = [NSMutableArray array];
    }
    return _imageModelArray;
}

- (CDKImageViewer *)viewer{
    if (!_viewer) {
        _viewer = [[CDKImageViewer alloc] init];;
    }
    return _viewer;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
