//
//  CDKSeletedWithDesVCTableViewController.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/11.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKSeletedWithDesVCTableViewController.h"

@interface CDKSeletedWithDesVCTableViewController ()

@end

@implementation CDKSeletedWithDesVCTableViewController

- (void)viewDidLoad {
    self.title=@"伴随疾病";
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    WeakSelf(weak);
    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithTitle:@"确定" style:(UIBarButtonItemStylePlain) handler:^(id  _Nonnull sender) {
        NSMutableArray *marr=[NSMutableArray array];
        NSArray *selects=weak.tableView.indexPathsForSelectedRows;
        for (NSIndexPath *indexpath in selects) {
            [marr addObject:weak.datas[indexpath.row]];
        }
        if (weak.callback) {
            weak.callback(marr);
        }
        [weak.navigationController popViewControllerAnimated:YES];
    }];
    self.tableView.editing=YES;
    self.navigationItem.rightBarButtonItem=item;
    for (CDKWithDesModel *model in self.seleted_data) {
        
        NSInteger index=[self.datas indexOfObject:model];
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
        
     
    }
    self.tableView.tableFooterView=[UIView new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.datas.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    // Configure the cell...
    CDKWithDesModel *model=self.datas[indexPath.row];
    cell.textLabel.text=model.name;
    cell.tintColor=RGB(103,205,47);
    cell.multipleSelectionBackgroundView = [UIView new];
    return cell;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
