//
//  CDKNewPatientBuildProfile_1.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/18.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKNewPatientBuildProfile_1.h"
#import "CDKCDKNewPatientBuildProfile_2.h"
@interface CDKNewPatientBuildProfile_1 ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tx_phone;

@end

@implementation CDKNewPatientBuildProfile_1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title=@"新患者建档";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)next_step:(id)sender {
    if (![NSString isMobileNumber:self.tx_phone.text]) {
        [self showMessageHud:@"请输入正确的手机号"];
        return;
    }
    [self.tx_phone resignFirstResponder];
    [self checkPationt];
   
}
-(void)checkPationt{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/QueryPatient";
    NSDictionary *para=@{@"Mobile":self.tx_phone.text};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            CDKPationInfoModel *model=[CDKPationInfoModel modelWithJSON:responseObject[@"result"]];
            CDKCDKNewPatientBuildProfile_2 *vc=[[CDKCDKNewPatientBuildProfile_2 alloc] init];
            if (model) {
                vc.model=model;
            }else{
                CDKPationInfoModel *model=[[CDKPationInfoModel alloc] init];
                model.loginPhone=self.tx_phone.text;
                model.areaCode=@"";
                vc.model=model;
            }
            
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];

        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (![NSString isNumber:string]) {
        return NO;
    }
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
