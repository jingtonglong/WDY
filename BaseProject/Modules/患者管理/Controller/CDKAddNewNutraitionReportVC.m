//
//  CDKAddNewNutraitionReportVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/16.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKAddNewNutraitionReportVC.h"
#import "CDKDeseaseSituaionAdd.h"
#import "CDKNutraitionVauleAdd.h"
#import "CDKPrincelpleAdd.h"
#import "CDKPrinscribleMessureAdd.h"
@interface CDKAddNewNutraitionReportVC ()<CDKAddDeseaseSituationViewDelegate,CDKNutraitionVauleAddViewDelegate,CDKPrincelpleAddViewDelegate,CDKPrinscribleMessureAddViewDelegate>
@property(nonatomic,strong) CDKDeseaseSituaionAdd *deseaseSituationAddView;//新增患病情况
@property(nonatomic,strong) CDKNutraitionVauleAdd *nutraitionVauleAddView;//新增营养评估
@property(nonatomic,strong) CDKPrincelpleAdd *princelpleAddView;//新增处方原则
@property(nonatomic,strong) CDKPrinscribleMessureAdd *prinscribleMessureAddView;//新增处方措施
@property(nonatomic,strong) UIScrollView *scrollView;


@end
#define contentHeight 340
@implementation CDKAddNewNutraitionReportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}
-(void)setupUI{
    UIScrollView *scroll=[UIScrollView new];
    self.scrollView=scroll;
    self.view.backgroundColor=[UIColor whiteColor];
    if (@available(iOS 11,*)) {
        scroll.contentInsetAdjustmentBehavior=UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets=NO;
    }
    // 创建顶部视图
    scroll.bounces=NO;
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    UIView *top=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 144+StatusHeight)];
    //    top.backgroundColor=RGB(99,220,175);
    UIView *statusView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, StatusHeight)];
    UIImageView *imgv=[[UIImageView alloc] initWithFrame:top.bounds];
    imgv.image=[UIImage imageNamed:@"top-bg"];
    [top addSubview:imgv];
    [top addSubview:statusView];
    
    UIView *navbar=[[UIView alloc] initWithFrame:CGRectMake(0, StatusHeight, KScreenWidth, 44)];
    [top addSubview:navbar];
    UIButton *back=[UIButton buttonWithType:UIButtonTypeCustom];
    [back setImage:[UIImage imageNamed:@"左翻页"] forState:UIControlStateNormal];
   [back addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];   back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [navbar addSubview:back];
    
    [back mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.centerY.equalTo(navbar);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(40);
    }];
    
    UILabel *title=[UILabel new];
    title.text=@"新增营养报告";
    title.font=SYSTEMFONT(16);
    title.textColor=[UIColor whiteColor];
    [title sizeToFit];
    [navbar addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(navbar);
    }];
   
    UIButton *down=[UIButton buttonWithType:UIButtonTypeCustom];
    [down setTitle:@"完成" forState:UIControlStateNormal];
    down.titleLabel.font=SYSTEMFONT(14);
   
    [navbar addSubview:down];
    [down mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(title);
        make.right.mas_equalTo(-5);
        make.width.mas_equalTo(40);
    }];
    
    // 个人信息
    UILabel *nameLab=[UILabel new];
    nameLab.text=self.model.name;
    nameLab.font=SYSTEMFONT(18);
    nameLab.textColor=[UIColor whiteColor];
    [nameLab sizeToFit];
    [top addSubview:nameLab];
    [nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(navbar.mas_bottom).offset(30);
        make.left.mas_equalTo(15);
    }];
    UILabel *sex=[UILabel new];
    sex.text=self.model.sex;
//    if (![self.model.sex isEqualToString:@"1"]) {
//       sex.text=@"女";
//    }
    sex.font=SYSTEMFONT(14);
    sex.textColor=[UIColor whiteColor];
    [sex sizeToFit];
    [top addSubview:sex];
    [sex mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(nameLab);
        make.left.equalTo(nameLab.mas_right).offset(21);
    }];
    
    UILabel *age=[UILabel new];
    age.text=self.model.age;
    age.font=SYSTEMFONT(14);
    age.textColor=[UIColor whiteColor];
    [age sizeToFit];
    [top addSubview:age];
    [age mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(nameLab);
        make.left.equalTo(sex.mas_right).offset(21);
    }];
    [scroll addSubview:top];
    // 患病情况
    CDKDeseaseSituaionAdd *deseaseSituation=[CDKDeseaseSituaionAdd xibinstanse];
    [scroll addSubview:deseaseSituation];
    [deseaseSituation mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.equalTo(top.mas_bottom).offset(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(228);
        make.centerX.equalTo(scroll);
    }];
    deseaseSituation.delegate=self;
    self.deseaseSituationAddView=deseaseSituation;
    
    // 营养评估
    CDKNutraitionVauleAdd *nutraitionVaule=[CDKNutraitionVauleAdd xibinstanse];
    [scroll addSubview:nutraitionVaule];
    [nutraitionVaule mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.equalTo(deseaseSituation.mas_bottom).offset(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(1603);
        make.centerX.equalTo(scroll);
    }];
    nutraitionVaule.delegate=self;
    self.nutraitionVauleAddView=nutraitionVaule;
    
    // 处方原则
    CDKPrincelpleAdd *princelpleAdd=[CDKPrincelpleAdd xibinstanse];
    [scroll addSubview:princelpleAdd];
    [princelpleAdd mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.equalTo(nutraitionVaule.mas_bottom).offset(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(431);
        make.centerX.equalTo(scroll);
    }];
    princelpleAdd.delegate=self;
    self.princelpleAddView=princelpleAdd;
    
    // 处方措施
    CDKPrinscribleMessureAdd *prinscribleMessureAdd=[CDKPrinscribleMessureAdd xibinstanse];
    [scroll addSubview:prinscribleMessureAdd];
    [prinscribleMessureAdd mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.equalTo(princelpleAdd.mas_bottom).offset(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(1447);
        make.centerX.equalTo(scroll);
    }];
    prinscribleMessureAdd.delegate=self;
    self.prinscribleMessureAddView=prinscribleMessureAdd;
//    [scroll setContentSize:CGSizeMake(0,)];

}
-(void)backClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    kApplication.statusBarStyle=UIStatusBarStyleLightContent;
    //    YSLog(@"viewDidAppear");
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   
 [self.scrollView setContentSize:CGSizeMake(0,  contentHeight+1603+228+431+1447)];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    kApplication.statusBarStyle=UIStatusBarStyleDefault;
    //    self.navigationController.navigationBar.hidden=NO;
    //    YSLog(@"viewWillDisappear");
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
}

#pragma CDKAddDeseaseSituationViewDelegate
-(void)openOrCloseAddDeseaseSituationView:(UIButton *)sender{
    if (sender.isSelected) {
        
        [self.deseaseSituationAddView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48+180);
        }];
       
        CGFloat height=self.scrollView.contentSize.height;
       
        [self.scrollView setContentSize:CGSizeMake(KScreenWidth, height+180)];
        
    }else{
        
        [self.deseaseSituationAddView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48);
        }];
        CGFloat height=self.scrollView.contentSize.height;
        [self.scrollView setContentSize:CGSizeMake(KScreenWidth, height-180)];
        
    }
}
#pragma CDKNutraitionVauleAddViewDelegate
-(void)openOrCloseNutraitionVauleAddView:(UIButton *)sender{
    if (sender.isSelected) {
        
        [self.nutraitionVauleAddView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48+1555);
        }];
        
        CGFloat height=self.scrollView.contentSize.height;
        
        [self.scrollView setContentSize:CGSizeMake(KScreenWidth, height+1555)];
        
    }else{
        
        [self.nutraitionVauleAddView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48);
        }];
        CGFloat height=self.scrollView.contentSize.height;
        [self.scrollView setContentSize:CGSizeMake(KScreenWidth, height-1555)];
        
    }
}
#pragma CDKPrincelpleAddViewDelegate

-(void)openOrCloseCDKPrincelpleAddView:(UIButton *)sender{
    if (sender.isSelected) {
        
        [self.princelpleAddView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48+1555);
        }];
        
        CGFloat height=self.scrollView.contentSize.height;
        
        [self.scrollView setContentSize:CGSizeMake(KScreenWidth, height+383)];
        
    }else{
        
        [self.princelpleAddView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48);
        }];
        CGFloat height=self.scrollView.contentSize.height;
        [self.scrollView setContentSize:CGSizeMake(KScreenWidth, height-383)];
        
    }
}
#pragma CDKPrinscribleMessureAddViewDelegate

-(void)openOrCloseCDKPrinscribleMessureAddView:(UIButton *)sender{
    if (sender.isSelected) {
        
        [self.prinscribleMessureAddView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48+1399);
        }];
        
        CGFloat height=self.scrollView.contentSize.height;
        
        [self.scrollView setContentSize:CGSizeMake(KScreenWidth, height+1399)];
        
    }else{
        
        [self.prinscribleMessureAddView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48);
        }];
        CGFloat height=self.scrollView.contentSize.height;
        [self.scrollView setContentSize:CGSizeMake(KScreenWidth, height-1399)];
        
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
