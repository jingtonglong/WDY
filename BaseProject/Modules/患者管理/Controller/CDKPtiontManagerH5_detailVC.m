//
//  CDKPtiontManagerH5_detailVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/7/4.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKPtiontManagerH5_detailVC.h"
#import "NSObject+WebView.h"
@protocol JSObjcDelegate<JSExport>
-(NSString*)getToken;
-(void)finish;
@end
@interface CDKPtiontManagerH5_detailVC ()<UIWebViewDelegate,JSObjcDelegate>
@property(nonatomic,strong) UIWebView *webView;
@property(nonatomic,strong) JSContext *jsContext;


@end

@implementation CDKPtiontManagerH5_detailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIView *naview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, NavHeight)];
    naview.backgroundColor=RGB(98,228,150);
    UIButton *back=[UIButton buttonWithType:UIButtonTypeCustom];
    [back setImage:[UIImage imageNamed:@"返回箭头"] forState:UIControlStateNormal];
    back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [naview addSubview:back];
    [back mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(naview.mas_bottom).offset(-2);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(40);
    }];
    UILabel *lab=[UILabel new];
    lab.textColor=[UIColor whiteColor];
    lab.font=SYSTEMFONT(15);
    lab.text=self.title_str;
    [naview addSubview:lab];
    [lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(naview);
        make.centerY.equalTo(back);
    }];
    [self.view addSubview:naview];
    
    self.webView = [[UIWebView alloc] init];
    self.webView.delegate = self;
    NSURL *url = [NSURL URLWithString:self.url];
    NSURLRequest *request = [NSURLRequest requestWithURL: url];
    [self.webView loadRequest:request];
    self.webView.scrollView.bounces=NO;
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(naview.mas_bottom);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(creatJSContex:) name:@"CreatJSContex" object:nil];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)creatJSContex:(NSNotification*)noti

{
    
        YSLog(@"%@",noti);
    
    //注意以下代码如果不在主线程调用会发生闪退。
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.jsContext = [_webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
        
        self.jsContext[@"app"] = self;
        
        
        
    });
    
}

-(void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
//    JSContext *context=[webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
//    context[@"app"]=self;// 绑定对象
}
-(NSString*)getToken{
    
    return [UserInfoManager shareInstance].tokenInfo.access_token;
}
-(void)finish{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.callback) {
            self.callback();
        }
         [self.navigationController popViewControllerAnimated:YES];
    });
   
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
    [UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleLightContent;

    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];
    [UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleDefault;


}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
