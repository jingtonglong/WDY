//
//  CDKCDKNewPatientBuildProfile_2.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/18.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

typedef enum : NSUInteger {
    EDUCATION,
    REPORTWAY,//报销方式
    ORIGINDES,
    WITHDES
} PICKERTYPE;
#import "CDKSeletedWithDesVCTableViewController.h"
#import "CDKCDKNewPatientBuildProfile_2.h"
#import "LTPickerView.h"
#import "STPickerArea.h"
#import "YSDatePicker.h"
#import "AreaModel.h"
#import "CDKPatientManagerVC.h"
@interface CDKCDKNewPatientBuildProfile_2 ()<STPickerAreaDelegate>
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UIButton *sexMan;
@property (weak, nonatomic) IBOutlet UIButton *sexwomen;
@property (weak, nonatomic) IBOutlet UILabel *birthday;
@property (weak, nonatomic) IBOutlet UILabel *education;
@property (weak, nonatomic) IBOutlet UITextField *detail_adress;
@property (weak, nonatomic) IBOutlet UITextField *id_card;
@property (weak, nonatomic) IBOutlet UILabel *aeroa;

@property (weak, nonatomic) IBOutlet UILabel *report_way;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UILabel *origin_desease;
@property (weak, nonatomic) IBOutlet UILabel *with_desease;
@property (weak, nonatomic) IBOutlet UITextField *remark;
@property (weak, nonatomic)  UIButton *selected_sex;
@property(nonatomic,strong) NSArray *edu_datas;
@property(nonatomic,strong) NSArray *report_datas;
@property(nonatomic,strong) NSArray *origin_des_datas;
@property(nonatomic,strong) NSArray *with_des_datas;
@property(nonatomic,strong) LTPickerView *picker;
@property(nonatomic,assign) PICKERTYPE type;
@property(nonatomic,strong) STPickerArea *pickerAreo;
@property(nonatomic,strong) YSDatePicker *pickerDate;







@end

@implementation CDKCDKNewPatientBuildProfile_2
-(LTPickerView *)picker{
    if (!_picker) {
        _picker=[[LTPickerView alloc] init];
        WeakSelf(weak);
        _picker.block = ^(id obj, NSString *str, int num) {
            if (weak.type==EDUCATION) {
                weak.education.text=str;
                for (NSDictionary *dic in weak.edu_datas) {
                    if ([dic[@"name"] isEqualToString:str]) {
                        weak.model.education=dic[@"id"];
                    }
                }
            }else if (weak.type==REPORTWAY){
                weak.report_way.text=str;
                for (NSDictionary *dic in weak.report_datas) {
                    if ([dic[@"name"] isEqualToString:str]) {
                        weak.model.expenseType=dic[@"id"];
                    }
                }
            }else if (weak.type==ORIGINDES){
                weak.origin_desease.text=str;
                for (NSDictionary *dic in weak.origin_des_datas) {
                    if ([dic[@"name"] isEqualToString:str]) {
                        weak.model.orgDisease=dic[@"id"];
                    }
                }
            }else if (weak.type==WITHDES){
                weak.with_desease.text=str;
                for (NSDictionary *dic in weak.with_des_datas) {
                    if ([dic[@"name"] isEqualToString:str]) {
                        weak.model.withDisease=dic[@"id"];
                    }
                }
            }
        };
    }
    return _picker;
    
}
-(STPickerArea *)pickerAreo{
    if(!_pickerAreo){
        _pickerAreo=[[STPickerArea alloc] init];
//        _pickerAreo.saveHistory=YES;
        _pickerAreo.delegate=self;
    }
    return _pickerAreo;
}
#pragma LPAreodelegate

-(void)pickerArea:(STPickerArea *)pickerArea province:(NSString *)province city:(NSString *)city area:(nonnull NSString *)area areo_id:(nonnull NSString *)areoid{
    self.model.areaCode=areoid;
    self.aeroa.text=[NSString stringWithFormat:@"%@/%@/%@",province,city,area];
}

-(YSDatePicker *)pickerDate{
    if (!_pickerDate) {
        _pickerDate=(YSDatePicker*)[UIView xibinstance:@"YSDatePicker"];
        WeakSelf(weak);
        _pickerDate.callback = ^(NSDate *date) {
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            formatter.dateFormat=@"yyyy-MM-dd";
                        weak.birthday.text=[formatter stringFromDate:date];
            weak.model.brithday=[formatter stringFromDate:date];
            
        };
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        formatter.dateFormat=@"yyyy-MM-dd";
        if(self.model.brithday.length>0){
            _pickerDate.defaultDate=[formatter dateFromString:self.model.brithday];
        }
        
    }
    return _pickerDate;
}
- (void)viewDidLoad {
    [super viewDidLoad];
     self.title=@"新患者建档";
    self.phone.text=self.model.loginPhone;
    // Do any additional setup after loading the view from its nib.
     self.selected_sex=self.sexMan;
    if ([self.model.sex isEqualToString:@"0"]) {
        [self sex:self.sexMan];
    }else{
        [self sex:self.sexwomen];

    }
    if (self.model.name.length>0) {
        self.name.text=self.model.name;
    }
    if (self.model.brithday.length>0) {
        self.birthday.text=self.model.brithday;
    }
    if (self.model.areaCode.length>0) {
        self.aeroa.text=self.model.areaCode;
    }
    if (self.model.fullAddress.length>0) {
        self.detail_adress.text=self.model.fullAddress;
    }
    if (self.model.cardNo.length>0) {
        self.id_card.text=self.model.cardNo;
    }
    if (self.model.remarkCKD.length>0) {
        self.remark.text=self.model.remarkCKD;
    }
    
    [self showpreviusdata];
    if (self.model.areaCode.length>0) {
        NSString *path = [[NSBundle bundleForClass:[STPickerView class]] pathForResource:@"area_1" ofType:@"plist"];
        
        NSArray *areo_arr = [NSArray modelArrayWithClass:[AreaModel class] json:[[NSArray alloc]initWithContentsOfFile:path]];
        NSString *proinceid=self.model.provinceCode;
        NSString *cityid=self.model.cityCode;
        NSString *areoid=self.model.areaCode;
        NSString *address=@"";
        for (AreaModel *province in areo_arr) {
            
            if([[province.Id stringValue] isEqualToString:proinceid]){
                address=[address stringByAppendingString:province.AreaName];// 省
                for (AreaModel *city in province.Children) {
                    if([[city.Id stringValue] isEqualToString:cityid]){
                        address=[address stringByAppendingString:city.AreaName];// 市
                        for (AreaModel *areo  in city.Children) {
                            if ([[areo.Id stringValue] isEqualToString:areoid]) {
                                address=[address stringByAppendingString:areo.AreaName];// 区
                                self.aeroa.text=address;
                                return;
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    
//    self.education.text=self.model.education?self.model.education:@"请选择";
//    self.report_way.text=self.model.expenseType?self.model.expenseType:@"请选择";// 报销方式
//    self.origin_desease.text=self.model.orgDisease?self.model.orgDisease:@"请选择";// 原发病
//    self.with_desease.text=self.model.withDisease?self.model.withDisease:@"请选择";// 伴随发病
   
}
- (IBAction)seleted_bithday:(id)sender {
    [self.view endEditing:YES];
    [self.pickerDate showDatePiker];
}
- (IBAction)seleted_area:(id)sender {//选择区域
    [self.view endEditing:YES];

    [self.pickerAreo show];
    
}
- (IBAction)seleted_education:(id)sender {//选择文品
    [self.view endEditing:YES];

    self.type=EDUCATION;
    if (self.edu_datas.count>0) {
        NSMutableArray *datas=[NSMutableArray array];
        for (NSDictionary *dic in self.edu_datas) {
            NSString *text=dic[@"name"];
            [datas addObject:text];
        }
        self.picker.dataSource=datas;
        [self.picker show];
    }else{
        
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/GetDropDownList";

    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"Key":@"whcd"} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            self.edu_datas=responseObject[@"result"];
            NSMutableArray *datas=[NSMutableArray array];
            for (NSDictionary *dic in self.edu_datas) {
                NSString *text=dic[@"name"];
                [datas addObject:text];
            }
            self.picker.dataSource=datas;
            [self.picker show];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];

        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
        
    }
    
}
- (IBAction)seleted_report_away:(id)sender {
    [self.view endEditing:YES];

    //选择报销方式
    self.type=REPORTWAY;
    if (self.report_datas.count>0) {
        NSMutableArray *datas=[NSMutableArray array];
        for (NSDictionary *dic in self.report_datas) {
            NSString *text=dic[@"name"];
            [datas addObject:text];
        }
        self.picker.dataSource=datas;
        [self.picker show];
    }else{
        
        [self showLoadingHud:nil];
        NSString *api=@"/api/services/ckd/patient/GetDropDownList";
        
        [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"Key":@"bxfs"} success:^(id responseObject) {
            [self dismissHud];
            if (SUCCEESS) {
                self.report_datas=responseObject[@"result"];
                NSMutableArray *datas=[NSMutableArray array];
                for (NSDictionary *dic in self.report_datas) {
                    NSString *text=dic[@"name"];
                    [datas addObject:text];
                }
                self.picker.dataSource=datas;
                [self.picker show];
            }else{
                [self showMessageHud:responseObject[@"error"][@"message"]];
                
            }
            
        } errorHandel:^(NSError *error) {
            [self dismissHud];
            if (error.code ==-1001 ) {
                [self showMessageHud:@"请求超时"];
            }else{
                [self showMessageHud:@"请检查网络"];
            }
        }];
        
    }
}
- (IBAction)selted_origin_des:(id)sender {
    [self.view endEditing:YES];

    //选择原发病
    self.type=ORIGINDES;
    if (self.origin_des_datas.count>0) {
        NSMutableArray *datas=[NSMutableArray array];
        for (NSDictionary *dic in self.origin_des_datas) {
            NSString *text=dic[@"name"];
            [datas addObject:text];
        }
        self.picker.dataSource=datas;
        [self.picker show];
    }else{
        
        [self showLoadingHud:nil];
        NSString *api=@"/api/services/ckd/patient/GetDropDownList";
        
        [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"Key":@"yfb"} success:^(id responseObject) {
            [self dismissHud];
            if (SUCCEESS) {
                self.origin_des_datas=responseObject[@"result"];
                NSMutableArray *datas=[NSMutableArray array];
                for (NSDictionary *dic in self.origin_des_datas) {
                    NSString *text=dic[@"name"];
                    [datas addObject:text];
                }
                self.picker.dataSource=datas;
                [self.picker show];
            }else{
                [self showMessageHud:responseObject[@"error"][@"message"]];
                
            }
            
        } errorHandel:^(NSError *error) {
            [self dismissHud];
            if (error.code ==-1001 ) {
                [self showMessageHud:@"请求超时"];
            }else{
                [self showMessageHud:@"请检查网络"];
            }
        }];
        
    }
}
- (IBAction)seleted_with_dse:(id)sender {
    [self.view endEditing:YES];

    //选择伴随疾病
    self.type=WITHDES;
    if (self.with_des_datas.count>0) {
        
        CDKSeletedWithDesVCTableViewController *vc=[[CDKSeletedWithDesVCTableViewController alloc] init];
        vc.datas=self.with_des_datas;
        NSArray *withdes = [self.model.withDisease componentsSeparatedByString:@","];
        if (withdes.count>0) {
            NSMutableArray *arr=[NSMutableArray array];
            for (NSString *str in withdes) {
                for (CDKWithDesModel *model in self.with_des_datas) {
                    if ([str isEqualToString:model.id]) {
                        [arr addObject:model];
                    }
                }
            }
            vc.seleted_data=arr;
        }
        WeakSelf(weak);
        vc.callback = ^(NSArray *selets) {
            if (selets.count>0) {
                NSMutableArray *arr=[NSMutableArray array];
               NSMutableArray *arr_id=[NSMutableArray array];
                    for (CDKWithDesModel *model in selets) {
                        
                            [arr addObject:model.name];
                            [arr_id addObject:model.id];
                        
                    }
                weak.model.withDisease=[arr_id componentsJoinedByString:@","];
                weak.with_desease.text=[arr componentsJoinedByString:@","];
                }
            
            
            
            
        };
        [self.navigationController pushViewController:vc animated:YES];
        
    }else{
        
        [self showLoadingHud:nil];
        NSString *api=@"/api/services/ckd/patient/GetDropDownList";
        
        [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"Key":@"bsjb"} success:^(id responseObject) {
            [self dismissHud];
            if (SUCCEESS) {
                
                self.with_des_datas=[NSArray modelArrayWithClass:[CDKWithDesModel class] json:responseObject[@"result"]];
               
                CDKSeletedWithDesVCTableViewController *vc=[[CDKSeletedWithDesVCTableViewController alloc] init];
                vc.datas=self.with_des_datas;
                
                NSArray *withdes = [self.model.withDisease componentsSeparatedByString:@","];
                if (withdes.count>0) {
                    NSMutableArray *arr=[NSMutableArray array];
                    for (NSString *str in withdes) {
                        for (CDKWithDesModel *model in self.with_des_datas) {
                            if ([str isEqualToString:model.id]) {
                                [arr addObject:model];
                            }
                        }
                    }
                    vc.seleted_data=arr;
                }
                WeakSelf(weak);
                vc.callback = ^(NSArray *selets) {
                    if (selets.count>0) {
                        NSMutableArray *arr=[NSMutableArray array];
                        NSMutableArray *arr_id=[NSMutableArray array];
                        for (CDKWithDesModel *model in selets) {
                            
                            [arr addObject:model.name];
                            [arr_id addObject:model.id];
                            
                        }
                        weak.model.withDisease=[arr_id componentsJoinedByString:@","];
                        weak.with_desease.text=[arr componentsJoinedByString:@","];
                    }
                    
                    
                    
                    
                };
                [self.navigationController pushViewController:vc animated:YES];
               
            }else{
                [self showMessageHud:responseObject[@"error"][@"message"]];
                
            }
            
        } errorHandel:^(NSError *error) {
            [self dismissHud];
            if (error.code ==-1001 ) {
                [self showMessageHud:@"请求超时"];
            }else{
                [self showMessageHud:@"请检查网络"];
            }
        }];
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sex:(UIButton*)sender {
    [self.view endEditing:YES];

    self.selected_sex.selected=NO;
    sender.selected=YES;
    self.selected_sex=sender;
}

- (IBAction)save:(id)sender {//建档
    [self save_pationt_info:NO];
}
- (IBAction)saveandtreate:(id)sender {//建档并诊疗
    [self save_pationt_info:YES];

}
-(void)save_pationt_info:(BOOL)treat{
    [self.view endEditing:YES];
    
    if([self.selected_sex.currentTitle containsString:@"男"]){
        self.model.sex=@"0";
    }else{
        self.model.sex=@"1";
    }
    if(self.name.text.length>0){
        self.model.name=self.name.text;
    }else{
        [self showMessageHud:@"请输入姓名"];
        return;
    }
    if (!(self.model.brithday.length>0)) {
        [self showMessageHud:@"请选择出生日期"];
        return;
    }
    
    
//    if (![YSUtil validateIdentityCard:self.id_card.text]) {
//        [self showMessageHud:@"请输入正确的身份证"];
//        return;
//    }
    
    
    if(self.id_card.text.length==0){
        self.model.cardNo=@"";

    }else{
        if (!(self.id_card.text.length == 15 || self.id_card.text.length == 18)) {
            [self showMessageHud:@"请输入正确的身份证"];
            return;
        }
        self.model.cardNo=self.id_card.text;

    }
    self.model.remarkCKD=self.remark.text;
    self.model.fullAddress=self.detail_adress.text;
    if (self.model.education.length<=0) {
        self.model.education=@"";
    }
    if (self.model.expenseType.length<=0) {
        self.model.expenseType=@"";
    }
    if (self.model.orgDisease.length<=0) {
        self.model.orgDisease=@"";
    }
    if (self.model.withDisease.length<=0) {
        self.model.withDisease=@"";
    }
    
    id obj=[self.model modelToJSONObject];
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/Create";
    //    NSMutableDictionary *para=[NSMutableDictionary dictionary];
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:obj success:^(id responseObject) {
        [self dismissHud];
        if(SUCCEESS){
            
            [self showMessageHud:@"操作成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (treat) {
                    CDKPatientManagerVC *vc=[[CDKPatientManagerVC alloc] init];
                    CDKPationtListModel *model=[[CDKPationtListModel alloc] init];
                    model.id=responseObject[@"result"];
                    model.name=self.model.name;
                    model.sex=@"女";
                    model.registerType=@"短信";
                    
                    if ([self.model.sex isEqualToString:@"0"]) {
                        model.sex=@"男";
                    }
                    
                    vc.pationtModel=model;
                    [self.navigationController pushViewController:vc animated:YES];
                }else{
                    [self.navigationController popToRootViewControllerAnimated:YES];

                }
            });
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)showpreviusdata{
    
    // 伴随疾病
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/GetDropDownList";
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"Key":@"bsjb"} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            self.with_des_datas=[NSArray modelArrayWithClass:[CDKWithDesModel class] json:responseObject[@"result"]];
            if(self.model.withDisease.length>0){
                NSArray *withdes = [self.model.withDisease componentsSeparatedByString:@","];
                NSMutableArray *arr=[NSMutableArray array];
                for (NSString *str in withdes) {
                    for (CDKWithDesModel *model in self.with_des_datas) {
                        if ([model.id isEqualToString:str]) {
                            [arr addObject:model.name];
                        }
                    }
                }
                self.with_desease.text=[arr componentsJoinedByString:@","];
            }
        }else{
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
     
    }];
    
    // 文品
    [self showLoadingHud:nil];
//    NSString *api=@"/api/services/ckd/patient/GetDropDownList";
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"Key":@"whcd"} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            self.edu_datas=responseObject[@"result"];
            if (self.model.education.length>0) {
                for (NSDictionary *dic in self.edu_datas) {
                    if ([self.model.education isEqualToString:dic[@"id"]]) {
                        self.education.text=dic[@"name"];
                    }
                    
                }
            }
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
    }];
   
    
    // 原发病
    [self showLoadingHud:nil];
//    NSString *api=@"/api/services/ckd/patient/GetDropDownList";
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"Key":@"yfb"} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            self.origin_des_datas=responseObject[@"result"];
            if (self.model.orgDisease.length>0) {
                for (NSDictionary *dic in self.origin_des_datas) {
                    if ([self.model.orgDisease isEqualToString:dic[@"id"]]) {
                        self.origin_desease.text=dic[@"name"];
                    }
                }
            }
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
       
    }];
    // 报销方式
    [self showLoadingHud:nil];
//    NSString *api=@"/api/services/ckd/patient/GetDropDownList";
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"Key":@"bxfs"} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            self.report_datas=responseObject[@"result"];
            if (self.model.expenseType.length>0) {
                for (NSDictionary *dic in self.report_datas) {
                    if ([self.model.expenseType isEqualToString:dic[@"id"]]) {
                        self.report_way.text=dic[@"name"];
                    }
                }
            }
           
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
