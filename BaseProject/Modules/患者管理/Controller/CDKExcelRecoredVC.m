//
//  CDKExcelRecoredVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/7/4.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//
#import "CDKPtiontManagerH5_detailVC.h"
#import "CDKExcelRecoredVC.h"
#import "CDKPationtManagerTimeCell.h"
@interface CDKExcelRecoredVC ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *datas;
@property(nonatomic,assign) NSInteger page;
@property(nonatomic,copy) NSString *time_str;


@end

@implementation CDKExcelRecoredVC
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] init];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.rowHeight=UITableViewAutomaticDimension;
       
        [_tableView registerNib:[UINib nibWithNibName:@"CDKPationtManagerTimeCell" bundle:nil] forCellReuseIdentifier:@"time"];
        
        _tableView.backgroundColor=RGB(247,247,247);
      
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"month_cell"]; _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        UIView *footer=[UIView new];
        _tableView.tableFooterView= footer;
    }
    return _tableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.datas=[NSMutableArray array];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    // 各种量表评估记录时间列表
    // Do any additional setup after loading the view.
//    self.title=@"";
    WeakSelf(weak);
    self.tableView.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weak loadDatas];
    }];
    self.tableView.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weak loadMoredatas];
    }];
    self.tableView.mj_footer.ignoredScrollViewContentInsetBottom=iPhoneX_BOTTOM_HEIGHT;
    [self.tableView.mj_header beginRefreshing];
    
    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithTitle:@"新增评估" style:(UIBarButtonItemStylePlain) handler:^(id  _Nonnull sender) {
        // 新增评估
        CDKPtiontManagerH5_detailVC *vc=[[CDKPtiontManagerH5_detailVC alloc] init];
        vc.url=[NSString stringWithFormat:@"%@/h5/index.html#/%@?&pid=%@",BASE_URL,weak.scale,weak.pationtid];
        vc.title_str=@"新增评估";
        vc.callback = ^{
            [weak.tableView.mj_header beginRefreshing];
        };
        [weak.navigationController pushViewController:vc animated:YES];
    }];
    self.navigationItem.rightBarButtonItem=item;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    id dic=self.datas[indexPath.row];
    if ([dic isKindOfClass:[NSString class]]) {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"month_cell" forIndexPath:indexPath];
        cell.textLabel.text=[NSString stringWithFormat:@" %@",dic];
        cell.textLabel.textColor=RGB(154,154,154);
        cell.textLabel.font=[UIFont systemFontOfSize:14];
        cell.backgroundColor=RGB(247,247,247);
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;//
    }else{
        CDKPationtManagerTimeCell *cell=[tableView dequeueReusableCellWithIdentifier:@"time" forIndexPath:indexPath];
        cell.detaiLab.text=@"";//
        NSString *time=dic[@"createTime"];
        cell.timeLab.text=[[time componentsSeparatedByString:@" "] firstObject];
        return cell;//
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id dic=self.datas[indexPath.row];
    if ([dic isKindOfClass:[NSString class]]) {
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // 查看量表评估详情
    CDKPtiontManagerH5_detailVC *vc=[[CDKPtiontManagerH5_detailVC alloc] init];
   vc.url=[NSString stringWithFormat:@"%@/h5/index.html#/%@?&pid=%@&id=%@",BASE_URL,self.scale,self.pationtid,dic[@"id"]];
    vc.title_str=@"评估详情";
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)loadDatas{//
    [self.tableView.mj_footer endRefreshing];
    NSString *api=@"/api/services/ckd/patient/QueryScaleList";
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"PatientId":self.pationtid,@"scale":self.scale,@"PageIndex":@1,@"PageSize":@"15"} success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        if (SUCCEESS) {
            self.time_str=nil;
            [self.datas removeAllObjects];
            NSArray *arr=responseObject[@"result"][@"items"];
            for (NSDictionary *obj in arr) {
                NSString *time=obj[@"createTime"];
                NSArray *component=[time componentsSeparatedByString:@"-"];
                NSString *month=component[1];
                if (![month isEqualToString:self.time_str]) {
                    
                    [self.datas addObject:[NSString stringWithFormat:@"%@月",month]];
                    self.time_str=component[1];
                    [self.datas addObject:obj];
                    
                }else{
                    [self.datas addObject:obj];
                }
                
            }
            
            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        [self.tableView.mj_header endRefreshing];

        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
    
}
-(void)loadMoredatas{//
    [self.tableView.mj_header endRefreshing];
    NSString *api=@"/api/services/ckd/patient/QueryScaleList";
    self.page++;
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"PatientId":self.pationtid,@"scale":self.scale,@"PageIndex":@(self.page),@"PageSize":@"15"} success:^(id responseObject) {
        if (SUCCEESS) {
                NSArray *arr=@[];
                arr=responseObject[@"result"][@"items"];
                for (NSDictionary *obj in arr) {
                    NSString *time=obj[@"createTime"];
                    NSArray *component=[time componentsSeparatedByString:@"-"];
                    NSString *month=component[1];
                    if (![month isEqualToString:self.time_str]) {
                        
                        [self.datas addObject:[NSString stringWithFormat:@"%@月",month]];
                        self.time_str=month;
                        [self.datas addObject:obj];
                    }else{
                        [self.datas addObject:obj];
                    }
                    
                }
            if (arr.count>0) {
                [self.tableView.mj_footer endRefreshing];
                [self.tableView reloadData];
            }else{
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                if (self.page>1) {
                    self.page--;
                }
            }
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            if (self.page>1) {
                self.page--;
            }
            
        }
     
    } errorHandel:^(NSError *error) {
        if (self.page>1) {
            self.page--;
        }
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
