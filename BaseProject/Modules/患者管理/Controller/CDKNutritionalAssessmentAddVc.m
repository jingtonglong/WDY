//
//  CDKBiochemistryLookVc.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKNutritionalAssessmentAddVc.h"
#import "CDKPatientMannagerRecordCell.h"
#import "CDKAssessmentModel.h"

static NSString *patientMannagerRecordCell = @"patientMannagerRecordCell";

@interface CDKNutritionalAssessmentAddVc ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,copy) NSMutableArray *dataArray;
@property (nonatomic,copy) NSArray *leftTitleArray;
@property (nonatomic,copy) NSArray *rightTitleArray;
@property (nonatomic,copy) NSArray *textArray;
@property (nonatomic,strong) UILabel *rightLabel;
@property (nonatomic,copy) NSString *pHeight;
@property (nonatomic,copy) NSString *pWeight;
@property (nonatomic,copy) NSString *bim;
@property (nonatomic,copy) NSString *upperArm;
@property (nonatomic,copy) NSString *tricepsBrachii;
@property (nonatomic,copy) NSString *leftGrip;
@property (nonatomic,copy) NSString *rightGrip;
@property (nonatomic,copy) NSString *lowBlood;
@property (nonatomic,copy) NSString *highBlood;

@end

@implementation CDKNutritionalAssessmentAddVc

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"营养评估";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshBMI) name:@"refreshBMI" object:nil];
    
    self.leftTitleArray = @[@"身高",@"体重",@"BMI",@"上臀围",@"肱三头肌皮褶厚度",@"左手握力",@"右手握力",@"收缩压",@"舒张压"];
    self.rightTitleArray = @[@"cm",@"kg",@"",@"cm",@"mm",@"kg",@"kg",@"mmHg",@"mmHg"];
    self.textArray = @[@"",@"",@"",@"",@"",@"",@"",@"",@""];
    
    for (int i = 0; i < self.leftTitleArray.count; i ++) {
        CDKAssessmentModel *model = [[CDKAssessmentModel alloc] init];
        model.leftText = self.leftTitleArray[i];
        model.rightText = self.rightTitleArray[i];
        model.valueText = self.textArray[i];
        [self.dataArray addObject:model];
    }
    
    [self setupUI];
}

- (void)setupUI{
    
    __weak typeof(self) weakSelf = self;
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"完成提交" style:UIBarButtonItemStyleDone handler:^(id  _Nonnull sender) {
        [weakSelf.tableView endEditing:YES];
        [weakSelf networkByAdd];
    }];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    [self.tableView reloadData];
}


#pragma mark -- network
- (void)networkByAdd{
    
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/AddNutritionalAssessment";
    
    for (int i = 0; i < self.dataArray.count; i ++) {
        CDKAssessmentModel *model = self.dataArray[i];
        if (i == 0) {
            self.pHeight = model.valueText;
        }else if (i == 1){
            self.pWeight = model.valueText;
        }else if (i == 2){
            self.bim = model.valueText;
        }else if (i == 3){
            self.upperArm = model.valueText;
        }else if (i == 4){
            self.tricepsBrachii = model.valueText;
        }else if (i == 5){
            self.leftGrip = model.valueText;
        }else if (i == 6){
            self.rightGrip = model.valueText;
        }else if (i == 7){
            self.highBlood = model.valueText;
        }else if (i == 8){
            self.lowBlood = model.valueText;
        }
    }
    
    if (self.lowBlood.length > 0) {
        if (self.highBlood.length < 1) {
            [self showMessageHud:@"舒张压、收缩压需要同时填写"];
            return;
        }
    }else if (self.highBlood.length >0){
        if (self.lowBlood.length < 1) {
            [self showMessageHud:@"舒张压、收缩压需要同时填写"];
            return;
        }
    }
    
    if (self.pHeight.length < 1|| self.pWeight.length < 1 || self.bim.length < 1 || self.upperArm.length < 1 || self.tricepsBrachii.length < 1 || self.leftGrip.length < 1 || self.rightGrip.length < 1 || self.lowBlood.length < 1) {
        
    }
    
    NSDictionary *dic = @{
                          @"pHeight":self.pHeight?self.pHeight:@"",
                          @"pWeight":self.pWeight?self.pWeight:@"",
                          @"bim":self.bim?self.bim:@"",
                          @"upperArm":self.upperArm?self.upperArm:@"",
                          @"tricepsBrachii":self.tricepsBrachii?self.tricepsBrachii:@"",
                          @"leftGrip":self.leftGrip?self.leftGrip:@"",
                          @"rightGrip":self.rightGrip?self.rightGrip:@"",
                          @"highBlood":self.highBlood?self.highBlood:@"",
                          @"lowBlood":self.lowBlood?self.lowBlood:@"",
                          @"patientId":self.model.id,
                          };
    
    __block NSString *isNull = @"yes";
    [dic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        NSString *text = obj;
        NSString *keyString = key;
        if (![keyString isEqualToString:@"patientId"]) {
            if (text.length > 0) {
                isNull = @"no";
            }
        }
        
    }];
    
    if ([isNull isEqualToString:@"yes"]) {
        [self showMessageHud:@"至少输入一项"];
        return;
    }
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:dic success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [self showMessageHud:@"新增成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

#pragma mark -- Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CDKAssessmentModel *model = self.dataArray[indexPath.row];
    CDKPatientMannagerRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:patientMannagerRecordCell forIndexPath:indexPath];
    cell.index = indexPath.row;
    cell.assessModel = model;
    if (indexPath.row == 2) {
        cell.textField.enabled = NO;
    }else{
        cell.textField.enabled = YES;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50 * Iphone6ScaleHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


/**
 收到通知计算BMI
 */
- (void)refreshBMI{
    CDKAssessmentModel *model1 = self.dataArray[0];
    CDKAssessmentModel *model2 = self.dataArray[1];
    CGFloat height = [model1.valueText floatValue];
    CGFloat weight = [model2.valueText floatValue];
    
    CDKAssessmentModel *model3 = self.dataArray[2];
    
    height = height * 0.01;
    CGFloat bmi = weight / (height * height);
    
    if (height < 0.01) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        CDKPatientMannagerRecordCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.textField.text = @"";
        model3.valueText = @"";
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        return;
    }
    
    if (bmi > 0 && weight > 0) {
        model3.valueText = [NSString stringWithFormat:@"%.2f",bmi];
    }else{
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        CDKPatientMannagerRecordCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.textField.text = @"";
        model3.valueText = @"";
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark -- Lazy
-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CDKPatientMannagerRecordCell class]) bundle:nil] forCellReuseIdentifier:patientMannagerRecordCell];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

-(NSMutableArray *) dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDian = YES;
    
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
    
    if ([string length] > 0) {
        
        unichar single = [string characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
            
            //            //首字母不能为0和小数点
            //            if([textField.text length] == 0){
            //                if(single == '.') {
            //                    [SVProgressHUD showInfoWithStatus:@"第一个数字不能为小数点"];
            //                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
            //                    return NO;
            //                }
            //                if (single == '0') {
            //                    [SVProgressHUD showInfoWithStatus:@"第一个数字不能为0"];
            //                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
            //                    return NO;
            //                }
            //            }
            
            //输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian = YES;
                    return YES;
                    
                }else{
                    [self showMessageHud:@"您已经输入过小数点了"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                if (isHaveDian) {//存在小数点
                    
                    //判断小数点的位数
                    NSRange ran = [textField.text rangeOfString:@"."];
                    if (range.location - ran.location <= 2) {
                        return YES;
                    }else{
                        [self showMessageHud:@"最多输入两位小数"];
                        return NO;
                    }
                }else{
                    return YES;
                }
            }
        }else{//输入的数据格式不正确
            [self showMessageHud:@"您输入的格式不正确"];
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end


