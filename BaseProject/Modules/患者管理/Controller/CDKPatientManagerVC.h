//
//  CDKPatientManagerVC.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/12.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKPationtListModel.h"
@interface CDKPatientManagerVC : UIViewController
@property(nonatomic,strong) CDKPationtListModel *pationtModel;
@property(nonatomic,assign) BOOL isPOPToRoot;
@end
