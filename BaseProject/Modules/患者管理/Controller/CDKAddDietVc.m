//
//  CDKAddDietVc.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/15.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKAddDietVc.h"
#import "CDKAddDietLeftCell.h"
#import "CDKAddDietRightCell.h"
#import "CDKFoodTypeModel.h"
#import "CDKFoodDietDetailsModel.h"

#import "CDKHaveMealsTypeModel.h"
#import "CDKFoodDietDetailsModel.h"

static NSString *leftCell = @"leftCell";
static NSString *rightCell = @"rightCell";

@interface CDKAddDietVc ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property(nonatomic,strong) UITableView *leftTableView;
@property(nonatomic,strong) UITableView *rightTableView;
@property(nonatomic,strong) UITextField *searchTextField;

/** 左边数据*/
@property (nonatomic,copy) NSArray *leftDataArray;

/** 右边数据*/
@property (nonatomic,strong) NSMutableArray *rightDataArray;

/** 默认选中第一行*/
@property (nonatomic,assign) NSInteger index;

/** 首字母数组*/
@property (nonatomic,strong) NSMutableArray *abcArray;
@property(nonatomic,strong) NSMutableArray *allDatas;
@property(nonatomic,strong) NSArray *originAllFoodsModel;
@property(nonatomic,strong) NSMutableArray *searchResults;
@property(nonatomic,strong) HanyuPinyinOutputFormat *pinyinFormat;


@end

@implementation CDKAddDietVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.index = 0;
    self.pinyinFormat=[[HanyuPinyinOutputFormat alloc] init];
    [self.pinyinFormat setToneType:ToneTypeWithoutTone];
    [self.pinyinFormat setVCharType:VCharTypeWithV];
    [self.pinyinFormat setCaseType:CaseTypeLowercase];
    [self setupUI];
    self.searchResults=[NSMutableArray array];
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0, 0, 40, 40);
    [btn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithCustomView:btn];
    btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=item;
    
}
-(void)back{
    if (self.searchTextField.text.length>0) {
        self.searchTextField.text=@"";
        [self textfileChange:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];

    }
}

- (void)setupUI{
    
    self.searchTextField = [[UITextField alloc] init];
    self.searchTextField.size = CGSizeMake(250, 30);
    self.searchTextField.font = [UIFont systemFontOfSize:12];
    self.searchTextField.placeholder = @"输入饮食名称";
    self.searchTextField.backgroundColor = RGB(247,247,247);
    self.searchTextField.delegate = self;
    self.searchTextField.clearButtonMode=UITextFieldViewModeAlways;

    UIImageView *searchIcon = [[UIImageView alloc] init];
    searchIcon.image = [UIImage imageNamed:@"搜索"];
    searchIcon.contentMode = UIViewContentModeCenter;
    searchIcon.size = CGSizeMake(30, 30);
    self.searchTextField.leftView = searchIcon;
    self.searchTextField.leftViewMode = UITextFieldViewModeAlways;
    self.navigationItem.titleView = self.searchTextField;
    [YSUtil makeCornerRadius:self.searchTextField.height /2 view:self.searchTextField];
    self.searchTextField.returnKeyType=UIReturnKeySearch;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textfileChange:) name:UITextFieldTextDidChangeNotification object:self.searchTextField];
    
    __weak typeof(self) weakSelf = self;
    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain handler:^(id  _Nonnull sender) {
        [weakSelf.view endEditing:YES];
        if (weakSelf.searchTextField.text.length>0) {
            [weakSelf back];
            return ;
        }
        NSMutableArray *seletc_foods=[NSMutableArray array];
        for (CDKFoodTypeModel *type_model in weakSelf.allDatas) {
            NSArray *foods=type_model.children;
            
            for (NSArray *arr in foods) {
                
                for (CDKFoodModel *food_model in arr) {
                    if (food_model.gram.length>0) {
                        CDKFoodDietDetailsModel *model=[CDKFoodDietDetailsModel new];
                        model.haveMealsTypeId = weakSelf.haveMealsTypeId;
                        model.foodName=food_model.foodName;
                        model.foodId=food_model.id;
                        model.foodTypeId=food_model.foodTypeId;
                        model.gram=food_model.gram;
                        model.dietImagePath=food_model.picPath;
                        [seletc_foods addObject:model];
                    }
                }
            }
        }
        if (seletc_foods.count<=0) {
            [weakSelf showMessageHud:@"至少输入一个值"];
        }else{
            if (weakSelf.callback) {
                weakSelf.callback(seletc_foods);
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
        
        
    }];
    self.navigationItem.rightBarButtonItem = item;
    
    self.allDatas=[NSMutableArray array];
    [self loadfoodType];
    
    [self.view addSubview:self.leftTableView];
    [self.leftTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        //            CGRectMake(0, 0, 90 * Iphone6ScaleWidth, KScreenHeight - NavHeight)
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(90 * Iphone6ScaleWidth);
        make.bottom.mas_equalTo(0);
    }];
    [self.view addSubview:self.rightTableView];
    [self.rightTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        //            CGRectMake(90 * Iphone6ScaleWidth, 0,KScreenWidth -  90 * Iphone6ScaleWidth, KScreenHeight - NavHeight)
        make.left.equalTo(self.leftTableView.mas_right).offset(0);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
}
-(void)textfileChange:(NSNotification*)ntf{
    YSLog(@"");
    if (self.searchTextField.text.length==0) {
        [self.leftTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(90 * Iphone6ScaleWidth);
        }];
        [self.rightTableView reloadData];
    }else{
        
    }
}
#pragma mark -- Network
- (void)loadfoodType{//获取食物类型
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/GetFoodTypes";
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:nil success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [self.allDatas removeAllObjects];
            NSArray *arr=[NSArray modelArrayWithClass:[CDKFoodTypeModel class] json:responseObject[@"result"]];
            [self.allDatas addObjectsFromArray:arr];
            [self.leftTableView reloadData];
            [self loadall_foods];
        }else{
           [self showMessageHud:responseObject[@"error"][@"message"]];
        }
        
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadall_foods{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/GetAllFoods";
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{} success:^(id responseObject) {
//        [self dismissHud];
        if (SUCCEESS) {
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                NSArray *arr=[NSArray modelArrayWithClass:[CDKFoodModel class] json:responseObject[@"result"]];
                
                for (CDKFoodTypeModel *type_model in self.allDatas) {
                    NSMutableArray *foods=[NSMutableArray array];
                    for (CDKFoodModel *food in arr) {
                        if ([type_model.id isEqualToString:food.foodTypeId]) {
                            [foods addObject:food];
                        }
                        
                        for (CDKFoodDietDetailsModel *subModel in self.modelArray) {
                            
                            if ([food.id isEqualToString:subModel.foodId]) {
                                
                                    food.gram = subModel.gram;
                                }
                            }
                            
                        
                        
                    }
                    
                    [self _sortDataArray:foods :type_model];
                }
                self.originAllFoodsModel=arr;
                
                
                for(CDKFoodModel *food in arr){
                    NSString *outputPinyin=[PinyinHelper toHanyuPinyinStringWithNSString:food.foodName withHanyuPinyinOutputFormat:self.pinyinFormat withNSString:@""];
                    food.pinying=outputPinyin;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self dismissHud];
                    [self.rightTableView reloadData];
                });
            });
            
            
        }else{
            [self dismissHud];
            [self showMessageHud:responseObject[@"error"][@"message"]];

        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadFoodData:(CDKFoodTypeModel*)type{
    
    if (type.children.count>0) {
        [self.rightTableView reloadData];
      
    }else{
        [self loadall_foods];
    }
    
  
}
#pragma mark -- Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if ([tableView isEqual:self.leftTableView]) {
        return 1;
    }else{
        if (self.searchTextField.text.length>0) {
            return 1;
        }
        if (self.allDatas.count>0) {
            CDKFoodTypeModel *model=self.allDatas[self.index];
            
            
            return  model.section_titles.count;
        }
        return 0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([tableView isEqual:self.leftTableView]) {
        return self.allDatas.count;
    }else{
        if (self.searchTextField.text.length>0) {
            return self.searchResults.count;
        }
        CDKFoodTypeModel *model=self.allDatas[self.index];
        NSArray *children=model.children;
        NSArray *foods=children[section];
        return foods.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:self.leftTableView]) {
        CDKAddDietLeftCell *cell = [tableView dequeueReusableCellWithIdentifier:leftCell forIndexPath:indexPath];
        CDKFoodTypeModel *typemodel=self.allDatas[indexPath.row];
        cell.leftLabel.text = typemodel.foodTypeName;
        [cell.leftImageView sd_setImageWithURL:URL(typemodel.imagePath)];
        if (indexPath.row == self.index) {
            cell.bgView.backgroundColor = RGB(69,213,160);
            cell.leftLabel.textColor = RGB(255,255,255);
        }else{
            cell.bgView.backgroundColor = RGB(245,248,247);
            cell.leftLabel.textColor = RGB(101,101,101);
        }
        
        return cell;
    }else{
        if (self.searchTextField.text.length>0) {
            CDKAddDietRightCell *cell = [tableView dequeueReusableCellWithIdentifier:rightCell forIndexPath:indexPath];
            cell.leftLabelWidth.constant = 150;
            CDKFoodModel *food=self.searchResults[indexPath.row];
            cell.model=food;
            return cell;
        }
        CDKAddDietRightCell *cell = [tableView dequeueReusableCellWithIdentifier:rightCell forIndexPath:indexPath];
        cell.leftLabelWidth.constant = 70;
        CDKFoodTypeModel *model=self.allDatas[self.index];
        CDKFoodModel *food=model.children[indexPath.section][indexPath.row];
        cell.model=food;
        
        
       
        return cell;
    }
}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (self.searchTextField.text.length>0) {
        return nil;
    }
    if ([tableView isEqual:self.rightTableView]&& (self.allDatas.count>0)) {
        
        CDKFoodTypeModel *model=self.allDatas[self.index];
      
        return [model.section_titles objectAtIndex:section];
    }else{
        return nil;
    }
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    if (self.searchTextField.text.length>0) {
        return nil;
    }
    if ([tableView isEqual:self.rightTableView]) {
        if (self.allDatas.count>0) {
            CDKFoodTypeModel *model=self.allDatas[self.index];
            return model.section_titles;
        }
        return nil;
        
    }else{
        return nil;
    }
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    NSIndexPath *selectIndexPath = [NSIndexPath indexPathForRow:0 inSection:index];
    [tableView scrollToRowAtIndexPath:selectIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    return index;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.leftTableView]) {
        return 75 ;
    }else{
        return 60 ;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:self.leftTableView]) {
        self.index = indexPath.row;
        [self.leftTableView reloadData];
        [self loadFoodData:self.allDatas[self.index]];
    }
}

- (void)_sortDataArray:(NSArray *)foodList :(CDKFoodTypeModel*)type
{
//    [self.rightDataArray removeAllObjects];
    NSMutableArray *sectiontitles=[NSMutableArray array];
    NSMutableArray *contactsSource = [NSMutableArray arrayWithArray:foodList];
    
    //从获取的数据中剔除黑名单中的好友
    //    NSArray *blockList = [[EMClient sharedClient].contactManager getBlackList];
    //    for (NSString *buddy in buddyList) {
    //        if (![blockList containsObject:buddy]) {
    //            [contactsSource addObject:buddy];
    //        }
    //    }
    
    //建立索引的核心, 返回27，是a－z和＃
    UILocalizedIndexedCollation *indexCollation = [UILocalizedIndexedCollation currentCollation];
    [sectiontitles addObjectsFromArray:[indexCollation sectionTitles]];
    
    NSInteger highSection = [sectiontitles count];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithCapacity:highSection];
    for (int i = 0; i < highSection; i++) {
        NSMutableArray *sectionArray = [NSMutableArray arrayWithCapacity:1];
        [sortedArray addObject:sectionArray];
    }
    
    //按首字母分组
    for (CDKFoodModel *food in contactsSource) {
            NSString *firstLetter = [EaseChineseToPinyin pinyinFromChineseString:food.foodName];
            NSInteger section;
            if (firstLetter.length > 0) {
                section = [indexCollation sectionForObject:[firstLetter substringToIndex:1] collationStringSelector:@selector(uppercaseString)];
            } else {
                section = [sortedArray count] - 1;
            }
            
            NSMutableArray *array = [sortedArray objectAtIndex:section];
            [array addObject:food];
        
    }
    
    //每个section内的数组排序
    for (int i = 0; i < [sortedArray count]; i++) {
        NSArray *array = [[sortedArray objectAtIndex:i] sortedArrayUsingComparator:^NSComparisonResult(CDKFoodModel *obj1, CDKFoodModel *obj2) {
            NSString *firstLetter1 = [EaseChineseToPinyin pinyinFromChineseString:obj1.foodName];
            firstLetter1 = [[firstLetter1 substringToIndex:1] uppercaseString];
            
            NSString *firstLetter2 = [EaseChineseToPinyin pinyinFromChineseString:obj2.foodName];
            firstLetter2 = [[firstLetter2 substringToIndex:1] uppercaseString];
            
            return [firstLetter1 caseInsensitiveCompare:firstLetter2];
        }];
        
        
        [sortedArray replaceObjectAtIndex:i withObject:[NSMutableArray arrayWithArray:array]];
    }
    
    //去掉空的section
    for (NSInteger i = [sortedArray count] - 1; i >= 0; i--) {
        NSArray *array = [sortedArray objectAtIndex:i];
        if ([array count] == 0) {
            [sortedArray removeObjectAtIndex:i];
            [sectiontitles removeObjectAtIndex:i];
        }
    }
//    [self.rightDataArray addObjectsFromArray:sortedArray];
    
    type.children=sortedArray;
    type.section_titles=sectiontitles;
//    [self.rightTableView reloadData];
}
-(void)dealloc{
    YSLog(@"--dealloc add diret--");
}
#pragma mark -- Lazy
-(UITableView *)leftTableView {
    if (!_leftTableView) {
        _leftTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        _leftTableView.backgroundColor = RGB(245,248,247);
        _leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _leftTableView.tableFooterView = [UIView new];
        [_leftTableView registerNib:[UINib nibWithNibName:NSStringFromClass([CDKAddDietLeftCell class]) bundle:nil] forCellReuseIdentifier:leftCell];
       
    }
    return _leftTableView;
}

-(UITableView *)rightTableView {
    if (!_rightTableView) {
        _rightTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        _rightTableView.sectionIndexBackgroundColor = RGB(239,238,224);
        _rightTableView.backgroundColor = [UIColor whiteColor];
        _rightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _rightTableView.tableFooterView = [UIView new];
        [_rightTableView registerNib:[UINib nibWithNibName:NSStringFromClass([CDKAddDietRightCell class]) bundle:nil] forCellReuseIdentifier:rightCell];
        _rightTableView.sectionIndexColor=RGB(97,97,97);
        
       
    }
    return _rightTableView;
}
-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    
    [header.textLabel setTextColor:RGB(154,154,154)];
    
}


- (NSMutableArray *)abcArray{
    if (!_abcArray) {
        _abcArray = [NSMutableArray array];
    }
    return _abcArray;
}

- (NSMutableArray *)rightDataArray{
    if (!_rightDataArray) {
        _rightDataArray = [NSMutableArray array];
    }
    return _rightDataArray;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.text.length>0) {
        [self.searchResults removeAllObjects];
        NSString *searchStr =[PinyinHelper toHanyuPinyinStringWithNSString:textField.text withHanyuPinyinOutputFormat:self.pinyinFormat withNSString:@""];
        
        // 谓词搜索
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self contains [cd] %@",searchStr];
        for (CDKFoodModel *obj in self.originAllFoodsModel) {
            BOOL isContain =  [predicate evaluateWithObject:obj.pinying];
            if (isContain) {
                [self.searchResults addObject:obj];
            }
        }
        [self.leftTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(0);
        }];
        [self.rightTableView reloadData];
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
