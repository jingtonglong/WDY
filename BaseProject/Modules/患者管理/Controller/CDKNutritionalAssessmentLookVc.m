//
//  CDKBiochemistryLookVc.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKNutritionalAssessmentLookVc.h"
#import "CDKPatientMannagerRecordCell.h"

static NSString *patientMannagerRecordCell = @"patientMannagerRecordCell";

@interface CDKNutritionalAssessmentLookVc ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,copy) NSMutableArray *dataArray;
@property (nonatomic,copy) NSArray *leftTitleArray;
@property (nonatomic,copy) NSArray *rightTitleArray;
@property (nonatomic,strong) UILabel *rightLabel;

@property (nonatomic,copy) NSString *pHeight;
@property (nonatomic,copy) NSString *pWeight;
@property (nonatomic,copy) NSString *bim;
@property (nonatomic,copy) NSString *upperArm;
@property (nonatomic,copy) NSString *tricepsBrachii;
@property (nonatomic,copy) NSString *leftGrip;
@property (nonatomic,copy) NSString *rightGrip;
@property (nonatomic,copy) NSString *lowBlood;
@property (nonatomic,copy) NSString *highBlood;

/** 请求的数据模型*/
@property (nonatomic,strong) CDKNutrationHistoryModel *networkModel;
@end

@implementation CDKNutritionalAssessmentLookVc

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"营养评估";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticeChangeText) name:@"assessChangeText" object:nil];
    
    [self setupUI];
}

- (void)setupUI{
    self.leftTitleArray = @[@"身高",@"体重",@"BMI",@"上臀围",@"肱三头肌皮褶厚度",@"左手握力",@"右手握力",@"收缩压",@"舒张压"];
    self.rightTitleArray = @[@"cm",@"kg",@"",@"cm",@"mm",@"kg",@"kg",@"mmHg",@"mmHg"];
    
    if (self.historyModel.brId ==0) {
        __weak typeof(self) weakSelf = self;
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"修改" style:UIBarButtonItemStyleDone handler:^(id  _Nonnull sender) {
            [JXTAlertTools showAlertWith:weakSelf title:@"提示" message:@"是否确认修改" callbackBlock:^(NSInteger btnIndex) {
                if (btnIndex == 1) {
                    [weakSelf.tableView endEditing:YES];
                    [weakSelf networkByEidt];
                }
            } cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"确定", nil];
        }];
        self.navigationItem.rightBarButtonItem = rightItem;
    }
    
    [self network];
}

- (void)network{
    [self showLoadingHud:nil];
    NSString *api = @"/api/services/ckd/patient/QueryToDay";
    NSDictionary *dic = @{
                          @"patientId":self.model.id
                          };
    [YSNetworkingManager requestWithUrl:api :POST paramiters:dic success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            NSLog(@"%@",responseObject);
            self.networkModel = [CDKNutrationHistoryModel modelWithJSON:responseObject[@"result"]];
            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            [self.tableView reloadData];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}


#pragma mark -- Network
- (void)networkByEidt{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/UpdateNutritionalAssessment";
    NSDictionary *dic = @{
                          @"pHeight":self.pHeight?self.pHeight:@"",
                          @"pWeight":self.pWeight?self.pWeight:@"",
                          @"bim":self.bim?self.bim:@"",
                          @"upperArm":self.upperArm?self.upperArm:@"",
                          @"tricepsBrachii":self.tricepsBrachii?self.tricepsBrachii:@"",
                          @"leftGrip":self.leftGrip?self.leftGrip:@"",
                          @"rightGrip":self.rightGrip?self.rightGrip:@"",
                          @"highBlood":self.highBlood?self.highBlood:@"",
                          @"lowBlood":self.lowBlood?self.lowBlood:@"",
                          @"patientId":self.model.id,
                          };
    [YSNetworkingManager requestWithUrl:api :POST paramiters:dic success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [self showMessageHud:@"修改成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

- (void)noticeChangeText{
    
}

#pragma mark -- Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.leftTitleArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CDKPatientMannagerRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:patientMannagerRecordCell forIndexPath:indexPath];
    cell.leftLabel.text = self.leftTitleArray[indexPath.row];
    cell.rightLabel.text = self.rightTitleArray[indexPath.row];
//    cell.textField.text = self.dataArray[indexPath.row];
    cell.index = indexPath.row;
    CDKNutrationHistoryModel *model = self.networkModel;
    if ([cell.leftLabel.text isEqualToString:@"身高"]) {
        cell.textField.text = model.pHeight;
        self.pHeight = model.pHeight;
    }else if ([cell.leftLabel.text isEqualToString:@"体重"]){
        cell.textField.text = model.pWeight;
        self.pWeight = model.pWeight;
    }else if ([cell.leftLabel.text isEqualToString:@"BMI"]){
        cell.textField.text = model.bim;
        self.bim = model.bim;
    }else if ([cell.leftLabel.text isEqualToString:@"上臀围"]){
        cell.textField.text = model.upperArm;
        self.upperArm = model.upperArm;
    }else if ([cell.leftLabel.text isEqualToString:@"肱三头肌皮褶厚度"]){
        cell.textField.text = model.tricepsBrachii;
        self.tricepsBrachii = model.tricepsBrachii;
    }else if ([cell.leftLabel.text isEqualToString:@"左手握力"]){
        cell.textField.text = model.leftGrip;
        self.leftGrip = model.leftGrip;
    }else if ([cell.leftLabel.text isEqualToString:@"右手握力"]){
        cell.textField.text = model.rightGrip;
        self.rightGrip = model.rightGrip;
    }else if ([cell.leftLabel.text isEqualToString:@"舒张压"]){
        cell.textField.text = model.lowBlood;
        self.lowBlood = model.lowBlood;
    }else if ([cell.leftLabel.text isEqualToString:@"收缩压"]){
        cell.textField.text = model.highBlood;
        self.highBlood = model.highBlood;
    }
    
    cell.index = indexPath.row;
    __weak typeof(self) weakSelf = self;
    cell.block = ^(NSInteger index, NSString *value) {
        if (index == 0) {
            weakSelf.pHeight = value;
            
            CGFloat height = [value floatValue];
            CGFloat weight = [weakSelf.pWeight floatValue];
            height = [value floatValue] * 0.01;
            CGFloat bmi = weight / (height * height);
            weakSelf.bim = [NSString stringWithFormat:@"%.2f",bmi];
            model.bim = weakSelf.bim;
            model.pHeight = weakSelf.pHeight;
        }else if (index == 1){
            weakSelf.pWeight = value;
            
            CGFloat height = [weakSelf.pHeight floatValue];
            height = height * 0.01;
            CGFloat weight = [value floatValue];
            CGFloat bmi = weight / (height * height);
            weakSelf.bim = [NSString stringWithFormat:@"%.2f",bmi];
            model.bim = weakSelf.bim;
            model.pWeight = weakSelf.pWeight;
        }else if (index == 2){
//            weakSelf.bim = value;
        }else if (index == 3){
            weakSelf.upperArm = value;
            model.upperArm = weakSelf.upperArm;
        }else if (index == 4){
            weakSelf.tricepsBrachii = value;
            model.tricepsBrachii = weakSelf.tricepsBrachii;
        }else if (index == 5){
            weakSelf.leftGrip = value;
            model.leftGrip = weakSelf.leftGrip;
        }else if (index == 6){
            weakSelf.rightGrip = value;
            model.rightGrip = weakSelf.rightGrip;
        }else if (index == 7){
            weakSelf.highBlood = value;
            model.highBlood = weakSelf.highBlood;
        }else if (index == 8){
            weakSelf.lowBlood = value;
            model.lowBlood = weakSelf.lowBlood;
        }
        [weakSelf.tableView reloadData];
    };
    
    if (self.historyModel.brId==0) {
        cell.textField.enabled = YES;
    }else{
        cell.textField.enabled = NO;
    }
    
    if (indexPath.row == 2) {
        cell.textField.enabled = NO;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50 * Iphone6ScaleHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


#pragma mark -- Lazy
-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CDKPatientMannagerRecordCell class]) bundle:nil] forCellReuseIdentifier:patientMannagerRecordCell];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

-(NSMutableArray *) dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

