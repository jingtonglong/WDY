//
//  CDKBiochemistryLookVc.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKBiochemistryLookVc : UIViewController
@property(nonatomic,copy) NSString *fromType;

/** 患者id*/
@property (nonatomic,copy) NSString *patientId;

/** 检查id*/
@property (nonatomic,copy) NSString *treatmentId;

/** 时间*/
@property (nonatomic,copy) NSString *time;

/**
 {
 "id" : 202,--检查id
 "measureDate" : "2017-12-28"
 }
 */
@property(nonatomic,strong) NSDictionary *data;

@end
