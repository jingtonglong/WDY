//
//  CDKChangeRemarkVC.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/9.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKPationInfoModel.h"


@interface CDKChangeRemarkVC : UIViewController
@property(nonatomic,strong) CDKPationInfoModel *model;
@property(nonatomic,copy) void(^callback)(void);

@end
