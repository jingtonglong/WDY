//
//  CDKNutritionalAssessmentLookVc.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/14.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKPationtListModel.h"
#import "CDKNutrationHistoryModel.h"

@interface CDKNutritionalAssessmentLookVc : UIViewController
@property(nonatomic,strong) CDKPationtListModel *model;
/** 模型*/
@property (nonatomic,strong) CDKNutrationHistoryModel *historyModel;

@end
