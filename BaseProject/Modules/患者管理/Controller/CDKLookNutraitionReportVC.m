//
//  CDKLookNutraitionReportVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/14.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKLookNutraitionReportVC.h"
#import "CDKNutraitionView.h"
#import "CDKPrescribePrinceple.h"
#import "CDKPriscripMessureView.h"
@interface CDKLookNutraitionReportVC ()<CDKNutraiViewDelegate,CDKPricelpleDelegate,CDKMessureDelegate>
@property(nonatomic,strong) UIButton *illOpenBtn;
@property(nonatomic,strong) UIView *illViewContaint;// 患病情况

@property(nonatomic,strong) CDKNutraitionView *nutaitionVauleView;//营养评估

@property(nonatomic,strong) CDKPrescribePrinceple *princepleView;// 处方原则

@property(nonatomic,strong) CDKPriscripMessureView *messureView;// 处方措施

@end
#define contentHeight 430
@implementation CDKLookNutraitionReportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}
-(void)setupUI{
    UIScrollView *scroll=[UIScrollView new];
    self.view=scroll;
    self.view.backgroundColor=[UIColor whiteColor];
    if (@available(iOS 11,*)) {
        scroll.contentInsetAdjustmentBehavior=UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets=NO;
    }
    // 创建顶部视图
    scroll.bounces=NO;
    
    UIView *top=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 144+StatusHeight)];
//    top.backgroundColor=RGB(99,220,175);
    UIView *statusView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, StatusHeight)];
    UIImageView *imgv=[[UIImageView alloc] initWithFrame:top.bounds];
    imgv.image=[UIImage imageNamed:@"top-bg"];
    [top addSubview:imgv];
    [top addSubview:statusView];
    
    UIView *navbar=[[UIView alloc] initWithFrame:CGRectMake(0, StatusHeight, KScreenWidth, 44)];
    [top addSubview:navbar];
    UIButton *back=[UIButton buttonWithType:UIButtonTypeCustom];
    [back setImage:[UIImage imageNamed:@"左翻页"] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];     back.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [navbar addSubview:back];
    
    [back mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.centerY.equalTo(navbar);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(40);
    }];
    
    UILabel *title=[UILabel new];
    title.text=@"营养报告查看";
    title.font=SYSTEMFONT(16);
    title.textColor=[UIColor whiteColor];
    [title sizeToFit];
    [navbar addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(navbar);
    }];
    UIImageView *downarrow=[UIImageView new];
    downarrow.image=[UIImage imageNamed:@"三角形选择"];
    [navbar addSubview:downarrow];
    [downarrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(navbar);
        make.right.equalTo(navbar.mas_right).offset(-10);
        make.width.mas_equalTo(7);
        make.height.mas_equalTo(6);

    }];
    UILabel *timelab=[UILabel new];
    timelab.text=@"2018-09-23";
    timelab.font=SYSTEMFONT(13);
    timelab.textColor=[UIColor whiteColor];
    [timelab sizeToFit];
    [navbar addSubview:timelab];
    [timelab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(downarrow);
        make.right.equalTo(downarrow.mas_left).offset(-5);
    }];
    
    // 个人信息
    UILabel *nameLab=[UILabel new];
    nameLab.text=@"张家豪";
    nameLab.font=SYSTEMFONT(18);
    nameLab.textColor=[UIColor whiteColor];
    [nameLab sizeToFit];
    [top addSubview:nameLab];
    [nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(navbar.mas_bottom).offset(30);
        make.left.mas_equalTo(15);
    }];
    UILabel *sex=[UILabel new];
    sex.text=@"男";
    sex.font=SYSTEMFONT(14);
    sex.textColor=[UIColor whiteColor];
    [sex sizeToFit];
    [top addSubview:sex];
    [sex mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(nameLab);
        make.left.equalTo(nameLab.mas_right).offset(21);
    }];
    
    UILabel *age=[UILabel new];
    age.text=@"45岁";
    age.font=SYSTEMFONT(14);
    age.textColor=[UIColor whiteColor];
    [age sizeToFit];
    [top addSubview:age];
    [age mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(nameLab);
        make.left.equalTo(sex.mas_right).offset(21);
    }];
    
    UILabel *doctor=[UILabel new];
    doctor.text=@"医生：张峰";
    doctor.font=SYSTEMFONT(14);
    doctor.textColor=[UIColor whiteColor];
//    [age sizeToFit];
    [top addSubview:doctor];
    [doctor mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.bottom.equalTo(nameLab);
    }];
    
    [scroll addSubview:top];
    // 患病情况
    UIView *illView=[[UIView alloc] init];
    illView.clipsToBounds=YES;
//    illView.backgroundColor=[UIColor redColor];
    [scroll addSubview:illView];
    self.illViewContaint=illView;
    [illView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(top.mas_bottom).offset(10);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(48);
        make.centerX.equalTo(scroll);
    }];
    [YSUtil makeCornerRadius:6 view:illView];
    [YSUtil makeBorderWidth:1 view:illView borderColor:RGB(213,236,223)];
    
    
    UIView *illtopView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth-20, 48)];
//    illtopView.backgroundColor=[UIColor redColor];
    [illView addSubview:illtopView];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(swichIllView)];
    [illtopView addGestureRecognizer:tap];
    UIView *colorView1=[[UIView alloc] init];
    colorView1.backgroundColor=RGB(255,139,0);
    [YSUtil makeCornerRadius:3 view:colorView1];
    [illtopView addSubview:colorView1];
    [colorView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(illtopView);
        make.width.mas_equalTo(6);
        make.left.mas_equalTo(12);
        make.height.mas_equalTo(14);
    }];
    UILabel *illsituation=[UILabel new];
    illsituation.text=@"患病情况";
    illsituation.font=SYSTEMFONT(14);
    illsituation.textColor=RGB(50,50,50);
    [illtopView addSubview:illsituation];
    [illsituation mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(colorView1.mas_right).offset(10);
        make.centerY.equalTo(colorView1);
    }];
    UIButton *illArrowBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    self.illOpenBtn=illArrowBtn;
    illArrowBtn.userInteractionEnabled=NO;
    [illArrowBtn setImage:[UIImage imageNamed:@"展开"] forState:UIControlStateNormal];
    [illArrowBtn setImage:[UIImage imageNamed:@"收起"] forState:UIControlStateSelected];
    [illtopView addSubview:illArrowBtn];
    [illArrowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.centerY.equalTo(colorView1);
        make.width.mas_equalTo(13);
        make.height.mas_equalTo(7);
    }];
    
    NSArray *illList=@[@"01ㅣ慢性肾病",@"02ㅣ其它合并疾病"];
    NSArray *illdetailtitles=@[@"3期",@"糖尿病"];
    CGFloat illsubviewHeight=70;
    for (int i=0;i<illList.count;i++) {
        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, i*illsubviewHeight+48, KScreenWidth-20, illsubviewHeight)];
        UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake(12, 5,KScreenWidth-20 , 20)];
        title.text=illList[i];
        title.font=SYSTEMFONT(15);
        title.textColor=RGB(69,213,160);
        [view addSubview:title];
        UIView *dot=[[UIView alloc] initWithFrame:CGRectMake(15, 45, 6, 6)];
        dot.backgroundColor=RGB(255,191,129);
        [YSUtil makeCornerRadius:3 view:dot];
        [view addSubview:dot];
        
        UILabel *ildetailLab=[[UILabel alloc] init];
        ildetailLab.text=illdetailtitles[i];
        ildetailLab.font=SYSTEMFONT(14);
        ildetailLab.textColor=RGB(50,50,50);
        [view addSubview:ildetailLab];
        [ildetailLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(dot.mas_right).offset(5);
            make.centerY.equalTo(dot);
        }];
        
        if (i==0) {
            UIView *line=[UIView new];
            line.backgroundColor=RGB(213,236,223);
            [view addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.mas_equalTo(0);
                make.left.mas_equalTo(10);
                make.right.mas_equalTo(-10);
                make.height.mas_equalTo(1);
            }];
            
        }
       [illView addSubview:view];
    }
    
    // 营养评估
    CDKNutraitionView *nutraitionvaule=[CDKNutraitionView xibinstanse];
    [scroll addSubview:nutraitionvaule];
    [nutraitionvaule mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.equalTo(illView.mas_bottom).offset(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(48);
        make.centerX.equalTo(scroll);
    }];
    nutraitionvaule.delegate=self;
    self.nutaitionVauleView=nutraitionvaule;
    // 处方原则
    CDKPrescribePrinceple *princeple=[CDKPrescribePrinceple xibinstanse];
    [scroll addSubview:princeple];
    [princeple mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.equalTo(nutraitionvaule.mas_bottom).offset(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(48);
        make.centerX.equalTo(scroll);
    }];
    princeple.delegate=self;
    self.princepleView=princeple;
    
    // 处方措施
    CDKPriscripMessureView *messure=[CDKPriscripMessureView xibinstanse];
    
    [scroll addSubview:messure];
    [messure mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.equalTo(princeple.mas_bottom).offset(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(48);
        make.centerX.equalTo(scroll);
    }];
    messure.delegate=self;
    self.messureView=messure;

//    [scroll setContentSize:CGSizeMake(KScreenWidth, 396)];
}
-(void)backClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma 展开或收起视图

-(void)swichIllView{
    self.illOpenBtn.selected=!self.illOpenBtn.selected;
    if (self.illOpenBtn.isSelected) {
        
            [self.illViewContaint mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(48+140);
            }];
        
        UIScrollView *scroll =  (UIScrollView*)self.view;
        CGFloat height=scroll.contentSize.height;
        if (height<=0) {
            height=contentHeight;
        }
        [scroll setContentSize:CGSizeMake(KScreenWidth, height+140)];
       
    }else{
     
            [self.illViewContaint mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(48);
            }];
        UIScrollView *scroll =  (UIScrollView*)self.view;
        CGFloat height=scroll.contentSize.height;
        [scroll setContentSize:CGSizeMake(KScreenWidth, height-140)];
       
    }
   
}
#pragma NutraitionViewDelegate
-(void)openOrCloseNutraitionView:(UIButton *)sender{
    if (sender.isSelected) {
        
        [self.nutaitionVauleView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48+1386);
        }];
        UIScrollView *scroll =  (UIScrollView*)self.view;
        CGFloat height=scroll.contentSize.height;
        if (height<=0) {
            height=contentHeight;
        }
        [scroll setContentSize:CGSizeMake(KScreenWidth, height+1386)];
        
    }else{
        
        [self.nutaitionVauleView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48);
        }];
        UIScrollView *scroll =  (UIScrollView*)self.view;
        CGFloat height=scroll.contentSize.height;
        [scroll setContentSize:CGSizeMake(KScreenWidth, height-1386)];
        
    }
}
#pragma PrincelpleViewDelegate
-(void)openOrClosePricelpleView:(UIButton *)sender{
    if (sender.isSelected) {
        
        [self.princepleView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48+383);
        }];
        UIScrollView *scroll =  (UIScrollView*)self.view;
        CGFloat height=scroll.contentSize.height;
        if (height<=0) {
            height=contentHeight;
        }
        [scroll setContentSize:CGSizeMake(KScreenWidth, height+383)];
        
    }else{
        
        [self.princepleView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48);
        }];
        UIScrollView *scroll =  (UIScrollView*)self.view;
        CGFloat height=scroll.contentSize.height;
        [scroll setContentSize:CGSizeMake(KScreenWidth, height-383)];
        
    }
}

#pragma MessureViewDelegate

-(void)openOrCloseMessureView:(UIButton *)sender{
    if (sender.isSelected) {
        
        [self.messureView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48+775);
        }];
        UIScrollView *scroll =  (UIScrollView*)self.view;
        CGFloat height=scroll.contentSize.height;
        if (height<=0) {
            height=contentHeight;
        }
        [scroll setContentSize:CGSizeMake(KScreenWidth, height+775)];
        
    }else{
        
        [self.messureView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(48);
        }];
        UIScrollView *scroll =  (UIScrollView*)self.view;
        CGFloat height=scroll.contentSize.height;
        [scroll setContentSize:CGSizeMake(KScreenWidth, height-775)];
        
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    kApplication.statusBarStyle=UIStatusBarStyleLightContent;
    //    YSLog(@"viewDidAppear");
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    UIScrollView *scroll =  (UIScrollView*)self.view;
////    CGFloat height=scroll.contentSize.height;
//    [scroll setContentSize:CGSizeMake(KScreenWidth, 396)];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    kApplication.statusBarStyle=UIStatusBarStyleDefault;
    //    self.navigationController.navigationBar.hidden=NO;
    //    YSLog(@"viewWillDisappear");
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    
}
@end
