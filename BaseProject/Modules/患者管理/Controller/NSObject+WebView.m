//
//  NSObject+WebView.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/7/6.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "NSObject+WebView.h"


@implementation NSObject (WebView)
- (void)webView:(id)unused didCreateJavaScriptContext:(JSContext *)ctx forFrame:(id)alsoUnused {
    
    if (!ctx)
        
        return;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CreatJSContex" object:ctx];
    
}

@end
