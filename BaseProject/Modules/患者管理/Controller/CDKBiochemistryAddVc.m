//
//  CDKBiochemistryLookVc.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKBiochemistryAddVc.h"
#import "CDKPatientMannagerRecordCell.h"
#import "STPickerDate.h"
static NSString *patientMannagerRecordCell = @"patientMannagerRecordCell";

@interface CDKBiochemistryAddVc ()<UITableViewDelegate,UITableViewDataSource,STPickerDateDelegate>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,copy) NSMutableArray *dataArray;
@property (nonatomic,strong) UILabel *rightLabel;

/** 选择年月日*/
@property (nonatomic,strong) STPickerDate *pickView;
@end

@implementation CDKBiochemistryAddVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"生化数据";
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    [self setupUI];
    [self networkByBaseData];
    
}

- (void)setupUI{
    __weak typeof(self) weakSelf = self;
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"完成提交" style:UIBarButtonItemStyleDone handler:^(id  _Nonnull sender) {
        [weakSelf.tableView endEditing:YES];
        [weakSelf networkByTime];
    }];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UIView *headView = [UIView new];
    headView.backgroundColor = RGB(247,247,247);
    [self.view addSubview:headView];
    [headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top);
        make.height.mas_equalTo(50 * Iphone6ScaleHeight);
    }];
    
    UILabel *leftLabel = [UILabel new];
    leftLabel.text = @"检查日期";
    leftLabel.font = SYSTEMFONT(14);
    leftLabel.textColor = RGB(50,50,50);
    [headView addSubview:leftLabel];
    [leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headView.mas_left).offset(15);
        make.centerY.equalTo(headView.mas_centerY);
    }];
    
    UIImageView *rightImageView = [UIImageView new];
    rightImageView.image = [UIImage imageNamed:@"展开"];
    [headView addSubview:rightImageView];
    [rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(headView.mas_right).offset(-15);
        make.centerY.equalTo(headView.mas_centerY);
    }];
    
    self.rightLabel = [UILabel new];
    self.rightLabel.text = @"选择时间";
    self.rightLabel.font = SYSTEMFONT(14);
    self.rightLabel.textColor = RGB(50,50,50);
    self.rightLabel.textAlignment = NSTextAlignmentRight;
    [headView addSubview:self.rightLabel];
    [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(rightImageView.mas_left).offset(-10);
        make.centerY.equalTo(headView.mas_centerY);
    }];
    
    [YSUtil addClickEvent:self action:@selector(clickHeadView) owner:headView];
    
}

#pragma mark -- Network

/**
 获取基础数据
 */
- (void)networkByBaseData{
    [self showLoadingHud:nil];
    NSString *url = @"/api/services/ckd/audit/QueryInspectstandard";
    [YSNetworkingManager requestWithUrl:url :POST paramiters:nil success:^(id responseObject) {
        if (SUCCEESS) {
            [self.dataArray addObjectsFromArray:[NSArray modelArrayWithClass:[CDKBiochemistryBaseDataModel class] json:responseObject[@"result"]]];
            [self.tableView reloadData];
        }
        [self dismissHud];
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}


/**
 新增数据
 */
- (void)networkByAdd{
    [self showLoadingHud:nil];
    NSString *type = @"1";
    NSString *one = @"";
    NSString *two = @"";
    for (CDKBiochemistryBaseDataModel *model in self.dataArray) {
        if (model.value.length > 0) {
            type = @"2";
        }
        if ([model.id isEqualToString:@"1"]) {
            if (model.value.length > 0) {
                one = @"haveValue";
            }
        }
        
        if ([model.id isEqualToString:@"2"]) {
            if (model.value.length > 0) {
                two = @"haveValue";
            }
        }
    }
    
    if ([type isEqualToString:@"1"]) {
        [self showMessageHud:@"请填写任意一项或多项检查指标数据"];
        return;
    }
    
    if (one.length > 0 && two.length > 0) {
        
    }else if (one.length == 0 && two.length == 0){
        
    }else{
        [self showMessageHud:@"舒张压、收缩压需要同时填写"];
        return;
    }
    
    NSArray *jsonArray = [self.dataArray modelToJSONObject];
    
    NSDictionary *dic = @{
                          @"patientId":self.model.id,
                          @"measureDate":self.rightLabel.text,
                          @"inspectrecords":jsonArray,
                          };
    
    NSString *url = @"/api/services/ckd/patient/AddTreatmentInspectrecord";
    [YSNetworkingManager requestWithUrl:url :POST paramiters:dic success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [self showMessageHud:@"提交成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}


/**
 验证时间
 */
- (void)networkByTime{
    if ([self.rightLabel.text isEqualToString:@"选择时间"]) {
        [self showMessageHud:@"请选择时间"];
        return;
    }
    NSString *url = @"/api/services/ckd/patient/CheckDate";
    NSDictionary *dic = @{
                          @"patientId":self.model.id,
                          @"measureDate":self.rightLabel.text,
                          };
    [YSNetworkingManager requestWithUrl:url :POST paramiters:dic success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [self networkByAdd];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

#pragma mark -- Action
- (void)clickHeadView{
    self.pickView = [[STPickerDate alloc]init];
    [self.pickView setYearLeast:1900];
    [self.pickView setYearSum:200];
    [self.pickView setDelegate:self];
    [self.pickView show];
}

#pragma mark -- Delegate
- (void)pickerDate:(STPickerDate *)pickerDate year:(NSInteger)year month:(NSInteger)month day:(NSInteger)day{
    self.rightLabel.text = [NSString stringWithFormat:@"%zd-%zd-%zd", year, month, day];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CDKPatientMannagerRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:patientMannagerRecordCell forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    cell.textField.enabled = YES;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50 * Iphone6ScaleHeight;
}


#pragma mark -- Lazy
-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 50 * Iphone6ScaleHeight, KScreenWidth, KScreenHeight - 50 * Iphone6ScaleHeight - NavHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CDKPatientMannagerRecordCell class]) bundle:nil] forCellReuseIdentifier:patientMannagerRecordCell];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

-(NSMutableArray *) dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

