//
//  CDKChangeRemarkVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/9.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKChangeRemarkVC.h"

@interface CDKChangeRemarkVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textfiled;


@end

@implementation CDKChangeRemarkVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title=@"修改备注";
    
    [YSUtil makeCornerRadius:4 view:self.textfiled];
    [YSUtil makeBorderWidth:0.8 view:self.textfiled borderColor:[UIColor colorWithHexString:@"#9A9A9A"]];
    
    WeakSelf(weak);
    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithTitle:@"保存" style:(UIBarButtonItemStylePlain) handler:^(id  _Nonnull sender) {
        [weak saveRemark];
    }];
    self.navigationItem.rightBarButtonItem=item;
    self.textfiled.text=self.model.remarkCkd;
}
-(void)saveRemark{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/UpdateRemarkById";
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"id":self.model.id,@"Remark":self.textfiled.text?self.textfiled.text:@""} success:^(id responseObject) {
        
        [self dismissHud];
        if (SUCCEESS) {
            [self showMessageHud:@"修改成功"];
            self.model.remarkCkd=self.textfiled.text;
            if (self.callback) {
                self.callback();
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [self showMessageHud:@"修改失败"];

        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
