//
//  CDKSeletedWithDesVCTableViewController.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/11.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKWithDesModel.h"
@interface CDKSeletedWithDesVCTableViewController : UITableViewController
@property(nonatomic,strong) NSArray *datas;
@property(nonatomic,strong) NSMutableArray *seleted_data;
@property(nonatomic,copy) void(^callback)(NSArray*);

@end
