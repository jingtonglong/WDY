//
//  CDKDieteticDetailVc.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKEatMoel.h"
@interface CDKDieteticDetailVc : BaseViewController
@property(nonatomic,copy) NSString *pationtID;//患者ID
@property(nonatomic,copy) NSString *dietTime;//饮食日期

/** 上个界面传来的数据模型*/
@property (nonatomic,copy) NSArray *eatDatasModels;
/**
 饮食记录ID 没有表示新增
 */
@property(nonatomic,copy) NSString *DietBodyId;

@end
