//
//  CDKAddDietVc.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/15.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CDKAddDietVc : UIViewController
@property(nonatomic,copy) NSString *haveMealsTypeId;//用餐类型id
@property(nonatomic,copy) void(^callback)(NSArray*);

/** 回显模型数组*/
@property (nonatomic,copy) NSArray *modelArray;
@end
