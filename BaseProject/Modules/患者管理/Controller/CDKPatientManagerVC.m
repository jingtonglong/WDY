//
//  CDKPatientManagerVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/12.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//



#import "CDKEatMoel.h"
#import <UIKit/UIKit.h>
#import "CDKNutrationHistoryModel.h"
#import "CDKPatientManagerVC.h"
#import "CDKPationtMHeader.h"
#import "CDKLookNutraitionReportVC.h"
#import "CDKSerilThreeDayCell.h"
#import "CDKEatDataDetailCell.h"
#import "CDKEatDetailAddCell.h"
#import "CDKBiologeCheckCell.h"
#import "CDKPationtManagerTimeCell.h"
#import "CDKPationtManagerTimeSection.h"
#import "CDKPationtManagerInfoCell.h"
#import "CDKAddNewNutraitionReportVC.h"
#import "CDKChartVauleCellTableViewCell.h"
#import "CDKDieteticDetailVc.h"
#import "CDKAddDietVc.h"
#import "CDKBiochemistryAddVc.h"
#import "CDKBiochemistryLookVc.h"
#import "CDKNutritionalAssessmentAddVc.h"
#import "CDKNutritionalAssessmentLookVc.h"
#import "CDKWebViewVC.h"
#import "CDKExcelRecoredVC.h"
#import "CDKPtiontManagerH5_detailVC.h"
@interface CDKPatientManagerVC ()<UITableViewDelegate,UITableViewDataSource,CDKPationtMHeaderDelegate>
@property(nonatomic,strong) UITableView  *tableView;
@property(nonatomic,assign) SeletedType seletType;

@property(nonatomic,strong) NSMutableArray *eatDatas;//饮食数据
@property(nonatomic,strong) NSMutableArray *checkDatas;//生化检查日期
@property(nonatomic,strong) NSMutableArray *nutritionDatas;//营养评估
@property(nonatomic,strong) NSMutableArray *ExcelDatas;
@property(nonatomic,assign) BOOL isfold;// 是否展开
@property(nonatomic,strong) NSArray *chartVauleDatas;// 量表评估
@property(nonatomic,strong) NSArray *chartVauleColors;// 量表评估颜色数组

@property(nonatomic,strong) NSMutableArray *nutritionReportDatas;
@property(nonatomic,strong) CDKPationInfoModel *pationInfoModel;

@property(nonatomic,strong) CDKEatMoel *eatmodel;// 近7天连续三天数据模型

@property(nonatomic,copy) NSString *check_time_str;
@property(nonatomic,copy) NSString *nutraition_time_str;
@property(nonatomic,copy) NSString *nutraition_report_time_str;

@property(nonatomic,assign) NSInteger nutraitionPage;//营养评估分页

@property(nonatomic,assign) NSInteger nutraition_report_Page;//营养报告分页
@end

@implementation CDKPatientManagerVC
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] init];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.rowHeight=UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight=150;
        [_tableView registerNib:[UINib nibWithNibName:@"CDKChartVauleCellTableViewCell" bundle:nil] forCellReuseIdentifier:@"chart"];
         [_tableView registerNib:[UINib nibWithNibName:@"CDKSerilThreeDayCell" bundle:nil] forCellReuseIdentifier:@"ThreeDay"];
        [_tableView registerNib:[UINib nibWithNibName:@"CDKEatDataDetailCell" bundle:nil] forCellReuseIdentifier:@"eat"];
          [_tableView registerNib:[UINib nibWithNibName:@"CDKEatDetailAddCell" bundle:nil] forCellReuseIdentifier:@"eatadd"];
         [_tableView registerNib:[UINib nibWithNibName:@"CDKBiologeCheckCell" bundle:nil] forCellReuseIdentifier:@"check"];
         [_tableView registerNib:[UINib nibWithNibName:@"CDKPationtManagerTimeCell" bundle:nil] forCellReuseIdentifier:@"time"];
         [_tableView registerNib:[UINib nibWithNibName:@"CDKPationtManagerInfoCell" bundle:nil] forCellReuseIdentifier:@"info"];
        _tableView.backgroundColor=RGB(247,247,247);
        [_tableView registerClass:[CDKPationtManagerTimeSection class] forHeaderFooterViewReuseIdentifier:@"timeSection"];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"month_cell"]; _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        UIView *footer=[UIView new];
        footer.backgroundColor=[UIColor whiteColor];
        _tableView.tableFooterView= footer;
    }
    return _tableView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CDKPationtMHeader_SeletedType:self.seletType];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"详情数据";
    self.view.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    CDKPationtMHeader *headView=[[CDKPationtMHeader alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 100)];
    self.tableView.tableHeaderView=headView;
    self.seletType=EatData;
    headView.delegate=self;
    self.chartVauleDatas=@[@"SGA主观全面评定",@"MIS营养不良炎症评定",@"营养风险筛查NRS2002评估",@"MNA简易评估",@"改良SGA主观全面评定"];
    self.chartVauleColors=@[RGB(255,139,0),
                            RGB(75,197,234),RGB(191,139,255),RGB(255,137,195),RGB(163,185,255),RGB(163,255,243)];
    
    [self loadPationtInfo];
    [self loadEatData];
    [self load_week_eatdata];
    self.eatDatas=[NSMutableArray array];
    self.checkDatas=[NSMutableArray array];
    self.nutritionDatas=[NSMutableArray array];
    self.nutritionReportDatas=[NSMutableArray array];
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0, 0, 40, 40);
    [btn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithCustomView:btn];
    btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=item;
    
}
-(void)back{
    if (self.isPOPToRoot) {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
-(void)loadPationtInfo{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/QueryPatientById";
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"PatientId":self.pationtModel.id} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            self.pationInfoModel=[CDKPationInfoModel modelWithJSON:responseObject[@"result"]];
            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];

        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadEatData{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/ThreeDaysData";
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"PatientId":self.pationtModel.id} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            self.eatmodel=[CDKEatMoel modelWithJSON:responseObject[@"result"][0]];
            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)load_week_eatdata{// 近七天饮食数据
    
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/QueryDietByPatientId";
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"PatientId":self.pationtModel.id} success:^(id responseObject) {
        
        [self dismissHud];
        if (SUCCEESS) {
            [self.eatDatas removeAllObjects];
            NSArray *arr=[NSArray modelArrayWithClass:[CDKEatMoel class] json:responseObject[@"result"]];
            [self.eatDatas addObjectsFromArray:arr];
            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadBiochemicaldate{//获取生化检查日期
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/QueryTreatmentDate";
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"PatientId":self.pationtModel.id} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            self.check_time_str=nil;
            [self.checkDatas removeAllObjects];
            NSArray *arr=responseObject[@"result"];
//            [self.checkDatas addObjectsFromArray:arr];
            for (NSDictionary *obj in arr) {
                NSString *time=obj[@"measureDate"];
                NSArray *component=[time componentsSeparatedByString:@"-"];
                NSString *month=component[1];
                if (![month isEqualToString:self.check_time_str]) {
                    
                    [self.checkDatas addObject:[NSString stringWithFormat:@"%@月",month]];
                    self.check_time_str=component[1];
                    [self.checkDatas addObject:obj];

                }else{
                         [self.checkDatas addObject:obj];
                     }
                     
            }
            
            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
    
}
-(void)loadNutraitionHistory:(SeletedType)type{
    [self showLoadingHud:nil];
    [self.tableView.mj_footer endRefreshing];
    NSString *api=@"";
    if (type==Nutrition) {//营养评估历史
        api=@"/api/services/ckd/patient/HistoryList";
        self.nutraitionPage=1;
    }else{//营养报告历史
        api=@"/api/services/ckd/patient/BusinessReportHistory";
        self.nutraition_report_Page=1;

    }
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"PatientId":self.pationtModel.id,@"PageIndex":@1,@"PageSize":@15} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            if (type==Nutrition) {//营养评估历史
                self.nutraition_time_str=nil;
                [self.nutritionDatas removeAllObjects];
                NSArray *arr=[NSArray modelArrayWithClass:[CDKNutrationHistoryModel class] json:responseObject[@"result"][@"items"]];
                for (CDKNutrationHistoryModel *obj in arr) {
                    NSString *time=obj.assessmentTime;
                    NSArray *component=[time componentsSeparatedByString:@"-"];
                    NSString *month=component[1];
                    if (![month isEqualToString:self.nutraition_time_str]) {
                       
                        [self.nutritionDatas addObject:[NSString stringWithFormat:@"%@月",month]];
                        self.nutraition_time_str=month;
                        [self.nutritionDatas addObject:obj];
                    }else{
                        [self.nutritionDatas addObject:obj];
                    }
                    
                }
            }else{//营养报告历史
//                "businessReportId" : "2",
//                "createTime" : "2018-05-16",
//                "doctorName" : {
//                }
                self.nutraition_report_time_str=nil;
                [self.nutritionReportDatas removeAllObjects];
                NSArray *arr=responseObject[@"result"][@"items"];
                for (NSDictionary *obj in arr) {
                    NSString *time=obj[@"createTime"];
                    NSArray *component=[time componentsSeparatedByString:@"-"];
                    NSString *month=component[1];
                    if (![month isEqualToString:self.nutraition_report_time_str]) {
                        
                        [self.nutritionReportDatas addObject:[NSString stringWithFormat:@"%@月",month]];
                        self.nutraition_report_time_str=month;
                          [self.nutritionReportDatas addObject:obj];
                    }else{
                        [self.nutritionReportDatas addObject:obj];
                    }
                    
                }
                
            }
           
            
            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadMoreNutraitionHistory:(SeletedType)type{//营养评估历史
//    [self.tableView.mj_header endRefreshing];
//    [self showLoadingHud:nil];
    NSString *api=@"";
    NSInteger page=1;
    if (type==Nutrition) {//营养评估历史
        api=@"/api/services/ckd/patient/HistoryList";
        self.nutraitionPage++;
        page=self.nutraitionPage;
    }else{//营养报告历史
        api=@"/api/services/ckd/patient/BusinessReportHistory";
        self.nutraition_report_Page++;
        page=self.nutraition_report_Page;
    }
   
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"PatientId":self.pationtModel.id,@"PageIndex":@(page),@"PageSize":@15} success:^(id responseObject) {
//        [self dismissHud];
        if (SUCCEESS) {
            NSArray *arr=@[];
            if (type==Nutrition) {//营养评估历史
                arr=[NSArray modelArrayWithClass:[CDKNutrationHistoryModel class] json:responseObject[@"result"][@"items"]];
                for (CDKNutrationHistoryModel *obj in arr) {
                    NSString *time=obj.assessmentTime;
                    NSArray *component=[time componentsSeparatedByString:@"-"];
                    NSString *month=component[1];
                    if (![month isEqualToString:self.nutraition_time_str]) {
                       
                        [self.nutritionDatas addObject:[NSString stringWithFormat:@"%@月",month]];
                        self.nutraition_time_str=month;
                         [self.nutritionDatas addObject:obj];
                    }else{
                        [self.nutritionDatas addObject:obj];
                    }
                    
                }
            }else{//营养报告历史
               arr=responseObject[@"result"][@"items"];
                for (NSDictionary *obj in arr) {
                    NSString *time=obj[@"createTime"];
                    NSArray *component=[time componentsSeparatedByString:@"-"];
                    NSString *month=component[1];
                    if (![month isEqualToString:self.nutraition_report_time_str]) {
                       
                        [self.nutritionReportDatas addObject:[NSString stringWithFormat:@"%@月",month]];
                        self.nutraition_report_time_str=month;
                        [self.nutritionReportDatas addObject:obj];
                    }else{
                        [self.nutritionReportDatas addObject:obj];
                    }
                    
                }
            }
           
            if (arr.count>0) {
                [self.tableView.mj_footer endRefreshing];
                [self.tableView reloadData];
            }else{
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                if (self.nutraitionPage>1) {
                    self.nutraitionPage--;
                }
            }
            
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            if (self.nutraitionPage>1) {
                self.nutraitionPage--;
            }
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (self.nutraitionPage>1) {
            self.nutraitionPage--;
        }
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.seletType==EatData||self.seletType==Excel) {
        return 2;
    }else{
        return 2;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        if (self.seletType==Excel) {
            return 1;
        }
        return 2;
    }else{
        switch (self.seletType) {
            case EatData:
                return self.eatDatas.count;
                break;
            case Excel:
                return self.chartVauleDatas.count;
                break;
            case Check:
                return self.checkDatas.count;
                break;
            case Nutrition:
                return self.nutritionDatas.count;
                break;
            case NutritionReport:
                return self.nutritionReportDatas.count;
                break;
            default:
                return 7;
                break;
        }
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        if (self.seletType==Nutrition) {
            id dic=self.nutritionDatas[indexPath.row];
            if ([dic isKindOfClass:[CDKNutrationHistoryModel class]]) {
                CDKNutrationHistoryModel *model=(CDKNutrationHistoryModel*)dic;
                if (model.brId==0) {
                    return YES;
                }
            }
            
        }
    }
    return NO;
}
/**
 *  左滑cell时出现什么按钮
 */
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WeakSelf(weak);
    id dic=self.nutritionDatas[indexPath.row];
        CDKNutrationHistoryModel *model=(CDKNutrationHistoryModel*)dic;
       
    
    UITableViewRowAction *action1 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
//        tableView.editing = NO;
        UIAlertController *alertvc= [UIAlertController alertControllerWithTitle:@"确认删除" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *comfirm=[UIAlertAction actionWithTitle:@"确认" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
            // 删除操作
            [weak deleteTodayNutaition:model];
        }];
        UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            //
            weak.tableView.editing=NO;
        }];
       
        [alertvc addAction:comfirm];
        [alertvc addAction:cancel];
        [self presentViewController:alertvc animated:YES completion:nil];

    }];
//    action1.backgroundColor=RGB(254,91,0);
    
    return @[action1];
}

/**
 删除今天的营养报告

 @param model 营养报告模型
 */
-(void)deleteTodayNutaition:(CDKNutrationHistoryModel*)model{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/Remove";
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"patientId":self.pationtModel.id} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
//            [self.nutritionDatas removeObject:model];
             [self loadNutraitionHistory:Nutrition];
            
        }else{
            self.tableView.editing=NO;
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
        
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            CDKPationtManagerInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"info" forIndexPath:indexPath];
            if (self.pationInfoModel) {
                cell.pation_info_model=self.pationInfoModel;
            }else{
                cell.infoModel=self.pationtModel;
            }
            
            
            cell.arrowBtn.selected=self.isfold;
            if (self.isfold) {
                cell.containtinfoheight.constant=352;
                cell.infocontaintView.hidden=NO;
            }else{
                cell.infocontaintView.hidden=YES;

                cell.containtinfoheight.constant=0;

            }
            __weak typeof(self) weak=self;
            cell.callback = ^{
                weak.isfold=!weak.isfold;
                [weak.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:(UITableViewRowAnimationFade)];
            };
            return cell;
        }else{
           
            switch (self.seletType) {
                case EatData:// 饮食数据cell连续三天
                    {
                        CDKSerilThreeDayCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ThreeDay" forIndexPath:indexPath];
                        cell.energe.text=self.eatmodel.energy?self.eatmodel.energy:@"-";
                        cell.protien.text=self.eatmodel.protein?self.eatmodel.protein:@"-";
                        cell.potassium.text=self.eatmodel.potassium?self.eatmodel.potassium:@"-";
                        cell.calcium.text=self.eatmodel.calcium?self.eatmodel.calcium:@"-";
                        cell.phosphorus.text=self.eatmodel.phosphorus?self.eatmodel.phosphorus:@"-";
                        cell.sodium.text=self.eatmodel.sodium?self.eatmodel.sodium:@"-";
                        cell.water.text=self.eatmodel.water?self.eatmodel.water:@"-";
                        return cell;
                    }
                    break;
                case Check:
                {
                    CDKBiologeCheckCell *cell=[tableView dequeueReusableCellWithIdentifier:@"check" forIndexPath:indexPath];
                    cell.colorView.backgroundColor=RGB(69,213,160);
                    [cell.titleLab setTitle:@"生化检查" forState:UIControlStateNormal];
                    [cell.addTitleLab setTitle:@" 新增数据" forState:UIControlStateNormal];
//                     [cell.addTitleLab sizeToFit];
                    return cell;
                }
                    break;
                case Nutrition:
                {
                    CDKBiologeCheckCell *cell=[tableView dequeueReusableCellWithIdentifier:@"check" forIndexPath:indexPath];
                    cell.colorView.backgroundColor=RGB(69,213,160);
                    [cell.titleLab setTitle:@"营养评估" forState:UIControlStateNormal];
                    [cell.addTitleLab setTitle:@" 新增评估" forState:UIControlStateNormal];
//                     [cell.addTitleLab sizeToFit];
                    return cell;
                }
                    break;
                case NutritionReport:
                {
                    CDKBiologeCheckCell *cell=[tableView dequeueReusableCellWithIdentifier:@"check" forIndexPath:indexPath];
                    cell.colorView.backgroundColor=RGB(69,213,160);

                    [cell.titleLab setTitle:@"营养报告" forState:UIControlStateNormal];
                    [cell.addTitleLab setTitle:@" 新增报告" forState:UIControlStateNormal];
//                     [cell.addTitleLab sizeToFit];
                    return cell;
                }
                    break;
                default: {
                    CDKBiologeCheckCell *cell=[tableView dequeueReusableCellWithIdentifier:@"check" forIndexPath:indexPath];
                    cell.colorView.backgroundColor=RGB(69,213,160);
                    [cell.titleLab setTitle:@"营养评估" forState:UIControlStateNormal];
                    [cell.addTitleLab setTitle:@" 新增评估" forState:UIControlStateNormal];
//                    [cell.addTitleLab sizeToFit];
                    return cell;
                }
                    break;
            }
        }
        
    }else{
        switch (self.seletType) {
            case EatData:
            {
                CDKEatMoel *model=self.eatDatas[indexPath.row];
                if (model.id) {
                    CDKEatDataDetailCell *cell=[tableView dequeueReusableCellWithIdentifier:@"eat" forIndexPath:indexPath];
                    cell.model=model;
                    return cell;// 饮食详情以第一个cell
                    
                }else{
                    CDKEatDetailAddCell *cell=[tableView dequeueReusableCellWithIdentifier:@"eatadd" forIndexPath:indexPath];
                    cell.time.text=model.basicTime;
                    return cell;
                }
            }
                break;
            case Check:
            {
                id dic=self.checkDatas[indexPath.row];
                if ([dic isKindOfClass:[NSString class]]) {
                    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"month_cell" forIndexPath:indexPath];
                    cell.textLabel.text=[NSString stringWithFormat:@" %@",dic];
                    cell.textLabel.textColor=RGB(154,154,154);
                    cell.textLabel.font=[UIFont systemFontOfSize:14];
                    cell.backgroundColor=RGB(247,247,247);
                   cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    
                    return cell;//
                }else{
                    CDKPationtManagerTimeCell *cell=[tableView dequeueReusableCellWithIdentifier:@"time" forIndexPath:indexPath];
                    cell.detaiLab.text=@"";//生化检查
                    
                    cell.timeLab.text=dic[@"measureDate"];
                    return cell;//
                }
                
            }
                break;
            case Nutrition:
            {
                id dic=self.nutritionDatas[indexPath.row];
                if ([dic isKindOfClass:[NSString class]]) {
                    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"month_cell" forIndexPath:indexPath];
                    cell.textLabel.text=[NSString stringWithFormat:@" %@",dic];
                    cell.textLabel.textColor=RGB(154,154,154);
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    cell.textLabel.font=[UIFont systemFontOfSize:14];
                    cell.backgroundColor=RGB(247,247,247);
                    return cell;
                    
                }
                CDKNutrationHistoryModel *model=(CDKNutrationHistoryModel*)dic;
                CDKPationtManagerTimeCell *cell=[tableView dequeueReusableCellWithIdentifier:@"time" forIndexPath:indexPath];
//                0：未引用，可编辑，1：已引用，不可编辑，2：已过期，不可编辑
                if(model.brId == 1){
                   cell.detaiLab.text=@"已引用，不可编辑";//营养评估
                }else if(model.brId == 2){
                    cell.detaiLab.text=@"已过期，不可编辑";//营养评估
                }else{
                    cell.detaiLab.text=@"未引用，可编辑";//营养评估
                }
                
                cell.timeLab.text=model.assessmentTime;
                return cell;// 营养评估
            }
                break;
            case NutritionReport:
            {
                id dic=self.nutritionReportDatas[indexPath.row];
                if ([dic isKindOfClass:[NSString class]]) {
                    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"month_cell" forIndexPath:indexPath];
                    cell.textLabel.text=[NSString stringWithFormat:@" %@",dic];
                    cell.textLabel.textColor=RGB(154,154,154);
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                    cell.textLabel.font=[UIFont systemFontOfSize:14];
                    cell.backgroundColor=RGB(247,247,247);
                    return cell;
                    
                }
                CDKPationtManagerTimeCell *cell=[tableView dequeueReusableCellWithIdentifier:@"time" forIndexPath:indexPath];
                cell.detaiLab.text=dic[@"doctorName"];//营养报告
                cell.timeLab.text=dic[@"createTime"];
                return cell;//
            }
                break;
            case Excel:
            {
                CDKChartVauleCellTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"chart" forIndexPath:indexPath];
                cell.colorView.backgroundColor=self.chartVauleColors[indexPath.row];//量表评估
                cell.titleLab.text=self.chartVauleDatas[indexPath.row];
                return cell;// 饮食详情以第一个cell
            }
                break;
//            default: {
//                CDKSerilThreeDayCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ThreeDay" forIndexPath:indexPath];
//
//                return cell;
//            }
//                break;
        }
        
        
        
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (self.seletType) {
        case EatData:
            {
                if (section==0) {
                    return nil;
                }else{
                    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 50)];
                    
                    view.backgroundColor=[UIColor whiteColor];
                    
                    UIView *colorview=[[UIView alloc] initWithFrame:CGRectMake(10, 18.5, 6, 13)];
                    colorview.backgroundColor=RGB(255,139,0);
                    colorview.layer.cornerRadius=3;
                    colorview.layer.masksToBounds=YES;
                    [view addSubview:colorview];
                    UIButton *btn=[[UIButton alloc] init];
                    
                    [btn setTitle:@"近7天饮食详情" forState:UIControlStateNormal];
                    btn.userInteractionEnabled=NO; btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
                    btn.titleLabel.font=SYSTEMFONT(15);
                   [btn setTitleColor:RGB(73,73,73) forState:UIControlStateNormal];
                    btn.frame=CGRectMake(33, 0, KScreenWidth-10, 50);
                    [view addSubview:btn];
                    return view;
                   
                }
            }
            break;
        case Excel:{
            return nil;
        }
        default:
        {
            return  nil;
            
        };
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (self.seletType) {
        case EatData:
        {
            if (section==0) {
                return 0;
            }else{
                return 50;
            }
        }
            break;
        case Excel:{
            return 0;
        }
        default:{
           
            return 0;
           
        };
            break;
    }

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.section==0&&indexPath.row==0) {
//        if (<#condition#>) {
//            <#statements#>
//        }
//    }
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    YSLog(@"indexPath---%@",indexPath);
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==0&&indexPath.row==0) {
        return;
    }
    if(indexPath.section==0&&indexPath.row==1){
        switch (self.seletType) {
            case EatData:
                
                break;
            case Check:// 新增生化检查
            {
                CDKBiochemistryAddVc *vc=[[CDKBiochemistryAddVc alloc] init];
                vc.model=self.pationtModel;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case Nutrition:// 新增生营养评估
            {
                [self showLoadingHud:nil];
                NSString *api=@"/api/services/ckd/patient/Alert";
                [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"patientId":self.pationtModel.id} success:^(id responseObject) {
                    [self dismissHud];
                    if (SUCCEESS) {
                        if ([responseObject[@"result"][@"isSucceed"] integerValue]==0) {
                            CDKNutritionalAssessmentAddVc *vc=[[CDKNutritionalAssessmentAddVc alloc] init];
                            vc.model=self.pationtModel;
                            [self.navigationController pushViewController:vc animated:YES];
                        }else{
                           [self showMessageHud:responseObject[@"result"][@"description"]];
                        }
                        
                    }else{
                          [self showMessageHud:responseObject[@"error"][@"message"]];
                    }
                  
                    
                } errorHandel:^(NSError *error) {
                    [self dismissHud];
                    if (error.code ==-1001 ) {
                        [self showMessageHud:@"请求超时"];
                    }else{
                        [self showMessageHud:@"请检查网络"];
                    }
                }];
                
            }
                break;
            case NutritionReport:// 新增营养报告
            {
                
                        
//                            CDKAddNewNutraitionReportVC *vc=[[CDKAddNewNutraitionReportVC alloc] init];
//                            vc.model=self.pationtModel;
//                            [self.navigationController pushViewController:vc animated:YES];
                        CDKPtiontManagerH5_detailVC *vc=[[CDKPtiontManagerH5_detailVC alloc] init];
                        vc.url=[NSString stringWithFormat:@"%@/h5/index.html#/addreport?&pid=%@",BASE_URL,self.pationtModel.id];
                        vc.title_str=@"新增营养报告";
                        [self.navigationController pushViewController:vc animated:YES];
                   
                    
                
                
            }
                break;
            default:
                break;
        }
    }else{
        switch (self.seletType) {
            case EatData:// 饮食详情或新增饮食
            {
                CDKEatMoel *model=self.eatDatas[indexPath.row];
//                if (model.id) {// 查看饮食详情 如果ID为nil 表示新增
                    CDKDieteticDetailVc *vc=[[CDKDieteticDetailVc alloc] init];
                if(model.id){
                   vc.DietBodyId=model.id;
                    vc.dietTime=model.basicTime;
                    vc.pationtID=self.pationtModel.id;
                }else{
                    vc.pationtID=self.pationtModel.id;
                    vc.dietTime=model.basicTime;
                }
                vc.eatDatasModels = self.eatDatas;
                [self.navigationController pushViewController:vc animated:YES];
               
            }
                break;
            case Check:// 生化数据查看
            {
                id obj=self.checkDatas[indexPath.row];
                if ([obj isKindOfClass:[NSString class]]) {
                    return;
                }
                CDKBiochemistryLookVc *vc=[[CDKBiochemistryLookVc alloc] init];
                vc.data=(NSDictionary*)obj;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case Nutrition:// 营养评估查看
            {
                id obj=self.nutritionDatas[indexPath.row];
                if ([obj isKindOfClass:[NSString class]]) {
                    return;
                }
                CDKNutritionalAssessmentLookVc *vc=[[CDKNutritionalAssessmentLookVc alloc] init];
//                vc.data=obj;
                vc.model=self.pationtModel;
                vc.historyModel = obj;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case NutritionReport:// 查看营养报告
            {
                id obj=self.nutritionReportDatas[indexPath.row];
                if ([obj isKindOfClass:[NSString class]]) {
                    return;
                }
                
//                CDKLookNutraitionReportVC *vc=[[CDKLookNutraitionReportVC alloc] init];
//                vc.data=obj;
                //营养报告详情
                CDKPtiontManagerH5_detailVC *vc=[[CDKPtiontManagerH5_detailVC alloc] init];
                vc.url=[NSString stringWithFormat:@"%@/h5/index.html#/report?&pid=%@&id=%@",BASE_URL,self.pationtModel.id,obj[@"businessReportId"]];
                vc.title_str=@"营养报告详情";
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case Excel:// 量表评估记录
            {
                NSArray *arr=@[@"SGA",@"MIS",@"NRS",@"MNA",@"GLSGA"];
                CDKExcelRecoredVC *vc=[[CDKExcelRecoredVC alloc] init];
//
                vc.pationtid=self.pationtModel.id;
                vc.scale=arr[indexPath.row];
                vc.title=self.chartVauleDatas[indexPath.row];
                [self.navigationController pushViewController:vc animated:YES];
                
            }
                break;
            default:
                break;
        }
    }
   
}
#pragma CDKPationtMHeaderDelegate
-(void)CDKPationtMHeader_SeletedType:(SeletedType)type{
//    if (type!=self.seletType) {
        self.seletType=type;
    self.tableView.mj_footer=nil;
        if (type==Excel) {
            self.tableView.backgroundColor=[UIColor whiteColor];
        }else{
            self.tableView.backgroundColor=RGB(247,247,247);
        }
    
        switch (type) {
            case EatData:
            {
                [self loadEatData];
                [self load_week_eatdata];
                break;
            }
            case Check:
            {
                [self loadBiochemicaldate];
           
                break;
            }
            case Nutrition:
            {
                WeakSelf(weak); self.tableView.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
                    [weak loadMoreNutraitionHistory:Nutrition];
                }];
                self.tableView.mj_footer.ignoredScrollViewContentInsetBottom=iPhoneX_BOTTOM_HEIGHT;
                [self loadNutraitionHistory:Nutrition];
//                [self.tableView reloadData];

                break;
            }
            case NutritionReport:
            {
            WeakSelf(weak);
                
                self.tableView.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
                [weak loadMoreNutraitionHistory:NutritionReport];
            }];
                [self loadNutraitionHistory:NutritionReport];
                self.tableView.mj_footer.ignoredScrollViewContentInsetBottom=iPhoneX_BOTTOM_HEIGHT;
                break;
            }
            default:
                 [self.tableView reloadData];
                break;
        }
//        [self.tableView reloadData];
//    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
