//
//  CDKDieteticDetailVc.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/17.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//
#import "CDKImageViewer.h"
#import "CDKDieteticDetailVc.h"
#import "CDKDieteticDetailTopView.h"
#import "CDKDieteticDetailCell.h"
#import "CDKDieteticDetailHeader.h"
#import "CDKDieteticDetailFooter.h"
#import "CDKAddDietVc.h"
#import "CDKEatMoel.h"
#import "CDKHaveMealsTypeModel.h"
#import "CDKFoodDietDetailsModel.h"
#import "CDKFoodModel.h"
#import "CDKFoodImagesModel.h"
#import "CDKFoodCellStytleModel.h"

static NSString *detailCell = @"detailCell";
static NSString *detailHead = @"detailHead";
static NSString *detailFoot = @"detailFoot";

@interface CDKDieteticDetailVc ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) CDKDieteticDetailTopView *topView;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,copy) NSMutableArray *dataArray;

/** 头部标题数组*/
@property (nonatomic,copy) NSMutableArray *headTitleArray;

/** 是否显示footer*/
@property (nonatomic,assign) BOOL isShowFooter;

/** 新增的数据模型数组*/
@property (nonatomic,copy) NSArray *updateModelArray;

/** 是否显示删除按钮*/
@property (nonatomic,copy) NSString *isShowDelete;

/** 图片模型*/
@property (nonatomic,strong) NSMutableArray *imageCellStytle;
@end

@implementation CDKDieteticDetailVc

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
    kApplication.statusBarStyle=UIStatusBarStyleDefault;

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    kApplication.statusBarStyle=UIStatusBarStyleLightContent;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self setupUI];
    
}

- (void)setupUI{
    NSMutableArray *timeArray = [NSMutableArray array];
    NSMutableArray *timeArrayWiithYMD = [NSMutableArray array];
    NSMutableArray *idArray = [NSMutableArray array];
    
    for (CDKEatMoel *model in self.eatDatasModels) {
        [timeArrayWiithYMD addObject:model.basicTime];
        NSString *str = [model.basicTime substringFromIndex:model.basicTime.length - 2];
        [timeArray addObject:str];
        if (!model.id) {
            model.id = @"";
        }
        [idArray addObject:model.id];
    }
    
    self.topView = [[CDKDieteticDetailTopView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 180)withTime:timeArray idArray:idArray timeYearMonthDay:timeArrayWiithYMD selectedDaty:self.dietTime];
    __weak typeof(self) weakSelf = self;
    weakSelf.topView.block = ^(NSString *chooseId, NSString *updateTime) {
        weakSelf.DietBodyId = chooseId;
        weakSelf.dietTime = updateTime;
        [weakSelf networkByFoodType];
    };
    [self.view addSubview:self.topView];
    [self.topView.backButton addTarget:self action:@selector(clickBackButton) forControlEvents:UIControlEventTouchUpInside];

    [self.topView.editButton addTarget:self action:@selector(clickEditButton) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.DietBodyId.length > 0) {
        [self.topView.editButton setTitle:@"修改" forState:UIControlStateNormal];
        self.isShowDelete = @"no";
    }else{
        [self.topView.editButton setTitle:@"完成" forState:UIControlStateNormal];
        self.isShowDelete = @"yes";
    }
    
    [self networkByFoodType];
}


#pragma mark -- Action

/**
 点击修改按钮
 */
- (void)clickEditButton{
    [self.tableView endEditing:YES];
    if (self.DietBodyId.length > 0) {
        if ([self.topView.editButton.currentTitle isEqualToString:@"修改"]) {
            [self.topView.editButton setTitle:@"完成" forState:UIControlStateNormal];
            self.isShowFooter = YES;
            self.isShowDelete = @"yes";
            [self.tableView reloadData];
        }else{
            self.isShowFooter = NO;
            self.isShowDelete = @"no";
            [self.tableView reloadData];
            [self.topView.editButton setTitle:@"修改" forState:UIControlStateNormal];
            [self networkByAdd:NO];
        }
    }else{
        if ([self.topView.editButton.currentTitle isEqualToString:@"完成"]) {
            [self networkByAdd:YES];
        }
    }
}

#pragma mark -- Network

/**
 获取用餐类型
 */
- (void)networkByFoodType{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/GetHaveMealsType";

    [YSNetworkingManager requestWithUrl:api :POST paramiters:nil success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [self dismissHud];
            [self.headTitleArray removeAllObjects];
            [self.headTitleArray addObjectsFromArray:[NSArray modelArrayWithClass:[CDKHaveMealsTypeModel class] json:responseObject[@"result"]]];
            
            if (self.DietBodyId.length > 0) {
                if ([self.isShowDelete isEqualToString:@"yes"]) {// 编辑模式中
                    self.isShowFooter = YES;
                    [self networkByDietDetails];
                }else{// 不需要新增 直接获取详情
                    [self.topView.editButton setTitle:@"修改" forState:UIControlStateNormal];
                    self.isShowFooter = NO;
                    [self networkByDietDetails];
                }
            }else{// 点进来为新增
                self.isShowFooter = YES;
                [self.topView.editButton setTitle:@"完成" forState:UIControlStateNormal];
                [self.tableView reloadData];
            }
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

/**
 获取饮食详细
 */
- (void)networkByDietDetails{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/QueryDietDetails";
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"DietBodyId":self.DietBodyId} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [self dismissHud];
            [self.dataArray removeAllObjects];
            NSArray *array = [NSArray modelArrayWithClass:[CDKFoodDietDetailsModel class] json:responseObject[@"result"]];
            
            for (CDKHaveMealsTypeModel *model in self.headTitleArray) {
                for (CDKFoodDietDetailsModel *subModel in array) {
                    if ([subModel.haveMealsTypeId isEqualToString:model.id]) {
                        [model.subArray addObject:subModel];
                    }
                }
            }
            [self networkByCellsImages];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

/**
 新增饮食记录
 */
- (void)networkByAdd:(BOOL)isADD{
    NSString *api=@"";
    NSDictionary *dic;
    
    NSMutableArray *dataarr=[NSMutableArray array];
    for (CDKHaveMealsTypeModel *typemodel in self.headTitleArray) {
        if (typemodel.subArray.count>0) {
            [dataarr addObjectsFromArray:typemodel.subArray];
        }
    }
    if (dataarr.count<=0) {
        [self showMessageHud:@"未添加任何饮食"];
        return;
    }
    
    
    [self showLoadingHud:nil];
   
    NSArray *jsonArray = [dataarr modelToJSONObject];
    if(isADD){
        api=@"/api/services/ckd/patient/AddDiets";
        dic = @{
                @"patientId":self.pationtID,
                @"dietTime":self.dietTime,
                @"diets":jsonArray,
                };
        
    }else{
        api=@"/api/services/ckd/patient/UpdateDiets";
        dic = @{
               
                @"DietBodyId":self.DietBodyId,
                @"diets":jsonArray,
                };
    }
    [YSNetworkingManager requestWithUrl:api :POST paramiters:dic success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [self showMessageHud:@"操作成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}


/**
 删除数据
 */
- (void)networkBydelete:(NSString *)deleteId{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/RemoveDietDetail";
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"id":deleteId} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [self dismissHud];
            [self networkByFoodType];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}


/**
 查询图片

 @param typeId id
 */
- (void)networkByImage:(NSString *)typeId{
    
    if(!self.DietBodyId){
        [self showMessageHud:@"患者还没有上传图片"];
        return;
    }
        
    
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/patient/QueryDietImage";
    NSDictionary *dic = @{
                          @"dietBodyId":self.DietBodyId,
                          @"haveMealsTypeId":typeId,
                          };
    NSMutableArray *array = [NSMutableArray array];
    [YSNetworkingManager requestWithUrl:api :POST paramiters:dic success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [self dismissHud];
            [array addObjectsFromArray:[NSArray modelArrayWithClass:[CDKFoodImagesModel class] json:responseObject[@"result"]]];
            
            if (array.count<=0) {
                [self showMessageHud:@"没有图片"];
                return ;
            }
            
            NSMutableArray *imageUrlsArray = [NSMutableArray array];
            for (CDKFoodImagesModel *model in array) {
                [imageUrlsArray addObject:model.dietImagePath];
            }
            CDKImageViewer *viewer =  [[CDKImageViewer alloc] init];
            viewer.imgDatas = imageUrlsArray;
            [viewer showImageViewer:kAppWindow];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

/**
 查询图片 判断header上图片文字样式
 */
- (void)networkByCellsImages{
    NSString *api=@"/api/services/ckd/patient/DietImageCount";
    NSDictionary *dic = @{
                          @"dietBodyId":self.DietBodyId,
                          };
    [YSNetworkingManager requestWithUrl:api :POST paramiters:dic success:^(id responseObject) {
        if (SUCCEESS) {
            [self.imageCellStytle removeAllObjects];
            [self.imageCellStytle addObjectsFromArray:[NSArray modelArrayWithClass:[CDKFoodCellStytleModel class] json:responseObject[@"result"]]];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
        [self.tableView reloadData];
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

#pragma mark -- Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.headTitleArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    CDKHaveMealsTypeModel *model = self.headTitleArray[section];
    return model.subArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.topView.editButton.currentTitle isEqualToString:@"完成"]) {
        self.isShowFooter = YES;
    }
    
    CDKDieteticDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:detailCell forIndexPath:indexPath];
    if ([self.isShowDelete isEqualToString:@"yes"]) {
        cell.deleteButton.hidden = NO;
        cell.numberTextfield.userInteractionEnabled = YES;
    }else{
        cell.deleteButton.hidden = YES;
        cell.numberTextfield.userInteractionEnabled = NO;
    }
    
    CDKHaveMealsTypeModel *model = self.headTitleArray[indexPath.section];
    CDKFoodDietDetailsModel *subModel = model.subArray[indexPath.row];
    cell.model = subModel;
    WeakSelf(weakSelf);
    
    cell.block = ^(CDKFoodDietDetailsModel *blockModel) {
        if (weakSelf.DietBodyId) {// 修改时 删除
            for (CDKHaveMealsTypeModel *model in weakSelf.headTitleArray) {
                for (CDKFoodDietDetailsModel *childModel in model.subArray) {
                    if ([blockModel.id isEqualToString:childModel.id]) {
                        [weakSelf networkBydelete:blockModel.id];
                        break;
                    }
                }
            }
        }else{// 新增时 删除
            for (CDKHaveMealsTypeModel *model in weakSelf.headTitleArray) {
                for (CDKFoodDietDetailsModel *childModel in model.subArray) {
                    if ([blockModel isEqual:childModel]) {
                        [model.subArray removeObject:blockModel];
                        [weakSelf.tableView reloadData];
                        break;
                    }
                }
            }
        }
    };
    cell.leftLabel.text = subModel.foodName;
    cell.numberTextfield.text = subModel.gram;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CDKDieteticDetailHeader *head = [tableView dequeueReusableHeaderFooterViewWithIdentifier:detailHead];
    head.section=section;
    
    CDKHaveMealsTypeModel *model = self.headTitleArray[section];
    head.headerLabel.text = model.haveMealsTypeName;
    if (self.imageCellStytle.count != 0) {
        CDKFoodCellStytleModel *stytleModel = self.imageCellStytle[section];
        head.stytleModel = stytleModel;
    }

    WeakSelf(weak);
    head.callback = ^(NSInteger sec) {
        [weak networkByImage:model.id];
    };
    return head;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    CDKDieteticDetailFooter *foot = [tableView dequeueReusableHeaderFooterViewWithIdentifier:detailFoot];
    foot.layer.masksToBounds = YES;
    CDKHaveMealsTypeModel *model = self.headTitleArray[section];
    [foot.footerButton setTitle:[NSString stringWithFormat:@"   %@%@",@"新增",model.haveMealsTypeName] forState:(UIControlStateNormal)];
    
    __weak typeof(self) weakSelf = self;
    foot.callback = ^{
        CDKAddDietVc *vc=[CDKAddDietVc new];
        vc.haveMealsTypeId=model.id;
        vc.callback = ^(NSArray *array) {
            for (CDKHaveMealsTypeModel *outmodel in weakSelf.headTitleArray) {
                if ([outmodel.id isEqualToString:model.id]) {
                    [outmodel.subArray removeAllObjects];
                }
                for (CDKFoodDietDetailsModel *subModel in array) {
                    if ([outmodel.id isEqualToString:subModel.haveMealsTypeId]) {
                        [outmodel.subArray addObject:subModel];
                    }
                }
            }
            [weakSelf.tableView reloadData];
        };
        
        vc.modelArray = model.subArray;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    return foot;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50 * Iphone6ScaleHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40 *Iphone6ScaleHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (self.isShowFooter) {
        return 50 * Iphone6ScaleHeight;
    }else{
        return 0.01f;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


#pragma mark -- Lazy
-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 180, KScreenWidth, KScreenHeight - 180) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CDKDieteticDetailCell class]) bundle:nil] forCellReuseIdentifier:detailCell];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CDKDieteticDetailHeader class]) bundle:nil] forHeaderFooterViewReuseIdentifier:detailHead];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CDKDieteticDetailFooter class]) bundle:nil] forHeaderFooterViewReuseIdentifier:detailFoot];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

-(NSMutableArray *) headTitleArray {
    if (!_headTitleArray) {
        _headTitleArray = [NSMutableArray array];
    }
    return _headTitleArray;
}

-(NSMutableArray *) dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)imageCellStytle{
    if (!_imageCellStytle) {
        _imageCellStytle = [NSMutableArray array];
    }
    return _imageCellStytle;
}

#pragma mark -- Action
- (void)clickBackButton{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
