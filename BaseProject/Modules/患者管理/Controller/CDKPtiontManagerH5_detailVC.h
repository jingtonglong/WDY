//
//  CDKPtiontManagerH5_detailVC.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/7/4.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "BaseViewController.h"

@interface CDKPtiontManagerH5_detailVC : BaseViewController
@property(nonatomic,copy) NSString *url;
@property(nonatomic,copy) NSString *title_str;
@property(nonatomic,copy) void(^callback)(void);
@end
