//
//  MyQRVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "MyQRVC.h"
@interface MyQRVC ()
@property (weak, nonatomic) IBOutlet UIImageView *QRImageView;
@property (weak, nonatomic) IBOutlet UIView *contantView;
@property (weak, nonatomic) IBOutlet UIImageView *headImgV;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property(nonatomic,copy) NSString *qr_code;

@end

@implementation MyQRVC
- (instancetype)init
{
    self = [super init];
    if (self) {
        UIStoryboard *st=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self=[st instantiateViewControllerWithIdentifier:@"MyQRVC"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.QRImageView.layer.borderColor=[UIColor whiteColor].CGColor;
    self.QRImageView.layer.borderWidth=5;
    self.contantView.layer.cornerRadius=3;
    self.contantView.layer.masksToBounds=YES;
    self.contantView.layer.borderColor=RGB(202,202,202).CGColor;
    self.contantView.layer.borderWidth=1;
    [self loadData];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.headImgV sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[UserInfoManager shareInstance].userInfo.headPicture]] placeholderImage:[UIImage imageNamed:@"默认头像"]];
    self.name.text=[UserInfoManager shareInstance].userInfo.name;
}
-(void)loadData{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/user/QRcode";
//    NSDictionary *para=@{};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            self.qr_code=responseObject[@"result"];
            CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
            // 2. 恢复滤镜的默认属性
            [filter setDefaults];
            // 3. 将字符串转换成NSData
            NSData *data = [self.qr_code dataUsingEncoding:NSUTF8StringEncoding];
            // 4. 通过KVO设置滤镜inputMessage数据
            [filter setValue:data forKey:@"inputMessage"];
            // 5. 获得滤镜输出的图像
            CIImage *outputImage = [filter outputImage];
            // 6. 将CIImage转换成UIImage，并显示于imageView上 (此时获取到的二维码比较模糊,所以需要用下面的createNonInterpolatedUIImageFormCIImage方法重绘二维码)
            
            self.QRImageView.image = [self createNonInterpolatedUIImageFormCIImage:outputImage withSize:KScreenWidth-60];//重绘二维码,使其显示清晰
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
- (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat) size
{
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    // 2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
