//
//  AccuntSecurityVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "AccuntSecurityVC.h"
#import "ModifyPasswordVC.h"
#import "ModifyPhoneVC.h"
@interface AccuntSecurityVC ()

@end

@implementation AccuntSecurityVC
- (instancetype)init
{
    self = [super init];
    if (self) {
        UIStoryboard *st=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self=[st instantiateViewControllerWithIdentifier:@"AccuntSecurityVC"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row==0) {
        ModifyPasswordVC *vc=[[ModifyPasswordVC alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        ModifyPhoneVC *vc=[[ModifyPhoneVC alloc] init];
        [self.navigationController pushViewController:vc animated:YES];

    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
        
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
        
    }
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
        
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
