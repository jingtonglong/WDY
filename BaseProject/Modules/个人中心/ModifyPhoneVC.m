//
//  ModifyPhoneVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "ModifyPhoneVC.h"

@interface ModifyPhoneVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *containtView1;

@property (weak, nonatomic) IBOutlet UIView *containtView2;
@property (weak, nonatomic) IBOutlet UIButton *comfirmBtn;
@property (weak, nonatomic) IBOutlet UITextField *tx_phone;
@property (weak, nonatomic) IBOutlet UIButton *getCodeBtn;
@property (weak, nonatomic) IBOutlet UITextField *tx_securityCode;
@property(nonatomic,assign) NSInteger timer_Counter;
@property(nonatomic,strong) NSTimer *timer;
@property(nonatomic,copy) NSString *codeStr;



@end

@implementation ModifyPhoneVC
- (instancetype)init
{
    self = [super init];
    if (self) {
        UIStoryboard *st=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self=[st instantiateViewControllerWithIdentifier:@"ModifyPhoneVC"];
    }
    return self;
}
#pragma textfiledelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (![NSString isNumber:string]) {
        return NO;
    }
    
    if((textField.text.length+string.length)>11){
        return NO;
        
    }
    return YES;
}
-(void)setupUI:(UIView*)sender{
    sender.layer.cornerRadius=21;
    sender.layer.masksToBounds=YES;
    sender.layer.borderWidth=1;
    sender.layer.borderColor=RGB(173,173,173).CGColor;
}
- (IBAction)comfirmClicked:(id)sender {
    if (![self check]) {
        return;
    }
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/user/UpdateMobile";
    NSDictionary *para=@{@"Mobile":self.tx_phone.text};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            
            [self showMessageHud:@"修改成功"];
            [UserInfoManager shareInstance].isLogin=NO;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:[NSNumber numberWithBool:NO]];
            });
            
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];

        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(BOOL)check{
    if (![NSString isMobileNumber:self.tx_phone.text]) {
        [self showMessageHud:@"请输入正确的手机号"];
        return NO;
    }else if (![self.tx_securityCode.text isEqualToString:self.codeStr]){
        [self showMessageHud:@"验证码不正确"];
        return NO;
    }
    return YES;
}
// 获取验证码
- (IBAction)getSecurityCode:(id)sender {
    if (![NSString isMobileNumber:self.tx_phone.text]) {
        [self showMessageHud:@"请输入正确的手机号"];
        return;
    }
    [self getCode];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI:self.containtView1];
    [self setupUI:self.containtView2];
    self.timer_Counter=60;

}

-(void)getCode{
    
    NSString *api=@"/api/services/app/user/CheckAndSendMobileCode";
    
    NSDictionary *dic=@{@"mobile":self.tx_phone.text
                        };
    //获取验证码
    self.getCodeBtn.userInteractionEnabled=NO;
    [self showLoadingHud:nil];
    [YSNetworkingManager requestWithUrl:api :POST paramiters:dic success:^(id responseObject) {
        [self dismissHud];
        if(SUCCEESS){
            [self showMessageHud:@"发送验证码成功"];
            self.codeStr=responseObject[@"result"][@"code"];
            [self.getCodeBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            self.timer.fireDate = [NSDate distantPast];
            
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            self.getCodeBtn.userInteractionEnabled=YES;
        }
        
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
        self.getCodeBtn.userInteractionEnabled=YES;
    }];
    
}

-(NSTimer*)timer{
    
    if (!_timer) {
        _timer= [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
    }
    return _timer;
}
-(void)timerAction{
    [self.getCodeBtn setTitle:[NSString stringWithFormat:@"%ld秒后重发",self.timer_Counter--] forState:UIControlStateNormal];
    if (self.timer_Counter==-1) {
        
        [self.getCodeBtn setTitle:@"重发验证码" forState:UIControlStateNormal];
        self.getCodeBtn.userInteractionEnabled=YES;
        [self.getCodeBtn setTitleColor:RGB(255, 191, 129) forState:UIControlStateNormal];
        self.timer.fireDate=[NSDate distantFuture];
        self.timer_Counter=60;
    }
}
-(void)dealloc{
    
    YSLog(@"---");
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [self.timer invalidate];
    self.timer=nil;
    self.timer_Counter=60;
    [self.getCodeBtn setTitleColor:RGB(255, 191, 129) forState:UIControlStateNormal];
    self.getCodeBtn.userInteractionEnabled=YES;
    [self.getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
