//
//  CDKAboutUsVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/6.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKAboutUsVC.h"
#import <NSAttributedString+YYText.h>
@interface CDKAboutUsVC ()
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation CDKAboutUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title=@"关于我们与帮助";
    NSMutableAttributedString *matt=[[NSMutableAttributedString alloc] initWithString:@"东泽医疗成立于2004年，是一家专注于肾功能衰竭治疗领域及危重血液净化领域的专业化运营商。\n\n\n公司以血液净化相关产品的研发、生产、销售、服务为核心业务。"];
    CGFloat scale=1;
    if (KScreenWidth>320) {
        scale=1.12;
    }
    
   
    matt.font=[UIFont systemFontOfSize:14*scale];
    matt.lineSpacing=3;
    matt.kern=@2;
    self.label.attributedText=matt;
    
}
- (IBAction)tel:(UIButton*)sender {
    NSString *url=[NSString stringWithFormat:@"tel:%@",sender.currentTitle];
    if ([[UIApplication sharedApplication] canOpenURL:URL(url)]) {
//        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"拨打用户电话" message:nil preferredStyle:(UIAlertControllerStyleAlert)];
//        UIAlertAction *comfirm=[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:URL(url)];
//        }];
        
        
//        UIAlertAction *cacncel=[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
//
//        }];
        
//        [alert addAction:cacncel];
//        [alert addAction:comfirm];
//        [self presentViewController:alert animated:YES completion:nil];
        
    }


}
- (IBAction)openurl:(UIButton*)sender {
    if ([[UIApplication sharedApplication] canOpenURL:URL(sender.currentTitle)]) {
        [[UIApplication sharedApplication] openURL:URL(sender.currentTitle)];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
