//
//  ModifyPasswordVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/1.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "ModifyPasswordVC.h"

@interface ModifyPasswordVC ()
@property (weak, nonatomic) IBOutlet UIView *containtView1;
@property (weak, nonatomic) IBOutlet UIView *containtView2;

@property (weak, nonatomic) IBOutlet UIButton *comfirmBtn;
@property (weak, nonatomic) IBOutlet UIView *containtView3;
@property (weak, nonatomic) IBOutlet UITextField *tx_originPassword;
@property (weak, nonatomic) IBOutlet UITextField *tx_newPassword;//新密码
@property (weak, nonatomic) IBOutlet UITextField *tx_newPassword1;//确认新密码

@end

@implementation ModifyPasswordVC
- (instancetype)init
{
    self = [super init];
    if (self) {
        UIStoryboard *st=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self=[st instantiateViewControllerWithIdentifier:@"ModifyPasswordVC"];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    [self setupUI:self.containtView1];
    [self setupUI:self.containtView2];
    [self setupUI:self.containtView3];
    
    
}
-(void)setupUI:(UIView*)sender{
    sender.layer.cornerRadius=21;
    sender.layer.masksToBounds=YES;
    sender.layer.borderWidth=1;
    sender.layer.borderColor=RGB(173,173,173).CGColor;
}
- (IBAction)comfirmClicked:(id)sender {
    if (![self check]) {
        return;
    }
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/user/UpdatePassword";
    NSDictionary *para=@{@"OldPassword":self.tx_originPassword.text,@"NewPassword":self.tx_newPassword.text,@"NewPassword2":self.tx_newPassword1.text};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [self showMessageHud:@"修改成功"];
            [UserInfoManager shareInstance].isLogin=NO;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:[NSNumber numberWithBool:NO]];
            });
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];

        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(BOOL)check{
    if (self.tx_originPassword.text.length==0) {
        [self showMessageHud:@"请输入原密码"];
        return NO;
    }else if (self.tx_newPassword.text.length==0){
        [self showMessageHud:@"请输入新密码"];
        return NO;
    }else if (![self.tx_newPassword.text isEqualToString:self.tx_newPassword1.text]){
        [self showMessageHud:@"新密码与确认新密码不一致"];
        return NO;
    }
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
