//
//  CDKMyProfileVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/6.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKMyProfileVC.h"
#import "CDKmodifyHeadPotraitVC.h"
#import "ModifyPhoneVC.h"
#import "YSDatePicker.h"
@interface CDKMyProfileVC ()
@property (weak, nonatomic) IBOutlet UIImageView *headpotraitView;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *sexLab;
@property (weak, nonatomic) IBOutlet UILabel *birthday;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *hospitalLab;
@property (weak, nonatomic) IBOutlet UILabel *roleLab;
@property(nonatomic,strong) YSDatePicker *pickerDate;


@end

@implementation CDKMyProfileVC
- (instancetype)init
{
    self = [super init];
    if (self) {
        UIStoryboard *st=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self=[st instantiateViewControllerWithIdentifier:@"CDKMyProfileVC"];
    }
    return self;
}
-(YSDatePicker *)pickerDate{
    if (!_pickerDate) {
        _pickerDate=(YSDatePicker*)[UIView xibinstance:@"YSDatePicker"];
        WeakSelf(weak);
        _pickerDate.callback = ^(NSDate *date) {
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            formatter.dateFormat=@"yyyy/MM/dd";
//            weak.birthday.text=[formatter stringFromDate:date];
            [weak modifyBirthday:[formatter stringFromDate:date]];
            
        };
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        formatter.dateFormat=@"yyyy/MM/dd";
//
        NSDate *date_default=[formatter dateFromString:[UserInfoManager shareInstance].userInfo.brithday];
        if (date_default) {
            _pickerDate.defaultDate=date_default;
        }
        
    }
    return _pickerDate;
}
-(void)modifyBirthday:(NSString*)birthday{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/user/UpdateBrithday";
    NSDictionary *para=@{@"brithday":birthday};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [UserInfoManager shareInstance].userInfo.brithday=birthday;
            self.birthday.text=birthday;
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];

        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"我的资料";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
//    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain handler:^(id  _Nonnull sender) {
//        // 修改资料
//    }];
//     self.navigationItem.rightBarButtonItem = item;
    CDKUserInfo *info=[UserInfoManager shareInstance].userInfo;
    self.nameLab.text=info.name;
    self.sexLab.text=info.sex;
    self.birthday.text=info.brithday;
    self.phoneLab.text=info.mobile;
    self.roleLab.text=info.role;
    self.hospitalLab.text=info.hospitalName;
   
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     [self.headpotraitView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[UserInfoManager shareInstance].userInfo.headPicture]] placeholderImage:[UIImage imageNamed:@"修改头像"]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row==0) {
        CDKmodifyHeadPotraitVC *vc=[[CDKmodifyHeadPotraitVC alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row==3){
        //出生日期
        [self.pickerDate showDatePiker];
    }else if (indexPath.row==4){
        // 修改电话号码
        ModifyPhoneVC *vc=[[ModifyPhoneVC alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        return;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
        
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)])
    {
        [cell setPreservesSuperviewLayoutMargins:NO];
        
    }
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
        
    }
    
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
