//
//  CDKHomeBiochemistryAuditBottomView.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/30.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKHomeBiochemistryAuditBottomView.h"

@implementation CDKHomeBiochemistryAuditBottomView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"CDKHomeBiochemistryAuditBottomView" owner:nil options:nil] lastObject];
        self.backgroundColor = [UIColor whiteColor];
        
        [self.auditYesButton addTarget:self action:@selector(clickAuditYesButton) forControlEvents:UIControlEventTouchUpInside];
        
        [self.aduitNoButton addTarget:self action:@selector(clickAuditNoButton) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)clickAuditYesButton{
    if (self.aduitYesBlock) {
        self.aduitYesBlock();
    }
}

- (void)clickAuditNoButton{
    if (self.aduitNoBlock) {
        self.aduitNoBlock();
    }
}
@end
