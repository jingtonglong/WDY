//
//  CDKHomeDataAuditChooseView.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/21.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CDKHomeDataAuditChooseView : UIView
@property (weak, nonatomic) IBOutlet UIButton *allButton;
@property (weak, nonatomic) IBOutlet UIButton *auditNoButton;
@property (weak, nonatomic) IBOutlet UIButton *auditYesButton;

@end
