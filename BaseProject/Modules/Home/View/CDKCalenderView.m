//
//  CDKCalenderView.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/19.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//
#import "CDKCalenderView.h"
@implementation CDKCalenderView

+(CDKCalenderView*)xibinstanse{
    NSArray *arr=[[NSBundle mainBundle] loadNibNamed:@"CDKCalenderView" owner:nil options:nil];
    return arr.firstObject;
}
-(void)awakeFromNib{
    [super awakeFromNib];
   
}
- (IBAction)dateBtnClick:(UIButton*)sender {
    sender.selected=!sender.selected;
    if (self.delegate) {
        [self.delegate openOrClose:sender];
    }
}
- (IBAction)backClick:(id)sender {
    UIViewController *vc=[self View:self];
    if (vc) {
        [vc.navigationController popViewControllerAnimated:YES];
    }
    
}
- (IBAction)gotoSeartch:(id)sender {
    UIViewController *vc=[self View:self];
    if (vc) {
        CDKHomeSearchVC *search=[[CDKHomeSearchVC alloc] init];
        search.type=0;
        search.time=self.date_btn.currentTitle;
        [vc.navigationController pushViewController:search animated:YES];
    }
}
//可以获取到父容器的控制器的方法,就是这个黑科技.
- (UIViewController *)View:(UIView *)view{
    UIResponder *responder = view;
    //循环获取下一个响应者,直到响应者是一个UIViewController类的一个对象为止,然后返回该对象.
    while ((responder = [responder nextResponder])) {
        if ([responder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)responder;
        }
    }
    return nil;
}
@end
