//
//  CDKModifyVisitTimeView.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/19.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKModifyVisitTimeView.h"

@implementation CDKModifyVisitTimeView
-(void)awakeFromNib{
    
    [super awakeFromNib];
    self.datepiker.minimumDate=[NSDate date];
}

- (IBAction)comfirmBtnClicked:(id)sender {
    if (self.block) {
        self.block([self.datepiker date],self.model);
    }
}
-(void)setPrevious_time:(NSString *)previous_time{
    _previous_time=previous_time;
    NSString *date=previous_time;
    NSString *currentdate=[NSString stringWithFormat:@"患者%@的随访日期从 %@ \n更改到：",self.model.name,date];
    NSRange range=[currentdate rangeOfString:date];
    NSMutableParagraphStyle*paragraphStyle = [NSMutableParagraphStyle new];
    //调整行间距
    paragraphStyle.lineSpacing= 20;
    
    NSMutableAttributedString *mattrai=[[NSMutableAttributedString alloc] initWithString:currentdate attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:RGB(73,73,73),NSParagraphStyleAttributeName:paragraphStyle}];
    [mattrai addAttribute:NSForegroundColorAttributeName value:RGB(255,139,0) range:range];
    self.previvusLab.attributedText=mattrai;
}
-(void)setTitle_str:(NSString *)title_str{
    _title_str=title_str;
    self.titleLab.text=title_str;
//    NSString *date=previous_time;
    NSString *currentdate=[NSString stringWithFormat:@"将患者%@的随访日期\n添加到：",self.model.name];
//    NSRange range=[currentdate rangeOfString:date];
//    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
//
//    //调整行间距
//    paragraphStyle.lineSpacing= 20;
//
//    NSMutableAttributedString *mattrai=[[NSMutableAttributedString alloc] initWithString:currentdate attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:RGB(73,73,73),NSParagraphStyleAttributeName:paragraphStyle}];
//    [mattrai addAttribute:NSForegroundColorAttributeName value:RGB(255,139,0) range:range];
    self.previvusLab.text=currentdate;
    self.previvusLab.textColor=RGB(255,139,0);
}
-(void)setComfirm_title:(NSString *)comfirm_title{
    [self.comfirm_btn setTitle:comfirm_title forState:UIControlStateNormal];
}
-(void)setModel:(CDKPationtListModel *)model{
    _model=model;
}
@end
