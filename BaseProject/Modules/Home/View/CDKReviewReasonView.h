//
//  CDKReviewReasonView.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/22.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XXTextView.h"
@interface CDKReviewReasonView : UIView
@property (weak, nonatomic) IBOutlet XXTextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *reReviewButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
