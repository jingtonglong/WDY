//
//  CDKPationtSearchResultCell.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/20.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKAuditListSearchModel.h"
@interface CDKPationtSearchResultCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLable;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;

/** 是否为选中模式*/
//@property (nonatomic,copy) NSString *isEdit;

/** 模型*/
@property (nonatomic,strong) CDKAuditListSearchModel *model;

//首页跳转的搜索结果模型
@property(nonatomic,strong) CDKPationtListModel *search_result;

@end
