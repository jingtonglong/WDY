//
//  CDKVisitCell.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/19.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKVisitCell.h"

@implementation CDKVisitCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setFrame:(CGRect)frame{
    if (frame.size.height>=90) {
        frame.size.height-=10;
        
    }
    [super setFrame:frame];
}
-(void)setModel:(CDKPationtListModel *)model{
    _model=model;
    [self.head sd_setImageWithURL:URL(model.headPicture) placeholderImage:[UIImage imageNamed:@"默认头像"]];
    self.name.text=model.name;
    if ([model.registerType isEqualToString:@"短信"]) {
        self.register_type.image= [UIImage imageNamed:@"短信"];
    }else{
        self.register_type.image= [UIImage imageNamed:@"手机"];
    }
    self.sex.text=model.sex;
    self.age.text=[NSString stringWithFormat:@"%@岁",model.age];
    if (model.lastVisitTime) {
        self.last_time.text=[NSString stringWithFormat:@"上次随访：%@",model.lastVisitTime];
    }else{
        self.last_time.text=[NSString stringWithFormat:@"上次随访：%@",@""];
    }
    
    if (model.visitTime) {
        self.next_time.text=[NSString stringWithFormat:@"下次随访：%@",model.visitTime];
    }else{
        self.next_time.text=@"下次随访：";
    }
    
}
@end
