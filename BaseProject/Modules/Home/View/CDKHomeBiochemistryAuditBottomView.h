//
//  CDKHomeBiochemistryAuditBottomView.h
//  BaseProject
//
//  Created by QianYuZ on 2018/5/30.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CDKHomeBiochemistryAuditBottomViewAduitYesBlock)();
typedef void(^CDKHomeBiochemistryAuditBottomViewAduitNoBlock)();

@interface CDKHomeBiochemistryAuditBottomView : UIView
@property (weak, nonatomic) IBOutlet UIButton *aduitNoButton;
@property (weak, nonatomic) IBOutlet UIButton *auditYesButton;

/** 审核通过回调*/
@property (nonatomic,copy) CDKHomeBiochemistryAuditBottomViewAduitYesBlock aduitYesBlock;

/** 审核不通过回调*/
@property (nonatomic,copy) CDKHomeBiochemistryAuditBottomViewAduitNoBlock aduitNoBlock;
@end
