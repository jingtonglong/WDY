//
//  CDKReviewReasonView.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/22.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKReviewReasonView.h"

@implementation CDKReviewReasonView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"CDKReviewReasonView" owner:nil options:nil] lastObject];
        self.frame = frame;
        self.textView.xx_placeholder = @"请填写审核不通过原因";
        self.textView.xx_placeholderFont = SYSTEMFONT(14);
        self.textView.xx_placeholderColor = RGB(154,154,154);
        
        [YSUtil addClickEvent:self action:@selector(clickBgView) owner:self.bgView];
    }
    return self;
}

- (void)clickBgView{
    [self removeFromSuperview];
}
@end
