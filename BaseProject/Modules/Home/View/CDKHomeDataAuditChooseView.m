//
//  CDKHomeDataAuditChooseView.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/21.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKHomeDataAuditChooseView.h"

@implementation CDKHomeDataAuditChooseView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"CDKHomeDataAuditChooseView" owner:nil options:nil] lastObject];
    }
    return self;
}

@end
