//
//  CDKCalenderView.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/19.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LXCalender.h"
#import "CDKHomeSearchVC.h"
@protocol CDKCalenderViewDelegate<NSObject>
-(void)openOrClose:(UIButton*)sender;

@end
@interface CDKCalenderView : UIView
@property (weak, nonatomic) IBOutlet UIButton *date_btn;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topinsets;
@property(nonatomic,weak) id<CDKCalenderViewDelegate> delegate;

+(CDKCalenderView*)xibinstanse;
@end
