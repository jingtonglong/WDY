//
//  CDKPationtSearchResultCell.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/20.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKPationtSearchResultCell.h"

@implementation CDKPationtSearchResultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setModel:(CDKAuditListSearchModel *)model{
    _model = model;
    NSString *url = model.headPicture;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"默认头像"]];
    self.nameLabel.text = model.name;
    self.sexLabel.text = model.sex;
    self.ageLabel.text = model.age;
    NSArray  *array = [model.createTime componentsSeparatedByString:@" "];
    self.timeLable.text = array[0];
    
}
-(void)setSearch_result:(CDKPationtListModel *)search_result{
    _search_result=search_result;
    NSString *url = search_result.headPicture;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"默认头像"]];
    self.nameLabel.text = search_result.name;
    self.sexLabel.text = search_result.sex;
    self.ageLabel.text = search_result.age;
//    self.timeLable.text = search_result.createTime;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
