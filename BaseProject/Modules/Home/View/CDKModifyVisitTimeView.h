//
//  CDKModifyVisitTimeView.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/19.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDKPationtListModel.h"
@interface CDKModifyVisitTimeView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIDatePicker *datepiker;
@property (weak, nonatomic) IBOutlet UIButton *comfirm_btn;
@property(nonatomic,copy) void(^block)(NSDate* date,CDKPationtListModel* model);
@property (weak, nonatomic) IBOutlet UILabel *previvusLab;
@property(nonatomic,copy) NSString *previous_time;
@property(nonatomic,copy) NSString *title_str;
@property(nonatomic,copy) NSString *comfirm_title;
@property(nonatomic,strong) CDKPationtListModel *model;

@end
