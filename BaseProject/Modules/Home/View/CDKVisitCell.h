//
//  CDKVisitCell.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/19.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKVisitCell : UITableViewCell
@property(nonatomic,strong) CDKPationtListModel *model;
@property (weak, nonatomic) IBOutlet UILabel *next_time;
@property (weak, nonatomic) IBOutlet UIImageView *head;
@property (weak, nonatomic) IBOutlet UILabel *age;
@property (weak, nonatomic) IBOutlet UILabel *last_time;
@property (weak, nonatomic) IBOutlet UIImageView *register_type;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *sex;

@end
