//
//  CDKBiochemistryImageModel.h
//  BaseProject
//
//  Created by QianYuZ on 2018/7/25.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKBiochemistryImageModel : NSObject

/** id*/
@property (nonatomic,copy) NSString *id;

/** 路径*/
@property (nonatomic,copy) NSString *imagePath;
@end
