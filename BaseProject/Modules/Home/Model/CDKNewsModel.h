//
//  CDKNewsModel.h
//  BaseProject
//
//  Created by QianYuZ on 2018/6/25.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKNewsModel : NSObject

/** 标题*/
@property (nonatomic,copy) NSString *title;

/** 图片地址*/
@property (nonatomic,copy) NSString *imageUrl;

/** 链接地址*/
@property (nonatomic,copy) NSString *linkUrl;

/** 显示位置*/
@property (nonatomic,copy) NSString *position;

/** 食谱id*/
@property (nonatomic,copy) NSString *id;

/** 食谱内容*/
@property (nonatomic,copy) NSString *content;

/** 发布时间*/
@property (nonatomic,copy) NSString *times;
@end

