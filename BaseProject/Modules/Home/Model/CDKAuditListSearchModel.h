//
//  CDKAuditListSearchModel.h
//  BaseProject
//
//  Created by QianYuZ on 2018/6/13.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKAuditListSearchModel : NSObject

/** 检查id*/
@property (nonatomic,copy) NSString *treatmentId;

/** 患者id*/
@property (nonatomic,copy) NSString *patientId;

/** 头像*/
@property (nonatomic,copy) NSString *headPicture;

/** 姓名*/
@property (nonatomic,copy) NSString *name;

/** 性别*/
@property (nonatomic,copy) NSString *sex;

/** 年龄*/
@property (nonatomic,copy) NSString *age;

/** 电话*/
@property (nonatomic,copy) NSString *phone;

/** 时间*/
@property (nonatomic,copy) NSString *createTime;

/** 备注*/
@property (nonatomic,copy) NSString *remarkCKD;

/** 时间*/
@property (nonatomic,copy) NSString *measureDate;
@end
