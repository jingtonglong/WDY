//
//  CDKBiochemistryBaseDataModel.h
//  BaseProject
//
//  Created by QianYuZ on 2018/6/14.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//  生化检查指标基础数据

#import <Foundation/Foundation.h>

@interface CDKBiochemistryBaseDataModel : NSObject

/** 名称*/
@property (nonatomic,copy) NSString *name;

/** 最小值*/
@property (nonatomic,copy) NSString *minValue;

/** 最大值*/
@property (nonatomic,copy) NSString *maxValue;

/** 单位*/
@property (nonatomic,copy) NSString *unit;

/** id*/
@property (nonatomic,copy) NSString *id;

/** 值*/
@property (nonatomic,copy) NSString *value;

/** 用来验证的标识符*/
@property (nonatomic,copy) NSString *sort;
@end
