//
//  CDKAddMorePationtToTodayVisitVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/20.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKAddMorePationtToTodayVisitVC.h"
#import "CDKVisitCell.h"
@interface CDKAddMorePationtToTodayVisitVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *datas;
@property(nonatomic,strong) UIView *bottomView;
@property(nonatomic,strong) UIButton *seletedAllBtn;
@property(nonatomic,strong) UILabel *countLab;
@property(nonatomic,assign) NSInteger page;



@end

@implementation CDKAddMorePationtToTodayVisitVC
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] init];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.rowHeight=90;
        [_tableView registerNib:[UINib nibWithNibName:@"CDKVisitCell" bundle:nil] forCellReuseIdentifier:@"visit"];
        _tableView.backgroundColor=RGB(247,247,247);
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        UIView *footer=[[UIView alloc]init];
        footer.backgroundColor=[UIColor whiteColor];
        WeakSelf(weak);
        _tableView.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weak loadData];
        }];
        _tableView.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weak loadMoreData];
        }];
        _tableView.tableFooterView= footer;
    }
    return _tableView;
}
-(UIView *)bottomView{
    if (!_bottomView) {
        _bottomView=[[UIView alloc] init];
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:@"未选择"] forState:UIControlStateNormal];
          [btn setImage:[UIImage imageNamed:@"选择_add"] forState:UIControlStateSelected];
        [btn setTitleColor:RGB(83,83,83) forState:UIControlStateNormal];
        btn.titleLabel.font=[UIFont systemFontOfSize:14];
        [btn setTitle:@"  全选" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(seletedAll:) forControlEvents:UIControlEventTouchUpInside];
        self.seletedAllBtn=btn;
        [_bottomView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(13);
            make.left.mas_equalTo(15);
            make.centerY.mas_equalTo(0);
        }];
        
        UILabel *lab=[UILabel new];
        lab.textColor=RGB(101,101,101);
        lab.font=SYSTEMFONT(14);
        
        lab.text=@"共0人";
        self.countLab=lab;
        [_bottomView addSubview:lab];
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-10);
            make.centerY.equalTo(btn);
        }];
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
        line.backgroundColor=RGB(229,229,229);
        [_bottomView addSubview:line];
        _bottomView.backgroundColor=[UIColor whiteColor];
        
    }
    return _bottomView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"选择随访患者";//添加更多患者到今日随访
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(iPhoneX_BOTTOM_HEIGHT+50);
    }];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.bottom.equalTo(self.bottomView.mas_top);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
    self.datas=[NSMutableArray array];
    self.tableView.editing=YES;
    WeakSelf(weak);
    UIBarButtonItem *item=[[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain handler:^(id  _Nonnull sender) {
        //确定
        [weak addvisit_to_today];
    }];
    self.navigationItem.rightBarButtonItem=item;
    
    self.datas=[NSMutableArray array];
    [self.tableView.mj_header beginRefreshing];
    
}
-(void)addvisit_to_today{
    NSArray *seletsindexs=self.tableView.indexPathsForSelectedRows;

    if (seletsindexs.count==0) {
        [self showMessageHud:@"请至少选择一个患者"];
        return;
    }
    [self showLoadingHud:nil];
    
    NSString *api=@"/api/services/ckd/visit/BatchAddVisit";
    NSMutableArray *arr=[NSMutableArray array];
    for (NSIndexPath *index in seletsindexs) {
        CDKPationtListModel *model=self.datas[index.row];
        [arr addObject:model.id];
    }
    NSDictionary *para=@{@"visitTime":self.time_str,@"ids":[arr componentsJoinedByString:@","]};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
        [self showMessageHud:@"添加成功"];
        if (self.callback) {
                self.callback();
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
           
            
        }else{
          [self showMessageHud:responseObject[@"error"][@"message"]];
        }
        
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadData{
    
    [self.tableView.mj_footer endRefreshing];
    
    self.page=1;
    NSString *api=@"/api/services/ckd/visit/BatchAddList";
    NSDictionary *para=@{@"keyword":@"",@"visitTime":self.time_str,@"pageIndex":@(self.page),@"pageSize":@10};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        if (SUCCEESS) {
            
            
            [self.datas removeAllObjects];
            NSArray *arr=[NSArray modelArrayWithClass:[CDKPationtListModel class] json:responseObject[@"result"][@"items"]];
            [self.datas addObjectsFromArray:arr];
            [self.tableView reloadData];
            self.countLab.text=@"共0人";
            self.seletedAllBtn.selected=NO;
            
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadMoreData{
    
    [self.tableView.mj_header endRefreshing];
    self.page++;
    NSString *api=@"/api/services/ckd/visit/BatchAddList";
    
    NSDictionary *para=@{@"visitTime":self.time_str,@"pageIndex":@(self.page),@"pageSize":@10};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        
        if (SUCCEESS) {
            NSArray *arr=[NSArray modelArrayWithClass:[CDKPationtListModel class] json:responseObject[@"result"][@"items"]];
            if (arr.count>0) {
                [self.tableView.mj_footer endRefreshing];
            }else {
                if (self.page>1) {
                    self.page--;
                }
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            
            if (arr.count>0) {
                NSMutableArray *apped=[NSMutableArray array];
                for (int i=0; i<arr.count; i++) {
                    NSIndexPath *index=[NSIndexPath indexPathForRow:self.datas.count+i inSection:0];
                    [apped addObject:index];
                }
                [self.datas addObjectsFromArray:arr];
                [self.tableView insertRowsAtIndexPaths:apped withRowAnimation:UITableViewRowAnimationFade];
                self.seletedAllBtn.selected=NO;
            }
            
            
        }else{
            [self.tableView.mj_footer endRefreshing];
            [self showMessageHud:responseObject[@"error"][@"message"]];
            if (self.page>1) {
                self.page--;
            }
        }
    } errorHandel:^(NSError *error) {
        [self.tableView.mj_footer endRefreshing];
        [self dismissHud];
        if (self.page>1) {
            self.page--;
        }
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
         
-(void)seletedAll:(UIButton*)sender{
    sender.selected=!sender.selected;
    if (sender.isSelected) {// 全选
        for (int i=0;i<self.datas.count; i++) {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
        self.countLab.text=[NSString stringWithFormat:@"共%ld人",self.datas.count];
    }else{
        [self.tableView reloadData];
        self.countLab.text=@"共0人";
    }
}
#pragma tableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CDKVisitCell *cell=[tableView dequeueReusableCellWithIdentifier:@"visit" forIndexPath:indexPath];
    cell.tintColor=RGB(103,205,47);
    cell.multipleSelectionBackgroundView = [UIView new];
    cell.model=self.datas[indexPath.row];
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *sectionVeiw=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 40)];
    sectionVeiw.backgroundColor=RGB(247,247,247);
    UILabel *lab=[UILabel new];
   
    NSString *date=self.time_str;
    NSString *currentdate=[NSString stringWithFormat:@"您将要选择这些患者到 %@ 随访",date];
    NSRange range=[currentdate rangeOfString:date];
   
    NSMutableAttributedString *mattrai=[[NSMutableAttributedString alloc] initWithString:currentdate attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:RGB(101,101,101)}];
    [mattrai addAttribute:NSForegroundColorAttributeName value:RGB(255,139,0) range:range];
    lab.attributedText=mattrai;
    [sectionVeiw addSubview:lab];
    [lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.centerY.equalTo(sectionVeiw);
    }];
    return sectionVeiw;
}

//-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
//    return YES;
//}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete|UITableViewCellEditingStyleInsert;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *indexs=self.tableView.indexPathsForSelectedRows;
    self.seletedAllBtn.selected=indexs.count==self.datas.count?YES:NO;
    NSArray *arr=self.tableView.indexPathsForSelectedRows;
    self.countLab.text=[NSString stringWithFormat:@"共%ld人",arr.count];
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.seletedAllBtn.selected=NO;
    NSArray *arr=self.tableView.indexPathsForSelectedRows;
    self.countLab.text=[NSString stringWithFormat:@"共%ld人",arr.count];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
