//
//  CDKAddNewChartVC_Home.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/20.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKAddNewChartVC_Home.h"
#import "CDKExcelRecoredVC.h"
#import "CDKChartVauleCellTableViewCell.h"
@interface CDKAddNewChartVC_Home ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSArray *colors;
@property(nonatomic,strong) NSArray *titles;



@end

@implementation CDKAddNewChartVC_Home
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] init];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.rowHeight=UITableViewAutomaticDimension;
        _tableView.rowHeight=60;
        [_tableView registerNib:[UINib nibWithNibName:@"CDKChartVauleCellTableViewCell" bundle:nil] forCellReuseIdentifier:@"chart"];
        _tableView.backgroundColor=[UIColor whiteColor];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
        _tableView.tableFooterView= [UIView new];
    }
    return _tableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    self.title=@"新增量表评估";
    self.titles=@[@"SGA主观全面评定",@"MIS营养不良炎症评定",@"营养风险筛查NRS2002评估",@"MNA简易评估",@"改良SGA主观全面评定"];
    self.colors=@[RGB(255,139,0),
                            RGB(75,197,234),RGB(191,139,255),RGB(255,137,195),RGB(163,255,243)];
 
}
#pragma tableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titles.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 54;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CDKChartVauleCellTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"chart" forIndexPath:indexPath];
    cell.colorView.backgroundColor=self.colors[indexPath.row];
    cell.titleLab.text=self.titles[indexPath.row];
    
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 54)];
    sectionView.backgroundColor=RGB(247,247,247);
    
   
    UILabel *lab=[[UILabel alloc] init];
    lab.text=@"    您可以为患者填写以下5种评估问卷";
    lab.backgroundColor=[UIColor whiteColor];
    lab.textColor=RGB(73,73,73);
    lab.font=SYSTEMFONT(14);
    [sectionView addSubview:lab];
    [lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(44);
        
    }];
   
    return sectionView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *arr=@[@"SGA",@"MIS",@"NRS",@"MNA",@"GLSGA"];
    CDKExcelRecoredVC *vc=[[CDKExcelRecoredVC alloc] init];
    //
    vc.pationtid=self.model.id;
    vc.scale=arr[indexPath.row];
    vc.title=self.titles[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
