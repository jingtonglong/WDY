//
//  CDKHomeSearchVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/19.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//
#import "CDKNutritionalAssessmentAddVc.h"
#import "CDKHomeSearchVC.h"
#import "CDKVisitCell.h"
#import "CDKAddNewChartVC_Home.h"
#import "CDKPationtSearchResultCell.h"
#import "CDKModifyVisitTimeView.h"
#import "CDKBiochemistryAddVc.h"
@interface CDKHomeSearchVC ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource>
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *datas;
@property(nonatomic,assign) NSInteger page;
@property(nonatomic,strong) UITextField *textfield;
@property(nonatomic,strong) CDKModifyVisitTimeView *modifyVisitTimeView;
@property(nonatomic,strong) NSDateFormatter *dateFormatter;
@property(nonatomic,strong) UIView *bgView;
@property(nonatomic,assign) BOOL isFirst;
@property(nonatomic,assign) BOOL isAdd;



@end

@implementation CDKHomeSearchVC
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] init];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        
        _tableView.emptyDataSetSource=self;
        [_tableView registerNib:[UINib nibWithNibName:@"CDKVisitCell" bundle:nil] forCellReuseIdentifier:@"visit"];
         [_tableView registerNib:[UINib nibWithNibName:@"CDKPationtSearchResultCell" bundle:nil] forCellReuseIdentifier:@"other"];
        if (self.type==0) {
            _tableView.backgroundColor=RGB(247,247,247);
                    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;

        }else{
            _tableView.backgroundColor=[UIColor whiteColor];
        }
        
        UIView *footer=[[UIView alloc]init];
        footer.backgroundColor=[UIColor whiteColor];
        
        _tableView.tableFooterView= footer;
        WeakSelf(weak);
        _tableView.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weak refreshdata];
        }];
        _tableView.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weak loadMoreData];
        }];
        _tableView.mj_footer.ignoredScrollViewContentInsetBottom=iPhoneX_BOTTOM_HEIGHT;
    }
    return _tableView;
}
-(CDKModifyVisitTimeView *)modifyVisitTimeView{
    if (!_modifyVisitTimeView) {
        _modifyVisitTimeView=(CDKModifyVisitTimeView*)[UIView xibinstance:@"CDKModifyVisitTimeView"];
        WeakSelf(weak);
        
        _modifyVisitTimeView.block = ^(NSDate *date,CDKPationtListModel *model) {
            YSLog(@"更改了随访时间");
            [weak hiddenDatePicker];
            [weak change_visit_time:date model:model];
        };
        
    }
    return _modifyVisitTimeView;
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.isFirst=YES;
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    // Do any additional setup after loading the view.
//    UIView *titleview=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 44)];
////    titleview.backgroundColor=[UIColor redColor];
//    UIView *search=[[UIView alloc] initWithFrame:CGRectMake(0, 10, KScreenWidth-100, 24)];
    
//    [titleview addSubview:search];
//    search.cornerRadius=12;
//    search.borderColor=RGB(217,217,217);
//    search.borderWidth=1;
//    [search mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(0);
//        make.right.mas_equalTo(40);
//        make.height.mas_equalTo(24);
//        make.centerY.equalTo(titleview);
//    }];
//    UIImageView *logoView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"搜索"]];
//    [search addSubview:logoView];
//    logoView.frame=CGRectMake(7, 6, 10, 12);
//    [logoView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(search);
//        make.width.mas_equalTo(10);
//        make.height.mas_equalTo(11);
//        make.left.mas_equalTo(7);
//    }];
//    UITextField *textfile=[[UITextField alloc] initWithFrame:CGRectMake(22, 0, KScreenWidth-100, 24)];
//    textfile.placeholder=@"输入姓名/手机后四位查询患者";
//    textfile.font=SYSTEMFONT(12);
//    textfile.borderStyle=UITextBorderStyleNone;
//    textfile.spellCheckingType=UITextSpellCheckingTypeNo;
//    textfile.autocorrectionType=UITextAutocorrectionTypeNo;
    self.textfield = [[UITextField alloc] init];
//    self.searchTextField.tag = 50001;
    self.textfield.height=30;
    self.textfield.clearButtonMode = UITextFieldViewModeAlways;
    self.textfield.font = [UIFont systemFontOfSize:12];
    self.textfield.placeholder = @"输入姓名/手机后四位查询患者";
    self.textfield.backgroundColor = RGB(247,247,247);
    self.textfield.returnKeyType = UIReturnKeySearch;
    self.textfield.delegate = self;
    self.textfield.cornerRadius=15;
    UIImageView *searchIcon = [[UIImageView alloc] init];
    searchIcon.image = [UIImage imageNamed:@"搜索"];
    searchIcon.contentMode = UIViewContentModeCenter;
    searchIcon.size = CGSizeMake(30, 30);
    self.textfield.leftView = searchIcon;
    self.textfield.leftViewMode = UITextFieldViewModeAlways;
    self.navigationItem.titleView = self.textfield;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textfileChange:) name:UITextFieldTextDidChangeNotification object:self.textfield];

    
    UIView *sectionVeiw=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 40)];
    sectionVeiw.backgroundColor=RGB(247,247,247);
    UILabel *lab=[UILabel new];
    lab.textColor=RGB(154,154,154);
    lab.text=@"查询结果";
    lab.font=SYSTEMFONT(14);
    
    [sectionVeiw addSubview:lab];
    [lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.centerY.equalTo(sectionVeiw);
    }];
    [self.view addSubview:sectionVeiw];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.equalTo(sectionVeiw.mas_bottom);
        make.bottom.mas_equalTo(0);
    }];
    self.datas=[NSMutableArray array];
//    [self loadData];
//    if (self.type==0) {
//        self.tableView.editing=YES;
//    }
}
-(void)textfileChange:(NSNotification*)ntf{
    NSLog(@"");
    if (self.textfield.text.length==0) {
        [self.datas removeAllObjects];
        [self.tableView reloadData];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if (textField.text.length==0) {
        
        [self showMessageHud:@"请输入搜索内容"];
    }else{
        [self loadData];
    }
    return YES;
}
-(void)refreshdata{
    [self.tableView.mj_footer endRefreshing];
    self.isFirst=NO;
    NSString *api=nil;
    NSDictionary *para=nil;
    NSString *keywords=@"";
    if (self.textfield.text.length>0) {
        keywords=self.textfield.text;
    }
    self.page=1;
    if(self.type==0){// 随访搜索
        api=@"/api/services/ckd/visit/SearchList";
        para=@{@"keyword":keywords,@"visitTime":@"",@"pageIndex":@(self.page),@"pageSize":@15};
    }else{// 患者搜索
        api=@"/api/services/ckd/patient/QueryList";
        
        para=@{@"keyword":keywords,@"pageIndex":@(self.page),@"pageSize":@15};
        
    }

    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        if (SUCCEESS) {
            [self.datas removeAllObjects];
            NSArray *arr=[NSArray modelArrayWithClass:[CDKPationtListModel class] json:responseObject[@"result"][@"items"]];
            [self.datas addObjectsFromArray:arr];
            [self.tableView reloadData];
            
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];

}
-(void)loadMoreData{
    [self.tableView.mj_header endRefreshing];
    self.page++;
    NSString *api=nil;
    NSDictionary *para=nil;
    NSString *keywords=@"";
    if (self.textfield.text.length>0) {
        keywords=self.textfield.text;
    }
    if(self.type==0){// 随访搜索
        api=@"/api/services/ckd/visit/SearchList";
        para=@{@"keyword":keywords,@"visitTime":@"",@"pageIndex":@(self.page),@"pageSize":@15};
    }else{//
        api=@"/api/services/ckd/patient/QueryList";
        para=@{@"keyword":keywords,@"pageIndex":@(self.page),@"pageSize":@15};
    }
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        
        if (SUCCEESS) {
            NSArray *arr=[NSArray modelArrayWithClass:[CDKPationtListModel class] json:responseObject[@"result"][@"items"]];
            if (arr.count>0) {
                [self.tableView.mj_footer endRefreshing];
            }else {
                if (self.page>1) {
                    self.page--;
                }
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            [self.datas addObjectsFromArray:arr];
            [self.tableView reloadData];
            
        }else{
            [self.tableView.mj_footer endRefreshing];
            [self showMessageHud:responseObject[@"error"][@"message"]];
            if (self.page>1) {
                self.page--;
            }
        }
    } errorHandel:^(NSError *error) {
        [self.tableView.mj_footer endRefreshing];
        [self dismissHud];
        if (self.page>1) {
            self.page--;
        }
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadData{
    
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    self.isFirst=NO;
    NSString *api=nil;
    NSDictionary *para=nil;
    NSString *keywords=@"";
    if (self.textfield.text.length>0) {
        keywords=self.textfield.text;
    }
    [self showLoadingHud:nil];
    self.page=1;
    if(self.type==0){// 随访搜索
        api=@"/api/services/ckd/visit/SearchList";
        para=@{@"keyword":keywords,@"visitTime":@"",@"pageIndex":@(self.page),@"pageSize":@15};
    }else{// 患者搜索
        api=@"/api/services/ckd/patient/QueryList";

        para=@{@"keyword":keywords,@"pageIndex":@(self.page),@"pageSize":@15};
       
    }

    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            [self.datas removeAllObjects];
            NSArray *arr=[NSArray modelArrayWithClass:[CDKPationtListModel class] json:responseObject[@"result"][@"items"]];
            [self.datas addObjectsFromArray:arr];
            [self.tableView reloadData];
            
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

#pragma tableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   return self.datas.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.type==0) {
        return 90;
    }
    return 60;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.type==0) {
        CDKVisitCell *cell=[tableView dequeueReusableCellWithIdentifier:@"visit" forIndexPath:indexPath];
        cell.model=self.datas[indexPath.row];
        
        return cell;
    }
    CDKPationtSearchResultCell *cell=[tableView dequeueReusableCellWithIdentifier:@"other" forIndexPath:indexPath];
    
    cell.search_result=self.datas[indexPath.row];
    return cell;
}


#pragma EmptyDatasetSource
-(UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{
    UIImage *img=[UIImage imageNamed:@"nodata_palceholder"];
    return img;
}
-(CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView{
    return -100*Iphone6ScaleWidth;
}
-(NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    NSString *word=@"";
    if (self.isFirst) {
        word=@"请输入查询内容";
    }else{
        word=@"没有搜到你要查询的内容";
    }
    NSAttributedString *str=[[NSAttributedString alloc] initWithString:word attributes:@{NSForegroundColorAttributeName:RGB(154,154,154),NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    return str;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/**
 *  只要实现了这个方法，左滑出现按钮的功能就有了
 (一旦左滑出现了N个按钮，tableView就进入了编辑模式, tableView.editing = YES)
 */

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.type==0) {
        return YES;
    }else{
        return NO;
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{

}


/**
 *  左滑cell时出现什么按钮
 */
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CDKPationtListModel *model=self.datas[indexPath.row];
    NSString *title=@"添加随访日期";
    if (model.visitTime.length>0) {
        title=@"更改日期";
    }
    WeakSelf(weak);
    UITableViewRowAction *action0 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:title handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        YSLog(@"更改日期");
        weak.modifyVisitTimeView.model=weak.datas[indexPath.row];
        if (model.visitTime.length>0) {
            weak.modifyVisitTimeView.previous_time=model.visitTime;
        }else{
            weak.modifyVisitTimeView.title_str=@"添加随访日期";
            weak.modifyVisitTimeView.comfirm_title=@"确认添加";
        }
        
        [weak showDatePickerView];
        // 收回左滑出现的按钮(退出编辑模式)
        tableView.editing = NO;
    }];
    UITableViewRowAction *action1 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"移除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        tableView.editing = NO;
        
        UIAlertController *alertvc= [UIAlertController alertControllerWithTitle:@"确认移除" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *comfirm=[UIAlertAction actionWithTitle:@"确认" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
            // 删除操作
            [weak removeVisitTime:weak.datas[indexPath.row] indexpath:indexPath];
        }];
        UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            //
            weak.tableView.editing=NO;
        }];
        
        [alertvc addAction:comfirm];
        [alertvc addAction:cancel];
        [weak presentViewController:alertvc animated:YES completion:nil];

    }];
    action1.backgroundColor=RGB(254,91,0);
    if(model.visitTime.length>0){
        return @[action1, action0];
    }else{
        return @[action0];
    }
    
}
- (nullable UISwipeActionsConfiguration *)tableView:(UITableView *)tableView trailingSwipeActionsConfigurationForRowAtIndexPath:(NSIndexPath *)indexPath API_AVAILABLE(ios(11.0)) API_UNAVAILABLE(tvos){
    CDKPationtListModel *model=self.datas[indexPath.row];
    NSString *title=@"添加随访日期";
    if (model.visitTime.length>0) {
        title=@"更改日期";
    }
    WeakSelf(weak);
    // delete action
    UIContextualAction *deleteAction = [UIContextualAction
                                        contextualActionWithStyle:UIContextualActionStyleDestructive
                                        title:@"移除"
                                        handler:^(UIContextualAction * _Nonnull action,
                                                  __kindof UIView * _Nonnull sourceView,
                                                  void (^ _Nonnull completionHandler)(BOOL))
                                        {
                                            
                                            
                                            
//                                            [tableView setEditing:NO animated:YES];
//                                            [weak removeVisitTime:weak.datas[indexPath.row] indexpath:indexPath];
                                            UIAlertController *alertvc= [UIAlertController alertControllerWithTitle:@"确认移除" message:nil preferredStyle:UIAlertControllerStyleAlert];
                                            UIAlertAction *comfirm=[UIAlertAction actionWithTitle:@"确认" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
                                                // 删除操作
                                                [weak removeVisitTime:weak.datas[indexPath.row] indexpath:indexPath];
                                            }];
                                            UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
                                                //
                                                weak.tableView.editing=NO;
                                            }];
                                            
                                            [alertvc addAction:comfirm];
                                            [alertvc addAction:cancel];
                                            [weak presentViewController:alertvc animated:YES completion:nil];
                                            
                                            completionHandler(false);
                                        }];
    // chagne action
    UIContextualAction *chageaction = [UIContextualAction
                                       contextualActionWithStyle:UIContextualActionStyleNormal
                                       title:title
                                       handler:^(UIContextualAction * _Nonnull action,
                                                 __kindof UIView * _Nonnull sourceView,
                                                 void (^ _Nonnull completionHandler)(BOOL))
                                       {
                                           
                                           
                                           
                                           [tableView setEditing:NO animated:YES];
                                           weak.modifyVisitTimeView.model=weak.datas[indexPath.row];
                                           if (model.visitTime.length>0) {
                                               weak.modifyVisitTimeView.previous_time=model.visitTime;
                                           }else{
                                               weak.modifyVisitTimeView.title_str=@"添加随访日期";
                                               weak.modifyVisitTimeView.comfirm_title=@"确认添加";
                                           }
                                           
                                           [weak showDatePickerView];
                                           completionHandler(true);
                                       }];
    UISwipeActionsConfiguration *actions;
    if (model.visitTime.length>0) {
         actions= [UISwipeActionsConfiguration configurationWithActions:@[deleteAction,chageaction]];
    }else{
         actions= [UISwipeActionsConfiguration configurationWithActions:@[chageaction]];
    }
    
    actions.performsFirstActionWithFullSwipe = NO;
    deleteAction.backgroundColor=RGB(254,91,0);
    return actions;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.type==1) {// 新增评估
        CDKNutritionalAssessmentAddVc *vc=[[CDKNutritionalAssessmentAddVc alloc] init];
        vc.model=self.datas[indexPath.row];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (self.type==2){// 量表
        CDKAddNewChartVC_Home *vc=[[CDKAddNewChartVC_Home alloc] init];
        vc.model=self.datas[indexPath.row];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (self.type==3){// 生化数据
        CDKBiochemistryAddVc *vc=[[CDKBiochemistryAddVc alloc] init];
        vc.model=self.datas[indexPath.row];
        [self.navigationController pushViewController:vc animated:YES];
    }else{// 0
        
    }
}

#pragma visit method

-(void)removeVisitTime:(CDKPationtListModel*)model indexpath:(NSIndexPath*)indexpath{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/visit/RemoveVisit";
    
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"patientId":model.id} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            
//            [self.datas removeObject:model];
//            [self.tableView deleteRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationMiddle];
            model.visitTime=nil;
            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

-(void)change_visit_time:(NSDate*)date model:(CDKPationtListModel*)model{
    [self showLoadingHud:nil];
    NSString *date_str=[self.dateFormatter stringFromDate:date];
    NSString *api=@"/api/services/ckd/visit/BatchAddVisit";
//    CDKPationtListModel *model=self.datas[indexPath.row];
    NSDictionary *para=@{
                         @"visitTime":date_str,
                         @"ids":model.id
                         };
    if (model.visitTime.length>0) {
     
        api=@"/api/services/ckd/visit/UpdateVisit";
        para=@{
               @"visitTime":date_str,
               @"id":model.id
               };
    }
    
    
   
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            if (![date_str isEqualToString:model.visitTime]) {
//                [self.datas removeObject:model];
                model.visitTime=date_str;
                [self.tableView reloadData];
            }
            [self showMessageHud:@"操作成功"];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
            
        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
    
}
-(UIView *)bgView{
    if (!_bgView) {
        _bgView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenDatePicker)];
        [_bgView addGestureRecognizer:tap];
        _bgView.backgroundColor=RGB(0,0,0);
        _bgView.alpha=0.4;
    }
    return _bgView;
}
-(void)hiddenDatePicker{
    [self.bgView removeFromSuperview];
    [self.modifyVisitTimeView removeFromSuperview];
}
-(void)showDatePickerView{
    UIWindow *window=kAppWindow;
    [window addSubview:self.bgView];
    [window addSubview:self.modifyVisitTimeView];
    
    [self.modifyVisitTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(290+iPhoneX_BOTTOM_HEIGHT);
    }];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
