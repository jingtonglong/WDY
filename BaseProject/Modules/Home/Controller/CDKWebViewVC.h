//
//  CDKWebViewVC.h
//  BaseProject
//
//  Created by QianYuZ on 2018/7/3.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKWebViewVC : UIViewController

/** 标题*/
@property (nonatomic,copy) NSString *titleName;

/** url*/
@property (nonatomic,copy) NSString *url;
@end
