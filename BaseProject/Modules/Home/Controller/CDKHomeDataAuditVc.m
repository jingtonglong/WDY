//
//  CDKHomeDataAuditVc.m
//  BaseProject
//
//  Created by QianYuZ on 2018/5/21.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
// 首页数据审核跳转controller

#import "CDKHomeDataAuditVc.h"
#import "CDKPationtSearchResultCell.h"
#import "CDKHomeDataAuditChooseView.h"
#import "CDKReviewReasonView.h"
#import "CDKBiochemistryLookVc.h"
#import "CDKAuditListSearchModel.h"
static NSString *resultCell = @"resultCell";

@interface CDKHomeDataAuditVc ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource>

@property(nonatomic,strong) UITextField *searchTextField;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,copy) NSMutableArray *dataArray;
@property (nonatomic,strong)NSMutableArray *selectorPatnArray;// 选中数据

/** 底部审核view*/
@property (nonatomic,strong) CDKHomeDataAuditChooseView *chooseView;

/** 审核不通过View*/
@property (nonatomic,strong) CDKReviewReasonView *reasonView;

/** 页数*/
@property (nonatomic,assign) NSInteger page;

/** 是否为选择状态*/
@property (nonatomic,copy) NSString *selectedStatus;

/** 标记cell是否显示编辑模式*/
@property (nonatomic,copy) NSString *isEdit;
@end

@implementation CDKHomeDataAuditVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = kBACKGROUDcolor;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(done:) name:@"doneAction" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self setupUI];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView.mj_header beginRefreshing];
}

- (void)done:(NSNotification *)notification{
    [self network];
}

-(void)textFieldDidChange :(UITextField *)theTextField{
    if (theTextField.text.length < 1) {
        [self network];
    }
}

- (void)setupUI{
    
    __weak typeof(self) weakSelf = self;
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"批量审核" style:UIBarButtonItemStylePlain handler:^(id  _Nonnull sender) {
        if ([weakSelf.navigationItem.rightBarButtonItem.title isEqualToString:@"批量审核"]) {// 点击批量审核事件
            weakSelf.selectedStatus = @"yes";
            [weakSelf.navigationItem.rightBarButtonItem setTitle:@"取消"];
            weakSelf.navigationItem.titleView = nil;
            weakSelf.title = @"批量审核";
            [weakSelf showChooseView];
            [weakSelf.selectorPatnArray removeAllObjects];
            [weakSelf.chooseView.allButton setTitle:[NSString stringWithFormat:@"  全选 (%ld人)",self.selectorPatnArray.count] forState:UIControlStateNormal];
            weakSelf.tableView.editing=YES;
            [weakSelf.tableView reloadData];
        }else{// 点击取消事件
            weakSelf.selectedStatus = @"no";
            [weakSelf.selectorPatnArray removeAllObjects];
            [weakSelf.chooseView.allButton setImage:[UIImage imageNamed:@"未选择"] forState:UIControlStateNormal];
            [weakSelf.navigationItem.rightBarButtonItem setTitle:@"批量审核"];
            [weakSelf setupSearchNav];
            [weakSelf hiddenChooseView];
            weakSelf.tableView.editing=NO;
            [weakSelf.tableView reloadData];
        }
        
    }];
    self.navigationItem.rightBarButtonItem = item;
    
    [self setupSearchNav];
}

- (void)setupSearchNav{
    self.searchTextField = [[UITextField alloc] init];
    self.searchTextField.tag = 50001;
    self.searchTextField.clearButtonMode = UITextFieldViewModeAlways;
    self.searchTextField.size = CGSizeMake(250, 30);
    self.searchTextField.font = [UIFont systemFontOfSize:12];
    self.searchTextField.placeholder = @"输入姓名/手机后四位查询患者";
    self.searchTextField.backgroundColor = RGB(247,247,247);
    self.searchTextField.returnKeyType = UIReturnKeySearch;
    self.searchTextField.delegate = self;
    [self.searchTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    UIImageView *searchIcon = [[UIImageView alloc] init];
    searchIcon.image = [UIImage imageNamed:@"搜索"];
    searchIcon.contentMode = UIViewContentModeCenter;
    searchIcon.size = CGSizeMake(30, 30);
    self.searchTextField.leftView = searchIcon;
    self.searchTextField.leftViewMode = UITextFieldViewModeAlways;
    self.navigationItem.titleView = self.searchTextField;
    [YSUtil makeCornerRadius:self.searchTextField.height / 2 view:self.searchTextField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self network];
    return YES;
}

- (void)showChooseView{
    [UIView animateWithDuration:0.3 animations:^{
        [self.chooseView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_bottom).offset(- 45 *Iphone6ScaleHeight);
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.height.mas_equalTo(45 * Iphone6ScaleHeight);
        }];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hiddenChooseView{
    [UIView animateWithDuration:0.3 animations:^{
        [self.chooseView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_bottom).offset(20);
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.height.mas_equalTo(45 * Iphone6ScaleHeight);
        }];
    } completion:nil];
}


#pragma mark -- Network
- (void)network{
    [self showLoadingHud:nil];
    NSString *url = @"/api/services/ckd/audit/QueryList";
    NSDictionary *dic = @{
                          @"keyword":self.searchTextField.text?self.searchTextField.text:@"",
                          @"pageIndex":@(self.page),
                          @"pageSize":@(10),
                          };
    
    [YSNetworkingManager requestWithUrl:url :POST paramiters:dic success:^(id responseObject) {
        if (SUCCEESS) {
            [self.dataArray removeAllObjects];
            [self.dataArray addObjectsFromArray:[NSArray modelArrayWithClass:[CDKAuditListSearchModel class] json:responseObject[@"result"][@"items"]]];
        }
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self dismissHud];
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        [self.tableView.mj_header endRefreshing];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

- (void)netWorkForMoreData{
    [self showLoadingHud:nil];
    NSString *url = @"/api/services/ckd/audit/QueryList";
    NSDictionary *dic = @{
                          @"keyword":self.searchTextField.text?self.searchTextField.text:@"",
                          @"pageIndex":@(self.page),
                          @"pageSize":@(10),
                          };
    
    [YSNetworkingManager requestWithUrl:url :POST paramiters:dic success:^(id responseObject) {
        if (SUCCEESS) {
            [self.dataArray addObjectsFromArray:[NSArray modelArrayWithClass:[CDKAuditListSearchModel class] json:responseObject[@"result"][@"items"]]];
        }
        [self.tableView reloadData];
        [self.tableView.mj_footer endRefreshing];
        [self dismissHud];
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        [self.tableView.mj_footer endRefreshing];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

#pragma mark -- Action

/**
 全选按钮
 */
- (void)clickAllButton{
    if ([self.chooseView.allButton.currentImage isEqual:[UIImage imageNamed:@"选择"]]) {// 取消全选
        [self.selectorPatnArray removeAllObjects];
        
        for (int i = 0; i < self.dataArray.count ; i ++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
        
        [self.chooseView.allButton setTitle:[NSString stringWithFormat:@"  全选 (%ld人)",self.selectorPatnArray.count] forState:UIControlStateNormal];
        [self.chooseView.allButton setImage:[UIImage imageNamed:@"未选择"] forState:UIControlStateNormal];
    }else{// 点击全选
        [self.selectorPatnArray removeAllObjects];
        
        for (int i = 0; i < self.dataArray.count ; i ++) {
            CDKAuditListSearchModel *model = self.dataArray[i];
            [self.selectorPatnArray addObject:model.treatmentId];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
        }
        
        [self.chooseView.allButton setImage:[UIImage imageNamed:@"选择"] forState:UIControlStateNormal];
        [self.chooseView.allButton setTitle:[NSString stringWithFormat:@"  全选 (%ld人)",self.selectorPatnArray.count] forState:UIControlStateNormal];
    }
}

/**
 审核通过
 */
- (void)clickAuditYesButton{
    if (self.selectorPatnArray.count > 0) {
        [self networkByAuditYes];
    }else{
        [self showMessageHud:@"请选择被审核人员"];
    }
}

/**
 审核不通过
 */
- (void)clickAydutNoButton{
    if (self.selectorPatnArray.count < 1) {
        [self showMessageHud: @"请选择被审核人员"];
        return;
    }
    
    self.reasonView = [[CDKReviewReasonView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
    [self.reasonView.reReviewButton addTarget:self action:@selector(clickReviewButton) forControlEvents:UIControlEventTouchUpInside];
    [self.reasonView.doneButton addTarget:self action:@selector(clickDoneButton) forControlEvents:UIControlEventTouchUpInside];
    [[UIApplication sharedApplication].keyWindow addSubview:self.reasonView];
}



///键盘显示事件
- (void) keyboardWillShow:(NSNotification *)notification {
    CGFloat kbHeight = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        self.reasonView.frame = CGRectMake(0.0f, -kbHeight + NavHeight, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

//键盘消失事件
- (void) keyboardWillHide:(NSNotification *)notify {
    //键盘动画时间
    double duration = [[notify.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        self.reasonView.frame = CGRectMake(0, 0, KScreenWidth, KScreenHeight);
    }];
    
}

/**
 审核弹窗完成按钮
 */
- (void)clickDoneButton{
    [self networkByAuditNo:self.reasonView.textView.text];
}

/**
 审核弹窗重新审核按钮
 */
- (void)clickReviewButton{
    [self.reasonView removeFromSuperview];
}

#pragma mark -- Network
/**
 审核通过网络请求
 */
- (void)networkByAuditYes{
    [self showLoadingHud:nil];
    NSString *treatmentIds = [self.selectorPatnArray componentsJoinedByString:@","];
    NSString *url = @"/api/services/ckd/audit/BatchAudit";
    NSDictionary *dic = @{
                          @"treatmentIds":treatmentIds,
                          @"remark":@"",
                          @"isPass":@1,
                          };
    [YSNetworkingManager requestWithUrl:url :POST paramiters:dic success:^(id responseObject) {
        if (SUCCEESS) {
            [self showMessageHud:@"审核成功"];
            [self.tableView setEditing:NO animated:YES];
            [self hiddenChooseView];
            [self.navigationItem.rightBarButtonItem setTitle:@"批量审核"];
            [self setupSearchNav];
            [self.reasonView removeFromSuperview];
            [self network];
        }else{
            [self dismissHud];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

/**
 审核不通过网络请求
 */
- (void)networkByAuditNo:(NSString *)reason{
    
    if (reason.length < 1) {
        [[UIApplication sharedApplication].keyWindow showMessageHud:@"请填写审核不通过原因"];
        return;
    }
    
    [self showLoadingHud:nil];
    NSString *treatmentIds = [self.selectorPatnArray componentsJoinedByString:@","];
    NSString *url = @"/api/services/ckd/audit/BatchAudit";
    NSDictionary *dic = @{
                          @"treatmentIds":treatmentIds,
                          @"remark":reason,
                          @"isPass":@0,
                          };
    [YSNetworkingManager requestWithUrl:url :POST paramiters:dic success:^(id responseObject) {
        if (SUCCEESS) {
            [self.tableView setEditing:NO animated:YES];
            [self hiddenChooseView];
            [self.navigationItem.rightBarButtonItem setTitle:@"批量审核"];
            [self setupSearchNav];
            [self.reasonView removeFromSuperview];
            [self network];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}

#pragma mark -- Delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CDKPationtSearchResultCell *cell = [tableView dequeueReusableCellWithIdentifier:resultCell forIndexPath:indexPath];
    cell.timeLable.hidden = NO;
//    cell.isEdit = self.selectedStatus;
    cell.model = self.dataArray[indexPath.row];
    cell.multipleSelectionBackgroundView = [UIView new];
    cell.tintColor = RGB(107, 203, 60);
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60 * Iphone6ScaleHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.selectedStatus isEqualToString:@"yes"]) {
        CDKAuditListSearchModel *model = self.dataArray[indexPath.row];
        [self.selectorPatnArray addObject:model.treatmentId];
        
        [self.chooseView.allButton setTitle:[NSString stringWithFormat:@"  全选 (%ld人)",self.selectorPatnArray.count] forState:UIControlStateNormal];
        
        if (self.selectorPatnArray.count == self.dataArray.count) {
            [self.chooseView.allButton setImage:[UIImage imageNamed:@"选择"] forState:UIControlStateNormal];
        }
        
    }else{
        CDKBiochemistryLookVc *vc = [[CDKBiochemistryLookVc alloc] init];
        CDKAuditListSearchModel *model = self.dataArray[indexPath.row];
        vc.patientId = model.patientId;
        vc.treatmentId = model.treatmentId;
        vc.time = model.measureDate;
        vc.fromType = @"Home_Audit";
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.selectorPatnArray.count > 0) {
        CDKAuditListSearchModel *model = self.dataArray[indexPath.row];
        [self.selectorPatnArray removeObject:model.treatmentId];
    }
    YSLog(@"%@",self.selectorPatnArray);
    
    if (self.selectorPatnArray.count != self.dataArray.count) {
        [self.chooseView.allButton setImage:[UIImage imageNamed:@"未选择"] forState:UIControlStateNormal];
    }
    
    [self.chooseView.allButton setTitle:[NSString stringWithFormat:@"  全选 (%ld人)",self.selectorPatnArray.count] forState:UIControlStateNormal];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}

#pragma EmptyDatasetSource
-(UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{
    UIImage *img=[UIImage imageNamed:@"nodata_palceholder"];
    return img;
}
-(CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView{
    return -100*Iphone6ScaleWidth;
}
-(NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    NSString *word = @"";
    word = @"没有搜到你要查询的内容";
    NSAttributedString *str=[[NSAttributedString alloc] initWithString:word attributes:@{NSForegroundColorAttributeName:RGB(154,154,154),NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    return str;
}

#pragma mark -- Lazy
-(UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.emptyDataSetDelegate = self;
        _tableView.emptyDataSetSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CDKPationtSearchResultCell class]) bundle:nil] forCellReuseIdentifier:resultCell];
        [self.view addSubview:_tableView];
        __weak typeof(self) weakSelf = self;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            if ([self.selectedStatus isEqualToString:@"yes"]) {
                [_tableView.mj_header endRefreshing];
                return ;
            }
            weakSelf.page = 1;
            [weakSelf network];
        }];
        
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            if ([self.selectedStatus isEqualToString:@"yes"]) {
                [_tableView.mj_footer endRefreshing];
                return ;
            }
            weakSelf.page++;
            [weakSelf netWorkForMoreData];
        }];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        _tableView.mj_footer.ignoredScrollViewContentInsetBottom=iPhoneX_BOTTOM_HEIGHT;
    }
    return _tableView;
}

- (CDKHomeDataAuditChooseView *)chooseView{
    if (!_chooseView) {
        _chooseView = [[CDKHomeDataAuditChooseView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 45)];
        [_chooseView.allButton addTarget:self action:@selector(clickAllButton) forControlEvents:UIControlEventTouchUpInside];
        [_chooseView.auditYesButton addTarget:self action:@selector(clickAuditYesButton) forControlEvents:UIControlEventTouchUpInside];
        [_chooseView.auditNoButton addTarget:self action:@selector(clickAydutNoButton) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_chooseView];
        [_chooseView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_bottom);
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.height.mas_equalTo(45 * Iphone6ScaleHeight);
        }];
    }
    return _chooseView;
}

-(NSMutableArray *) dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)selectorPatnArray{
    if (!_selectorPatnArray) {
        _selectorPatnArray = [NSMutableArray array];
    }
    return _selectorPatnArray;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
