//
//  CDKAddMorePationtToTodayVisitVC.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/20.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "BaseViewController.h"

@interface CDKAddMorePationtToTodayVisitVC : BaseViewController
@property(nonatomic,copy) NSString *time_str;
@property(nonatomic,copy) void(^callback)(void);

@end
