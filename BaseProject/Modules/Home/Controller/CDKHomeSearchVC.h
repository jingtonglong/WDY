//
//  CDKHomeSearchVC.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/19.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDKHomeSearchVC : BaseViewController
@property(nonatomic,assign) NSInteger type;// 0随访排班  1 营养评估  2 量表评估 3  生化数据 
@property(nonatomic,copy) NSString *time;

@end
