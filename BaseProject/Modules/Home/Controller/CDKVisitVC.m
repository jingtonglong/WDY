//
//  CDKVisitVC.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/19.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "CDKVisitVC.h"
#import "CDKCalenderView.h"
#import "CDKVisitCell.h"
#import "CDKModifyVisitTimeView.h"
#import "CDKAddMorePationtToTodayVisitVC.h"
#import "FSCalendar.h"
@interface CDKVisitVC ()<CDKCalenderViewDelegate,UITableViewDelegate,UITableViewDataSource,FSCalendarDataSource,FSCalendarDelegate,UIGestureRecognizerDelegate>
@property(nonatomic,strong) CDKCalenderView *topView;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *datas;
@property(nonatomic,strong) UIView *bgView;
@property(nonatomic,strong) CDKModifyVisitTimeView *modifyVisitTimeView;

@property(nonatomic,strong) FSCalendar *calendar;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) UIPanGestureRecognizer *scopeGesture;
@property(nonatomic,assign) NSInteger page;
@property(nonatomic,copy) NSDate *selectDate;
@property(nonatomic,copy) NSDate *today;
@property(nonatomic,strong) UIView *footer;



@end

@implementation CDKVisitVC
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView=[[UITableView alloc] init];
        _tableView.delegate=self;
        _tableView.dataSource=self;
//        _tableView.rowHeight=UITableViewAutomaticDimension;
        _tableView.rowHeight=90;
        [_tableView registerNib:[UINib nibWithNibName:@"CDKVisitCell" bundle:nil] forCellReuseIdentifier:@"visit"];
        _tableView.backgroundColor=RGB(247,247,247);
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        UIView *footer=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 100)];
        footer.backgroundColor=[UIColor whiteColor];
        UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, 22, KScreenWidth, 1)];
        _tableView.backgroundColor=RGB(247,247,247);
        [footer addSubview:line];
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:@" 添加更多患者到今日随访" forState:UIControlStateNormal];
        btn.titleLabel.font=SYSTEMFONT(14);
        [btn setTitleColor:RGB(255,139,0) forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"新增橙色"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(addmorebtnclicked) forControlEvents:UIControlEventTouchUpInside];
        [footer addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(line.mas_bottom).offset(22);
            make.centerX.equalTo(footer);
        }];
        _tableView.tableFooterView= footer;
        self.footer=footer;
        footer.clipsToBounds=YES;
//        WeakSelf(weak);
//        _tableView.mj_header=[MJRefreshNormalHeader headerWithRefreshingBlock:^{
//            [weak refreshData];
//        }];
//        _tableView.mj_footer=[MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//            [weak loadMoreData];
//        }];
//        _tableView.mj_footer.ignoredScrollViewContentInsetBottom=iPhoneX_BOTTOM_HEIGHT;
    }
    return _tableView;
}
-(UIView *)bgView{
    if (!_bgView) {
        _bgView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenDatePicker)];
        [_bgView addGestureRecognizer:tap];
        _bgView.backgroundColor=RGB(0,0,0);
        _bgView.alpha=0.4;
    }
    return _bgView;
}
-(CDKModifyVisitTimeView *)modifyVisitTimeView{
    if (!_modifyVisitTimeView) {
        _modifyVisitTimeView=(CDKModifyVisitTimeView*)[UIView xibinstance:@"CDKModifyVisitTimeView"];
        WeakSelf(weak);
        
        _modifyVisitTimeView.block = ^(NSDate *date,CDKPationtListModel *model) {
            YSLog(@"更改了随访时间");
            [weak hiddenDatePicker];
            [weak change_visit_time:date model:model];
        };
       
    }
    return _modifyVisitTimeView;
}
-(void)change_visit_time:(NSDate*)date model:(CDKPationtListModel*)model{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/visit/UpdateVisit";
    NSString *date_str=[self.dateFormatter stringFromDate:date];
    NSDictionary *para=@{
                         @"visitTime":date_str,
                         @"id":model.id
                         };
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            if (![date_str isEqualToString:[self.dateFormatter stringFromDate:self.selectDate]]) {
                [self.datas removeObject:model];
                [self.tableView reloadData];
            }
            [self showMessageHud:@"修改成功"];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];

        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
    
}
-(FSCalendar *)calendar{
    if (!_calendar) {
        _calendar=[[FSCalendar alloc] init];
        _calendar.delegate=self;
        _calendar.dataSource=self;
        _calendar.firstWeekday=2;
//        _calendar.backgroundColor=[UIColor clearColor];
        _calendar.allowsMultipleSelection=NO;
        _calendar.headerHeight=0;
//        _calendar.placeholderType=FSCalendarPlaceholderTypeFillHeadTail;
        _calendar.showsPlaceholders=NO;
        _calendar.appearance.weekdayTextColor=RGB(155,247,196);
        _calendar.appearance.titleDefaultColor=RGB(190,255,216);
        _calendar.appearance.titleSelectionColor=RGB(69,213,160);
        _calendar.appearance.todayColor=[UIColor clearColor];
        _calendar.appearance.selectionColor=[UIColor whiteColor];
        _calendar.appearance.titleTodayColor=RGB(215,250,201);
        _calendar.appearance.borderDefaultColor=RGB(215,250,201);
        _calendar.weekdayHeight=35;
        _calendar.backgroundColor=RGB(69,213,160);
        NSDate *date=[NSDate date];
        
        for (NSInteger i = 0; i < _calendar.calendarWeekdayView.weekdayPointers.count; i++) {
            NSInteger index = (i + self.calendar.firstWeekday-1) % 7;
            UILabel *label = [_calendar.calendarWeekdayView.weekdayPointers pointerAtIndex:i];
            label.font = self.calendar.appearance.weekdayFont;
            
            label.textColor = _calendar.appearance.weekdayTextColor;
            if (index==date.weekday-1) {
                label.textColor = [UIColor whiteColor];
            }
           
        }
        
    }
    return _calendar;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    [self.calendar selectDate:[NSDate date] scrollToDate:YES];
    
    
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self.calendar action:@selector(handleScopeGesture:)];
    panGesture.delegate = self;
    panGesture.minimumNumberOfTouches = 1;
    panGesture.maximumNumberOfTouches = 2;
    [self.view addGestureRecognizer:panGesture];
    self.scopeGesture = panGesture;
    
    // While the scope gesture begin, the pan gesture of tableView should cancel.
    [self.tableView.panGestureRecognizer requireGestureRecognizerToFail:panGesture];
    
//    [self.calendar addObserver:self forKeyPath:@"scope" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
    
    
//    self.title=@"随访排班";
    // Do any additional setup after loading the view.
    self.topView=[CDKCalenderView xibinstanse];
    NSString *todaydate=[self.dateFormatter stringFromDate:[NSDate date]];
    [self.topView.date_btn setTitle:[todaydate substringToIndex:7] forState:UIControlStateNormal];
    self.today=[self.dateFormatter dateFromString:todaydate];
    self.selectDate=[self.dateFormatter dateFromString:todaydate];
    [self.view addSubview:self.topView];
    self.topView.topinsets.constant=10+StatusHeight;
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(81+StatusHeight);
    }];
    self.topView.delegate=self;
    self.topView.backgroundColor=RGB(69,213,160);
    [self.view addSubview:self.calendar];
    [self.calendar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topView.mas_bottom).offset(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(300);
    }];
    self.calendar.scope = FSCalendarScopeWeek;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.calendar.mas_bottom);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
  
    self.datas=[NSMutableArray array];
    [self loadData];
}
-(void)removeVisitTime:(CDKPationtListModel*)model indexpath:(NSIndexPath*)indexpath{
    [self showLoadingHud:nil];
    NSString *api=@"/api/services/ckd/visit/RemoveVisit";
   
    [YSNetworkingManager requestWithUrl:api :POST paramiters:@{@"patientId":model.id} success:^(id responseObject) {
        [self dismissHud];
        if (SUCCEESS) {
            
            [self.datas removeObject:model];
            [self.tableView deleteRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationMiddle];
            [self.tableView reloadData];
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];

        }
        
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadData{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    if ([self.selectDate isEarlierThanDate:self.today]) {
        self.footer.height=0;
    }else{
        self.footer.height=100;
    }
    [self showLoadingHud:nil];
    self.page=1;
//    NSString *api=@"/api/services/ckd/visit/SearchList";
    
        NSString *api=@"/api/services/ckd/visit/QueryList";

    NSDictionary *para=@{@"keyword":@"",@"visitTime":[self.dateFormatter stringFromDate:self.selectDate]};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self dismissHud];
        
        if (SUCCEESS) {
            [self.datas removeAllObjects];
            NSArray *arr=[NSArray modelArrayWithClass:[CDKPationtListModel class] json:responseObject[@"result"]];
            [self.datas addObjectsFromArray:arr];
            [self.tableView reloadData];
            
            
        }else{
        [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self dismissHud];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)refreshData{
   
    [self.tableView.mj_footer endRefreshing];
    
    self.page=1;
//    NSString *api=@"/api/services/ckd/visit/SearchList";
    NSString *api=@"/api/services/ckd/visit/QueryList";
    
    NSDictionary *para=@{@"keyword":@"",@"visitTime":[self.dateFormatter stringFromDate:self.selectDate]/*,@"pageIndex":@(self.page),@"pageSize":@15*/};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        if (SUCCEESS) {
            

            [self.datas removeAllObjects];
            NSArray *arr=[NSArray modelArrayWithClass:[CDKPationtListModel class] json:responseObject[@"result"][@"items"]];
            [self.datas addObjectsFromArray:arr];
            [self.tableView reloadData];
            
        }else{
            [self showMessageHud:responseObject[@"error"][@"message"]];
        }
    } errorHandel:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)loadMoreData{
    
    [self.tableView.mj_header endRefreshing];
    self.page++;
    NSString *api=@"/api/services/ckd/visit/SearchList";
    
    NSDictionary *para=@{@"keyword":@"",@"visitTime":[self.dateFormatter stringFromDate:self.selectDate],@"pageIndex":@(self.page),@"pageSize":@15};
    [YSNetworkingManager requestWithUrl:api :POST paramiters:para success:^(id responseObject) {
       
        if (SUCCEESS) {
            NSArray *arr=[NSArray modelArrayWithClass:[CDKPationtListModel class] json:responseObject[@"result"][@"items"]];
            if (arr.count>0) {
                [self.tableView.mj_footer endRefreshing];
            }else {
                if (self.page>1) {
                    self.page--;
                }
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            [self.datas addObjectsFromArray:arr];
            [self.tableView reloadData];
            
        }else{
            [self.tableView.mj_footer endRefreshing];
            [self showMessageHud:responseObject[@"error"][@"message"]];
            if (self.page>1) {
                self.page--;
            }
        }
    } errorHandel:^(NSError *error) {
        [self.tableView.mj_footer endRefreshing];
        [self dismissHud];
        if (self.page>1) {
            self.page--;
        }
        if (error.code ==-1001 ) {
            [self showMessageHud:@"请求超时"];
        }else{
            [self showMessageHud:@"请检查网络"];
        }
    }];
}
-(void)addmorebtnclicked{
    CDKAddMorePationtToTodayVisitVC *vc=[[CDKAddMorePationtToTodayVisitVC alloc] init];
    vc.time_str=[self.dateFormatter stringFromDate:self.selectDate];
    vc.callback = ^{
        [self loadData];
    };
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)dealloc{
    YSLog(@"visit vc --dealloc");
}
#pragma mark - KVO


#pragma mark - <UIGestureRecognizerDelegate>

// Whether scope gesture should begin
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    BOOL shouldBegin = self.tableView.contentOffset.y <= -self.tableView.contentInset.top;
//    YSLog(@"%@",self.tableView.contentOffset);
    if (shouldBegin) {
        CGPoint velocity = [self.scopeGesture velocityInView:self.view];
        switch (self.calendar.scope) {
            case FSCalendarScopeMonth:
                return velocity.y < 0;
            case FSCalendarScopeWeek:
                return velocity.y > 0;
        }
    }
    return shouldBegin;
}
#pragma mark - <FSCalendarDelegate>

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    //    NSLog(@"%@",(calendar.scope==FSCalendarScopeWeek?@"week":@"month"));
    YSLog(@"%@",[NSValue valueWithCGRect:bounds]);
    [self.calendar mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(CGRectGetHeight(bounds));
        
    }];
    [self.view layoutIfNeeded];
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    YSLog(@"did select date %@",[self.dateFormatter stringFromDate:date]);
    self.selectDate=date;
//    [self.topView.date_btn setTitle:[self.dateFormatter stringFromDate:date] forState:UIControlStateNormal];
    [self loadData];
    NSMutableArray *selectedDates = [NSMutableArray arrayWithCapacity:calendar.selectedDates.count];
    [calendar.selectedDates enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [selectedDates addObject:[self.dateFormatter stringFromDate:obj]];
    }];
    NSLog(@"selected dates is %@",selectedDates);
    if (monthPosition == FSCalendarMonthPositionNext || monthPosition == FSCalendarMonthPositionPrevious) {
        [calendar setCurrentPage:date animated:YES];
    }
    for (NSInteger i = 0; i < _calendar.calendarWeekdayView.weekdayPointers.count; i++) {
        NSInteger index = (i + self.calendar.firstWeekday-1) % 7;
        UILabel *label = [_calendar.calendarWeekdayView.weekdayPointers pointerAtIndex:i];
        label.font = self.calendar.appearance.weekdayFont;
        
        label.textColor = _calendar.appearance.weekdayTextColor;
        if (index==date.weekday-1) {
            label.textColor = [UIColor whiteColor];
        }
        
    }
    NSString *datestr=[self.dateFormatter stringFromDate:date];
    
    [self.topView.date_btn setTitle:[datestr substringToIndex:7] forState:UIControlStateNormal];
//    if([date isEarlierThanDate:[NSDate date]]){
//
//    }
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar
{
    YSLog(@"%s %@", __FUNCTION__, [self.dateFormatter stringFromDate:calendar.currentPage]);
    NSString *date=[self.dateFormatter stringFromDate:calendar.currentPage];
    
    [self.topView.date_btn setTitle:[date substringToIndex:7] forState:UIControlStateNormal];
}


#pragma CDKCalenderViewDelegate
-(void)openOrClose:(UIButton *)sender{
//    if (sender.isSelected) {
//        self.calendar.scope=FSCalendarScopeMonth;
//    }else{
//        //    }
    if (self.calendar.scope==FSCalendarScopeWeek) {
        self.calendar.scope=FSCalendarScopeMonth;
    }else{
        self.calendar.scope=FSCalendarScopeWeek;

    }
    

}
-(void)showDatePickerView{
    UIWindow *window=kAppWindow;
    [window addSubview:self.bgView];
    [window addSubview:self.modifyVisitTimeView];
    
    [self.modifyVisitTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(290+iPhoneX_BOTTOM_HEIGHT);
    }];
}
-(void)hiddenDatePicker{
    [self.bgView removeFromSuperview];
    [self.modifyVisitTimeView removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
    kApplication.statusBarStyle=UIStatusBarStyleLightContent;

}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//     [self.navigationController setNavigationBarHidden:NO animated:YES];
    kApplication.statusBarStyle=UIStatusBarStyleDefault;

}
#pragma tableviewdelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CDKVisitCell *cell=[tableView dequeueReusableCellWithIdentifier:@"visit" forIndexPath:indexPath];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.model=self.datas[indexPath.row];
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 44)];
    sectionView.backgroundColor=[UIColor whiteColor];
    
    UIView *colorView=[[UIView alloc] init];
    colorView.backgroundColor=RGB(255,139,0);
    colorView.cornerRadius=3;
    [sectionView addSubview:colorView];
    [colorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(6);
        make.height.mas_equalTo(13);
        make.left.mas_equalTo(10);
        make.centerY.equalTo(sectionView);
    }];
    UILabel *lab=[[UILabel alloc] init];
    NSString *count=[@(self.datas.count) stringValue];
    NSString *text=[NSString stringWithFormat:@"今日已安排了 %@ 人随访",count];
    NSMutableAttributedString *attrstr=[[NSMutableAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:RGB(60,60,60)}];
    [attrstr addAttribute:NSForegroundColorAttributeName value:RGB(255,191,129) range:NSMakeRange(7, count.length)];
    lab.attributedText=attrstr;
    [sectionView addSubview:lab];
    [lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(colorView.mas_right).offset(10);
        make.centerY.equalTo(colorView);
    }];
    UIView *line=[[UIView alloc] init];
    line.backgroundColor=RGB(229,229,229);
    [sectionView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    return sectionView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}
/**
 *  只要实现了这个方法，左滑出现按钮的功能就有了
 (一旦左滑出现了N个按钮，tableView就进入了编辑模式, tableView.editing = YES)
 */
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.selectDate isEarlierThanDate:self.today]) {
        return NO;
    }
    return YES;
}
/**
 *  左滑cell时出现什么按钮
 */
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WeakSelf(weak);
    UITableViewRowAction *action0 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"更改日期" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        YSLog(@"更改日期");
        weak.modifyVisitTimeView.model=weak.datas[indexPath.row];
        weak.modifyVisitTimeView.previous_time=[weak.dateFormatter stringFromDate:weak.selectDate];
        
        [weak showDatePickerView];
        // 收回左滑出现的按钮(退出编辑模式)
        tableView.editing = NO;
    }];
    UITableViewRowAction *action1 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"移除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        tableView.editing = NO;
        UIAlertController *alertvc= [UIAlertController alertControllerWithTitle:@"确认移除" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *comfirm=[UIAlertAction actionWithTitle:@"确认" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
            // 删除操作
            [weak removeVisitTime:weak.datas[indexPath.row] indexpath:indexPath];
        }];
        UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            //
            weak.tableView.editing=NO;
        }];
        
        [alertvc addAction:comfirm];
        [alertvc addAction:cancel];
        [weak presentViewController:alertvc animated:YES completion:nil];
        
        
    }];
    action1.backgroundColor=RGB(254,91,0);
    
    return @[action1, action0];
}
- (nullable UISwipeActionsConfiguration *)tableView:(UITableView *)tableView trailingSwipeActionsConfigurationForRowAtIndexPath:(NSIndexPath *)indexPath API_AVAILABLE(ios(11.0)) API_UNAVAILABLE(tvos){
    WeakSelf(weak);
    // delete action
    UIContextualAction *deleteAction = [UIContextualAction
                                        contextualActionWithStyle:UIContextualActionStyleDestructive
                                        title:@"移除"
                                        handler:^(UIContextualAction * _Nonnull action,
                                                  __kindof UIView * _Nonnull sourceView,
                                                  void (^ _Nonnull completionHandler)(BOOL))
                                        {
                                            
                                            UIAlertController *alertvc= [UIAlertController alertControllerWithTitle:@"确认移除" message:nil preferredStyle:UIAlertControllerStyleAlert];
                                            UIAlertAction *comfirm=[UIAlertAction actionWithTitle:@"确认" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
                                                // 删除操作
                                                [weak removeVisitTime:weak.datas[indexPath.row] indexpath:indexPath];
                                            }];
                                            UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
                                                //
                                                weak.tableView.editing=NO;
                                            }];
                                            
                                            [alertvc addAction:comfirm];
                                            [alertvc addAction:cancel];
                                            [weak presentViewController:alertvc animated:YES completion:nil];
                                            
                                            
//                                            [tableView setEditing:NO animated:YES];
//                                            [weak removeVisitTime:weak.datas[indexPath.row] indexpath:indexPath];


                                            completionHandler(false);
                                        }];
    // chagne action
    UIContextualAction *chageaction = [UIContextualAction
                                        contextualActionWithStyle:UIContextualActionStyleNormal
                                        title:@"更改日期"
                                        handler:^(UIContextualAction * _Nonnull action,
                                                  __kindof UIView * _Nonnull sourceView,
                                                  void (^ _Nonnull completionHandler)(BOOL))
                                        {
                                            
                                            
                                            
                                            [tableView setEditing:NO animated:YES];
                                           weak.modifyVisitTimeView.model=weak.datas[indexPath.row]; weak.modifyVisitTimeView.previous_time=[weak.dateFormatter stringFromDate:weak.selectDate];
                                            
                                            [weak showDatePickerView];
                                            completionHandler(true);
                                        }];
    
    UISwipeActionsConfiguration *actions = [UISwipeActionsConfiguration configurationWithActions:@[deleteAction,chageaction]];
    actions.performsFirstActionWithFullSwipe = NO;
    deleteAction.backgroundColor=RGB(254,91,0);
    return actions;
}


@end
