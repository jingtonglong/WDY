//
//  AreaModel.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/11.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AreaModel : NSObject
@property(nonatomic,copy) NSString *AreaName;
@property(nonatomic,strong) NSArray *Children;
@property(nonatomic,copy) NSNumber *Id;


@end
