//
//  AreaModel.m
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/11.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "AreaModel.h"

@implementation AreaModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"Children" : [AreaModel class]
             };
}

@end
