//
//  CDKTokenInfo.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/5/31.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>
// token 相关信息
@interface CDKTokenInfo : NSObject
@property(nonatomic,copy) NSString   *access_token;
@property(nonatomic,copy) NSString *token_type;
@property(nonatomic,copy) NSString *expires_in;

@property(nonatomic,copy) NSString *refresh_token;

@end
