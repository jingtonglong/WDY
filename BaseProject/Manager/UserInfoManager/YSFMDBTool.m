//
//  YSFMDBTool.m
//  BaseProject
//
//  Created by sss on 2018/6/29.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import "YSFMDBTool.h"

@implementation YSFMDBTool
+ (YSFMDBTool *)shareInstance{
    static YSFMDBTool *fmdbtool = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        fmdbtool = [[super allocWithZone:NULL] init] ;
    });
    return fmdbtool;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    return [YSFMDBTool shareInstance];
}

- (id)copyWithZone:(struct _NSZone *)zone
{
    return [YSFMDBTool shareInstance] ;
}
-(FMDatabase*)dataBase{
    if (!_dataBase) {
        NSString *path=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        
        path =[path stringByAppendingPathComponent:@"cdk.db"];
        _dataBase=[FMDatabase databaseWithPath:path];
        
        if (![_dataBase open]) {
            _dataBase=nil;
        }else{
            NSLog(@"database open succese");
        }
    }
    // 创建表
    NSString *sql=@"CREATE TABLE IF NOT EXISTS Contact(name TEXT,easemobId TEXT,headPicture TEXT)";
    // 除了查询之外 （select）所有操作都是跟新
    BOOL succese=[_dataBase executeUpdate:sql];
    if(succese){
        // 创建表成功
    }else{
        // 失败
        YSLog(@"%@",[_dataBase lastErrorMessage]);
    }
    return _dataBase;
}

- (void)insertData:(NSDictionary*)dic {
    
    
    BOOL bo=  [self.dataBase executeUpdate:@"INSERT INTO Contact (name, easemobId, headPicture) VALUES (:name, :easemobId, :headPicture)" withParameterDictionary:dic];
    
    if (bo) {
        // 插入成功
        YSLog(@"// 插入数据成功%@",NSHomeDirectory());
       
    }else{
        
        // 插入失败
    }
    
}
// 根据sql语句查询数据库
-(NSArray*)queryDataWith:(NSString*)sql paramiter:(NSDictionary*)dic sign:(NSString*)sign{
    
    FMResultSet *set= [self.dataBase executeQuery:sql withParameterDictionary:dic];// 查询
    NSMutableArray *arr=[NSMutableArray array];
    while ([set next]) {
        NSMutableDictionary *dict=[NSMutableDictionary dictionary];
        [dict setObject:[set stringForColumnIndex:0] forKey:@"name"];
        [dict setObject:[set stringForColumnIndex:1] forKey:@"easemobId"];
        
        [dict setObject:[set stringForColumnIndex:2] forKey:@"headPicture"];
        
        //  某个字段取出来是什么类型  取决于定义表时  这一字段的类型
        [arr addObject:dict];
        //set intForColumnIndex:<#(int)#>
        
    }
    return arr;
}
- (NSDictionary*)readdata:(NSString*)easemobid {
    // select 字段名 from 表名 where 条件
    // 查询name为为 ‘git pull’ ‘git push’ ‘git commit -m’ 。。。的记录
    NSString *sql=@"SELECT name,easemobId,headPicture FROM Contact where easemobId in(:easemobId)";
//    NSDictionary *dic=@{@"name1":@"mike",
//                        @"name2":@"git push",
//                        @"name3":@"git commit -m"};
//    NSString *sign=@"查询 name为为 ‘git pull’ ‘git push’ ‘git commit -m’ 。。。的记录";
//    sql=@"select name,userID,age,nikeName from myTable where name like :name";
//    dic=@{@"name":@"%p"};// % 代表任意个任意字符
    // %p 以p结束  p% 以p开始  %p% 包含p
    // 分页查询
   NSString* sign=@"查询";
    //    sql=@"select name,userID,age,nikeName from myTable limit :start,:end";
    //    dic=@{@"start":@"3",@"end":@"2"};
    
    NSArray *arr= [self queryDataWith:sql paramiter:@{@"easemobId":easemobid} sign:sign];
 
    return arr.count>0?arr.firstObject:nil;
    
}
-(void)updateData:(NSString *)easemobid :(NSString *)headPicture :(NSString *)name{
    
    BOOL result = [self.dataBase executeUpdate:@"update Contact set headPicture = ?,name = ? where easemobId = ?",headPicture,name,easemobid];
    if (result) {
        YSLog(@"修改成功");
        
    }else{
        YSLog(@"修改失败");
        
    }
}
@end
