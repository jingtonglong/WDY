//
//  YSFMDBTool.h
//  BaseProject
//
//  Created by sss on 2018/6/29.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDatabase.h>
@interface YSFMDBTool : NSObject

@property (nonatomic,strong) FMDatabase *dataBase;

+ (YSFMDBTool *)shareInstance;
/**
 更新头像昵称

 @param easemobid 环信id
 @param headPicture 头像url
  @param name 名字
 */
-(void)updateData:(NSString*)easemobid :(NSString*)headPicture :(NSString*)name;

/**
 根据环信id查询

 @param easemobid 环信id
 @return dic@{@"name":name,@"headPicture":headPicture,@"easemobId":easemobid}
 */
- (NSDictionary*)readdata:(NSString*)easemobid ;

/**
 插入数据

 @param dic @{@"name":name,@"headPicture":headPicture,@"easemobId":easemobid}
 */
- (void)insertData:(NSDictionary*)dic ;

@end
