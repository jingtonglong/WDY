//
//  CDKUserInfo.h
//  BaseProject
//
//  Created by 范兴茂 on 2018/6/3.
//  Copyright © 2018年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDKUserInfo : NSObject
@property (nonatomic,copy) NSString *id;
@property(nonatomic,copy) NSString *headPicture;
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *sex;
@property(nonatomic,copy) NSString *brithday;
@property(nonatomic,copy) NSString *mobile;
@property(nonatomic,copy) NSString *easemobId;

@property(nonatomic,copy) NSString *hospitalName;

@property(nonatomic,copy) NSString *role;

//"headPicture":"",--头像路径
//"name" : "任林飞",
//"sex" : "男",
//"brithday" : "1994-01-01 00:00:00",
//"mobile" : "13558727528",
//"role" : "医生",
//"hospitalName" : "东泽医疗第一附属医院" 
@end
