//
//  UserInfoManager.h
//  BaseProject
//
//  Created by apple on 2017/6/22.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CDKTokenInfo.h"
#import "CDKUserInfo.h"
@interface UserInfoManager : NSObject
+ (UserInfoManager *) shareInstance;


/** 登录账号 */
@property (nonatomic,copy) NSString  *loginAccount;

/** 登录密码 */
@property (nonatomic,copy) NSString  *loginPassword;

/** 登录id */
@property (nonatomic,copy) NSString *userId;

/**
 登录状态
 */
@property(nonatomic,assign) BOOL isLogin;

/**
 token 信息
 */
@property(nonatomic,strong) CDKTokenInfo *tokenInfo;
@property(nonatomic,strong) CDKUserInfo *userInfo;
/** 默认医院群  */
@property (nonatomic,copy) NSString *defaltHospitoalGroupId;


@end
