//
//  UserInfoManager.m
//  BaseProject
//
//  Created by apple on 2017/6/22.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "UserInfoManager.h"

@implementation UserInfoManager

+ (UserInfoManager *)shareInstance{
    static UserInfoManager *userInfoManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        userInfoManager = [[super allocWithZone:NULL] init] ;
    });
    return userInfoManager;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    return [UserInfoManager shareInstance];
}

- (id)copyWithZone:(struct _NSZone *)zone
{
    return [UserInfoManager shareInstance] ;
}

@end
