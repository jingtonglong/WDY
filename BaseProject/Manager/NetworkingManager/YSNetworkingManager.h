//
//  YSNetworkingManager.h
//  BaseProject
//
//  Created by sss on 2017/6/20.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum : NSUInteger {
    GET,
    POST,
    PUT,
    DELETE
} requestMethod;


typedef void(^successHandel)(id  responseObject);
typedef void(^errorHandel)(NSError *error);

@interface YSNetworkingManager : NSObject
+(void)requestWithUrl:(NSString*)urlString :(requestMethod)method paramiters:(id)paramiters success:(successHandel)success errorHandel:(errorHandel)errorHandel;
+(void)updateimg:(NSString *)urlString img:(UIImage*)paramiters success:(successHandel)success errorHandel:(errorHandel)errorHandel;
@end
