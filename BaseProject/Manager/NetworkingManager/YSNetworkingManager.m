//
//  YSNetworkingManager.m
//  BaseProject
//
//  Created by sss on 2017/6/20.
//  Copyright © 2017年 ZQY.YXWL.com. All rights reserved.
//

#import "YSNetworkingManager.h"
#import "AFHTTPSessionManager.h"
@implementation YSNetworkingManager
+(void)requestWithUrl:(NSString *)urlString :(requestMethod)method paramiters:(id)paramiters success:(successHandel)success errorHandel:(errorHandel)errorHandel{
    
    AFHTTPSessionManager *manager = [self manager];
    //    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    if ([urlString isEqualToString:@"/Token"]) {
         manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    }else{
        
         manager.requestSerializer = [AFJSONRequestSerializer serializer];
    }
//    AFHTTPResponseSerializer *res=[AFHTTPResponseSerializer serializer];
//    res.removesKeysWithNullValues=YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html",@"text/plain", nil];
    manager.requestSerializer.timeoutInterval = 10;
    //    AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
    //    [securityPolicy setAllowInvalidCertificates:YES];
    //    [manager setSecurityPolicy:securityPolicy];
    //
    if ([UserInfoManager shareInstance].tokenInfo) {
        CDKTokenInfo *info = [UserInfoManager shareInstance].tokenInfo;
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@ %@",info.token_type,info.access_token] forHTTPHeaderField:@"Authorization"];
    }
    urlString=[NSString stringWithFormat:@"%@%@",BASE_URL,urlString];
    if (method==POST) {
        
        [manager POST:urlString parameters:paramiters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id _Nonnull responseObject) {
            
            success(responseObject);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError  * _Nonnull error) {
            errorHandel(error);
        }];
    }else if(method==GET){
        NSString *newurl=[NSString stringWithFormat:@"%@/%@",urlString,[(NSArray *)paramiters componentsJoinedByString:@"/"]];
        
        [manager GET:newurl parameters:paramiters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            errorHandel(error);
        }];
        
        
    }else if (method==PUT){
        
        [manager PUT:urlString parameters:paramiters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            errorHandel(error);
        }];
        
    }else{
        
        [manager DELETE:urlString parameters:paramiters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            errorHandel(error);
        }];
    }
    
    
}
+(void)updateimg:(NSString *)urlString img:(UIImage*)paramiters success:(successHandel)success errorHandel:(errorHandel)errorHandel{
    
    AFHTTPSessionManager *manager = [self manager];
    //    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    // manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html",@"text/plain", nil];
    manager.requestSerializer.timeoutInterval = 10;
    //    AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
    //    [securityPolicy setAllowInvalidCertificates:YES];
    //    [manager setSecurityPolicy:securityPolicy];
    //
    if ([UserInfoManager shareInstance].tokenInfo) {
        CDKTokenInfo *info = [UserInfoManager shareInstance].tokenInfo;
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"%@ %@",info.token_type,info.access_token] forHTTPHeaderField:@"Authorization"];
    }
    urlString=[NSString stringWithFormat:@"%@%@",BASE_URL,urlString];
    NSData *imgdata=UIImageJPEGRepresentation(paramiters, 1);
    if (imgdata.length>1024*500) {
        imgdata=UIImageJPEGRepresentation(paramiters, 0.5);
    }
    [manager POST:urlString parameters:@{@"dir":@"header"} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:imgdata name:@"file" fileName:@"fileName.png" mimeType:@"image/png"];
    } progress:^(NSProgress *progress){
        YSLog(@"progress-%@",progress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorHandel(error);

    }];
    
}
+(AFHTTPSessionManager*)manager{
    
    static AFHTTPSessionManager *manager=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager=[AFHTTPSessionManager manager];
    });
    return manager;
}
@end
